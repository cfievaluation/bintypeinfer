open Batteries
open RTL
open X86Syntax
open X86Semantics
open Big
open Printf

let instr_num = ref 0
let expr_num = ref 0
let non_trivial_inum = ref 0
let rnode_num = ref 0

let reset_numbers () =
  instr_num := 0;
  non_trivial_inum := 0;
  expr_num := 0;
  rnode_num := 0

let rec expr_count rtl_e =
  match rtl_e with
  | X86_RTL.Coq_arith_rtl_exp(_, _, rtl_e1, rtl_e2)
  | X86_RTL.Coq_test_rtl_exp(_, _, rtl_e1, rtl_e2) ->
    rnode_num := !rnode_num + 1;
    let num1 = expr_count rtl_e1 in
    let num2 = expr_count rtl_e2 in
    num1 + num2 + 1
  | X86_RTL.Coq_if_rtl_exp(s, rtl_e1, rtl_e2, rtl_e3) ->
    rnode_num := !rnode_num + 1; 
    let num1 = expr_count rtl_e1 in
    let num2 = expr_count rtl_e2 in
    let num3 = expr_count rtl_e3 in
    num1 + num2 + num3 + 1
  | X86_RTL.Coq_cast_s_rtl_exp(_, _, rtl_e')
  | X86_RTL.Coq_cast_u_rtl_exp(_, _, rtl_e') ->
    rnode_num := !rnode_num + 1; 
    let num1 = expr_count rtl_e' in
    num1 + 1
  | X86_RTL.Coq_imm_rtl_exp(_, _)
  | X86_RTL.Coq_get_loc_rtl_exp(_, _)
  | X86_RTL.Coq_get_ps_reg_rtl_exp(_, _) ->
    rnode_num := !rnode_num + 2;
    1
  | X86_RTL.Coq_get_array_rtl_exp(l, s, arr, rtl_e') ->
    rnode_num := !rnode_num + 2;
    let num1 = expr_count rtl_e' in
    num1 + 1
  | X86_RTL.Coq_get_byte_rtl_exp(rtl_e') ->
    rnode_num := !rnode_num + 1;
    let num1 = expr_count rtl_e' in
    num1 + 1
  | X86_RTL.Coq_get_random_rtl_exp(s) ->
    rnode_num := !rnode_num + 1;
    1
  | X86_RTL.Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,rtl_e1,rtl_e2) ->
    rnode_num := !rnode_num + 1;
    let num1 = expr_count rtl_rm in
    let num2 = expr_count rtl_e1 in
    let num3 = expr_count rtl_e2 in
    num1 + num2 + num3 + 1
  | X86_RTL.Coq_fcast_rtl_exp(s1,s2,s3,s4,rtl_e1,rtl_e2) ->
    rnode_num := !rnode_num + 1;
    let num1 = expr_count rtl_e1 in
    let num2 = expr_count rtl_e2 in
    num1 + num2 + 1

let rec instr_count rtl_i =
  match rtl_i with
  | X86_RTL.Coq_set_loc_rtl(_, rtl_e, _)
  | X86_RTL.Coq_set_ps_reg_rtl(_, rtl_e, _) -> 
    instr_num := !instr_num + 1;
    non_trivial_inum := !non_trivial_inum + 1;
    expr_num := !expr_num + (expr_count rtl_e)
  | X86_RTL.Coq_set_array_rtl(_,_,_,rtl_e1,rtl_e2)
  | X86_RTL.Coq_set_byte_rtl(rtl_e1, rtl_e2) ->
    instr_num := !instr_num + 1;
    non_trivial_inum := !non_trivial_inum + 1;
    expr_num := !expr_num + (expr_count rtl_e1) + (expr_count rtl_e2)
  | X86_RTL.Coq_advance_oracle_rtl ->
    instr_num := !instr_num + 1
  | X86_RTL.Coq_if_rtl(rtl_e, rtl_i') ->
    instr_num := !instr_num + 1;
    non_trivial_inum := !non_trivial_inum + 1;
    expr_num := !expr_num + (expr_count rtl_e);
    instr_count rtl_i'
  | X86_RTL.Coq_error_rtl ->
    instr_num := !instr_num + 1
  | X86_RTL.Coq_trap_rtl ->
    instr_num := !instr_num + 1
