open Batteries
open BatOptParse
open Printf
(* open Bigarray *)

(* open IO *)

(* module F=Format *)
(* module E=Errormsg *)
(* open Formatutil *)

(* open Config *)
(* open Abbrev *)
open Disassembler 
open Elf
open BB
open Ibaux
open Config_cfg
(* open Instr *)
(* open Enc_dec *)
open Dis 

open X86Syntax
open X86Semantics
open RTL
open Big
(*
open Core
open Smt_syntax
open Logical_formula
open Smt2_generation_for_Z3
*)
open Printer
open RTL_node_count
open RTL_opt
open Simp_for_ssa
open Block

open Auxlib
open Rtlssa

open CFAGen
(* Initialization *)


    (* { hostname   = sh "hostname"; *)
    (*   os_name    = sh "uname -s"; *)
    (*   cpu_arch   = sh "uname -p"; *)
    (*   timestamp  = Time.now (); *)

let emptyPre = 
  { lock_rep = None;
    seg_override = None;
    op_override = false;
    addr_override = false;
  }

(** The main function *)
let theMain () = 
  let usagemsg = 
    "main.native [options] target\nrun with -h or --help to see the list of options"
  in
  let optpsr = OptParser.make ?usage:(Some usagemsg) () in
  let rt_opt = StdOpt.store_true () in
  OptParser.add optpsr ?help:(Some "use recursive traversal method") ?long_name:(Some "rt") rt_opt;

  let argvs = OptParser.parse_argv optpsr in
  let target_path = 
    if argvs = [] then "./tests/tailf"
    else
      List.hd argvs
  in
  let rt_on = Opt.get rt_opt in
  let assembly_list = []
(*
    [PUSH(true, Reg_op EBP);
     MOV(true, Reg_op EBP, Reg_op ESP);
     MOV(true, Address_op {addrDisp = (of_int (-4)); addrBase = Some EBP; addrIndex = None}, Reg_op EDI);
     MOV(true, Reg_op EAX, Address_op {addrDisp = (of_int (-4)); addrBase = Some EBP; addrIndex = None});
     IMUL(true,Reg_op EAX, Some (Address_op {addrDisp = (of_int (-4)); addrBase = Some EBP; addrIndex = None}), None);
     POP(Reg_op EBP);
     RET(true, None)]
*)
  in
  let elf = read_elf_file target_path in
  let cr_list = 
    if rt_on then (
      preprocess elf; 
      rt_dis_cfg elf
    )
    else
      ls_dis_file elf 
  in
  (*dump_cr_list stdout cr_list; flush stdout;*)
  let lib_bb =
    mkBB "ffffffff" [emptyPre,RET(true,None),1] (-2)
  in
  let libed_cr_list =
  List.map (fun cr -> if not (BBSet.is_empty cr.bbs) then {cr with bbs = BBSet.add lib_bb cr.bbs} else cr) cr_list
  in
  config_preds libed_cr_list;
  let call_graph = generate_call_graph elf libed_cr_list in
  config_ret_by_call_graph libed_cr_list call_graph;
  (*config_preds cr_list;*)
  (*let call_graph = generate_call_graph elf cr_list in
  config_ret_by_call_graph cr_list call_graph;*)

  (*config_ret_in_crs libed_cr_list;*)
  (*List.iter (fun cr ->
    BBSet.iter (fun bb ->
      if List.mem "ffffffff" bb.bb_succs then 
        List.iter (fun inter_succ ->
	  let inter_succ_bb = locate_label_in_bbs inter_succ cr.bbs in
	  if List.mem "ffffffff" inter_succ_bb.bb_preds then ()
	  else
	    inter_succ_bb.bb_preds <- "ffffffff" :: inter_succ_bb.bb_preds
	) bb.bb_inter_succs
    ) cr.bbs
  ) libed_cr_list;*)
  (*dump_cr_list stdout libed_cr_list; flush stdout;*)
  (*let cr_list_rtl =
    List.map (fun cr -> 
      {
	start = cr.startAddr; 
	rbs = translate_bbs_to_rbs cr.bbs
      }
    ) libed_cr_list
  in
  let init_bblbl_to_rbid_map = StrIDMap.empty in
  let bblbl_to_rbid_map =
    List.fold_right (fun cr_rtl map1-> 
      List.fold_right (fun rb map2 -> 
	StrIDMap.add rb.rb_bscblk.bb_label rb.rb_ID map2
      ) cr_rtl.rbs map1
    ) cr_list_rtl init_bblbl_to_rbid_map
  in
  *)
  (*
  StrIDMap.iter (fun lbl id -> printf "%s -> %d\n" lbl id) bblbl_to_rbid_map;
  flush stdout*)
  (*List.iter (fun cr_rtl -> List.iter print_rb cr_rtl.rbs) configured_cr_list_rtl*)
  (*
  let simplified_cr_list_rtl =
    List.map 
      (fun cr_rtl -> 
	{
	  start = cr_rtl.start;
	  rbs = List.map 
	    (fun rb -> 
	      {
		rtl_bb = rb.rtl_bb; 
		rtl_instrs = optimize_rtl_ssa [] simpri_methods simpre_all_methods rb.rtl_instrs
	      }
	    ) cr_rtl.rbs
	}
      ) 
      cr_list_rtl
  in
  let simplified_and_filtered_cr_list_rtl =
    List.map 
      (fun cr_rtl -> 
	{
	  start = cr_rtl.start;
	  rbs = List.map 
	    (fun rb -> 
	      {
		rtl_bb = rb.rtl_bb; 
		rtl_instrs = optimize_rtl_ssa ssa_filters simpri_methods simpre_all_methods rb.rtl_instrs
	      }
	    ) cr_rtl.rbs
	}
      ) 
      cr_list_rtl
  in
  let tmp =
    List.map (fun cr_rtl ->
      {
	start = cr_rtl.start;
	rbs = List.map 
	  (fun rb ->
	    {
	      rtl_bb = rb.rtl_bb;
	      rtl_instrs = simp_ident_assign rb.rtl_instrs
	    }
	  ) cr_rtl.rbs
      }
    ) simplified_and_filtered_cr_list_rtl
  in
  let rtl_list = 
    translate_assembly_list emptyPre assembly_list 
  in
  let final_rtl_list = 
    optimize_rtl_ssa ssa_filters simpri_methods simpre_methods rtl_list
  in*)
  (*let rtl_funcs = List.flatten (List.map (collect_rtl_func elf emptyPre) libed_cr_list) in*)
  (*
  List.iter (fun rtl_func ->
    List.iter (fun rb ->
      rb.rb_ID <- StrIDMap.find rb.rb_bscblk.bb_label bblbl_to_rbid_map
    ) rtl_func.rf_blocks
  ) rtl_funcs;
  *)
(*
  let configured_rtl_funcs =
    List.map (fun rfunc -> 
      let configured_rfunc = 
	{ rfunc with
	  rf_blocks = List.map (fun rb -> 
	    {rb with 
	      rb_succs = List.filter_map (fun succ -> 
		try 
		  Some (StrIDMap.find succ bblbl_to_rbid_map)
		with Not_found -> None ) rb.rb_bscblk.bb_succs;
	      rb_inter_succs = List.filter_map (fun inter_succ -> 
		try 
		  Some (StrIDMap.find inter_succ bblbl_to_rbid_map)
		with Not_found -> None ) rb.rb_bscblk.bb_inter_succs;
	      rb_preds = List.filter_map (fun pred -> 
		try 
		  Some (StrIDMap.find pred bblbl_to_rbid_map)
		with Not_found -> None) rb.rb_bscblk.bb_preds;
	    }
	  ) rfunc.rf_blocks
	}
      in
      List.iter (fun rb -> 
	List.iter (fun inter_succ ->
	  let inter_succ_rb = 
	    List.find (fun rb -> rb.rb_ID = inter_succ) configured_rfunc.rtl_blocks
	  in
	  if List.mem rb.rb_ID inter_succ_rb.rb_inter_preds then ()
	  else 
	    inter_succ_rb.rb_inter_preds <- rb.rb_ID :: inter_succ_rb.rb_inter_preds
	) rb.rb_inter_succs
      ) configured_rfunc.rtl_blocks;
      configured_rfunc
    ) rtl_funcs
  in
*)
  (*List.iter print_rtl_func rtl_funcs*)

  (*let startID = StrIDMap.cardinal bblbl_to_rbid_map in*)
  (*
  let target_var_list = all_rv in
    (*
      List.map (fun rv -> 
      {rvar = rv; rvid = (0,None)}
      ) all_rv in
    *)
  let esp = X86_MACHINE.Coq_reg_loc ESP
    (*
      {
      rvar = VarLoc (X86_MACHINE.Coq_reg_loc ESP);
      rvid = (0,None);
      }
    *)
  in
  let eax = X86_MACHINE.Coq_reg_loc EAX in
  let rtl_funcs_in_ssa = 
    List.map (fun rf -> rtlssa_trans rf [eax;esp]) rtl_funcs
  in
  P.printf "Show SSA perimeters for RTL functions:\n";
  List.iter print_rtlssa_function rtl_funcs_in_ssa
  *)
  let asm_fun_list = List.flatten (List.map (fun cr -> collect_asm_func elf cr) libed_cr_list) in
  let cfaInfo = cfaGen asm_fun_list in
  ()




(*List.iter print_rtl_instr final_rtl_list;*)
  (*List.iter (fun cr_rtl -> List.iter (print_rb cr_rtl.start) cr_rtl.rbs) simplified_and_filtered_cr_list_rtl;*)
  (*List.iter (fun cr_rtl -> List.iter (print_rb cr_rtl.start) cr_rtl.rbs) tmp*)
  (*List.iter (fun cr -> List.iter (fun rb -> List.iter print_rtl_instr rb.rtl_instrs) cr) simplified_and_filtered_cr_list_rtl;*)
  
  (*List.iter (fun cr_rtl -> List.iter (fun rb -> List.iter instr_count rb.rtl_instrs) cr_rtl.rbs) cr_list_rtl;
  printf "Initially:\n";
  printf "instruction number: %d;\nnon-trivial instruction number: %d;\nexpression number: %d;\nrnode_number: %d.\n"
    !instr_num !non_trivial_inum !expr_num !rnode_num;
  
  reset_numbers ();
  List.iter (fun cr_rtl -> List.iter (fun rb -> List.iter instr_count rb.rtl_instrs) cr_rtl.rbs) simplified_cr_list_rtl;
  printf "After optimization:\n";
  printf "instruction number: %d;\nnon-trivial instruction number: %d;\nexpression number: %d;\nrnode_number: %d.\n"
    !instr_num !non_trivial_inum !expr_num !rnode_num;

  reset_numbers ();
  List.iter (fun cr_rtl -> List.iter (fun rb -> List.iter instr_count rb.rtl_instrs) cr_rtl.rbs) tmp;
  printf "After redundant assignment optimization:\n";
  printf "instruction number: %d;\nnon-trivial instruction number: %d;\nexpression number: %d;\nrnode_number: %d.\n"
    !instr_num !non_trivial_inum !expr_num !rnode_num;

  reset_numbers ();
  List.iter (fun cr_rtl -> List.iter (fun rb -> List.iter instr_count rb.rtl_instrs) cr_rtl.rbs) simplified_and_filtered_cr_list_rtl;
  printf "After filtering:\n";
  printf "instruction number: %d;\nnon-trivial instruction number: %d;\nexpression number: %d;\nrnode_number: %d.\n"
    !instr_num !non_trivial_inum !expr_num !rnode_num
  *)

(*
  let edi = SmtBvar(of_int 31, BasicName "EDI") in
  let eax = SmtBvar(of_int 31, BasicName "EAX") in
  let imul sbe1 sbe2 = SmtBvop(Bvmul, sbe1, sbe2) in
  let ieq sbe1 sbe2 = SmtCompare(X86_RTL.Coq_eq_op, sbe1, sbe2) in
  let post = LfsNot (LfsExp (ieq (imul edi edi) eax)) in
  let postcond = post in
  let lfs = wp rtl_list postcond in
    (*List.iter print_rtl_instr rtl_list;*)
  let file_creation = create_file file_name in
    if file_creation = (0,0) then
      let path_to_file = String.concat "" ["./FILES.SMT2/"; file_name] in
      let oc = open_out path_to_file in
        prepare_smt_file oc;
        assert_lfs oc lfs;
        end_smt_file oc;
    else 
      print_string "File creation failed!"

let check_sat file_name =
  Sys.command (String.concat "" ["z3 ./FILES.SMT2/"; file_name]);;
*)

let _ = 
  theMain ()
  (*check_sat "output1.smt2"*)
