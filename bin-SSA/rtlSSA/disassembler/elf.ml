(** An elf parser  *)

(* todo: seems like there is a lot of extensions of the elf format*)

open Batteries

open IO
open Printf
open String

module F = Format
module L = Logger

open Formatutil
open Util
open Config

(* A elfparsing logger for general debugging and information *)
let logger = L.make_log "elfparsing"
let debug_event (f: unit -> L.event) = L.log logger L.DEBUG f
let info_event (f: unit -> L.event) = L.log logger L.INFO f
let notice_event (f: unit -> L.event) = L.log logger L.NOTICE f

exception Invalid_elf_file of string

type elf_half = int (* in both elf32 and elf64, the half word has two bytes *)
type elf_word = int32 (* in both elf32 and elf64, a word has four bytes *)
type elf_mword = mword 


type elf_class = 
  | ELFCLASSNONE 			(** invalid class *)
  | ELFCLASS32				(** 32-bit objects *)
  | ELFCLASS64				(** 64-bit objects *)

type elf_data = 
  | ELFDATANONE				(** invalid data encoding *)
  | ELFDATA2LSB				(** little endian *)
  | ELFDATA2MSB				(** big endian  *)

type elf_version = 
  | EV_NONE				(** invalid version *)
  | EV_CURRENT				(** current version *)

type elf_type = 
  | ET_NONE 				(** no file type *)
  | ET_REL				(** relocatable file *)
  | ET_EXEC				(** executable file *)
  | ET_DYN				(** shared object file *)
  | ET_CORE				(** core file *)
  | ET_OS of int                        (** OS-specific: 0xfe00-0xfeff *)
  | ET_PROC of int			(** processor-specific: 0xff00-0xffff *)

type elf_machine = 
  | EM_386				(** Intel 80386 *)
  | EM_UNKNOWN				(** Unkown machine *)

type elf_ehdr = {
  e_class : elf_class;		(** elf class *)
  e_data : elf_data;		(** data endianess *)
  e_type : elf_type;		(** file type *)
  e_machine : elf_machine;	(** target machine *)
  e_version : elf_version;      (** file version *)
  e_entry : elf_mword;          (** start address *)
  e_phoff : elf_mword;          (** program header file offset *)
  e_shoff : elf_mword;          (** section header file offset *)
  e_flags : elf_word;           (** file flags *)
  e_ehsize : elf_half;          (** size of elf header *)
  e_phentsize : elf_half;       (** size of program header entries *)
  e_phnum : elf_half;           (** number of program header entries *)
  e_shentsize : elf_half;       (** size of segment header entries *)
  e_shnum : elf_half;           (** number of program header entries *)
  e_shstrndx : elf_half         (** section header string table index *)
}

(** program header type *)
type ph_type =
  | PT_NULL				(** empty (ignored) entries *)
  | PT_LOAD				(** a loadable segment *)
  | PT_DYNAMIC				(** for dynamic linkng *)
  | PT_INTERP				(** the interpreter *)
  | PT_NOTE				(** auxiliary information  *)
  | PT_SHLIB				(** unspecified *)
  | PT_PHDR				(** the program header itself *)
  | PT_TLS				(** thread-local storage template *)
  | PT_EH_FRAME				(** exception handler frame *)
  | PT_GNU_STACK			(** gnu stack *)
  | PT_GNU_RELRO			(** gnu relocation *) (* ??? *)
  | PT_PROC of elf_word			(** process-specific semantics *)

(** segment flags *)
type seg_flags = {pf_r:bool; pf_w:bool; pf_x:bool}

(** program header *)
type elf_phdr = {
  p_type : ph_type;			(** program header type *)
  p_offset : elf_mword;			(** segment file offset *)
  p_vaddr : elf_mword;			(** segment start virtual address *)
  p_paddr : elf_mword;			(** segment start physical address *)
  p_filesz : elf_mword;			(** segment size in file image *)
  p_memsz : elf_mword;			(** segment size in memory *)
  p_flags : seg_flags;			(** segment flags *)
  p_align : elf_mword			(** segment alignment *)
}

type sh_type = 
  | SHT_NULL				(** inactive section header *)
  | SHT_PROGBITS			(** program-specic info *)
  | SHT_SYMTAB				(** symbol table *)
  | SHT_STRTAB				(** string table *)
  | SHT_RELA				(** reloaction entries with addends *)
  | SHT_HASH				(** symbol hash table *)
  | SHT_DYNAMIC				(** info for dynamic linking *)
  | SHT_NOTE				(** note section *)
  | SHT_NOBITS				(** a section with no space in obj file *)
  | SHT_REL				(** relocation entries w/o addends *)
  | SHT_SHLIB				(** reserved but unspecied semantics *)
  | SHT_DYNSYM				(** symbol table for dynamic linking *)
  | SHT_INIT_ARRAY			(** an array of initialization functions *)
  | SHT_FINI_ARRAY			(** an array of termination functions *)
  | SHT_PREINIT_ARRAY			(** an array of pre-init functions *)
  | SHT_GROUP				(** section group *)
  | SHT_SYMTAB_SHNDX			(** extended section indexes *)
  | SHT_GNU_HASH		        (** gnu-style hash table *)
  | SHT_GNU_VERDEF			(** gnu verdef *)
  | SHT_GNU_VERNEED			(** gnu verneed *)
  | SHT_GNU_VERSYM			(** gnu version symbol table *)
  | SHT_PROC of elf_word                (** processor-specific: 0x70000000-7fffffff *)
  | SHT_USER of elf_word	        (** user-specific: 0x80000000-ffffffff *)


(** section flags *)
type section_flags_reg = 
  {shf_write:bool; shf_alloc:bool; shf_execinstr:bool; shf_merge:bool; 
   shf_strings:bool; shf_info_link:bool; shf_link_order:bool; 
   shf_os_nonconforming:bool; shf_group:bool; shf_tls:bool}

type section_flags = 
  | SHF_REG of section_flags_reg
  | SHF_MASKPROC of elf_mword 		(** processor-specific flags *)

(** section header *)
type elf_shdr = {
  sh_name : string;			(** section name; this is a string,
					    instead of an index to the 
					    string section *)
  sh_type : sh_type;			(** section type *)
  sh_flags : section_flags;		(** section flags *)
  sh_addr : elf_mword;			(** section memory start addr *)
  sh_offset : elf_mword;		(** section offset in file *)
  sh_size : elf_mword;			(** section size *)
  sh_link : elf_word;			(** section table index link *)
  sh_info : elf_word;			(** info that vary depending on sh_type *)
  sh_addralign : elf_mword;		(** section alignment *)
  sh_entsize : elf_mword;		(** enries size in a section *)
}

(** section header index *)
type elf_sh_idx = 
| SHN_UNDEF
| SHN_REGULAR of int			(** a regular section  *)
| SHN_PROC of int
| SHN_OS of int
| SHN_ABS				(** absolute values *)
| SHN_COMMON				(** common symbols  *)
| SHN_XINDEX				(** too large to fit in *)

(* the tag of a dynamic table entry *) 
type dt_tag =
| DT_NULL | DT_NEEDED | DT_PLTRELSZ | DT_PLTGOT
| DT_HASH | DT_STRTAB | DT_SYMTAB | DT_RELA
| DT_RELASZ | DT_RELEVANT | DT_STRSZ | DT_SYMENT
| DT_INIT | DT_FINI | DT_SONAME | DT_RPATH
| DT_SYMBOLIC | DT_REL | DT_RELSZ | DT_RELENT
| DT_PLTREL | DT_DEBUG | DT_TEXTREL | DT_JMPREL
| DT_BIND_NOW | DT_INIT_ARRAY | DT_FINI_ARRAY
| DT_INIT_ARRAYSZ | DT_FINI_ARRAYSZ | DT_RUNPATH
| DT_FLAGS | DT_ENCODING | DT_PREINIT_ARRAY | DT_PREINIT_ARRAYSZ
| DT_GNU_HASH | DT_FLAGS_1 | DT_VERDEF | DT_VERDEFNUM
| DT_VERNEED | DT_VERNEEDNUM | DT_VERSYM | DT_RELCOUNT
| DT_PROC of elf_mword		(** process-specific semantics **)
| DT_UNKNOWN (** to ignore types that are not standard; added by Dongrui **)

(** entries in a dynamic table section *)
type elf_dyn_entry = {
  dyn_tag : dt_tag;
  dyn_val : elf_mword;
}

(** symbol table visibility *)
type symtab_vis =
  STV_DEFAULT | STV_INTERNAL | STV_HIDDEN | STV_PROTECTED

type symtab_bind =
  STB_LOCAL | STB_GLOBAL | STB_WEAK
| STB_OS of int | STB_PROC of int

type symtab_type =
  STT_NOTYPE | STT_OBJECT | STT_FUNC | STT_SECTION 
| STT_FILE | STT_COMMON	| STT_TLS | STT_OS of int | STT_PROC of int

(** entries in a symbol table *)
type elf_symtab_entry = {
  st_name : string;		  (** the symbol name *)
  st_value : elf_mword;		  (** the value of the symbol  *)
  st_size : elf_mword;		  (** the associated size *)
  st_type : symtab_type;	  (** symbol type *)
  st_bind : symtab_bind;	  (** symbol binding  *)
  st_other : symtab_vis;	  (** the symbol's visibility  *)
  st_shndx : elf_sh_idx;	  (** the section related to the symbol  *)
}

type elf_symtab = {
  symtab_name : string;			(** symbol table name  *)
  symtab_entries : elf_symtab_entry list (** a list of entries  *)
}

(** A structure for representing the header info of the whole elf32 file *)
type elf_file = {
  fname : string;		      (** file name *)
  ehdr : elf_ehdr;		      (** elf header *)
  phdr_tab : elf_phdr list;	      (** program header table *)
  shdr_tab : elf_shdr list;	      (** section header table *)
  sym_tabs : elf_symtab list;	      (** a list of symbol tables *)
  dyn_tab : (elf_dyn_entry list) option; (** dynamic section  *)
}

(** A pretty printer for elf header information *)

let string_of_elf_class = function
  | ELFCLASSNONE -> "Invalid class"
  | ELFCLASS32 -> "ELF32"
  | ELFCLASS64 -> "ELF64"

let pp_elf_class fmt ec = F.fprintf fmt "%s" (string_of_elf_class ec)

let string_of_elf_data = function
  | ELFDATANONE -> "Invalid data encoding"
  | ELFDATA2LSB -> "little endian"
  | ELFDATA2MSB -> "big endian"

let pp_elf_data fmt ed = F.fprintf fmt "%s" (string_of_elf_data ed)

let string_of_elf_type = function
  | ET_NONE -> "No file type"
  | ET_REL -> "Relocatable file"
  | ET_EXEC -> "Executable file"
  | ET_DYN -> "Shared object file"
  | ET_CORE -> "Core file"
  | ET_OS n -> sprintf "OS-specific file with number %d" n
  | ET_PROC n -> sprintf "Processor-specific file with number %d" n

let pp_elf_type fmt et = F.fprintf fmt "%s" (string_of_elf_type et)

let string_of_elf_mach = function
  | EM_386 -> "Intel 80386"
  | _ -> "Unknown machine"

let pp_elf_mach fmt em = F.fprintf fmt "%s" (string_of_elf_mach em)

let string_of_elf_version = function
  | EV_NONE -> "Invalid version"
  | EV_CURRENT -> "Current version"

let pp_elf_version fmt ev = F.fprintf fmt "%s" (string_of_elf_version ev)

let pp_elf_ehdr fmt eh =
  F.pp_open_vbox fmt 2;
  F.fprintf fmt "ELF Header:@,";
  F.pp_open_tbox fmt ();
  pp_tab_header fmt [(35,"Class:"); (20, (string_of_elf_class eh.e_class))];
  F.fprintf fmt "Data:%t%a@," pp_tab pp_elf_data eh.e_data;
  F.fprintf fmt "Version:%t%a@," pp_tab pp_elf_version eh.e_version;
  F.fprintf fmt "Type:%t%a@," pp_tab pp_elf_type eh.e_type;
  F.fprintf fmt "Machine:%t%a@," pp_tab pp_elf_mach eh.e_machine;
  F.fprintf fmt "Entry point address:%t%a@," pp_tab pp_mword eh.e_entry;
  F.fprintf fmt "Start of program headers:%t%a (bytes into file)@," 
    pp_tab pp_mword_dec eh.e_phoff;
  F.fprintf fmt "Start of section headers:%t%a (bytes into file)@," 
    pp_tab pp_mword_dec eh.e_shoff;
  F.fprintf fmt "Flags:%t0x%08lx@," pp_tab eh.e_flags;
  F.fprintf fmt "Size of this header:%t%u (bytes)@," pp_tab eh.e_ehsize;
  F.fprintf fmt "Size of program headers:%t%u (bytes)@," pp_tab eh.e_phentsize;
  F.fprintf fmt "Number of program headers:%t%u@," pp_tab eh.e_phnum;
  F.fprintf fmt "Size of section headers:%t%u (bytes)@," pp_tab eh.e_shentsize;
  F.fprintf fmt "Number of section headers:%t%u@," pp_tab eh.e_shnum;
  F.fprintf fmt "Section header string table index:%t%u" pp_tab eh.e_shstrndx;
  F.pp_close_tbox fmt ();
  F.pp_close_box fmt ();
  F.fprintf fmt "@."

let string_of_phdr_type = function
  | PT_NULL -> "NULL"
  | PT_LOAD -> "LOAD"
  | PT_DYNAMIC -> "DYNAMIC"
  | PT_INTERP -> "INTERP"
  | PT_NOTE -> "NOTE"
  | PT_SHLIB -> "UNSPEC"
  | PT_PHDR -> "PHDR"
  | PT_TLS -> "TLS"
  | PT_EH_FRAME -> "EH_FRAME"
  | PT_GNU_STACK -> "GNU_STACK"
  | PT_GNU_RELRO -> "GNU_RELRO"
  | PT_PROC i -> sprintf "PROC-SPEC %d" (int_of_int32 i)

let pp_phdr_type fmt pt = F.fprintf fmt "%s" (string_of_phdr_type pt)

let pp_seg_flags fmt sf = 
  let aux b ch = if b then ch else '-'
  in F.fprintf fmt "%c%c%c" (aux sf.pf_r 'R') (aux sf.pf_w 'W') (aux sf.pf_x 'E') 

let pp_elf_phdr fmt ph = 
  F.fprintf fmt "%a%t%a%t%a%t%a%t%a%t%a%t%a%t%a"
    pp_phdr_type ph.p_type pp_tab
    pp_mword ph.p_offset pp_tab 
    pp_mword ph.p_vaddr pp_tab
    pp_mword ph.p_paddr pp_tab 
    pp_mword ph.p_filesz pp_tab
    pp_mword ph.p_memsz pp_tab 
    pp_seg_flags ph.p_flags pp_tab 
    pp_mword ph.p_align

let pp_elf_phdr_tab fmt pl = 
  F.pp_open_vbox fmt 2;
  F.fprintf fmt "Program Headers:@,";
  F.pp_open_tbox fmt ();
  if es.elf32 then
    pp_tab_header fmt 
      [(11,"Type"); (9, "Offset"); (9, "VirtAddr"); (9, "PhysAddr"); (9, "FileSiz"); 
       (9, "MemSiz"); (4, "Flg"); (9, "Align")]
  else
    pp_tab_header fmt 
      [(11,"Type"); (17, "Offset"); (17, "VirtAddr"); (17, "PhysAddr"); (17, "FileSiz"); 
       (17, "MemSiz"); (4, "Flg"); (9, "Align")];
  F.fprintf fmt "%a" (pp_list pp_elf_phdr) pl;
  F.pp_close_tbox fmt ();
  F.pp_close_box fmt ();
  F.fprintf fmt "@." 			(* flush and new line *)

let string_of_shdr_type = function
  | SHT_NULL -> "NULL"
  | SHT_PROGBITS -> "PROGBITS"
  | SHT_SYMTAB -> "SYMTAB"
  | SHT_STRTAB -> "STRTAB"
  | SHT_RELA -> "RELA"
  | SHT_HASH -> "HASH"
  | SHT_DYNAMIC -> "DYNAMIC"
  | SHT_NOTE -> "NOTE"
  | SHT_NOBITS -> "NOBITS"
  | SHT_REL -> "REL"
  | SHT_SHLIB -> "SHLIB"
  | SHT_DYNSYM -> "DYNSYM"
  | SHT_INIT_ARRAY -> "INITARR"
  | SHT_FINI_ARRAY -> "FINIARR"
  | SHT_PREINIT_ARRAY -> "PREARR"
  | SHT_GROUP -> "GROUP"
  | SHT_SYMTAB_SHNDX -> "SYMIDX"
  | SHT_GNU_HASH -> "GNU_HASH"
  | SHT_GNU_VERDEF -> "VERDEF"
  | SHT_GNU_VERNEED -> "VERNEED"
  | SHT_GNU_VERSYM -> "VERSYM"
    (* todo: the following printing should be hex? *)
  | SHT_PROC i -> sprintf "PROC%d" (int_of_int32 i)
  | SHT_USER i -> sprintf "USER%d" (int_of_int32 i)

let pp_shdr_type fmt st = F.fprintf fmt "%s" (string_of_shdr_type st)

let pp_sec_flags fmt = function
  | SHF_MASKPROC i -> F.fprintf fmt "MASKPROC(%a)" pp_mword i
  | SHF_REG sf -> 
    let aux (b,ch) = if b then ch else "" in
    let l = List.map aux
      [(sf.shf_write, "W"); (sf.shf_alloc, "A"); (sf.shf_execinstr, "X"); 
       (sf.shf_merge, "M"); (sf.shf_strings, "S"); (sf.shf_info_link,"I");
       (sf.shf_link_order, "L"); (sf.shf_group, "G"); (sf.shf_tls, "T")] in
      F.fprintf fmt "%3s" (concat "" l)

let pp_elf_shdr fmt sh = 
  F.fprintf fmt "%s %t%a%t%a%t%a%t%a%t%a%t%2ld%t%2ld%t%a"
    sh.sh_name pp_tab pp_shdr_type sh.sh_type pp_tab
    pp_mword sh.sh_addr pp_tab pp_mword sh.sh_offset pp_tab 
    pp_mword sh.sh_size pp_tab
    pp_sec_flags sh.sh_flags pp_tab
    sh.sh_link pp_tab sh.sh_info pp_tab 
    pp_mword sh.sh_addralign

let pp_elf_shdr_tab fmt sl = 
  F.pp_open_vbox fmt 2;
  F.fprintf fmt "Section Headers:@,";
  F.pp_open_tbox fmt ();
  if es.elf32 then
    pp_tab_header fmt 
      [(20, "Name"); (10,"Type"); (9, "Addr(0x)"); (9, "Off(0x)"); (9, "Size(0x)");
       (4, "Flg"); (3, "Lk"); (4, "Inf"); (5, "Align")]
  else
    pp_tab_header fmt 
      [(20, "Name"); (10,"Type"); (17, "Addr(0x)"); (17, "Off(0x)"); (17, "Size(0x)");
       (4, "Flg"); (3, "Lk"); (4, "Inf"); (5, "Align")];
  F.fprintf fmt "%a" (pp_list pp_elf_shdr) sl;
  F.pp_close_tbox fmt ();
  F.pp_close_box fmt ();
  F.fprintf fmt "@." 			(* flush and new line *)


let string_of_dyn_tag = function
  | DT_NULL -> "NULL"
  | DT_NEEDED -> "NEEDED"
  | DT_PLTRELSZ -> "PLTRELSZ"
  | DT_PLTGOT -> "PLTGOT"
  | DT_HASH -> "HASH"
  | DT_STRTAB -> "STRTAB"
  | DT_SYMTAB -> "SYMTAB"
  | DT_RELA -> "RELA"
  | DT_RELASZ -> "RELASZ"
  | DT_RELEVANT -> "RELEVANT"
  | DT_STRSZ -> "STRSZ"
  | DT_SYMENT -> "SYMENT"
  | DT_INIT -> "INIT"
  | DT_FINI -> "FINI"
  | DT_SONAME -> "SONAME"
  | DT_RPATH -> "RPATH"
  | DT_SYMBOLIC -> "SYMBOLIC"
  | DT_REL -> "REL"
  | DT_RELSZ -> "RELSZ"
  | DT_RELENT -> "RELENT"
  | DT_PLTREL -> "PLTREL"
  | DT_DEBUG -> "DEBUG"
  | DT_TEXTREL -> "TEXTREL"
  | DT_JMPREL -> "JMPREL"
  | DT_BIND_NOW -> "BIND_NOW"
  | DT_INIT_ARRAY -> "INIT_ARRAY"
  | DT_FINI_ARRAY -> "FINI_ARRAY"
  | DT_INIT_ARRAYSZ -> "INIT_ARRAYSZ"
  | DT_FINI_ARRAYSZ -> "FINI_ARRAYSZ"
  | DT_RUNPATH -> "RUNPATH"
  | DT_FLAGS -> "FLAGS"
  | DT_ENCODING -> "ENCODING"
  | DT_PREINIT_ARRAY -> "PREINIT_ARRAY"
  | DT_PREINIT_ARRAYSZ -> "PREINIT_ARRAYSZ"

  | DT_GNU_HASH -> "GNU_HASH"
  | DT_FLAGS_1 -> "FLAGS_1"
  | DT_VERDEF -> "VERDEF"
  | DT_VERDEFNUM -> "VERDEFNUM"
  | DT_VERNEED -> "VERNEED"
  | DT_VERNEEDNUM -> "VERNEEDNUM"
  | DT_VERSYM -> "VERSYM"
  | DT_RELCOUNT -> "RELCOUNT"
  | DT_PROC tag -> sprintf "PROC%s" (str_of_mword_dec tag)
  | DT_UNKNOWN -> "UNKNOWN" (** added by Dongrui **)

let pp_elf_dyn_entry fmt de = 
  F.fprintf fmt "%s%t%a"
    (string_of_dyn_tag de.dyn_tag) pp_tab 
    pp_mword de.dyn_val

let pp_elf_dyn_tab fmt tab = 
  F.pp_open_vbox fmt 2;
  F.fprintf fmt "Dynamic section contains %d entries:@,"
    (List.length tab);
  F.pp_open_tbox fmt ();
  pp_tab_header fmt [(15,"Type"); (17, "Name/Value")];
  F.fprintf fmt "%a" (pp_list pp_elf_dyn_entry) tab;
  F.pp_close_tbox fmt ();
  F.pp_close_box fmt ();
  F.fprintf fmt "@." 	

let str_of_st_type = function
  | STT_NOTYPE -> "NOTYPE"
  | STT_OBJECT -> "OBJECT"
  | STT_FUNC -> "FUNC"
  | STT_SECTION -> "SEC"
  | STT_FILE -> "FILE"
  | STT_COMMON -> "COMMON"
  | STT_TLS -> "TLS"
  | STT_OS i -> "OS " ^ string_of_int i
  | STT_PROC i -> "PROC " ^ string_of_int i

let str_of_st_bind = function
  | STB_LOCAL -> "LOCAL"
  | STB_GLOBAL -> "GLOBAL"
  | STB_WEAK -> "WEAK"
  | STB_OS i -> "OS " ^ string_of_int i
  | STB_PROC i -> "PROC " ^  string_of_int i

let str_of_st_vis = function
  | STV_DEFAULT -> "DEFAULT"
  | STV_INTERNAL -> "INTERNAL"
  | STV_HIDDEN -> "HIDDEN"
  | STV_PROTECTED -> "PROT"

let str_of_elf_sh_idx = function
  | SHN_UNDEF -> "UND"
  | SHN_REGULAR i -> sprintf "%d" i
  | SHN_PROC i -> "PROC"
  | SHN_OS i -> "OS"
  | SHN_ABS -> "ABS"
  | SHN_COMMON -> "COM"
  | SHN_XINDEX -> "XIN"
    
let pp_elf_sym_entry fmt se = 
  F.fprintf fmt "%a%t%a%t%s%t%s%t%s%t%s%t%s"
    pp_mword se.st_value pp_tab
    pp_mword_dec se.st_size pp_tab
    (str_of_st_type se.st_type) pp_tab
    (str_of_st_bind se.st_bind) pp_tab
    (str_of_st_vis se.st_other) pp_tab
    (str_of_elf_sh_idx se.st_shndx) pp_tab
    se.st_name

let pp_elf_sym_tab fmt symtab = 
  F.pp_open_vbox fmt 2;
  F.pp_open_tbox fmt ();
  F.fprintf fmt "Symbol table %s contains %d entries:@,"
    symtab.symtab_name (List.length symtab.symtab_entries);
  if es.elf32 then
    pp_tab_header fmt 
      [(9, "Value"); (5,"Size"); (8, "Type"); (8, "Bind"); (9, "Vis");
       (4, "Ndx"); (5, "Name")]
  else
    pp_tab_header fmt 
      [(17, "Value"); (5,"Size"); (8, "Type"); (8, "Bind"); (9, "Vis");
       (4, "Ndx"); (5, "Name")];
  F.fprintf fmt "%a" (pp_list pp_elf_sym_entry) symtab.symtab_entries;
  F.pp_close_tbox fmt ();
  F.pp_close_box fmt ();
  F.fprintf fmt "@." 	

(** The output format follows readelf *)
let pp_elf_file (fmt:F.formatter) (elf:elf_file) =
  F.fprintf fmt "File: %s@." elf.fname;
  F.fprintf fmt "%a@.%a@.%a"
    pp_elf_ehdr elf.ehdr
    pp_elf_shdr_tab elf.shdr_tab
    pp_elf_phdr_tab elf.phdr_tab;
  List.iter 
    (fun st -> F.printf "@.%a"  pp_elf_sym_tab st)
    elf.sym_tabs;
  match elf.dyn_tab with
  | Some tab -> F.fprintf fmt "@.%a" pp_elf_dyn_tab tab
  | None -> ()

(** Elf parser *)

let read_ui16_endian ip = 
  if (es.small_endian) then IO.read_ui16 ip
  else IO.BigEndian.read_ui16 ip

let read_real_i32_endian ip = 
  if (es.small_endian) then IO.read_real_i32 ip 
  else IO.BigEndian.read_real_i32 ip

let read_i64_endian ip = 
  if (es.small_endian) then IO.read_i64 ip
  else IO.BigEndian.read_i64 ip

let read_mword_endian ip = 
  if es.elf32 then int64_of_int32 (read_real_i32_endian ip)
  else read_i64_endian ip

let read_half_endian = read_ui16_endian
let read_word_endian = read_real_i32_endian

let loproc = "0x70000000" 
let hiproc = "0x7fffffff"
let louser = "0x80000000"
let hiuser = "0xffffffff"

let elf_class_of_int = function
  | 0 -> ELFCLASSNONE
  | 1 -> ELFCLASS32
  | 2 -> ELFCLASS64
  | _ -> raise (Invalid_elf_file "wrong elf class")

let elf_endian_of_int = function
  | 0 -> ELFDATANONE
  | 1 -> ELFDATA2LSB
  | 2 -> ELFDATA2MSB
  | _ -> raise (Invalid_elf_file "wrong elf data endianess")

let elf_type_of_int = function
  | 0 -> ET_NONE
  | 1 -> ET_REL
  | 2 -> ET_EXEC
  | 3 -> ET_DYN
  | 4 -> ET_CORE
  | n -> if ((n>=0xfe00) && (n<=0xfeff)) then ET_OS n
    else if ((n>=0xff00) && (n<=0xffff)) then ET_PROC n
    else raise (Invalid_elf_file "wrong elf type")

(* This elf module only deals intel 80386 machine for now *)
let elf_machine_of_int = function
  | 3 -> EM_386
  | _ -> EM_UNKNOWN

let ph_type_of_i32 i = 
  if (Int32.compare i (Int32.of_string loproc) >= 0) then
    if (Int32.compare i (Int32.of_string hiproc) <= 0) then PT_PROC i
    else raise (Invalid_elf_file 
		  (sprintf "wrong program header type %d" (int_of_int32 i)))
  else match (int_of_int32 i) with
    | 0 -> PT_NULL 
    | 1 -> PT_LOAD
    | 2 -> PT_DYNAMIC
    | 3 -> PT_INTERP
    | 4 -> PT_NOTE
    | 5 -> PT_SHLIB
    | 6 -> PT_PHDR
    | 7 -> PT_TLS
    | 0x6474e550 -> PT_EH_FRAME		(* this is a negative number in int *)
    | 0x6474e551 -> PT_GNU_STACK
    | 0x6474e552 -> PT_GNU_RELRO
    | _ -> 
      raise (Invalid_elf_file (sprintf "wrong program header type %d" (int_of_int32 i)))

let dt_tag_of_mword i =
  let exn = Invalid_elf_file (sprintf "wrong dynamic entry type %s" (str_of_mword i)) in
  if (MWord.(>=%) i (MWord.of_string loproc)) then
    if (MWord.(<=%) i (MWord.of_string hiproc)) then DT_PROC i
    else raise exn
  else 
  match (MWord.to_int i) with
  | 0 -> DT_NULL
  | 1 -> DT_NEEDED 
  | 2 -> DT_PLTRELSZ
  | 3 -> DT_PLTGOT
  | 4 -> DT_HASH
  | 5 -> DT_STRTAB
  | 6 -> DT_SYMTAB
  | 7 -> DT_RELA
  | 8 -> DT_RELASZ
  | 9 -> DT_RELEVANT
  | 10 -> DT_STRSZ
  | 11 -> DT_SYMENT
  | 12 -> DT_INIT
  | 13 -> DT_FINI
  | 14 -> DT_SONAME
  | 15 -> DT_RPATH 
  | 16 -> DT_SYMBOLIC
  | 17 -> DT_REL
  | 18 -> DT_RELSZ
  | 19 -> DT_RELENT
  | 20 -> DT_PLTREL
  | 21 -> DT_DEBUG 
  | 22 -> DT_TEXTREL
  | 23 -> DT_JMPREL
  | 24 -> DT_BIND_NOW
  | 25 -> DT_INIT_ARRAY
  | 26 -> DT_FINI_ARRAY
  | 27 -> DT_INIT_ARRAYSZ
  | 28 -> DT_FINI_ARRAYSZ
  | 29 -> DT_RUNPATH
  | 30 -> DT_FLAGS
  | 31 -> DT_ENCODING
  | 32 -> DT_PREINIT_ARRAY
  | 33 -> DT_PREINIT_ARRAYSZ

  | 0x6ffffef5 -> DT_GNU_HASH
  | 0x6ffffffb -> DT_FLAGS_1
  | 0x6ffffffc -> DT_VERDEF
  | 0x6ffffffd -> DT_VERDEFNUM
  | 0x6ffffffe -> DT_VERNEED
  | 0x6fffffff -> DT_VERNEEDNUM
  | 0x6ffffff0 -> DT_VERSYM
  | 0x6ffffffa -> DT_RELCOUNT
  | _ -> DT_UNKNOWN (*raise exn*) (** modified by Dongrui **)


let elf_version_of_i32 x = 
  if (Int32.compare x Int32.zero = 0) then EV_NONE
  else if (Int32.compare x Int32.one = 0) then EV_CURRENT
  else raise (Invalid_elf_file "wrong elf version")

(* Returns true iff flag flg is on in i; 
   flg should be an int32 that has only one bit on *)
let check_flag (i:int32) (flg:int32) =
  Int32.compare (Int32.logand i flg) (Int32.zero) <> 0

(* 64-bit version *)
let check_flag_64 (i:int64) (flg:int64) =
  Int64.compare (Int64.logand i flg) (Int64.zero) <> 0

let bool_of_endian = function
  | ELFDATANONE	-> true
  | ELFDATA2LSB	-> true
  | ELFDATA2MSB	-> false

let bool_of_elf_class = function
  | ELFCLASSNONE -> true
  | ELFCLASS32 -> true
  | ELFCLASS64 -> false

let read_ehdr (ip:input) =
  let magic = nread ip 4 in
    if (compare magic "\127ELF" <> 0)
    then raise (Invalid_elf_file "magic number not matched")
    else
      let cls = elf_class_of_int (read_byte ip) in
      set_es_elf32 (bool_of_elf_class cls);
      let ed = elf_endian_of_int (read_byte ip) in
        set_es_endian (bool_of_endian ed);
        let _ = nread ip 10 in		(* ignore the version and padding bytes *)
	let et = elf_type_of_int (read_half_endian ip) in
	let em = elf_machine_of_int (read_half_endian ip) in
	let ev = elf_version_of_i32 (read_word_endian ip) in
	let w2 = read_mword_endian ip in
	let w3 = read_mword_endian ip in
	let w4 = read_mword_endian ip in
	let w5 = read_word_endian ip in
	let h3 = read_half_endian ip in
	let h4 = read_half_endian ip in
	let h5 = read_half_endian ip in
	let h6 = read_half_endian ip in
	let h7 = read_half_endian ip in
	let h8 = read_half_endian ip in
	{e_class=cls; e_data=ed;
	 e_type=et; e_machine=em; e_version=ev;
	 e_entry=w2; e_phoff=w3; e_shoff=w4; e_flags=w5;
	 e_ehsize=h3; e_phentsize=h4; e_phnum=h5; e_shentsize=h6;
	 e_shnum=h7; e_shstrndx=h8;
	}

let mk_seg_flags r w x = {pf_r=r; pf_w=w; pf_x=x;}

let int_of_pf_r : int32 = Int32.of_int 0x00000004
let int_of_pf_w : int32 = Int32.of_int 0x00000002
let int_of_pf_x : int32 = Int32.of_int 0x00000001

let to_seg_flags (i:int32) : seg_flags = 
  mk_seg_flags (check_flag i int_of_pf_r)
    (check_flag i int_of_pf_w) (check_flag i int_of_pf_x)

let of_seg_flags (sf:seg_flags) : int32 = 
  let aux b res = if b then res else Int32.zero in
  Int32.logor (aux sf.pf_r int_of_pf_r)
    (Int32.logor (aux sf.pf_w int_of_pf_w) (aux sf.pf_x int_of_pf_x))

let read_phdr32 (ip:input) =
  let pt = ph_type_of_i32 (read_word_endian ip) in
  let w1 = read_mword_endian ip in
  let w2 = read_mword_endian ip in
  let w3 = read_mword_endian ip in
  let w4 = read_mword_endian ip in
  let w5 = read_mword_endian ip in
  let flags = to_seg_flags (read_word_endian ip) in
  let w7 = read_mword_endian ip in
  {p_type=pt; p_offset=w1; p_vaddr=w2; p_paddr=w3;
   p_filesz=w4; p_memsz=w5; p_flags=flags; p_align=w7;
  }

(* need a separate phdr reader for elf64 because the ordering of fields
   are different from elf32 *)
let read_phdr64 (ip:input) = 
  let pt = ph_type_of_i32 (read_word_endian ip) in
  let flags = to_seg_flags (read_word_endian ip) in
  let w1 = read_mword_endian ip in
  let w2 = read_mword_endian ip in
  let w3 = read_mword_endian ip in
  let w4 = read_mword_endian ip in
  let w5 = read_mword_endian ip in
  let w7 = read_mword_endian ip in
  {p_type=pt; p_offset=w1; p_vaddr=w2; p_paddr=w3;
   p_filesz=w4; p_memsz=w5; p_flags=flags; p_align=w7;
  }

let read_phdr ip = 
  if es.elf32 then read_phdr32 ip else read_phdr64 ip

let sh_type_of_i32 i = 
  if (Int32.compare i (Int32.of_string loproc) >= 0) then
    if (Int32.compare i (Int32.of_string hiproc) <= 0) then SHT_PROC i
    else SHT_USER i
  else match (int_of_int32 i) with
    | 0 -> SHT_NULL
    | 1 -> SHT_PROGBITS	
    | 2 -> SHT_SYMTAB
    | 3 -> SHT_STRTAB
    | 4 -> SHT_RELA
    | 5 -> SHT_HASH
    | 6 -> SHT_DYNAMIC
    | 7 -> SHT_NOTE
    | 8 -> SHT_NOBITS	
    | 9 -> SHT_REL
    | 10 -> SHT_SHLIB
    | 11 -> SHT_DYNSYM
    | 14 -> SHT_INIT_ARRAY
    | 15 -> SHT_FINI_ARRAY
    | 16 -> SHT_PREINIT_ARRAY
    | 17 -> SHT_GROUP
    | 18 -> SHT_SYMTAB_SHNDX
    | 0x6ffffff6 -> SHT_GNU_HASH
    | 0x6ffffffd -> SHT_GNU_VERDEF
    | 0x6ffffffe -> SHT_GNU_VERNEED
    | 0x6fffffff -> SHT_GNU_VERSYM
    | _ -> 
      raise (Invalid_elf_file (sprintf "wrong section type %d" (int_of_int32 i)))

let mk_section_flags w a x m s il lo on g t = 
  SHF_REG {shf_write=w; shf_alloc=a; shf_execinstr=x; shf_merge=s;
	   shf_strings=s; shf_info_link=il; shf_link_order=lo;
	   shf_os_nonconforming=on; shf_group=g; shf_tls=t}

let int_of_shf_write  = Int64.of_int 0x00000001
let int_of_shf_alloc = Int64.of_int 0x00000002
let int_of_shf_execinstr = Int64.of_int 0x00000004
let int_of_shf_merge  = Int64.of_int 0x00000010
let int_of_shf_strings  = Int64.of_int 0x00000020
let int_of_shf_info_link = Int64.of_int 0x00000040
let int_of_shf_link_order = Int64.of_int 0x00000080
let int_of_shf_os_nonconforming = Int64.of_int 0x00000100
let int_of_shf_group = Int64.of_int 0x00000200
let int_of_shf_tls  = Int64.of_int 0x00000400
let int_of_shf_maskproc = Int64.of_string "0xf0000000"

let to_section_flags (i:int64) : section_flags = 
  if (Int64.compare (Int64.logand i int_of_shf_maskproc) Int64.zero <> 0)
  then SHF_MASKPROC i
  else mk_section_flags (check_flag_64 i int_of_shf_write)
       (check_flag_64 i int_of_shf_alloc) (check_flag_64 i int_of_shf_execinstr)
       (check_flag_64 i int_of_shf_merge) (check_flag_64 i int_of_shf_strings)
       (check_flag_64 i int_of_shf_info_link) (check_flag_64 i int_of_shf_link_order)
       (check_flag_64 i int_of_shf_os_nonconforming) 
       (check_flag_64 i int_of_shf_group) (check_flag_64 i int_of_shf_tls)

let read_shdr (ip:input) = 
  let name_idx = read_word_endian ip in
  let pt = sh_type_of_i32 (read_word_endian ip) in
  let sf = to_section_flags (read_mword_endian ip) in
  let w1 = read_mword_endian ip in
  let w2 = read_mword_endian ip in
  let w3 = read_mword_endian ip in
  let w4 = read_word_endian ip in
  let w5 = read_word_endian ip in
  let w6 = read_mword_endian ip in
  let w7 = read_mword_endian ip in
  (name_idx, 
   {sh_name=""; sh_type=pt; sh_flags=sf; sh_addr=w1;
   sh_offset=w2; sh_size=w3; sh_link=w4;
   sh_info=w5; sh_addralign=w6; sh_entsize=w7
   })

(* get a string form a string table and an index *)
let get_str_from_strtab (strtab:string) (start_idx:int) = 
  let end_idx = String.index_from strtab start_idx '\000' in
  String.sub strtab start_idx (end_idx - start_idx)

let parse_shdr_strings (s:string) idx_shdr_tab = 
  List.map 
    (fun (idx,shdr) ->
      {shdr with sh_name=get_str_from_strtab s (int_of_int32 idx)})
    idx_shdr_tab

let read_dyn_tab (ip:input) = 
  let rec read_dyn_tab' () = 
    let tg_mword = read_mword_endian ip in
    let tg = dt_tag_of_mword tg_mword in
    (*let () = print_string (string_of_dyn_tag tg);
 	     print_string "\n";
    in (** modified **)*)
    let v = read_mword_endian ip in
    (*let () = print_string (Int64.to_string v); 
 	     print_string "\n";
    in (** modified **)*)
    let de = {dyn_tag = tg; dyn_val = v} in
    if tg = DT_UNKNOWN then 
      let () =
	Printf.printf "Unknown Dynamic Type: %s\n" (str_of_mword tg_mword) in
      read_dyn_tab' ()
    else if tg = DT_NULL then [de]
    else de :: read_dyn_tab' ()
  in read_dyn_tab' ()

let st_type_of_info (info: int) = 
  let tp = info land 0xf in
  match tp with
  | 0 -> STT_NOTYPE
  | 1 -> STT_OBJECT
  | 2 -> STT_FUNC
  | 3 -> STT_SECTION
  | 4 -> STT_FILE
  | 5 -> STT_COMMON
  | 6 -> STT_TLS
  | _ ->
    if (tp >= 10) then
      if (tp <= 12) then STT_OS tp
      else STT_PROC tp
    else raise (Invalid_elf_file (sprintf "wrong symbol type %d" tp))

let st_bind_of_info info = 
  let bd = info lsr 4 in
  match bd with
  | 0 -> STB_LOCAL
  | 1 -> STB_GLOBAL
  | 2 -> STB_WEAK
  | _ -> 
    if (bd >= 10) then
      if (bd <= 12) then STB_OS bd
      else STB_PROC bd
    else raise (Invalid_elf_file (sprintf "wrong symbol binding %d" bd))

let symtab_vis_of_byte b = match b with
  | 0 -> STV_DEFAULT
  | 1 -> STV_INTERNAL
  | 2 -> STV_HIDDEN
  | 3 -> STV_PROTECTED
  | _ -> raise (Invalid_elf_file (sprintf "wrong symbol visibility %d" b))

let elf_sh_idx_of_int i = match i with
  | 0 -> SHN_UNDEF
  | 0xfff1 -> SHN_ABS
  | 0xfff2 -> SHN_COMMON
  | 0xffff -> SHN_XINDEX
  | _ -> 
    if (i < 0xff00) then SHN_REGULAR i
    else if (i < 0xff1f) then SHN_PROC i
    else if (i < 0xff3f) then SHN_OS i
    else raise (Invalid_elf_file (sprintf "wrong section header index %d" i))

let read_sym_entry32 (ip:input) strtab = 
  let nm_idx = read_mword_endian ip in 
  let name = get_str_from_strtab strtab (MWord.to_int nm_idx) in
  let vl = read_mword_endian ip in 
  let sz = read_mword_endian ip in
  let info = read_byte ip in
  let other = read_byte ip in
  let shndx = read_half_endian ip in
  {st_name = name; st_value = vl; st_size = sz;
   st_type = st_type_of_info info; st_bind = st_bind_of_info info;
   st_other = symtab_vis_of_byte other; 
   st_shndx = elf_sh_idx_of_int shndx }

let read_sym_entry64 (ip:input) strtab =
  let nm_idx = read_mword_endian ip in
  let name = get_str_from_strtab strtab (MWord.to_int nm_idx) in
  let info = read_byte ip in
  let other = read_byte ip in
  let shndx = read_half_endian ip in
  let vl = read_mword_endian ip in
  let sz = read_mword_endian ip in
  {st_name = name; st_value = vl; st_size = sz;
   st_type = st_type_of_info info; st_bind = st_bind_of_info info;
   st_other = symtab_vis_of_byte other; 
   st_shndx = elf_sh_idx_of_int shndx }

let read_sym_entry ip strtab = 
  if es.elf32 then read_sym_entry32 ip strtab
  else read_sym_entry64 ip strtab

let read_sym_tab ip shdr strtab = 
  let entries = List.init (MWord.to_int (MWord.(/%) shdr.sh_size shdr.sh_entsize))
    (fun _ -> read_sym_entry ip strtab) in
  {symtab_name=shdr.sh_name; symtab_entries=entries}

let read_sym_tabs fch (ip:input) shdr_tab = 
  let aux shdr = match shdr.sh_type with
    | SHT_SYMTAB | SHT_DYNSYM -> 
      (* for a symbol table, sh_link is the index of its string table *)
      let strtab_idx = shdr.sh_link in
      let strtab_shdr = List.nth shdr_tab (Int32.to_int strtab_idx) in 
      seek_in fch (MWord.to_int strtab_shdr.sh_offset);
      let strtab = nread ip (MWord.to_int strtab_shdr.sh_size) in
      seek_in fch (MWord.to_int shdr.sh_offset);
      Some (read_sym_tab ip shdr strtab)
    | _ -> None
  in List.filter_map aux shdr_tab

let read_elf_file (fname:string) : elf_file =
  let fch = Pervasives.open_in fname in
  let ip = input_channel fch in
  debug_event (fun _ -> "elf_parsing_start", []);
  let ehdr = read_ehdr ip in
  debug_event (fun _ -> "elf_parsing_ehdr_end", []);
  seek_in fch (MWord.to_int ehdr.e_phoff);
  let phdr_tab = List.init ehdr.e_phnum (fun _ -> read_phdr ip) in
    debug_event (fun _ -> "elf_parsing_phdr_end", []);
    seek_in fch (MWord.to_int ehdr.e_shoff);
    let idx_shdr_tab = List.init ehdr.e_shnum (fun _ -> read_shdr ip) in
    debug_event (fun _ -> "elf_parsing_shdr_end", []);
    let (_,strtab_shdr) = List.nth idx_shdr_tab (ehdr.e_shstrndx) in
      seek_in fch (MWord.to_int strtab_shdr.sh_offset);
      let str = nread ip (MWord.to_int strtab_shdr.sh_size) in
      let shdr_tab = parse_shdr_strings str idx_shdr_tab in
      let sym_tabs = read_sym_tabs fch ip shdr_tab in
      let dyn_entries =
      	try
          (* check if there is a dynamic entry in program headers *)
          let dyn = List.find (fun ph -> ph.p_type = PT_DYNAMIC) phdr_tab in
      	  seek_in fch (MWord.to_int dyn.p_offset);
	  (*print_int (MWord.to_int dyn.p_offset);
	  print_string "\n";*)
	  Some (read_dyn_tab ip)
      	with Not_found -> None in
      BatIO.close_in ip;
      Pervasives.close_in fch;
      {fname=fname; ehdr=ehdr; phdr_tab=phdr_tab; shdr_tab=shdr_tab;
       sym_tabs = sym_tabs; dyn_tab = dyn_entries}

(** Util functions for elf files *)

(** decide whether shdr represents a code section *)
let is_code_sec shdr = 
  match shdr.sh_flags  with
    | SHF_REG sf -> sf.shf_alloc && sf.shf_execinstr
    | _ -> false

(** decide if a segment is a code segment *)
let is_code_seg phdr = 
  phdr.p_type = PT_LOAD && phdr.p_flags.pf_r && phdr.p_flags.pf_x

let is_data_seg phdr =
  phdr.p_type = PT_LOAD && phdr.p_flags.pf_r && phdr.p_flags.pf_w
