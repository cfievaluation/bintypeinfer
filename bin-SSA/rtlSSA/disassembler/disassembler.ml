(** A couple of Disassembly algorithms: linear sweep, recursive diassembly *)

open Batteries
open Bigarray

open IO

module P=Printf
module F=Format
open Formatutil
module E=Errormsg
module L=Logger

open Util
open Config
open Ibaux
open BB

open Bits
open X86Syntax
open Decode
open Abbrev
open Instr
open Elf


(** a switch to turn on or off the disassembler testing *)
let enableTesting = ref false
let m_on = ref false
let dm_on = ref true
let ld_on = ref true

let icallsite_num = ref 0
let tailcall_num = ref 0
let icall_afp_num = ref 0
let icall_vfp_num = ref 0
let tcall_afp_num = ref 0
let tcall_vfp_num = ref 0

let allfp_num = ref 0

(* A disassembler logger for general debugging and information *)
let logger = L.make_log "disassembler"
let debug_event (f: unit -> L.event) = L.log logger L.DEBUG f
let info_event (f: unit -> L.event) = L.log logger L.INFO f
let notice_event (f: unit -> L.event) = L.log logger L.NOTICE f  

type disas_task = {
  dt_pred: string;
  dt_replace_pred: string;
  dt_target_symbol_label : string;
  dt_target : int;
  dt_jump_bound : int option;
}
   
let mkDT pred r_pred target bound_option =
  let symbol_label = match target with (loc,lbl) -> lbl in
  let target_addr = match target with (loc,lbl) -> loc in
  {
    dt_pred = pred;
    dt_replace_pred = r_pred;
    dt_target_symbol_label = symbol_label;
    dt_target = target_addr;
    dt_jump_bound = bound_option;
  }
  

(** the interface to the instruction decoder *)
module type DECODER = sig
  (** initiazation of the decoder *)
  val init : unit -> unit
  (** decode_instr takes an array of bytes and an offset in the array;
      it returns (prefix,instr) pair and the length of the instr *)
  val decode_instr : codeBytes -> int -> prefix * instr * int
end

module type DISASSEMBLER = sig
  val init: unit -> unit

  val decode_instr : codeBytes -> int -> prefix * instr * int
  
  (** "decode_basic_block label prog loc" decodes one basic block from prog
      at loc until meeting a control-flow instruction or the end of the
      prog; the string argument provides the label of the block *)
  val decode_basic_block : string -> codeBytes -> int -> basicblock

  (** A linear sweep decoder. It takes a list of bytes, and returns a list
      of instructions with their lengths. This decoder ignores the last few
      bytes that cannot be decoded into an instruction. *)
  val ls_dis_section : codeBytes -> elf_mword -> code_region

  (** Disassemble a file using linear sweep *)
  val ls_dis_file : elf_file -> code_region list

  (** Disassemble a file using recursive traversal *)
  (*val rt_dis_file : elf_file -> code_region list*)

  (** Disassemble a file using rt method and construct CFG **)
  val rt_dis_cfg : elf_file -> code_region list

end

(**************************************************)
(* Utility functions *)
(**************************************************)

(* read from file fanme a region at offset with size *)
let file_read_region (fname:string) (offset:int) (size:int) =
  (* alternative: use memory mapped files *)
  (* let fdesc = Unix.openfile fname [Unix.O_RDONLY] 0 in *)
  (* let prog = Array1.map_file fdesc ?pos:(Some (Int64.of_int32 shdr.sh_offset)) *)
  (*   int8_unsigned c_layout false (Int32.to_int shdr.sh_size) in *)
  (* Unix.close fdesc; *)
  let fch = Pervasives.open_in fname in
  seek_in fch offset;
  let ip = input_channel fch in
  (* limitation: only handle files whose size is less than max_int *)
  let prog = Array1.create int8_unsigned c_layout size in
  Array1.modify (fun _ -> (IO.read_byte ip)) prog;
  Pervasives.close_in fch;
  prog

(* annotate a list of instrs with their (relative) start addresses *)
(*let rec instrs_with_starts start instrs = match instrs with
  | [] -> []
  | (pre,ins,len) :: instrs' -> 
    (start,pre,ins,len) :: instrs_with_starts (start+len) instrs'
*)

(** illegal instruction at some loc *)
exception DF_IllegalInstr of int
(** two instructions have overlapping bytes; the two ints are the
    beginning addrs of the two instrs. *)
exception DF_Overlap of int*int
exception DF_Build_Table


module Gen_Disassembler (Di:DECODER) : DISASSEMBLER = struct
  include Di

  let init () = Di.init ()

(**** added by Dongrui ****)

(** get_successor : basicblock -> mword -> string list **)
  let get_successor (bb: basicblock) (start: mword) =
    let followupAddr = (MWord.to_int start) + bb.bb_relAddr + bb.bb_size in
    let handleJMP abs op =
      match op with
	| Imm_op imm ->
	  if abs then 
            [Big_int.to_int (signed32 imm)]
	    (*[get_relative_addr (MWord.of_big_int (unsigned32 imm)) start]*)
	  else [followupAddr + (Big_int.to_int (signed32 imm))]
	| _ -> [-1]
    in
    let (pre,ins,len) = (List.last bb.bb_instrs) in
    let int_lst = match ins with
      | CALL (true,abs,op,None) ->
	followupAddr :: handleJMP abs op (* speculative disassembly: assume the
					    address after call is a new beginning *)
      | CALL (_,_,_,_) -> E.s (E.unimp "We do not handle far calls!")
      | HLT | IRET -> [-1]
      | Jcc (_,disp) ->
	[followupAddr; followupAddr + (Big_int.to_int (signed32 disp))]
      | JCXZ b ->
	[followupAddr; followupAddr + (Big_int.to_int (signed8 b))]
      | JMP (true,abs,op,None) -> handleJMP abs op
      | JMP (_,_,_,_) -> E.s (E.unimp "We do not handle far jumps!")
      | RET (_,_) -> [-1]
      | _ -> [followupAddr]
    in
    List.map (fun i -> str_of_mword_flex (MWord.of_int i)) int_lst
(**************************)
    
  let rec decode_basic_block' (prog:codeBytes) (loc: int) =
    let (pre,ins,len) = decode_instr prog loc in
    if (control_flow_instr ins || loc+len >= Array1.dim prog) then [(pre,ins,len)]
    else (pre,ins,len) :: (decode_basic_block' prog (loc+len))
      
  let decode_basic_block label (prog:codeBytes) (loc: int) =
    let instrs = decode_basic_block' prog loc in
    mkBB label instrs loc

(** ls_dis_section : codeBytes -> elf_mword -> code_region **)
  let ls_dis_section (prog:codeBytes) (start:elf_mword) =
    let bbs = ref (BBSet.empty) in
    let rec decode_code_section' (loc:int) =
      let label = str_of_mword_flex (MWord.(+%) start (MWord.of_int loc)) in (**added**)
      let bb = decode_basic_block label prog loc in
      let loc' = loc+bb.bb_size in
      bbs := BBSet.add bb !bbs;
      if (loc' < Array1.dim prog) then
	decode_code_section' (loc+bb.bb_size)
    in
    decode_code_section' 0;
    BBSet.iter (fun bb -> bb.bb_succs <- get_successor bb start) !bbs; (**added**)
    (** after first round, we need to find all **)
    mkCR start !bbs

(** ls_dis_file :  elf_file -> code_region list **)
  let ls_dis_file (elf:elf_file) : code_region list = 
  (* sequentially decode any section that has alloc and executable flags *)
    let decode_one_sec shdr = (** decode_one_sec: shdr -> code_region **)
      if is_code_sec shdr then
	(try
	   let prog = file_read_region elf.fname (MWord.to_int shdr.sh_offset) 
	     (MWord.to_int shdr.sh_size) in
	   let decode = ls_dis_section prog shdr.sh_addr in
	   Some decode
 	 with DF_IllegalInstr l ->
	   F.printf "  Decoding failure at address 0x%a!\n"
	     pp_mword (MWord.(+%) shdr.sh_addr (MWord.of_int l));
	   None)
      else None
    in
    List.filter_map decode_one_sec elf.shdr_tab


 (*********************Devised by Dongrui*****************************)
  (* A recursive traversal CFG-constructing disassembler *)
  (**************************************************)

  (* For each address, we remember its status: whether it's the
     beginning of a basic block, the beginning of an instruction,
     an illegal code address, or unknown *)
  type addrAttr =
    | AA_Bbb of basicblock (* the address is the beginning of a basic block *) 
                           (** which means this basic block has been enterd before; depending**)
    | AA_Bins of basicblock (* the address is the beginning of an instruction,
			       but not the beginning of a basic block; the basic block
			       is the block where the instruction is in *)
                            (** which means the we need split this block since we jump into the middle **)
    | AA_Illegal  (* illegal address to target *)
    | AA_Unknown  (* an unprocessed address *)

  let string_of_addrattr aa = match aa with
    | AA_Bbb _ -> "block start"
    | AA_Bins _ -> "instr start"
    | AA_Illegal -> "illegal"
    | AA_Unknown -> "unknown"

  (* collecting an initial set of start addresses of basic blocks *)
  let collect_block_starts (elf:elf_file) (start:mword) = 
    (* collect all function starts in symbol tables *)
    let process_symtab_entry se = 
      match se.st_type, se.st_shndx with
	| STT_FUNC, SHN_REGULAR _ -> Some (se.st_value, se.st_name)
	| _ -> None 
    in
    let process_symtab st = 
      List.filter_map process_symtab_entry st.symtab_entries in 
    let func_starts = 
      List.concat (List.map process_symtab elf.sym_tabs) in
    (* collect all section starts *)
    let process_one_sec shdr = 
      if is_code_sec shdr then Some (shdr.sh_addr, shdr.sh_name) else None
    in 
    let sec_starts = List.filter_map process_one_sec elf.shdr_tab in 
    let abs_addr_list = 
      (elf.ehdr.e_entry, "_start") :: (List.append sec_starts func_starts) in
    let ret = List.map (fun (abs,lbl) -> (get_relative_addr abs start,lbl)) abs_addr_list in
    eliminate_duplication ret

  let is_func_start elf loc start =
    let block_starts = collect_block_starts elf start in
    List.exists (fun (location, label) -> location = loc) block_starts

  (** split the basic block at loc; any instr whose addr range is not
      less than loc is put into the first half; the old block is mutated
      and it returns a new block with the second half *)
  let split_bb bb loc start =
    let rec split_bb_aux istart instrs =
      match instrs with
	| [] -> ([],[])
	| (pre,ins,len) :: instrs' ->
	  if (istart + len < loc) then
	    let il1,il2 = split_bb_aux (istart+len) instrs' in
	    ((pre,ins,len)::il1, il2)
	  else 
	    ([(pre,ins,len)], instrs')
    in
    let (il1, il2) = split_bb_aux bb.bb_relAddr bb.bb_instrs in
    let bb_new_relAddr = loc in
    let bb_new_label = str_of_mword_flex (MWord.(+%) start (MWord.of_int bb_new_relAddr)) in
    let bb_new = mkBB bb_new_label il2 bb_new_relAddr in
    let () = bb_new.bb_succs <- bb.bb_succs in
    let () = bb_new.bb_inter_succs <- bb.bb_inter_succs in
    let () = bb.bb_succs <- [bb_new.bb_label] in
    let () = bb.bb_inter_succs <- [] in
    let () = bb.bb_instrs <- il1 in
    let () = bb.bb_size <- get_il_size il1 in
    bb_new

  (** find the first conflicting address for bb with 
      start being loc and end being bbEnd *)

  let rec find_first_conflict amap loc bbEnd =
    if loc < bbEnd then
      if amap.(loc) <> AA_Unknown then Some loc (* some conflict at loc *)
      else find_first_conflict amap (loc+1) bbEnd
    else None (* no conflicts *)

  let upd_addr_map_rng amap start size value = 
  (* debug_event  *)
  (*   (fun _ -> "rt_upd_amap",  *)
  (*     [("start", hex_string_of_int start); *)
  (*      ("len", hex_string_of_int size); *)
  (*      ("value", string_of_addrattr value)]); *)
    for loc=start to start+size-1 do amap.(loc) <- value done

  (** update the addr map with info in the new bb *)
  let upd_addr_map_by_bb amap bb =
    List.iter
      (fun (loc,_,_,len) ->
      (* middle of the instruction marked as illegal *)
	upd_addr_map_rng amap loc len AA_Illegal;
	upd_addr_map_rng amap loc 1 (AA_Bins bb))
      (instrs_with_starts bb.bb_relAddr bb.bb_instrs);
    amap.(bb.bb_relAddr) <- AA_Bbb bb

  (** Given a basic block, compute the set of (relative) addresses
      that follow the last instruction in terms of control
      flow. start is the start address of the section; it is
      needed because there may be absolute jumps **)

  let get_successor_addrs (elf:elf_file) (prog:codeBytes) (bb:basicblock) (start:mword) teqcs tf_without_tsig tcs lbl func_pool jbound bbs =
    let followupAddr = bb.bb_relAddr+bb.bb_size in
    let (pre,ins,len) = (List.last bb.bb_instrs) in
    let branch_site = followupAddr - len in
    (* let branch_site_mword = MWord.of_int (followupAddr - len) in *)
    (* let branch_site = MWord.(+%) branch_site_mword start in *)
    let rec handle_indirect_call op =
      let target_name = func_bb_in elf bb start in
      let type_matched_targets =
	if target_name <> "__libc_csu_init" then
	  (* call_t_typesig': ctype list *)
	  let vfps = collect_vfp_with_sinfo elf bb start in
	  let rec match_op_for_tsigs op =
	  match op with
	  | Reg_op reg ->
	    let touched_vfps = List.filter (fun vfp -> stored_in_reg vfp reg branch_site) vfps in
	    if touched_vfps = [] then 
	      (*let another_op = safe_pattern_match bb reg in
	      if another_op <> (Reg_op reg) then
	        match_op_for_tsigs another_op
	      else*)
	      collect_tsig_from_vfps vfps
	    else
	      let raw_typesigs = collect_tsig_from_vfps touched_vfps in
	      let typesigs = eliminate_dup_ctype raw_typesigs in
	      if List.length typesigs = 1 then typesigs
	      else collect_tsig_from_vfps vfps
	  | Address_op addr -> (
	    match addr with 
	    | {addrDisp = addr_big_int; addrBase = None; addrIndex = None} ->
	      (* absolute addresses; for global variables and static local variables *)
	      let memory_vfps = List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps in
	      let touched_vfps = List.filter (fun vfp -> stored_in_addr vfp addr_big_int branch_site) memory_vfps in
	      if touched_vfps = [] then collect_tsig_from_vfps memory_vfps
	      else 
		let raw_typesigs = collect_tsig_from_vfps touched_vfps in
		let typesigs = eliminate_dup_ctype raw_typesigs in
		if List.length typesigs = 1 then typesigs
		else collect_tsig_from_vfps memory_vfps
	    | {addrDisp = addr_big_int; addrBase = Some reg; addrIndex = None} ->
	      (* this situation includes stack variables and possibly other cases; *)
	      (*let () = P.printf "a possible stack operand is matched\n" in
	      let () = P.printf "%s\n" (dw_encoding_of_breg reg) in*)
	      let possible_vfps =
		if MWord.(>=%) (MWord.of_big_int addr_big_int) start then 
		  List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps 
		else
		  if reg = ESP then
		    List.filter (fun vfp -> vfp.vfp_visisrc = VS_param || vfp.vfp_visisrc = VS_local && not (is_vfp_static_local vfp)) vfps 
		  else vfps
	      in
	      let touched_vfps = 
		List.filter (fun vfp -> stored_in_breg vfp reg addr_big_int branch_site) possible_vfps 
	      in
	      if touched_vfps = [] then collect_tsig_from_vfps possible_vfps
	      else 
		let raw_typesigs = collect_tsig_from_vfps touched_vfps in
		let typesigs = eliminate_dup_ctype raw_typesigs in
		if List.length typesigs = 1 then typesigs
		else collect_tsig_from_vfps possible_vfps
	    | {addrDisp = addr_big_int; addrBase = None; addrIndex = Some (scale, reg)} ->
	      (* absolute addresses; for global variables and static local variables *)
	      let possible_vfps =
		if MWord.(>=%) (MWord.of_big_int addr_big_int) start then 
		  List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps 
		else 
		  if reg = ESP && scale = Scale1 then
		    List.filter (fun vfp -> vfp.vfp_visisrc = VS_param || vfp.vfp_visisrc = VS_local && not (is_vfp_static_local vfp)) vfps 
		  else vfps
	      in
	      (*let memory_vfps = List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps in*)
	      let touched_vfps = List.filter (fun vfp -> stored_in_addr vfp addr_big_int branch_site) possible_vfps in
	      if touched_vfps = [] then collect_tsig_from_vfps possible_vfps
	      else 
		let raw_typesigs = collect_tsig_from_vfps touched_vfps in
		let typesigs = eliminate_dup_ctype raw_typesigs in
		if List.length typesigs = 1 then typesigs
		else collect_tsig_from_vfps possible_vfps
	    | {addrDisp = addr_big_int; addrBase = Some regbase; addrIndex = Some (scale, regindex)} ->
	      (* absolute addresses; for global variables and static local variables *)
	      let possible_vfps =
		if MWord.(>=%) (MWord.of_big_int addr_big_int) start then 
		  List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps 
		else vfps
		  (*if regbase = ESP || regbase = EBP || regindex = ESP || regindex = EBP then
		    List.filter (fun vfp -> vfp.vfp_visisrc = VS_param || vfp.vfp_visisrc = VS_local && not (is_vfp_static_local vfp)) vfps 
		  else vfps*)
	      in
	      (*let memory_vfps = List.filter (fun vfp -> vfp.vfp_visisrc = VS_global || is_vfp_static_local vfp) vfps in*)
	      let touched_vfps = List.filter (fun vfp -> stored_in_addr vfp addr_big_int branch_site) possible_vfps in
	      if touched_vfps = [] then collect_tsig_from_vfps possible_vfps
	      else 
		let raw_typesigs = collect_tsig_from_vfps touched_vfps in
		let typesigs = eliminate_dup_ctype raw_typesigs in
		if List.length typesigs = 1 then typesigs
		else collect_tsig_from_vfps possible_vfps
	  )
	  | _ -> 
	    let () = simple_warn "Indirect call operand is not register of address!\n" in
	    collect_tsig_from_vfps vfps
	  in
	  let call_t_typesig' = match_op_for_tsigs op in
	  let all_call_typesig = collect_tsig_from_vfps vfps in
	  let call_t_typesig = 
	    eliminate_dup_ctype (
	      if !ld_on then call_t_typesig'
	      else all_call_typesig
	    )
	  in
	  let () = 
	    (*P.printf "indirect callsite %x type signature number: %d/%d\n" branch_site (List.length call_t_typesig) (List.length all_call_typesig);*)
	    if not (List.mem target_name 
		      ["deregister_tm_clones"; "register_tm_clones"; "frame_dummy"]) 
	    then (
	      if does_bb_icall bb then (
		icall_afp_num := !icall_afp_num + (List.length call_t_typesig);
		icall_vfp_num := !icall_vfp_num + (List.length all_call_typesig);
		icallsite_num := !icallsite_num + 1
	      ) else if does_bb_ijump bb then (
		tcall_afp_num := !tcall_afp_num + (List.length call_t_typesig);
		tcall_vfp_num := !tcall_vfp_num + (List.length all_call_typesig);
		tailcall_num := !tailcall_num + 1
	      )
	    )
	  in
	  let possible_targets_t = t_typesig_match call_t_typesig teqcs in
	  let final_targets = possible_targets_t in
	  final_targets
	else
	  read_init_array_for_targets elf prog bb start
      in
      if !tmatch_on then 
	type_matched_targets @ (List.map fst tf_without_tsig)
      else List.map fst func_pool
    in
    let handleCALL abs op =
      match op with
      | Imm_op imm ->
	if abs then 
	  [get_relative_addr (MWord.of_big_int (unsigned32 imm)) start]
	else [followupAddr + (Big_int.to_int (signed32 imm))]
      | _ -> 
	let succs = handle_indirect_call op in
	succs
    in
    let local_mword_addr mword_addr =
      let func_ranges = collect_func_ranges_from_symtab elf start in
      let visible_fs, visible_fe = 
	List.find (fun (fs, fe) -> fs <= branch_site && branch_site <= fe) func_ranges
      in
      let fs_mword = MWord.(+%) (MWord.of_int visible_fs) start in
      let fe_mword = MWord.(+%) (MWord.of_int visible_fe) start in
      MWord.(<=%) fs_mword mword_addr && MWord.(<=%) mword_addr fe_mword
    in
    let func_mword_addr mword_addr =
      List.exists (fun (faddr, fname) -> mword_addr = (MWord.(+%) (MWord.of_int faddr) start)) func_pool
    in
    let rec ijclassify op = 
      if reladdr_in_plt elf branch_site start then IJ_plt_t
      else if is_jmp_tail_call followupAddr tcs then IJ_function_t
      else match op with
      | Imm_op _ -> (** Impossible Case **)
	raise (Failure "Direct Jump found during IJClassification!")
      | Offset_op off ->
	let sim_addr = {addrDisp = off; addrBase = None; addrIndex = None} in
	ijclassify (Address_op sim_addr)
      | Address_op addr -> (
	(** Note that we assume plt, tail call, jump table, and label are the 
	    only source of indirect jump
	**)
	match addr with
	| {addrDisp = baseAddr; addrBase = _; addrIndex = _} -> 
	  if addrop_in_rodata elf addr then ( 
	    let target_mword = read_codeseg_for_target elf addr start in
	    if local_mword_addr target_mword then IJ_local_t
	    else if func_mword_addr target_mword then IJ_function_t
	    else IJ_unclassified_t
	  ) 
	  else if addrop_in_data elf addr then (
	    let target_mword_option = read_dataseg_for_target elf addr start in
	    if Option.is_none target_mword_option then IJ_unclassified_t
	    else
	      let target_mword = Option.get target_mword_option in
	      if local_mword_addr target_mword then IJ_local_t
	      else if func_mword_addr target_mword then IJ_function_t
	      else IJ_unclassified_t
	  )
	  else IJ_unclassified_t
      )
      | _ -> IJ_unclassified_t
    in
    let rec handle_indirect_jump op = 
      let ijump_type = 
	if !ijclass_on then ijclassify op else IJ_unclassified_t 
      in
      match ijump_type with
      | IJ_plt_t -> (
	match op with
	| Address_op addr ->
	  let dll_linker_option = read_dataseg_for_target elf addr start in
	  if Option.is_none dll_linker_option then [-1], []
	  else
	  let target = get_relative_addr (Option.get dll_linker_option) start in
	  (-1) :: [target], [target]
	| _ -> 
	  raise (Failure "In IJ_plt_t, operand is not address!") 
      )
      | IJ_local_t -> (
	match op with 
	| Address_op addr ->
	  (
	    match addr with 
	    | {addrDisp = baseAddr; addrBase = None; addrIndex = None} -> 
	      if addrop_in_rodata elf addr then 
		let target_mword = read_codeseg_for_target elf addr start in
		let target = get_relative_addr target_mword start in
		[target], [target]
	      else if addrop_in_data elf addr then
		let targets =
		  read_aligned_data_for_labels_from_addr elf prog bb start baseAddr jbound bbs
		in
		targets, targets
	      else
		raise (Failure "In LJ_local_t, direct operand does not point to data sections!")
	    | {addrDisp = baseAddr; addrBase = _; addrIndex = _} -> 
	      if addrop_in_rodata elf addr then
		let targets = 
		  read_aligned_rodata_for_targets_from_addr elf prog bb start baseAddr jbound bbs
		in
		targets, targets
	      else if addrop_in_data elf addr then
		let targets =
		  read_aligned_data_for_labels_from_addr elf prog bb start baseAddr jbound bbs
		in
		targets, targets
	      else 
		raise (Failure "In LJ_local_t, compound operand does not point to data sections!")
	)
	| Offset_op off ->
	  let sim_addr = {addrDisp = off; addrBase = None; addrIndex = None} in
	  if addrop_in_rodata elf sim_addr then
	    let target_mword = read_codeseg_for_target elf sim_addr start in
	    let target = get_relative_addr target_mword start in
	    [target], [target]
	  else if addrop_in_data elf sim_addr then
	    let targets =
	      read_aligned_data_for_labels_from_addr elf prog bb start off jbound bbs
	    in
	    targets, targets
	  else
	    raise (Failure "In LJ_local_t, offset operand does not point to data sections!")
	| _ -> 
	  raise (Failure "In LJ_local_t, operand is register!")
      )
      | IJ_function_t ->
	(handle_indirect_call op, [])
      | IJ_unclassified_t -> (
	let filter_out_funcs target_list =
	  List.filter (fun target -> not (List.mem target (List.map fst func_pool))) target_list
	in
	match op with
	| Reg_op reg ->
	  let real_op_option = 
	    try Some (pattern_match bb reg)
	    with TailCall -> None 
	  in 
	  if Option.is_none real_op_option then
	    handle_indirect_call op, []
	  else (
	    let real_op = Option.get real_op_option in
	    match real_op with 
	    | Reg_op reg -> 
	      let () = 
		P.printf "No pattern for register operand in unclassified category! %x\n" bb.bb_relAddr;
		flush stdout
	      in
	      let ic_targets = handle_indirect_call op in
	      let data_addrs = read_aligned_data_for_all elf prog bb start in
	      let rodata_addrs = read_aligned_rodata_for_all elf prog bb start in 
	      let data_targets = filter_out_funcs data_addrs in
	      let rodata_targets = filter_out_funcs rodata_addrs in
	      let targets = 
		ic_targets @ data_targets @ rodata_targets
	      in
	      targets, data_targets @ rodata_targets	      
	    | _ -> handle_indirect_jump real_op
	  )
	| _ ->
	  let () = 
	    P.printf "No pattern for non-register operand in unclassified category! %x\n" bb.bb_relAddr;
	    flush stdout
	  in
	  let ic_targets = handle_indirect_call op in
	  let data_addrs = read_aligned_data_for_all elf prog bb start in
	  let rodata_addrs = read_aligned_rodata_for_all elf prog bb start in 
	  let data_targets = filter_out_funcs data_addrs in
	  let rodata_targets = filter_out_funcs rodata_addrs in
	  let targets = 
	    ic_targets @ data_targets @ rodata_targets
	  in
	  (targets, data_targets @ rodata_targets) 
      )
    in
    let handleJMP abs op =
      match op with
      | Imm_op imm ->
	  if abs then 
	    [get_relative_addr (MWord.of_big_int (unsigned32 imm)) start],
	    [get_relative_addr (MWord.of_big_int (unsigned32 imm)) start]
	  else 
	    [followupAddr + (Big_int.to_int (signed32 imm))],
	    [followupAddr + (Big_int.to_int (signed32 imm))]
      | _ -> 
	  handle_indirect_jump op
    in
      match ins with
      | CALL (true,abs,op,None) ->
        let call_target = handleCALL abs op in
	let call_target = eliminate_dup_a' call_target in
	(call_target, [followupAddr],[followupAddr]) (* speculative disassembly: assume the
					    address after call is a new beginning *)
      | CALL (_,_,_,_) -> E.s (E.unimp "We do not handle far calls!")
      | HLT | IRET -> ([],[],[]) 
      | Jcc (_,disp) ->
	let succs = [followupAddr; followupAddr + (Big_int.to_int (signed32 disp))] in
	(succs, [], succs)
      | JCXZ b ->
	let succs = [followupAddr; followupAddr + (Big_int.to_int (signed8 b))] in
	(succs, [], succs)
      | JMP (true,abs,op,None) -> 
	let jmp_target, new_target = handleJMP abs op in
	(eliminate_dup_a' jmp_target, [], eliminate_dup_a' new_target)
      | JMP (_,_,_,_) -> E.s (E.unimp "We do not handle far jumps!")
      | RET (_,_) -> ([],[],[]) 
      | _ -> ([followupAddr], [], [followupAddr])

  let dump_amap amap =
    for i=0 to Array.length amap - 1 do
      F.printf "%x:%s@." i (string_of_addrattr amap.(i))
    done

  let str_of_workqueue workq =
    let strout = output_string () in
    Queue.print ~sep:"," (fun op (addr,_) -> Printf.fprintf op "%x" addr) strout workq;
    close_out strout

  (* mark valid code regions according to sections; the rest of the
   addresses are illegal *)
  let mark_valid_code_regions (elf:elf_file) amap start =
    List.iter 
      (fun shdr -> 
	if is_code_sec shdr then
	  (let sec_start = get_relative_addr shdr.sh_addr start in
	   upd_addr_map_rng amap sec_start (MWord.to_int shdr.sh_size) AA_Unknown))
      elf.shdr_tab

  let not_AA_Bbb amap (loc:int) =
    (*P.printf "test addr: %s\n" (str_of_mword_flex (MWord.of_int loc));*)
    try
      match amap.(loc) with
      | AA_Bbb _ -> false
      | _ -> true
    with Invalid_argument _ ->
      let () = P.printf "error addr: %s\n" (str_of_mword_flex (MWord.of_int loc)) in
      false

  (* todo: make processed part of the code section; it's a useful data
     structure to hang on to *)
  let rt_decode (elf:elf_file) (prog:codeBytes)  (start:mword) =
    let size = Array1.dim prog in
    let amap = Array.make size AA_Illegal in
    let workq = Queue.create () in
    (*let return_stack = Stack.create () in*)
    let all_functions = collect_all_functions elf start in (**collect entries**)
    (*let startAddrs = collect_block_starts elf start in *)
    (*P.printf "Symbol: %d\n" (List.length all_functions);*) 
    let init_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".init") elf.shdr_tab) in
  (*let () = P.printf "start:%s; size:%s\n" (str_of_mword_flex data_shdr.sh_addr) (str_of_mword_flex data_shdr.sh_size) in*)
    let init_start = init_shdr.sh_addr in
    let fini_shdr = List.hd (List.filter (fun shdr -> shdr.sh_name = ".fini") elf.shdr_tab) in
  (*let () = P.printf "start:%s; size:%s\n" (str_of_mword_flex data_shdr.sh_addr) (str_of_mword_flex data_shdr.sh_size) in*)
    let fini_start = fini_shdr.sh_addr in
    let fini_size = fini_shdr.sh_size in
    let code_sec_lopc = get_relative_addr init_start start in
    let code_sec_hipc = get_relative_addr (MWord.(+%) fini_start fini_size) start in
    List.iter (fun item -> let new_task = mkDT "" "" item None in
			   Queue.add new_task workq) all_functions;
    mark_valid_code_regions elf amap start;
    (*let () = 
      print_string "All possible functions in symbol table:\n";
      List.iter (fun (abs,lbl) -> print_string lbl; print_string "\n";) all_functions
    in*)
    (*let taken_entries = collect_possible_entries elf in*)
    (*let function_pool = all_functions in*)
    let function_pool = 
      if not !rel_on then all_functions
      else 
	let taken_entries = collect_possible_entries elf in
	(*let () = 
	  print_string "All possible entries in relocation information:\n";
	  List.iter (fun ent -> print_string ent; print_string "\n";) possible_entries
	  in*)
	let static_funcs = collect_static_funcs elf in
	let possible_entries = eliminate_dup_a' (static_funcs @ taken_entries) in
	let all_taken_funcs = select_taken_funcs possible_entries all_functions in
	(*let () = 
	  print_string "All taken functions::\n";
	  List.iter (fun (abs,lbl) -> print_string lbl; print_string "\n";) all_taken_funcs
	  in*)
	all_taken_funcs
    in
    (*P.printf "+ RelocaInfo: %d\n" (List.length function_pool);*)
    let tcs = collect_tail_call_site elf start in
    let goto_labels = collect_labels elf start in
    let fsig = collect_funcsig_list elf start in
    let teqcs = collect_teqcs elf function_pool start fsig in
    (* There could be functions that do not have type information in DBG; especially for DLL 
       functions *)
    let tf_without_tsig = List.filter (fun func -> not (in_teqcs func teqcs)) function_pool in
    (*let () = dump_teqcs teqcs in*)
    (*let compiler, olevel = decide_compiler_and_olevel elf in*)
    (*let () = P.printf "Compiler is %s and optimization level is %d\n" compiler olevel in*)

    let rec legitimate target_list =
      match target_list with
      | [] -> []
      | h :: t ->
	if (h < code_sec_lopc) || (h > code_sec_hipc) (*|| (amap.(h) = AA_Illegal)*) then legitimate t
	else h :: (legitimate t)
    in

    let count_target = 100 in
    let count = ref 0 in
    let bbs = ref (BBSet.empty) in
    while (not (Queue.is_empty workq)) do
      count := !count + 1 ;
      if !count = 1 then (
	let progressMsg = 
	  P.sprintf "Before this %d rounds, the Queue size is: %d" count_target (Queue.length workq)
	in
	progress_event progressMsg
      );
      (*print_string "Take one out of the queue to work...\n"; flush stdout;*)
      let dt = Queue.take workq in
      let loc = dt.dt_target in
      let lbl = dt.dt_target_symbol_label in
      let jump_bound = dt.dt_jump_bound in
      (*let set_pred dt bb =
	if dt.dt_pred <> "" then 
	  if dt.dt_replace_pred = "" || not (List.mem dt.dt_replace_pred bb.bb_preds) then
	    bb.bb_preds <- dt.dt_pred :: bb.bb_preds
	  else 
	    let modified_preds = List.map (fun pred -> if pred = dt.dt_replace_pred then dt.dt_pred else pred) bb.bb_preds in
	    bb.bb_preds <- modified_preds
	else ()
      in*)	    
      (match amap.(loc) with
	| AA_Bbb bb -> () (*set_pred dt bb*)
		
	| AA_Bins bb ->
	  (*print_string "A begining of an instruction is met, so split it...\n";
	  flush stdout;*)
	  (* The address is the middle of a block; need to split the block *)
	  (*P.printf "AA_Bins: %s\n\n" bb.bb_label;*)
	  let bb_new = split_bb bb loc start in
	  bb_new.bb_symbol_label <- lbl;
	  assert (bb.bb_relAddr + bb.bb_size = loc);
	  bbs := BBSet.add bb_new !bbs;
	  upd_addr_map_by_bb amap bb_new

	  (*P.printf "Split new: %s\n\n" bb_new.bb_label;*)
	| AA_Illegal -> 
	  (*print_string "An illegal location is met, so raise exception...\n";
	  flush stdout;*)
	  P.printf "DF_Overlap error happens at %x due to predecessor: %s\n" loc dt.dt_pred;
	  flush stdout;
	  raise (DF_Overlap (0,0)) 
	  (* todo: fix; report the two basic blocks that violate the overlapping requirement *)


	| AA_Unknown -> 
	  (*print_string "An unknown location is met, so work on it...\n";
	  flush stdout;*)
	  (*print_string "AA_Unknown: \n";*)
	  (*print_string "Decoding a basic block...\n";
	  flush stdout;*)
	  let bb = decode_basic_block lbl prog loc in
          let label = str_of_mword_flex (MWord.(+%) start (MWord.of_int bb.bb_relAddr)) in
	  let () = bb.bb_label <- label in
	  let () = bb.bb_symbol_label <- lbl in
	  (* check if the new bb is consistent with existing basic blocks:
	     if does not overlap with other parsed blocks or interduce instruction
	     boundaries that are the middle of parsed instructions *)
	  (*let () =
	    print_string "Looking for successors...\n";
	    flush stdout
	  in*)
	  let succs, inter_succs, new_targets = get_successor_addrs elf prog bb start teqcs tf_without_tsig tcs goto_labels function_pool jump_bound !bbs in
	  (*let legal_succs = legitimate succs in
	  let legal_inter_succs = legitimate inter_succs in*)
	  let legal_new_targets = legitimate new_targets in
	  let () = bb.bb_succs <- List.map (fun i -> if i <> (-1) then str_of_mword_flex (MWord.(+%) start (MWord.of_int i)) else str_of_mword_flex (MWord.of_int (-1))) succs in
	  let () = bb.bb_inter_succs <- List.map (fun i -> str_of_mword_flex (MWord.(+%) start (MWord.of_int i)) ) inter_succs in
	  (*let () = print_string "BB:" in
	  let () = print_string bb.bb_label; print_string "\n" in*)
	  let next_bound = if does_bb_jcc bb then search_max_bound bb else None in
	  let add_new_task addr =
	    let new_task = mkDT bb.bb_label "" (addr,"") next_bound in
	    Queue.add new_task workq
	  in
	  let () = (match find_first_conflict amap loc (loc+bb.bb_size) with
	    | None -> List.iter (add_new_task) legal_new_targets;
	    | Some conflict -> (** if conflicted, there is no need for adding successors onto the queue again **)
	      (*print_string "This block contains another existing block, so separate them...\n";
	      flush stdout;*)
	      match amap.(conflict) with 
	      | AA_Bbb bb_old -> 
		let _ = split_bb bb conflict start in
		if (bb.bb_relAddr + bb.bb_size <> conflict) then (
		  P.printf "bb_relAddr:%x; bb_size:%x; conflict:%x\n" bb.bb_relAddr bb.bb_size conflict;
		  print_bb stdout bb start;
		  print_bb stdout bb_old start;
		  P.printf "DF_Overlap exception happens at location %x; the conflict place is %x\n" loc conflict; 
		  flush stdout;
		  raise (DF_Overlap (loc, conflict))
		) (* fix ; why will this happen? when decoding wrong bytes happens*)
	      | _ -> assert false ) (* impossible case *)
	  in
	  (*print_string "One round is done, now adding legitimate successors to the work queue...\n";
	  flush stdout;*)
	  (* adding legitimate successor addreses to the worklist *)
	  bbs := BBSet.add bb !bbs;
	  upd_addr_map_by_bb amap bb;
      );
      if !count = count_target then (
	let progressMsg =
	  P.sprintf "After this %d rounds, the Queue size becomes: %d" count_target (Queue.length workq)
	in
	count := 0;
	progress_event progressMsg
      );
    done;
    if Queue.is_empty workq && !count <> 0 then (
      let progressMsg = 
	P.sprintf "Queue becomes empty after %d rounds!\n" !count
      in
      progress_event progressMsg
    );
    (*
    let all_function_pointers = collect_all_visible_function_pointers elf start in
    let avg_allfp = float_of_int (List.length all_function_pointers) in
    (*let avg_allfp = 
      if !icallsite_num <> 0 then
	(float_of_int !allfp_num) /. (float_of_int !icallsite_num)
      else 0.0
    in*)
    let avg_icall_visfp =
      if !icallsite_num <> 0 then
	(float_of_int !icall_vfp_num) /. (float_of_int !icallsite_num)
      else 0.0
    in
    let avg_icall_actfp =
      if !icallsite_num <> 0 then
	(float_of_int !icall_afp_num) /. (float_of_int !icallsite_num)
      else 0.0
    in
    let icall_improve1 = 
      if avg_allfp > 1.0 then
        (avg_allfp -. avg_icall_visfp) /. (avg_allfp -. 1.0)
      else 0.0
    in
    let icall_improve2 = 
      if avg_icall_visfp > 1.0 then
	(avg_icall_visfp -. avg_icall_actfp) /. (avg_icall_visfp -. 1.0)
      else 0.0
    in
    let icall_total_improve =
      if avg_allfp > 1.0 then
	(avg_allfp -. avg_icall_actfp) /. (avg_allfp -. 1.0)
      else 0.0
    in
    let avg_tcall_visfp =
      if !tailcall_num <> 0 then
	(float_of_int !tcall_vfp_num) /. (float_of_int !tailcall_num)
      else 0.0
    in
    let avg_tcall_actfp =
      if !tailcall_num <> 0 then
	(float_of_int !tcall_afp_num) /. (float_of_int !tailcall_num)
      else 0.0
    in
    let tcall_improve1 = 
      if avg_allfp > 0.0 then
        (avg_allfp -. avg_tcall_visfp) /. avg_allfp
      else 0.0
    in
    let tcall_improve2 = 
      if avg_tcall_visfp > 0.0 then
	(avg_tcall_visfp -. avg_tcall_actfp) /. avg_tcall_visfp
      else 0.0
    in
    let tcall_total_improve =
      if avg_allfp > 0.0 then
	(avg_allfp -. avg_tcall_actfp) /. avg_allfp
      else 0.0
    in
    P.printf "Indirect callsites: %d\nAverage AllFP: %f\nImprovement#1: %f\nAverage VisFP: %f\nImprovement#2: %f\nAverage ActFP: %f\nTotal Improvement: %f\n" 
      !icallsite_num 
      avg_allfp 
      icall_improve1 
      avg_icall_visfp 
      icall_improve2 
      avg_icall_actfp 
      icall_total_improve;
    P.printf "Indirect tail callsites: %d\nAverage AllFP: %f\nImprovement#1: %f\nAverage VisFP: %f\nImprovement#2: %f\nAverage ActFP: %f\nTotal Improvement: %f\n" 
      !tailcall_num
      avg_allfp 
      tcall_improve1 
      avg_tcall_visfp 
      tcall_improve2 
      avg_tcall_actfp 
      tcall_total_improve;
    *)
    flush stdout;
    mkCR start !bbs
      
  let rt_dis_cfg (elf:elf_file) =
  (* decode one segment; it decodes simultaneously all the
     segment's sections; we use simultaneous decoding here
     because sections may call one another. *)
    let decode_one_seg phdr =
      if is_code_seg phdr then
	(try
	   let prog = file_read_region elf.fname (MWord.to_int phdr.p_offset) 
	     (MWord.to_int phdr.p_filesz) in
	   Some (rt_decode elf prog phdr.p_vaddr)
 	 with DF_IllegalInstr l ->
	   F.printf "  Decoding failure at address 0x%a!\n"
	     pp_mword (MWord.(+%) phdr.p_vaddr (MWord.of_int l));
	   None)
      else None
    in
    List.filter_map decode_one_seg elf.phdr_tab

  (**************************************************)
  (* Exhaustive decoding: decoding from every byte *)
  (**************************************************)

  (* Decide if an instruction is safe; safety defined as those instructions
     that do not change segment registers *)
  let safe_instr pre ins =
    match pre.seg_override with
      | Some _ -> false (* no segment override *)
      | None ->
	match ins with
	  | MOVSR (false, _, _)
	  | POPSR _
	  | CALL (false, _, _ ,_)
	  | JMP (false, _, _, _)
	  | RET (false, _)
	  | LDS (_, _)
	  | LES (_,_)
	  | LFS (_,_)
	  | LGS (_,_)
	  | LSS (_,_) -> false
	  | _ -> true

  (* right now; it only reports statistics and doesn't return results *)
  let exhaustive_decode (prog:codeBytes) start =
    let unsafe = ref 0 in
    let undecodable = ref 0 in
    let rec exhaustive_decode' prog start loc =
      (try
	 let (pre,ins,len) = decode_instr prog loc in
	 if not (safe_instr pre ins) then
	   (F.printf "UNSAFE! "; unsafe := !unsafe + 1);
	 F.printf "@[%a: %2d %a@]@."
	   pp_mword_flex (MWord.(+%) start  (MWord.of_int loc))
	   len pp_prefix_instr (pre,ins) 
       with DF_IllegalInstr _ ->
	 (F.printf "@[%a: undecodable instruction@]@."
	    pp_mword_flex (MWord.(+%) start (MWord.of_int loc));
	  undecodable := !undecodable + 1));
      if (loc < Array1.dim prog) then
	exhaustive_decode' prog start (loc+1)
    in
    exhaustive_decode' prog start 0

end

(** The maximum number of bytes used by an instructions *)
let maxDecodingLen = 15

module Online_Deriv_Decoder : DECODER = struct
  open Parser

  let build_ini_decoder_state () =
    initial_naive_parser_state Decode.instruction_grammar

  let ini_state = build_ini_decoder_state () 

  let init () = ()

  let rec decode_instr' (n:int) (prog:codeBytes) (loc:int)
      (nps:naiveParserState) =
    if (n=0 || loc >= Array1.dim prog)
    then raise (DF_IllegalInstr (loc- (maxDecodingLen - n)))
    else match naive_parse_token nps (Big_int.of_int prog.{loc}) with
      | (ps', []) -> decode_instr' (n-1) prog (loc+1) ps'
      | (_, v::_) -> (v,loc)

  (** decode one instruction. Use 15 as the upper bound of number of bytes
      to decode. Return the instruction and its length. If the decode fails,
      raise an exception *)
  let decode_instr (prog:codeBytes) (loc:int) =
    let ((pre,ins),loc') = Obj.magic (decode_instr' maxDecodingLen prog loc ini_state) in
    (pre, ins, loc'-loc+1)
  
end

module Table_Decoder : DECODER = struct

  let get_dfa r  =
    let marshal_on = !m_on in
    let demarshal_on = !dm_on in
    let mres_file = "coq_DFA" in
    if marshal_on && not demarshal_on then 
      let dfa = Parser.build_dfa r in
      let moc = open_out_bin mres_file in
      let () = 
	let pstr = P.sprintf "Marshalling to file %s..." mres_file in
	progress_event pstr;
	Marshal.output moc dfa;
	close_out moc;
	progress_event "Marshalling finished!"
      in
      dfa
    else if marshal_on && demarshal_on then
      let dfa = Parser.build_dfa r in
      let moc = open_out_bin mres_file in
      let () = 
	let pstr = P.sprintf "Marshalling to file %s..." mres_file in
	progress_event pstr; 
	Marshal.output moc dfa;
	close_out moc;
	progress_event "Marshalling finished!\nStart demarshalling to test..."
      in
      let mic = open_in_bin mres_file in
      let demarshaled_value = (Marshal.input mic : Parser.coq_DFA) in
      let success = 
	Big.eq (Parser.dfa_num_states r demarshaled_value) (Parser.dfa_num_states r dfa) in
      let () =
	close_in mic;
	if success then
	  progress_event "Marshalling and demarshalling is correct on coq_DFA!"
	else progress_event "Error: unknown for now! Debug!"
      in
      dfa
    else if not marshal_on && demarshal_on then
      (** this case does the read from file operation **)
      let mic = open_in_bin mres_file in
      let () = 
	let pstr = P.sprintf "Demarshalling from file %s..." mres_file in
	progress_event pstr
      in
      let dfa = (Marshal.input mic : Parser.coq_DFA) in
      let () = 
	progress_event "Demarshalling finished!";
	close_in mic
      in
      dfa
    else 
      Parser.build_dfa r 

  let build_ini_decoder_state () : Parser.instParserState =
    Parser.initial_parser_state' get_dfa Decode.instruction_grammar

  (* let build_ini_decoder_state () : Parser.instParserState = *)
    (* let f1 = snd (RESet.singleton_xform r) in *)
    (* { Parser.dfa_ps = get_dfa r; Parser.row_ps = Big.zero;  *)
    (*   Parser.fixup_ps = Basics.compose (Obj.magic List0.map f) (Xform.xinterp f1) } *)
    
  (* let build_ini_decoder_state () =  *)
  (*   initial_parser_state Decode.instruction_grammar *)

  let pp_bool fmt b = 
    if b then F.fprintf fmt "true  " 
    else F.fprintf fmt "false " 
(*
   let rec size_of_uform f = 
     match uf with 
       | Uid -> 1 
       | Uzero _ -> 1 
       | Ucomp (u1,u2)  
       | Upair (u1,u2) 
       | Umatch (u1,u2) 
       | Ucons (u1,u2) 
   	-> 1 + size_of_uform u1 + size_of_uform u2 
       | Uchar _ -> 1 
       | Uunit -> 1 
       | Uempty _ -> 1 
       (\* | Ufst | Usnd -> 1 *\) 
       | Ufst _ | Usnd _ -> 1 
       | Uinl _ -> 1 
       | Uinr _ -> 1 
       | Umap u -> 1 + size_of_uform u 

   let pp_entry fmt (ent:uentry_t) = 
     F.fprintf fmt "(%d,%d) "  
       (Big_int.int_of_big_int ent.unext_state) 
       (size_of_uform ent.next_uform) 

   let rec dump_transition_tbl n t =  
     try 
       let row = List.nth t n in 
       F.printf "Row %d:\n" n; 
       F.printf "%a\n" (pp_list pp_entry) row.row_entries; (* row : transition_t*)
       dump_transition_tbl (n+1) t 
     with _ -> () 
*)
    
  let dump_dfa d =
    (* let oc = open_out_bin "dfa" in *)
    (*   Marshal.to_channel oc d [Compat_32;]; *)
    (*   flush oc; *)
    (*   close_out oc *)
    L.unregister_formatter "progress";
    L.register_formatter "dbg" L.dbg_formatter;
    info_event 
      (fun _ ->
        "DFA", 
        [("dfa_num_states", Big_int.string_of_big_int d.Parser.dfa_num_states)]);
    L.unregister_formatter "dbg";
    L.register_formatter "progress" progress_formatter
    (* F.printf "dfa_num_states: %d \n" *)
    (*   (Big_int.int_of_big_int d.dfa_num_states) *)
  (*  F.printf "dfa_states:\n";
      dump_wf_states d.dfa_states;
    F.printf "dfa_transition:\n"; 
      dump_transitions_t 0 d.dfa_transition*)
    (* (\* F.printf "%a\n" (pp_list pp_bool) (d.udfa_accepts) *\) *)

  let get_initial_state = 
    let opt_ini_state = ref None in
    fun () ->
      match !opt_ini_state with
	| None ->
           let is = Stats.time "decode_build_ini_state" build_ini_decoder_state () in
	   dump_dfa is.Parser.dfa_ps;
	   opt_ini_state := Some is;
	   is
	| Some is -> is

  let init () = let _ = get_initial_state () in ()
  
  let rec form_bytes_lst n k =
    if n <= 0 then []
    else if n = 1 then [k]
    else k :: (form_bytes_lst (n-1) (k+1))

  let form_abs_addr (data:codeBytes) (loc:int) =
    let a = str_of_mword_one_byte (MWord.of_int data.{loc}) in
    let b = str_of_mword_one_byte (MWord.of_int data.{loc+1}) in
    let c = str_of_mword_one_byte (MWord.of_int data.{loc+2}) in
    let d = str_of_mword_flex (MWord.of_int data.{loc+3}) in
    (*let () = P.printf "Before of_string: 0x%s%s%s%s\n" d c b a in*)
    MWord.of_string (P.sprintf "0x%s%s%s%s" d c b a) 

  let rec decode_instr' (n:int) (prog:codeBytes) (loc:int)
      (ps:Parser.instParserState) =
    if (n=0 || loc >= Array1.dim prog)
    then 
      let () = P.printf "The violation situation: n=%d, loc=%d\n" n loc in
      let byte_loc_lst = form_bytes_lst maxDecodingLen (loc-maxDecodingLen) in
      let bytes_lst = List.map (fun i -> str_of_mword_one_byte (MWord.of_int prog.{i})) byte_loc_lst in
      let () =
	print_string "The decode failure is caused by following bytes:\n";
	List.iter (fun byte -> P.printf "%s " byte) bytes_lst;
	print_string "\n";
      in
      raise (DF_IllegalInstr (loc - (maxDecodingLen - n)))
    else match Parser.parse_token ps (Big_int.of_int (prog.{loc})) with
      | (ps', []) -> decode_instr' (n-1) prog (loc+1) ps'
      | (_, v::_) -> (v,loc)

  let decode_instr prog loc =
    let ((pre,ins),loc') = 
      Obj.magic (decode_instr' maxDecodingLen prog loc (get_initial_state ()))
    in (pre,ins,loc'-loc+1)
end

(* vector-based table decoder *)
(* commented out for now, but this should be resurrected at some point 
module VTable_Decoder : DECODER = struct
  open Parser

  let build_ini_decoder_state () = 
    vinitial_parser_state Decode.instruction_grammar

  let dump_dfa d =
    F.printf "num of states=%d \n"
      (Big_int.int_of_big_int d.vdfa_num_states)
    (* F.printf "transition table:\n"; *)
    (* dump_transition_tbl 0 d.udfa_transition *)
    (* (\* F.printf "%a\n" (pp_list pp_bool) (d.udfa_accepts) *\) *)

  let get_initial_state = 
    let opt_ini_state = ref None in
    fun () ->
      match !opt_ini_state with
	| None ->
	  let is = Stats.time "decode_build_vtable" build_ini_decoder_state () in
	  dump_dfa is.vdfa_ps;
	  opt_ini_state := Some is;
	  is
	| Some is -> is

  let init () = let _ = get_initial_state () in ()

  let rec decode_instr' (n:int) (prog:codeBytes) (loc:int)
      (ps:vinstParserState) =
    if (n=0 || loc >= Array1.dim prog)
    then raise (DF_IllegalInstr (loc - (maxDecodingLen - n)))
    else match vparse_token ps (Big_int.of_int (prog.{loc})) with
      | (ps', []) -> decode_instr' (n-1) prog (loc+1) ps'
      | (ps', vs0) ->
      	(* match (List0.flat_map ps'.vfixup_ps vs0) with *)
      	match (flat_map' ps'.vfixup_ps vs0) with
      	  | [] -> decode_instr' (n-1) prog (loc+1) ps'
      	  | v :: _ -> (v, loc)

  let decode_instr prog loc =
    let ((pre,ins),loc') =
      Obj.magic (decode_instr' maxDecodingLen prog loc (get_initial_state ()))
    in (pre,ins,loc'-loc+1)

  let decode_instr prog loc =
    let ps = get_initial_state () in
    let vdfa = ps.vdfa_ps in
    let new_vparse_token (vrow: Big.big_int) (vfixup: re_set_fixfn) tk =
      let row = Vector.get vdfa.vdfa_transition vrow in
      let e = Vector.get row.vrow_entries tk in
      let next_i = e.next_state in
      let next_fixup = coerce_dom vfixup in
      let g = compose_flat_map next_fixup e.next_xform in
      let row' = Vector.get vdfa.vdfa_transition next_i in
      let vs0 = coerce row'.vrow_nils in
      (next_i, g, vs0)
    in
    let rec decode_instr' (n:int) (loc:int)
  	(vrow: Big.big_int) (vfixup: re_set_fixfn) =
      if (n=0 || loc >= Array1.dim prog)
      then raise (DF_IllegalInstr (loc - (maxDecodingLen - n)))
      else match new_vparse_token vrow vfixup (Big_int.of_int (prog.{loc})) with
  	| (vrow', vfixup', []) ->
  	  decode_instr' (n-1) (loc+1) vrow' vfixup'
  	| (vrow', vfixup', vs0) ->
      	  (* match (List0.flat_map ps'.vfixup_ps vs0) with *)
      	  match (flat_map' vfixup' vs0) with
      	    | [] -> decode_instr' (n-1) (loc+1) vrow' vfixup'
      	    | v :: _ -> (v, loc)
    in
    let ((pre,ins),loc') =
      Obj.magic (decode_instr' maxDecodingLen loc ps.vrow_ps ps.vfixup_ps)
    in (pre,ins,loc'-loc+1)
end
*)

(* commented out for now, but this can be resurrected later
module Parsing_Combinator_Decoder : DECODER = struct
  let init () = ()

  (* let build_ini_decoder_state () = *)
  (*   initial_naive_parser_state Decode.instruction_grammar *)

  (* let ini_state = *)
  (*   Stats.reset Stats.SoftwareTimer; *)
  (*   let is = Stats.time "build_initial_state" build_ini_decoder_state () in *)
  (*     Stats.print Pervasives.stdout "Timings:\n"; *)
  (*     is *)

  (* let rec decode_instr' (n:int) (prog:codeBytes) (loc:int) *)
  (*     (nps:naiveParserState) = *)
  (*   if (n=0 or loc >= Array1.dim prog) *)
  (*   then raise (DF_IllegalInstr (loc- (maxDecodingLen - n))) *)
  (*   else match naive_parse_token nps (int8_of_int prog.{loc}) with *)
  (*     | (ps', []) -> decode_instr' (n-1) prog (loc+1) ps' *)
  (*     | (_, v::_) -> (v,loc) *)

  open List

  (** decode one instruction. Use 15 as the upper bound of number of bytes
      to decode. Return the instruction and its length. If the decode fails,
      raise an exception *)
  let decode_instr (prog:codeBytes) (loc:int) =
    (* create a list of at most maxDecodingLen bytes from prog*)
    let rec get_list (l:int) = 
	if (l-loc+1 > maxDecodingLen || l >= Array1.dim prog) then []
	else (Big_int.of_int prog.{l}) :: (get_list (l+1))
    in let bl = get_list loc in
    let cl = List.flatten (List.map Grammar.token_id_to_chars bl)  in
    match Combinator.instruction_parser cl with
      | [] -> raise (DF_IllegalInstr loc)
      | (cl', (pre,ins)) :: _ -> 
	if (List.length cl' mod 8 <> 0) then raise (DF_IllegalInstr loc)
	else
	(* F.printf "listlen=%d\t length(cl)=%d \t length(cl')=%d\n" listlen (List.length cl) (List.length cl'); *)
	  (Obj.magic pre, Obj.magic ins, (List.length bl) - (List.length cl'/8))

  (* let decode_instr (prog:codeBytes) (loc:int) = *)
  (*   F.printf "loc=%d\n" loc; *)
    
  (*   (\* create a list of at most 15 bytes *\) *)
  (*   let rec get_list (l:int) =  *)
  (*     if l = loc then [] *)
  (*     else (Big_int.of_int prog.{l}) :: get_list (l-1) *)
  (*   in *)
  (*   let endloc =  *)
  (*     if loc+maxDecodingLen-1 < Array1.dim prog then loc+maxDecodingLen-1 *)
  (*     else Array1.dim prog - 1 *)
  (*   in *)
  (*   let listlen = endloc - loc + 1 in *)
  (*   let cl = List.flatten *)
  (*     (List.map Grammar.token_id_to_chars (get_list endloc))  in *)
  (*   match Combinator.instruction_parser cl with *)
  (*     | [] -> raise (DF_IllegalInstr loc) *)
  (*     | (cl', (pre,ins)) :: _ ->  *)
  (* 	if (List.length cl' mod 8 <> 0) then raise (DF_IllegalInstr loc) *)
  (* 	else *)
  (* 	(\* F.printf "listlen=%d\t length(cl)=%d \t length(cl')=%d\n" listlen (List.length cl) (List.length cl'); *\) *)
  (* 	  (Obj.magic pre, Obj.magic ins, listlen - (List.length cl'/8)) *)

end
 *)

(** Pretty printers for basic blocks and code sections *)
let pp_basic_block fmt (bb,crStart) =
  F.pp_open_vbox fmt 2;
  (if bb.bb_label = "" then F.fprintf fmt "@,"
   else F.fprintf fmt "%s:@," bb.bb_label);
  List.iter
    (fun (loc,pre,ins,len) ->
      F.fprintf fmt "%a: %2d %a@,"
  	pp_mword_flex (MWord.(+%) crStart (MWord.of_int loc))
      	len pp_prefix_instr (pre,ins))
    (instrs_with_starts bb.bb_relAddr bb.bb_instrs);
  F.pp_close_box fmt ()

let pp_code_region fmt cs =
  BBSet.iter 
    (fun bb -> F.fprintf fmt "%a@."
      pp_basic_block (bb, cs.startAddr))
    cs.bbs


(** Decide which disassembler to use; should add a global option for this *)

(* Disassembler based on online derivative parsing *)
(* module OD_Dis = Gen_Disassembler(Online_Deriv_Decoder) *)
(* module Dis = OD_Dis *)

(* Disassembler based on table-based derivative parsing *)
module Tbl_Dis = Gen_Disassembler(Table_Decoder)
module Dis = Tbl_Dis

(* Disassembler based on table-based derivative parsing using vectors instead of lists*)
(* module VTbl_Dis = Gen_Disassembler(VTable_Decoder)*)
(* module Dis = VTbl_Dis*)

(* module PC_Dis = Gen_Disassembler(Parsing_Combinator_Decoder) *)
(* module Dis = PC_Dis *)

(** The following code is a tester for the disasembler *)

let _ = 
  E.colorFlag := true;
  L.init [("elfparsing", L.NONE); ("disassembler", L.DEBUG); ("Progress", L.NONE)] 
    L.dbg_formatter

(* simple testing of the linear-sweep disassembler *)

(*
 80483fd:	55                   	push   ebp
 80483fe:	89 e5                	mov    ebp,esp
 8048400:	83 e4 f0             	and    esp,0xfffffff0
 8048403:	83 ec 20             	sub    esp,0x20
 8048406:	c7 04 24 04 00 00 00 	mov    DWORD PTR [esp],0x4
 804840d:	e8 ba ff ff ff       	call   80483cc <rec>
 8048412:	89 44 24 1c          	mov    DWORD PTR [esp+0x1c],eax
 8048416:	c7 04 24 04 00 00 00 	mov    DWORD PTR [esp],0x4
 804841d:	e8 72 ff ff ff       	call   8048394 <iter>
 8048422:	89 44 24 18          	mov    DWORD PTR [esp+0x18],eax
 8048426:	e8 c8 ff ff ff       	call   80483f3 <fin>
 804842b:	b8 00 00 00 00       	mov    eax,0x0
 8048430:	c9                   	leave  
 8048431:	c3                   	ret    
 8048432:	90                   	nop
*)

let bytes : int array =
  [| 0x55;
     0x89; 0xe5;
     0x83; 0xe4; 0xf0;
     0x83; 0xec; 0x20;
     0xc7; 0x04; 0x24; 0x04; 0x00; 0x00; 0x00;
     0xe8; 0xba; 0xff; 0xff; 0xff;
     0x89; 0x44; 0x24; 0x1c;
     0xc7; 0x04; 0x24; 0x04; 0x00; 0x00; 0x00;
     0xe8; 0x72; 0xff; 0xff; 0xff;
     0x89; 0x44; 0x24; 0x18;
     0xe8; 0xc8; 0xff; 0xff; 0xff;
     0xb8; 0x00; 0x00; 0x00; 0x00;
     0xc9;
     0xc3;
     0x90; 
     0x89; 0x3c; 0x24;
     0xe8; 0x20; 0x1e; 0xef; 0xff; 
     0xe9; 0x87; 0x00; 0x00; 0x00;
  |]

(* let bytes : int array = *)
(*   [| 0x55;  *)
(*      0x89; 0xe5; *)
(*      0x83; 0xe4; 0xf0; *)
(*      0x83; 0xec; 0x20; *)
(*      0xc7; 0x04; 0x24; 0x04; 0x00; 0x00; 0x00; *)
(*      0xe8; 0xba; 0xff; 0xff; 0xff; *)
(*   |] *)


(* let bytes : int array = *)
(*   [| 0xd9; 0xf8; *)
(*      0xdf; 0x94; 0x24; 0x8a; 0x00; 0x00; 0x00; *)
(*      0x99; *)
(*      0xdf; 0x54; 0x24; 0x02; *)
(*      0xdf; 0x10; *)
(*      0x0f; 0xae; 0x9c; 0x24; 0x80; 0x00; 0x00; *)
(*      0x66; 0x0f; 0x1f; 0x44; 0x00; 0x00 *)
(*  |] *)

let prog = Array1.of_array int8_unsigned c_layout bytes

let iterate_instrs (f: X86Syntax.prefix -> X86Syntax.instr -> mword -> unit)
    (prog:codeBytes) (start:mword) =
  let rec iterate_one_instr (loc:int) =
    let (pre,ins,len) = Dis.decode_instr prog loc in
    f pre ins (MWord.(+%) start (MWord.of_int loc));
    if (loc+len < Array1.dim prog) then iterate_one_instr (loc + len)
  in iterate_one_instr 0

(** Linear disassemble a file given its name*)
let ls_disassemble_fn (fname: string) =
  try
    let elf = read_elf_file fname in
    pp_elf_file F.std_formatter elf;
    F.printf "\n";
    let crs = Stats.time ("decode_file_"^fname) Dis.ls_dis_file elf in
    List.iter (fun cr ->  F.printf "%a" pp_code_region cr) crs with
    | Invalid_elf_file s -> F.printf "Warning: %s is an invalid elf file: %s\n" fname s
    | No_more_input ->
      F.printf "Warning: %s is an invalid elf file: no more input\n" fname

(* let sourceFiles = ref [] *)

(* let test_encode_decode_codeBytes = iterate_instrs test_encode_decode_instr *)

(* Testing the encode-decode on an elf file *)

(* let test_encode_decode_file (fname: string) = *)
(*   let elf = read_elf_file fname in *)
(*   let test_one_sec shdr = *)
(*     if is_code_sec shdr then *)
(*        try *)
(* 	  let prog = file_read_region elf.fname (MWord.to_int shdr.sh_offset) *)
(* 	    (MWord.to_int shdr.sh_size) in *)
(* 	  test_encode_decode_codeBytes prog shdr.sh_addr *)
(*  	with DF_IllegalInstr l -> *)
(* 	  F.printf "  Decoding failure at address 0x%a!\n" *)
(* 	    pp_mword (MWord.(+%) shdr.sh_addr (MWord.of_int l)) *)
(*   in *)
(*   F.printf "Start processing file %s" fname; F.print_newline (); (\* flush *\) *)
(*   List.iter test_one_sec elf.shdr_tab; *)
(*   F.printf "Finish processing file %s\n" fname; F.print_newline () *)

(* code for testing the parsing combinator approach *)

(* open Random *)
(* let _ = Random.self_init () *)

(* (\* randomly generate an array of length n; each byte is either  *)
(*    0xff or 0xfe *\) *)
(* let gen_random_bl (n:int) = *)
(*   let rec helper (i:int) = *)
(*     let b = if Random.bool () then 0xff else 0xfe in *)
(*     if (i=n) then [] *)
(*     else b :: helper (i+1) *)
(*   in helper 0 *)

(* let pp_big_int fmt bi = *)
(*   F.printf "0x%02x" (Big_int.to_int bi) *)

(* (\* Testing the a9 grammars in Combinator.v *\) *)
(* let gen_prefix (n:int) =  *)
(*   let rec helper (i:int) = *)
(*     if (i=n) then [] *)
(*     else 0xff :: helper (i+1) *)
(*   in helper 0 *)

(* let test_combinator_an (n:int) (an:Combinator.coq_parser) =  *)
(*   let bl1 = gen_prefix (Int.pow 2 n) in *)
(*   let bl2 = gen_random_bl n in *)
(*   let cl = List.flatten *)
(*     (List.map (fun b -> Grammar.token_id_to_chars (int8_of_int b)) (List.append bl1 bl2))  in *)
(*   let parse () = *)
(*     match an cl with *)
(*       | [] -> F.printf "no match\n" *)
(*       | (cl', v) :: _ -> () *)
(*       (\* let l = Obj.magic v in *\) *)
(*       (\* F.printf "match; list(cl')=%d;\n result=%a\n" (List.length cl') *\) *)
(*       (\* 	(pp_list pp_big_int) l *\) *)
(*   in Stats.repeattime 1.0 ("testing a" ^ string_of_int(n)) parse () *)

(* open Parser *)

(* let test_table_an (n:int) (an:Grammar.grammar) =  *)
(*   let bl1 = gen_prefix (Int.pow 2 n) in *)
(*   let bl2 = gen_random_bl n in *)
(*   let build_ini_decoder_state () = initial_parser_state an in *)
(*   let ini_state = *)
(*     Stats.time ("build_dfa for g" ^ string_of_int n) *)
(*       build_ini_decoder_state () in *)
(*   let parse () = *)
(*     match parse_tokens ini_state (List.map int8_of_int (List.append bl1 bl2)) with *)
(*       | (_, []) -> F.printf "no match\n" *)
(*       | ((cl', _), v :: _) -> () *)
(* 	(\* let l = Obj.magic v in *\) *)
(* 	(\* F.printf "match; list(cl')=%d;\n result=%a\n" (List.length cl') *\) *)
(*       	(\*   (pp_list pp_big_int) l *\) *)
(*   in *)
(*   F.printf "num of states for DFA(a%s)=%d \n" (string_of_int n) *)
(*     (Big_int.int_of_big_int ini_state.dfa_ps.dfa_num_states); *)
(*   Stats.repeattime 5.0 ("testing a" ^ string_of_int(n)) parse () *)
  
(* Testing the expotential behavior of g20 grammars in combinator.v *)

(* let test_combinator_gn (n:int) (gn:Combinator.coq_parser) =  *)
(*   let bl = gen_random_bl (Int.pow 2 n) in  *)
(*   let cl = List.flatten  *)
(*     (List.map (fun b -> Grammar.token_id_to_chars (int8_of_int b)) bl)  in *)
(*   let parse () = *)
(*     match gn cl with *)
(*       | [] -> F.printf "no match\n" *)
(*       | (cl', v) :: _ ->  () *)
(*       (\* let l = Obj.magic v in *\) *)
(*       (\* F.printf "match; list(cl')=%d;\n result=%a\n" (List.length cl') *\) *)
(*       (\* 	(pp_list pp_big_int) l *\) *)
(*   in Stats.repeattime 5.0 ("testing g" ^ string_of_int(n)) parse () *)

(* let test_combinator_g1 () = test_combinator_gn 1 Combinator.g1 *)
(* let test_combinator_g2 () = test_combinator_gn 2 Combinator.g2 *)
(* let test_combinator_g3 () = test_combinator_gn 3 Combinator.g3 *)
(* let test_combinator_g4 () = test_combinator_gn 4 Combinator.g4 *)
(* let test_combinator_g5 () = test_combinator_gn 5 Combinator.g5 *)
(* let test_combinator_g6 () = test_combinator_gn 6 Combinator.g6 *)
(* let test_combinator_g7 () = test_combinator_gn 7 Combinator.g7 *)
(* let test_combinator_g8 () = test_combinator_gn 8 Combinator.g8 *)
(* let test_combinator_g9 () = test_combinator_gn 9 Combinator.g9 *)
(* let test_combinator_g10 () = test_combinator_gn 10 Combinator.g10 *)

(* open Parser *)

(* let test_table_gn (n:int) (gn:Grammar.grammar) =  *)
(*   let bl = List.map int8_of_int (gen_random_bl (Int.pow 2 n)) in  *)
(*   (\* let cl = List.flatten  *\) *)
(*   (\*   (List.map (fun b -> Grammar.token_id_to_chars (int8_of_int b)) bl)  in *\) *)
(*   let build_ini_decoder_state () = initial_parser_state gn in *)
(*   let ini_state =  *)
(*     Stats.time ("build_dfa for g" ^ string_of_int n) *)
(*       build_ini_decoder_state () in *)
(*   let parse () = *)
(*     match parse_tokens ini_state bl with *)
(*       | (_, []) -> F.printf "no match\n" *)
(*       | ((cl', _), v :: _) -> () *)
(* 	(\* let l = Obj.magic v in *\) *)
(* 	(\* F.printf "match; list(cl')=%d;\n result=%a\n" (List.length cl') *\) *)
(*       	(\*   (pp_list pp_big_int) l *\) *)
(*   in  *)
(*   F.printf "num of states for DFA(g%s)=%d \n" (string_of_int n) *)
(*     (Big_int.int_of_big_int ini_state.dfa_ps.dfa_num_states); *)
(*   Stats.repeattime 5.0 ("testing g" ^ string_of_int(n)) parse () *)

(* let test_table_g1 () = test_table_gn 1 ParserTest.g1 *)
(* let test_table_g2 () = test_table_gn 2 ParserTest.g2 *)
(* let test_table_g3 () = test_table_gn 3 ParserTest.g3 *)
(* let test_table_g4 () = test_table_gn 4 ParserTest.g4 *)
(* let test_table_g5 () = test_table_gn 5 ParserTest.g5 *)
(* let test_table_g6 () = test_table_gn 6 ParserTest.g6 *)
(* let test_table_g7 () = test_table_gn 7 ParserTest.g7 *)
(* let test_table_g8 () = test_table_gn 8 ParserTest.g8 *)
(* let test_table_g9 () = test_table_gn 9 ParserTest.g9 *)
(* let test_table_g10 () = test_table_gn 10 ParserTest.g10 *)

let testDisassembler fname =
  Dis.init();
  F.printf "Testing decoding using a sequence of code bytes:@.";
  Stats.time "decode_simple_tests"
    (fun _ ->
      F.printf "%a" pp_code_region
  	(Dis.ls_dis_section prog (MWord.of_int 0x80483fd)))
    ()

  (* F.printf "Testing decoding using a list of elf files:@."; *)
  (* sourceFiles := fname :: !sourceFiles; *)
  (* process each file *)
  (* List.iter test_encode_decode_file !sourceFiles *)
  (* Stats.time "decode_files_tests" *)
  (*   (fun _ -> List.iter ls_disassemble_fn !sourceFiles) () *)

let _ =
  if (!enableTesting) then begin
      Stats.reset Stats.SoftwareTimer;
      testDisassembler ();
      Stats.print Pervasives.stdout "Timings:\n"
  end else ()

(*   let usageMsg = "Usage: main [options] source-files" in *)
(*   let optionDescr = [] in *)
(*   let recordFiles (fname:string) =  *)
(*     sourceFiles := fname :: !sourceFiles *)
(*   in *)
(* test_encode_decode_codeBytes prog (MWord.of_int 0x80483fd); *)

(*   (\* parse the command-line arguments *\) *)
(*   Arg.parse (Arg.align optionDescr) recordFiles usageMsg; *)
(*   sourceFiles := List.rev !sourceFiles; *)


  (* Testing the parsing combinators on g_n grammars *)
  (* test_combinator_g8 (); *)
  (* test_combinator_g9 (); *)
  (* test_combinator_g10 (); *)

  (* Testing the table-derivative parser on g_n grammars *)
  (* test_table_g8 (); *)
  (* test_table_g9 (); *)
  (* test_table_g10 (); *)

  (* test_combinator_an 8 Combinator.a8; *)
  (* test_combinator_an 9 Combinator.a9; *)
  (* test_combinator_an 10 Combinator.a10; *)

  (* test_table_an 8 ParserTest.a8; *)
  (* test_table_an 9 ParserTest.a9; *)
  (* test_table_an 10 ParserTest.a10; *)

