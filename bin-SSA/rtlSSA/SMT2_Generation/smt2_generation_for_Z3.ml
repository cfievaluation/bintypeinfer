open X86Syntax
open X86Semantics
open Smt_syntax
open Logical_formula
open Printf
open Big

let create_file fn = 
    let exist = Sys.file_exists "FILES.SMT2" in
    let signal1 = if exist = false then Sys.command "mkdir FILES.SMT2" else 0 in
    let cmd = String.concat "" ["touch "; "./FILES.SMT2/"; fn] in
    let signal2 = Sys.command cmd in
    (signal1, signal2)

let set_logic logic_name oc = 
    fprintf oc "(set-logic %s)\n" logic_name;
    flush oc

let set_info info oc = 
    fprintf oc "(set-info :%s)\n" info;
    flush oc

let set_option opt oc =
    fprintf oc "(set-option :%s)\n" opt;
    flush oc

let declare_arr arr_name s1 s2 oc =
    fprintf oc "(declare-fun %s () (Array (_ BitVec %d) (_ BitVec %d)))\n" arr_name s1 s2;
    flush oc
    
let declare_reg_loc oc = 
    fprintf oc "(declare-fun EAX () (_ BitVec 32))\n";
    fprintf oc "(declare-fun ECX () (_ BitVec 32))\n";
    fprintf oc "(declare-fun EDX () (_ BitVec 32))\n";
    fprintf oc "(declare-fun EBX () (_ BitVec 32))\n";
    fprintf oc "(declare-fun ESP () (_ BitVec 32))\n";
    fprintf oc "(declare-fun EBP () (_ BitVec 32))\n";
    fprintf oc "(declare-fun ESI () (_ BitVec 32))\n";
    fprintf oc "(declare-fun EDI () (_ BitVec 32))\n";
    flush oc

let declare_seg_reg_start_loc oc =
    fprintf oc "(declare-fun StartES () (_ BitVec 32))\n";
    fprintf oc "(declare-fun StartCS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun StartSS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun StartDS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun StartFS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun StartGS () (_ BitVec 32))\n";
    flush oc

let declare_seg_reg_limit_loc oc =
    fprintf oc "(declare-fun LimitES () (_ BitVec 32))\n";
    fprintf oc "(declare-fun LimitCS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun LimitSS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun LimitDS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun LimitFS () (_ BitVec 32))\n";
    fprintf oc "(declare-fun LimitGS () (_ BitVec 32))\n";
    flush oc

let declare_control_register_loc oc =
    fprintf oc "(declare-fun CR0 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun CR2 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun CR3 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun CR4 () (_ BitVec 32))\n";
    flush oc

let declare_debug_register_loc oc =
    fprintf oc "(declare-fun DR0 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DR1 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DR2 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DR3 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DR6 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DR7 () (_ BitVec 32))\n";
    flush oc

let declare_flag_loc oc =
    fprintf oc "(declare-fun ID () (_ BitVec 32))\n";
    fprintf oc "(declare-fun VIP () (_ BitVec 32))\n";
    fprintf oc "(declare-fun VIF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun AC () (_ BitVec 32))\n";
    fprintf oc "(declare-fun VM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun RF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun NT () (_ BitVec 32))\n";
    fprintf oc "(declare-fun IOPL () (_ BitVec 32))\n";
    fprintf oc "(declare-fun OF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun DF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun IF_flag () (_ BitVec 32))\n";
    fprintf oc "(declare-fun TF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun SF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun ZF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun AF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun PF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun CF () (_ BitVec 32))\n";
    flush oc

let declare_pc_loc oc =
    fprintf oc "(declare-fun PC () (_ BitVec 32))\n";
    flush oc

let declare_fpu_stktop_loc oc =
    fprintf oc "(declare-fun fpu_stktop () (_ BitVec 32))\n";
    flush oc

let declare_fpu_flag_loc oc =
    fprintf oc "(declare-fun F_Busy () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_C3 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_C2 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_C1 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_C0 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_ES () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_SF () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_PE () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_UE () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_OE () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_ZE () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_DE () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_IE () (_ BitVec 32))\n";
    flush oc

let declare_fpu_rctrl_loc oc =
    fprintf oc "(declare-fun fpu_rctrl () (_ BitVec 32))\n";
    flush oc

let declare_fpu_pctrl_loc oc =
    fprintf oc "(declare-fun fpu_pctrl () (_ BitVec 32))\n";
    flush oc

let declare_fpu_ctrl_flag_loc oc =
    fprintf oc "(declare-fun F_Res15 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_Res14 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_Res13 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_Res7 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_Res6 () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_IC () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_PM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_UM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_OM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_ZM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_DM () (_ BitVec 32))\n";
    fprintf oc "(declare-fun F_IM () (_ BitVec 32))\n";
    flush oc

let declare_fpu_lastInstrPtr_loc oc =
    fprintf oc "(declare-fun fpu_lastInstrPtr () (_ BitVec 32))\n";
    flush oc

let declare_fpu_lastDataPtr_loc oc =
    fprintf oc "(declare-fun fpu_lastDataPtr () (_ BitVec 32))\n";
    flush oc

let declare_fpu_lastOpcode_loc oc =
    fprintf oc "(declare-fun fpu_lastOpcode () (_ BitVec 32))\n";
    flush oc

let declare_location_variable oc = 
    declare_reg_loc oc;
    declare_seg_reg_start_loc oc;
    declare_seg_reg_limit_loc oc;
    declare_flag_loc oc;
    declare_control_register_loc oc ;
    declare_debug_register_loc oc;
    declare_pc_loc oc;
    declare_fpu_stktop_loc oc;
    declare_fpu_flag_loc oc;
    declare_fpu_rctrl_loc oc;
    declare_fpu_pctrl_loc oc;
    declare_fpu_ctrl_flag_loc oc;
    declare_fpu_lastInstrPtr_loc oc;
    declare_fpu_lastDataPtr_loc oc;
    declare_fpu_lastOpcode_loc oc;
    flush oc

let prepare_smt_file oc = 
    set_logic "QF_ABV" oc;
    set_info "smt-lib-version 2.0" oc;
    fprintf oc "\n"; flush oc;
    declare_arr "Fpu_drarray" 32 32 oc;
    declare_arr "Fpu_tarray" 32 32 oc;
    declare_arr "Mem" 32 8 oc;
    declare_location_variable oc;
    fprintf oc "\n";
    flush oc

let end_smt_file oc =
  let write = fprintf oc in
    write "\n"; flush oc;
    write "(check-sat)\n"; flush oc;
    (*write "(get-model)\n"; flush oc;*)
    write "(exit)\n";
    flush oc

let translate_top top = match top with
    X86_RTL.Coq_eq_op -> "="
  | X86_RTL.Coq_lt_op -> "bvslt"
  | X86_RTL.Coq_ltu_op -> "bvult"

let to_size s str = 
  let l = String.length str in
  let size = to_int s in
  let add_digits = size - l in
  let str' = String.make add_digits '0' in
    String.concat "" [str';str] 

let rec to_binary_string bstr bi = 
  let (new_bi, current_bit_bi) = quomod bi two in
  let current_bit = to_string current_bit_bi in
  let bitstr = String.concat "" [current_bit; bstr] in
  (if eq new_bi zero then bitstr else (to_binary_string bitstr new_bi))

let rec string_of_name nm = match nm with
    BasicName(str) -> str
  | IndexedName(nm', i) -> String.concat "" [(string_of_name nm'); (string_of_int i)]

let write_sbvop_in_file oc sbvop = match sbvop with
    Bvadd -> fprintf oc "bvadd"; flush oc;
  | Bvsub -> fprintf oc "bvsub"; flush oc;
  | Bvmul -> fprintf oc "bvmul"; flush oc;
  | Bvsdiv -> fprintf oc "bvsdiv"; flush oc;
  | Bvudiv -> fprintf oc "bvudiv"; flush oc;
  | Bvumod -> fprintf oc "bvumod"; flush oc;
  | Bvsmod -> fprintf oc "bvsmod"; flush oc;
  | Bvand -> fprintf oc "bvand"; flush oc;
  | Bvor -> fprintf oc "bvor"; flush oc;
  | Bvxor -> fprintf oc "bvxor"; flush oc;
  | Bvshl -> fprintf oc "bvshl"; flush oc;
  | Bvashr -> fprintf oc "bvashr"; flush oc;
  | Bvlshr -> fprintf oc "bvlshr"; flush oc

let write_dec_in_file oc d = 
  fprintf oc "%s" (to_string d); flush oc

let rec write_sbe_in_file oc sbe =
  let write str = fprintf oc "%s" str; flush oc in
  let write_sbe = write_sbe_in_file oc in
  let write_dec = write_dec_in_file oc in
    match sbe with
      SmtBv(sz, d) -> 
        write "#b"; 
        fprintf oc "%s" (to_size (Big.succ sz) (to_binary_string "" (Bits.Word.repr sz d))); flush oc;
    | SmtBvar(sz, nm) -> 
        write (string_of_name nm);
    | SmtSelect(barr, sbe') -> 
        write "(select "; 
        write_barr_in_file oc barr; 
        write " "; 
        write_sbe sbe'; 
        write ")" ;
    | SmtBvop(sbvop, sbe1, sbe2) ->
        write "("; 
        write_sbvop_in_file oc sbvop;
        write " "; 
        write_sbe sbe1;
        write " "; 
        write_sbe sbe2;
        write ")";
    | SmtConcat(sbe1, sbe2) ->
        write "(concat "; 
        write_sbe sbe1; 
        write " "; 
        write_sbe sbe2; 
        write ")";
    | SmtExtract(d1, d2, sbe') ->
        write "((_ extract ";
        write_dec d1;
        write " ";
        write_dec d2;
        write ") ";
        write_sbe sbe';
        write ")";
    | SmtSExtend(d, sbe') ->
        write "((_ sign_extend ";
        write_dec d;
        write ") ";
        write_sbe sbe';
        write ")";
    | SmtIte(sce, sbe1, sbe2) ->
        write "(ite "; 
        write_sce_in_file oc sce;
        write " ";
        write_sbe sbe1;
        write " ";
        write_sbe sbe2;
        write ")";
    | SmtError ->
	raise (Failure "Float point is not supported.");
        write "Generation is stuck here since we do not support float point for now\n";
	exit 0;

and write_barr_in_file oc barr =
  let write str = fprintf oc "%s" str; flush oc in
  let write_sbe sbe = 
    (try (write_sbe_in_file oc sbe) with 
      Failure explanation -> print_string explanation) in
    match barr with
      BasicArr(nm) -> write (string_of_name nm);
    | SmtStore(barr', sbe1, sbe2) ->
        write "(store ";
        write_barr_in_file oc barr';
        write " ";
        write_sbe sbe1;
        write " ";
        write_sbe sbe2;
        write ")";

and write_sce_in_file oc sce = 
  let write str = fprintf oc "%s" str; flush oc in
  let write_sbe = write_sbe_in_file oc in
    match sce with
      SmtCompare(scmp, sbe1, sbe2) ->
        write "("; 
        write (translate_top scmp);
        write " ";
        write_sbe sbe1;
        write " ";
        write_sbe sbe2;
        write ")"

let rec write_lfs_in_file oc lfs = 
  let write str = fprintf oc "%s" str; flush oc in
  let write_sce = write_sce_in_file oc in
  let write_lfs = write_lfs_in_file oc in
  let sbox op lfs = write "("; write op; write " "; write_lfs lfs; write ")" in
  let dbox op lfs1 lfs2 = write "("; write op; write " "; write_lfs lfs1; write " "; write_lfs lfs2; write ")" in
    match lfs with
      LfsTrue -> write "true";
    | LfsFalse -> write "false";
    | LfsExp (sce) -> write_sce sce;
    | LfsNot (lfs) -> sbox "not" lfs;
    | LfsAnd (lfs1, lfs2) -> dbox "and" lfs1 lfs2;
    | LfsOr (lfs1, lfs2) -> dbox "or" lfs1 lfs2;
    | LfsImply (lfs1, lfs2) -> dbox "=>" lfs1 lfs2

let assert_lfs oc lfs = 
  fprintf oc "(assert "; flush oc;
  write_lfs_in_file oc lfs;
  fprintf oc ")";
  flush oc

