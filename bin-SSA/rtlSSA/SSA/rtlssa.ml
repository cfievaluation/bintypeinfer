open Batteries
open Printf
module H = Hashtbl

open X86Semantics
open X86_RTL

open Ibaux
open Auxlib
open Block
(**
   The new plan is to do Phi-function computation at program level
   and renaming with indices at function level.

   For simplicity, I only consider part of registers as variables.
   And indices are single integers instead of previous integral 
   pairs.
**)

type rtlssa_id =
| NoID
| IntraID of int
| InterID of int * int

type rtlssa_var = {
  rvssa_var : rtl_var;
  rvssa_id : rtlssa_id;
}

let mkRVSSA rv rtlssa_id = {
  rvssa_var = rv;
  rvssa_id = rtlssa_id;
}

type rtlssa_phifun = rtlssa_var * rtlssa_var list

type rtlssa_block = {
  rbssa_rb : rtl_block;
  rbssa_def_vars : rtl_var list;
  rbssa_phifuns : rtlssa_phifun list;
  rbssa_renaming_map : (rtlssa_var option * rtlssa_var list) list;
  rbssa_entry_vars: rtlssa_var list;
  rbssa_exit_vars: rtlssa_var list;
}

let mkRBSSA rb phifuns rnm_map entry_vars exit_vars = {
  rbssa_rb = rb;
  rbssa_def_vars = eliminate_dup_a' (collect_defvars_in_rb rb);
  rbssa_phifuns = phifuns;
  rbssa_renaming_map = rnm_map;
  rbssa_entry_vars = entry_vars;
  rbssa_exit_vars = exit_vars;
}

type rtlssa_function = {
  rfssa_funcname : string;
  
  rfssa_funcID : int;

  rfssa_blocks : rtlssa_block list;

  (** Indices for variables at the entry of a function:
      for function level SSA transformation, the entry indices
      are all 0s. However, for program level, the entry indices
      could be a merge of possible callers' exit indices. **)
  rfssa_entry_vars : rtlssa_var list;

  (** Indices for variables at the exit of a function:
      important for handling inter-procedural SSA transformation **)
  rfssa_exit_vars : rtlssa_var list;
}

let str_of_rtlssa_id ridssa =
  match ridssa with
  | NoID -> ""
  | IntraID index -> sprintf "_%d" index
  | InterID (funcID, blockID) -> sprintf "_(f%d,b%d)" funcID blockID

let print_rtlssa_var rvssa =
  printf "%s%s " 
    (str_of_rtl_var rvssa.rvssa_var) 
    (str_of_rtlssa_id rvssa.rvssa_id)

let print_rtlssa_phifun rtlssa_phifun =
  let lhs, rhs = rtlssa_phifun in
  print_rtlssa_var lhs;
  printf " <-- Phi(";
  List.iter print_rtlssa_var rhs;
  printf ")\n"

let print_renaming_map rnm_map =
  let count = ref 0 in
  List.iter (fun (lhs, rhs) ->
    count := !count + 1;
    printf "Statement %d:\n" !count;
    printf "  Left-hand side: ";
    if Option.is_none lhs then
      printf "None"
    else
      print_rtlssa_var (Option.get lhs);
    printf "\n";
    printf "  Right-hand side: ";
    List.iter print_rtlssa_var rhs;
    printf "\n"
  ) rnm_map

let print_rtlssa_block rbssa =
  (*
  printf "Original RTL block contains information:\n";
  print_rb rbssa.rbssa_rb;*)
  printf "RTL-SSA block %s contains information:\n" (str_of_rbid rbssa.rbssa_rb.rb_ID) ;
  printf "Defined variables:\n";
  List.iter print_rtl_var rbssa.rbssa_def_vars;
  printf "Phi-functions:\n";
  List.iter print_rtlssa_phifun rbssa.rbssa_phifuns;
  printf "Renamings:\n";
  print_renaming_map rbssa.rbssa_renaming_map;
  printf "Entry variables:\n";
  List.iter print_rtlssa_var rbssa.rbssa_entry_vars;
  printf "\n";
  printf "Exit variables:\n";
  List.iter print_rtlssa_var rbssa.rbssa_exit_vars;
  printf "\n"

let print_rtlssa_function rfssa =
  printf "Function %s with ID: %d in SSA:\n" 
    rfssa.rfssa_funcname rfssa.rfssa_funcID;
  printf "At the entry, variables are indexed as following:\n";
  List.iter print_rtlssa_var rfssa.rfssa_entry_vars;
  printf "\n";
  printf "At the exit, variables are indexed as following:\n";
  List.iter print_rtlssa_var rfssa.rfssa_exit_vars;
  printf "\n";
  List.iter print_rtlssa_block rfssa.rfssa_blocks;
  printf "\n"
   
let doRCSSA rfunc targets =
  let incremental_gap = 
    (List.length rfunc.rf_blocks) + 1
  in
  let startIndex rb rv = 
    if List.exists (is_set_var rv) rb.rb_instrs then 
      (snd rb.rb_ID) + incremental_gap
    else
      (snd rb.rb_ID)
  in
  let init_rbssa rb =
    let rnm_map = [] in  
    if rb.rb_ID = (rfunc.rf_funcID, 1) then
      let phifuns = 
	List.map (fun rv ->
	  let lhs_rvssa = 
	    mkRVSSA rv (IntraID (startIndex rb rv))
	  in
	  let rhs_rvssa_list =
	    [mkRVSSA rv (IntraID 0)]
	  in
	  lhs_rvssa, rhs_rvssa_list
	) targets
      in
      let entry_vars = List.map fst phifuns in
      (*List.map (fun rv -> mkRVSSA rv (IntraID 0)) targets
      in*)
      let exit_vars = 
      	List.map (fun rv -> mkRVSSA rv (IntraID 1)) targets
      in
      mkRBSSA rb phifuns rnm_map entry_vars exit_vars
    else
      let phifuns = 
	List.map (fun rv -> 
	  let lhs_rvssa = mkRVSSA rv 
	    (IntraID (startIndex rb rv))
	  in
	  let rhs_rvssa_list = 
	    List.map (fun pred_id -> 
	      if (fst pred_id) = (fst rb.rb_ID) then
		mkRVSSA rv (IntraID (snd pred_id))
	      else
		mkRVSSA rv (InterID ((fst pred_id), (snd pred_id)))
	    ) rb.rb_preds
	  in
	  lhs_rvssa, rhs_rvssa_list
	) targets
      in
      let entry_vars = List.map fst phifuns in
      let exit_vars = 
	List.map (fun rv -> mkRVSSA rv (IntraID (snd rb.rb_ID))) targets
      in
      mkRBSSA rb phifuns rnm_map entry_vars exit_vars
  in
  let rfssa_fst_pass =
    {
      rfssa_funcname = rfunc.rf_funcname;
      rfssa_funcID = rfunc.rf_funcID;
      rfssa_blocks = List.map init_rbssa rfunc.rf_blocks;
      rfssa_entry_vars = List.map (fun rv -> mkRVSSA rv NoID) targets;
      rfssa_exit_vars = []; (* Configure this during renaming *)
    }
  in
  rfssa_fst_pass
(*
let rec rec_map rvssa_map rvssa =
  try
    rec_map 
      rvssa_map
      (H.find rvssa_map rvssa)
  with Not_found -> rvssa
*)  
let rec rec_map rvssa_map rvssa =
  try 
    let img1 = H.find rvssa_map rvssa in
    let img2 = rec_map rvssa_map img1 in
    H.replace rvssa_map rvssa img2;
    img2
  with Not_found -> rvssa

let optimizeSSA rfssa targets =
  let modiflag = ref true in

  let rvssa_map = H.create 32 in
  
  let modified_rfssa = ref rfssa in

  while !modiflag do
    modiflag := false;
    let new_blocks = 
      List.map (fun rbssa ->
	let new_phifun_list =
	  List.filter_map (fun phifun ->
	    let lhs_rvssa, rhs_rvssa_list = phifun in
	    let filtered_rhs_rvssa_list = 
	      List.filter (fun rhs_rvssa ->
		lhs_rvssa <> rhs_rvssa || 
		  lhs_rvssa <> (rec_map rvssa_map rhs_rvssa)
	      ) rhs_rvssa_list
	    in
	    (*
	    let filtered_rhs_rvssa_list =
	      List.filter (fun rhs_rvssa -> 
		rhs_rvssa.rvssa_id <> (InterID 0)
	      ) filtered_rhs_rvssa_list
	    in
	    *)
	    if List.is_empty filtered_rhs_rvssa_list then None
	    else
	      let rhs_rvssa_j = List.hd filtered_rhs_rvssa_list in
	      if List.for_all (fun rhs_rvssa -> 
		let mapped_rhs_rvssa = rec_map rvssa_map rhs_rvssa in
		rec_map rvssa_map rhs_rvssa_j = mapped_rhs_rvssa 
		&& 
		  begin match mapped_rhs_rvssa.rvssa_id with
		  | InterID _ -> false 
		  | _ -> true
		  end
	      ) filtered_rhs_rvssa_list 
	      then (
		modiflag := true;
		H.add rvssa_map lhs_rvssa rhs_rvssa_j;
		None
	      )
	      else
		Some (fst phifun, List.map (rec_map rvssa_map) (snd phifun))
	  ) rbssa.rbssa_phifuns 
	in
	{
	  rbssa with
	    rbssa_phifuns = new_phifun_list
	}
      ) (!modified_rfssa).rfssa_blocks
    in
    modified_rfssa := {
      !modified_rfssa with
	rfssa_blocks = new_blocks
    }
  done;
  (*
  printf "variable mapping for function %s:\n" rfssa.rfssa_funcname; 
  H.iter (fun rvssa1 rvssa2 ->
    print_rtlssa_var rvssa1;
    printf " == ";
    print_rtlssa_var rvssa2;
    printf "\n"
  ) rvssa_map;
  printf "\n";*)
  (!modified_rfssa, rvssa_map)
	
exception Inc_NoID

exception Inc_InterID

let renameSSA rfssa rvssa_map targets =
  let incre_gap = 1 + (List.length rfssa.rfssa_blocks) in

  let inc_rtlssa_id rtlssa_id =
    match rtlssa_id with
    | NoID -> raise Inc_NoID
    | IntraID index -> IntraID (index + incre_gap)
    | InterID _ -> raise Inc_InterID
  in
  
  let rename_rbssa rbssa =
    let curr_rvssa = ref rbssa.rbssa_entry_vars in
    let prev_rvssa = ref !curr_rvssa in
    let fst_pass = {
      rbssa with
	rbssa_renaming_map = List.map (fun ri ->
	  let lhs_rv = get_lhs_from_ri ri in
	  if Option.is_none lhs_rv then (None, !prev_rvssa)
	  else (
	    try
	      let lhs_rvssa = 
		List.find (fun curr_rvssa ->
		  curr_rvssa.rvssa_var = (Option.get lhs_rv)
		) !curr_rvssa	      
	      in
	      let inc_lhs_rvssa = {
		lhs_rvssa with
		  rvssa_id = inc_rtlssa_id lhs_rvssa.rvssa_id 
	      }
	      in
	      curr_rvssa := List.map (fun rvssa ->
		if rvssa.rvssa_var = Option.get lhs_rv then inc_lhs_rvssa
		else rvssa
	      ) !prev_rvssa;
	      ((Some inc_lhs_rvssa), !prev_rvssa)
	    with Not_found -> 
	      (None, !prev_rvssa)
	  )
	) rbssa.rbssa_rb.rb_instrs
    }	    
    in

    let later_than_rvssa rvssa1 rvssa2 =
      match rvssa1.rvssa_id, rvssa2.rvssa_id with
      | IntraID _, NoID -> true
      | IntraID _, InterID _ -> 
	printf "Warning: there are still inter-procedural ID used!\n";
	flush stdout;
	true
      | IntraID index1, IntraID index2 -> index1 > index2
      | _ -> false
    in
    let snd_pass =
      let redefined_rvssa_list = List.filter_map (fun rnm_entry ->
	let lhs_rvssa = fst rnm_entry in
	lhs_rvssa
      ) fst_pass.rbssa_renaming_map
      in
      let snd_pass_rvssa_map = H.create 32 in
      List.iter (fun target_rv ->
	if List.exists (fun redef_rvssa -> 
	  redef_rvssa.rvssa_var = target_rv) redefined_rvssa_list
	then (
	  let maxid_target_rvssa = ref (mkRVSSA target_rv NoID) in
	  List.iter (fun redef_rvssa -> 
	    if redef_rvssa.rvssa_var = target_rv then
	      if later_than_rvssa redef_rvssa !maxid_target_rvssa then
		maxid_target_rvssa := redef_rvssa
	      else ()
	    else ()
	  ) redefined_rvssa_list;
	  let related_exit_rvssa =
	    try
	      List.find (fun exit_rvssa ->
		exit_rvssa.rvssa_var = (!maxid_target_rvssa).rvssa_var
	      ) fst_pass.rbssa_exit_vars
	    with Not_found ->
	      raise (Failure "Exit variables do not track properly!")
	  in
	  H.add snd_pass_rvssa_map !maxid_target_rvssa related_exit_rvssa
	)
      ) targets;
      (*
      printf "RTL-SSA Block:%s:" (str_of_rbid fst_pass.rbssa_rb.rb_ID);
      H.iter (fun lhs rhs ->
	print_rtlssa_var lhs;
	printf " == ";
	print_rtlssa_var rhs;
	printf "\n";
	flush stdout
      ) snd_pass_rvssa_map;
      printf "\n";
      flush stdout;
      *)

      let easyfind rvssa_map rvssa =
	try H.find rvssa_map rvssa
	with Not_found -> rvssa
      in
      {
	fst_pass with
	  rbssa_renaming_map = List.map (fun rnm_entry ->
	    let lhs_rvssa_opt, rhs_rvssa_list = rnm_entry in
	    if Option.is_none lhs_rvssa_opt then
	      None, List.map (easyfind snd_pass_rvssa_map) rhs_rvssa_list 
	    else
	      Some (easyfind snd_pass_rvssa_map (Option.get lhs_rvssa_opt)), (List.map (easyfind snd_pass_rvssa_map) rhs_rvssa_list)
	  ) fst_pass.rbssa_renaming_map
      }
    in
    snd_pass
  in
  
  let blk_n_x_configured_rfssa =
    {
      rfssa with
	rfssa_blocks = List.map (fun rbssa ->
	  {
	    rbssa with
	      rbssa_entry_vars = 
	      List.map (rec_map rvssa_map) rbssa.rbssa_entry_vars;
	      rbssa_exit_vars = List.map (rec_map rvssa_map) rbssa.rbssa_exit_vars;
	  }
	) rfssa.rfssa_blocks;
    }
  in
  let renamed_rfssa =
    {
      blk_n_x_configured_rfssa with
	rfssa_blocks = List.map rename_rbssa blk_n_x_configured_rfssa.rfssa_blocks
    }
  in
  renamed_rfssa

let rtlssa_trans (rfunc: rtl_function) (target_rvssa_list : rtl_var list) =
  let rfssa = doRCSSA rfunc target_rvssa_list in
  let opt_rfssa, rvssa_map = optimizeSSA rfssa target_rvssa_list in
  let renamed_rfssa = renameSSA opt_rfssa rvssa_map target_rvssa_list in
  renamed_rfssa
