open Batteries
open Printf

open Config_cfg
open Block
open Ibaux

open X86Syntax
open X86Semantics
open X86_RTL

open Simp_for_ssa
open Printer

type rtl_var = X86_MACHINE.location
(*| VarPsReg of pseudo_reg*)

let all_reg =
  [EAX;ECX;EDX;EBX;ESP;EBP;ESI;EDI]

let all_rv = 
  let all_loc =
    List.map (fun reg -> X86_MACHINE.Coq_reg_loc reg) all_reg
  in
  all_loc

let rec get_lhs_from_ri instr =
  match instr with
  | Coq_set_loc_rtl(_,_,loc) ->
    Some loc
  | Coq_if_rtl(_,ri) ->
    get_lhs_from_ri ri
  | _ -> None

let rec is_set_var rvar instr =
  match instr with
  | Coq_set_loc_rtl(_,_,loc) ->
    loc = rvar
  | Coq_if_rtl(_,ri) ->
    is_set_var rvar ri
  (*
    | Coq_set_ps_reg_rtl(_, _, ps_reg) ->
    begin match rvar with
    | VarPsReg varpsreg -> varpsreg = ps_reg
    | _ -> false
    end
  *)
  | _ -> false
	     
type rtl_function = {
  rf_funcname: string;
  rf_funcID: int;
  rf_blocks: rtl_block list;
  rf_def_vars: rtl_var list;
}

let str_of_rtl_var = str_of_loc
(*  match rv with
  | VarLoc loc ->
    str_of_loc loc*)
  (*| VarPsReg ps_reg ->
    str_of_ps_reg ps_reg*)

let print_rtl_var rv =
  printf "%s\n" (str_of_rtl_var rv)

let print_rtl_func rf =
  printf "%s:\n" rf.rf_funcname;
  List.iter print_rb rf.rf_blocks;
  printf "Defined variables in this function:\n";
  List.iter (fun rv -> printf "%s " (str_of_rtl_var rv)) rf.rf_def_vars;
  printf "\n"

let collect_defvars_in_rb rb =
  let defvars =
    List.filter_map get_lhs_from_ri rb.rb_instrs
  in
  List.filter (fun defvar -> List.mem defvar all_rv) defvars

let collect_defvars rb_list =
  eliminate_dup_a' (List.flatten (List.map collect_defvars_in_rb rb_list))

let collect_rtl_func elf pre cr =
  let asm_func_list = collect_asm_func elf cr in
  let lib_rb = {
    rb_ID = (0,1);
    rb_label = "ffffffff";
    rb_instrs = [];
    rb_succs = [];
    rb_inter_succs = [];
    rb_preds = [];
    rb_inter_preds = [];
  }
  in
  let libfunc = {
    rf_funcname = "__library__";
    rf_funcID = 0;
    rf_blocks = [lib_rb];
    rf_def_vars = all_rv;
  }
  in
  (*let label_addend = ref 0 in*)
  let rec split_rbaux rbaux =
    try
      let fst_if_index, fst_if_instr = List.findi (fun i ri -> 
	match ri with
	| Coq_if_rtl(_,_) -> true
	| _ -> false
      ) rbaux.rbaux_instrs
      in
      let fst_half_instrs, snd_half_instrs = List.split_at fst_if_index rbaux.rbaux_instrs in
      (*label_addend := !label_addend + 1;*)
      let if_rbaux_label = sprintf "%s'" rbaux.rbaux_label in
      (*label_addend := !label_addend + 1;*)
      let snd_half_rbaux_label = sprintf "%s''" rbaux.rbaux_label in
      let fst_half_rbaux =
	{ rbaux with
	  rbaux_instrs = fst_half_instrs;
	  rbaux_succs = [if_rbaux_label];
	  rbaux_inter_succs = [];
	}
      in
      let if_rbaux_succs =
	if List.is_empty (List.tl snd_half_instrs) then 
	  rbaux.rbaux_succs
	else
	  [snd_half_rbaux_label]
      in
      let if_rbaux_inter_succs =
	if List.is_empty (List.tl snd_half_instrs) then 
	  rbaux.rbaux_inter_succs
	else
	  []
      in
      let if_rbaux = {
	rbaux_funcID = rbaux.rbaux_funcID;
	rbaux_label = if_rbaux_label;
	rbaux_instrs = [fst_if_instr];
	rbaux_succs = if_rbaux_succs;
	rbaux_inter_succs = if_rbaux_inter_succs;
	rbaux_preds = [rbaux.rbaux_label];
	rbaux_inter_preds = [];
      }
      in
      if List.is_empty (List.tl snd_half_instrs) then 
	[fst_half_rbaux; if_rbaux]
      else
	let snd_half_rbaux = {
	  rbaux with
	    rbaux_label = snd_half_rbaux_label;
	    rbaux_instrs = List.tl snd_half_instrs;
	    rbaux_preds = [if_rbaux_label];
	    rbaux_inter_preds = [];
	}
	in
	let splitted_snd_half = split_rbaux snd_half_rbaux in
	fst_half_rbaux :: if_rbaux :: splitted_snd_half
    with Not_found ->
      [rbaux]
  in
  let lbl_id_map = StrIDMap.empty in
  let funcID = ref 0 in
  let all_rbaux_lists =
    List.map (fun asm_func -> 
      funcID := !funcID + 1;
      let rbaux_list = List.map (bb_to_rbaux !funcID) asm_func.func_bbs in
      let precise_rbaux_list = List.flatten (List.map split_rbaux rbaux_list) in
      precise_rbaux_list
    ) asm_func_list
  in
  let lbl_id_map = 
    List.fold_right (fun rbaux_list idmap1 ->
      let rbaux_id = ref 0 in
      List.fold_right (fun rbaux idmap2 ->
	rbaux_id := !rbaux_id + 1;
	StrIDMap.add rbaux.rbaux_label (rbaux.rbaux_funcID, !rbaux_id) idmap2
      ) (List.rev rbaux_list) idmap1
    ) all_rbaux_lists lbl_id_map
  in
  let lbl_id_map = StrIDMap.add "ffffffff" (0,1) lbl_id_map in
  (*StrIDMap.iter (fun lbl id -> printf "%s --> %s\n" lbl (str_of_rbid id)) lbl_id_map;
  flush stdout;*)
  let real_functions =
    funcID := 0;
    List.map (fun asm_func ->
      funcID := !funcID + 1;
      let rbaux_list = List.map (bb_to_rbaux !funcID) asm_func.func_bbs in
      let precise_rbaux_list = List.flatten (List.map split_rbaux rbaux_list) in
      let rbs = List.map (rbaux_to_rb lbl_id_map) precise_rbaux_list in
      {
	rf_funcname = asm_func.func_name;
	rf_funcID = !funcID;
	rf_blocks = rbs;
	rf_def_vars = collect_defvars rbs;
      }
    ) asm_func_list
  in
  let config_inter_preds rf_list =
    List.iter (fun rf ->
      List.iter (fun rb ->
	let inter_succ_rbs =
	  List.filter_map (fun inter_succ -> 
	    try Some (List.find (fun rb -> rb.rb_ID = inter_succ) rf.rf_blocks)
	    with Not_found -> None
	  ) rb.rb_inter_succs
	in
	List.iter (fun inter_succ_rb ->
	  if List.mem rb.rb_ID inter_succ_rb.rb_inter_preds then ()
	  else
	    inter_succ_rb.rb_inter_preds <- rb.rb_ID :: inter_succ_rb.rb_inter_preds
	) inter_succ_rbs
      ) rf.rf_blocks
    ) rf_list
  in
  config_inter_preds real_functions;
  libfunc :: real_functions

let optimize_rb ssa_filters simpri_methods simpre_methods rb =
  { rb with 
    rb_instrs = optimize_rtl_ssa ssa_filters simpri_methods simpre_methods rb.rb_instrs
  }
