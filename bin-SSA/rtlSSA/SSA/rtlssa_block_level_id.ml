(** Intra-procedural SSA transformation on RTL**)
open Batteries
open Printf

open X86Semantics
open X86_RTL

open Auxlib
open Block
module H = Hashtbl


(** each variable ID consists of two integers:
    one for block ID and the other one for variable ID **)
type rvID = int * (int option)

type rtl_var_info = {
  rvar: rtl_var;
  rvid: rvID
}
(*
type rtl_instr_info = {
  rinstr : rtl_instr;
  riid : int
}
*)
(**
   A phi function is list of SSA-IDs of predecessor blocks with 
   respect to one variable
**)

type phifun = rtl_var_info * rvID list

type rtl_func_ssa = {
  (** function name **)
  rfssa_funcname : string;

  (** for each rtl block, we have a map from statement id to a list of ssa-variables 
      in that statement **)
  rfssa_blocks : ((int, rtl_var_info option * (rtl_var_info list)) H.t * rtl_block) list;

  (** a phimap is from block ID to a phi-function list **)
  phimap : (int, phifun list) H.t 
}

let print_rvinfo rv =
  if Option.is_none (snd rv.rvid) then
    printf "%s_%d " (str_of_rtl_var rv.rvar) (fst rv.rvid)
  else
    printf "%s_%d_%d " (str_of_rtl_var rv.rvar) (fst rv.rvid) (Option.get (snd rv.rvid))

let print_phifun phifun =
  let rv, id_list = phifun in
  print_rvinfo rv;
  printf " --> ";
  List.iter (fun (id1,id2) -> 
    if Option.is_none id2 then
      printf "%d " id1
    else
      printf "%d_%d " id1 (Option.get id2)
  ) id_list;
  printf "\n"

let print_phimap phimap =
  H.iter (fun blkID phifun_list ->
    printf "Block %d:\n" blkID;
    List.iter print_phifun phifun_list
  ) phimap

let print_rtl_func_ssa rfs_in_ssa =
  printf "Function %s in SSA:\n" rfs_in_ssa.rfssa_funcname;
  List.iter (fun rfssa_block ->
    let stmtid_rvinfo_map, rb = rfssa_block in
    H.iter (fun stmtid (lhs_rvinfo, rhs_rvinfo_list) ->
      printf "statement %d: " stmtid;
      if Option.is_none lhs_rvinfo then 
	printf "None "
      else
	print_rvinfo (Option.get lhs_rvinfo); 
      printf "<-- ";
      List.iter print_rvinfo rhs_rvinfo_list;
      printf "\n"
    ) stmtid_rvinfo_map;
    print_rb rb
  ) rfs_in_ssa.rfssa_blocks;
  printf "PhiMap (block ID --> Phi-functions):\n";
  print_phimap rfs_in_ssa.phimap

let get_var_from_ri ri =
  match ri with
  | Coq_set_loc_rtl(_,_,loc) -> 
    Some { rvar = VarLoc loc; rvid = 0, None }
  | Coq_set_ps_reg_rtl(_,_,ps_reg) ->
    Some { rvar = VarPsReg ps_reg; rvid = 0, None }
  | _ -> None

let doRCSSA (rfunc : rtl_func) (ssavars : rtl_var_info list) (startID: int) : rtl_func_ssa =
  let trans_rb rb =
    let stmt_rvinfo_map = H.create 32 in
    let id = ref 0 in
    List.iter (fun ri ->
      id := !id + 1;
      let lhs_rvinfo_of_ri = get_var_from_ri ri in
      H.add stmt_rvinfo_map !id (lhs_rvinfo_of_ri, ssavars)
    ) rb.rb_instrs;
    stmt_rvinfo_map, rb
  in

  let rc_rfssa_blocks = List.map trans_rb rfunc.rtl_blocks in

  let rc_phimap = H.create 32 in

  (*let id = ref startID in*)

  let addPhiFuns (rb:rtl_block) : unit =
    let startIndex rb rvar =
      if List.exists (is_set_var rvar) rb.rb_instrs || rb.rb_ID = 1 
      then 1 else 0
    in
    (*id := !id + 1;*)
    let rb_phifun_list =
      List.map (fun rv_info ->
	{rv_info with rvid = rb.rb_ID, Some (startIndex rb rv_info.rvar) }, 
	List.map (fun pred_id -> pred_id, Some 0) rb.rb_preds
      ) ssavars
    in
    (*if List.length rb.rb_preds >= 2 then*)
    H.add rc_phimap rb.rb_ID rb_phifun_list
  in
  List.iter addPhiFuns rfunc.rtl_blocks;
  {
    rfssa_funcname = rfunc.rtl_funcname;
    rfssa_blocks = rc_rfssa_blocks;
    phimap = rc_phimap;
  }

let collect_phimap rf_ssa =
  printf "%s:\n" rf_ssa.rfssa_funcname;
  List.iter (fun (_, rb) -> printf "%d " rb.rb_ID) rf_ssa.rfssa_blocks;
  printf "\n";
  (*print_phimap rf_ssa.phimap*)
  let infunc_rb_list = List.map (fun (_, rb) -> rb.rb_ID) rf_ssa.rfssa_blocks in
  H.iter (fun blkID phifun_list ->
    let infunc_phifun_list = List.filter (fun (var_info, ssaid_list) ->
      (List.mem (fst var_info.rvid) (List.map fst ssaid_list)) && 
      List.fold_right (fun ssaid b -> 
	let bid = fst ssaid in
	b || List.mem bid infunc_rb_list
      ) ssaid_list false
    ) phifun_list
    in
    printf "Block %d:\n" blkID;
    List.iter print_phifun infunc_phifun_list
    ) rf_ssa.phimap;
  rf_ssa.phimap
    
let minimizable phifun =
  let var_info, id_list = phifun in
  let tmp = List.filter (fun ssaid ->
    ssaid <> var_info.rvid
  ) id_list
  in
  if List.is_empty tmp then true
  else 
    let j = List.hd tmp in
    List.for_all (fun id -> j = id) tmp
    
let minimPhimap phimap =
  let modiflag = ref true in
  
  let update_phimap var_info replace_id phimap =
  H.iter (fun blkid phifun_list ->
    let new_phifun_list =
      List.map (fun (var_info, id_list) ->
	let new_id_list = 
	  List.map (fun id -> 
	    if id = var_info.rvid then replace_id else id
	  ) id_list
	in
	(*
	if new_id_list <> id_list then (
	  printf "One update happens! %d_%d is replaced with %d_%d\n" 
	    (fst var_info.rvid)
	    (Option.get (snd var_info.rvid))
	    (fst replace_id)
	    (Option.get (snd replace_id)); 
	  flush stdout;
	);*)
	var_info, new_id_list
      ) phifun_list
    in
    H.replace phimap blkid new_phifun_list
  ) phimap
  in

  while !modiflag do 
    modiflag := false;
    H.filter_map_inplace (fun blkID phifun_list ->
      let new_phifun_list = 
	List.filter_map (fun (var_info, id_list) ->
	  let tmp = List.filter (fun id -> id <> var_info.rvid) id_list in
	  if List.is_empty tmp then None
	  else 
	    let j = List.hd tmp in
	    if List.for_all (fun id' -> j = id') tmp then (
	      modiflag := true;
	      (*printf "Found one possibility!\n"; flush stdout;*)
	      update_phimap var_info j phimap;
	      None
	    )
	    else
	      Some (var_info, id_list)
	) phifun_list
      in
      Some new_phifun_list
    ) phimap
  done

exception Contradiction

let renameSSA rf_ssa ssavars =
  let init_ssavars rb ssavars =
    let phimap = rf_ssa.phimap in
    List.map (fun ssavar ->
      let phifun_list = H.find phimap rb.rb_ID in
      if List.is_empty phifun_list then 
	{ssavar with 
	  rvid = (*
	    printf "rb: %d; preds: " rb.rb_ID;
	    List.iter (printf "%d ") rb.rb_preds;
	    printf "\n"; flush stdout;*)
	    let dom_preds =
	      List.filter (fun rbid -> rbid <> rb.rb_ID) rb.rb_preds
	    in
	    if List.is_empty dom_preds then rb.rb_ID, None
	    else if List.length dom_preds = 1 then
	      List.hd dom_preds, Some 0
	    else
	      raise Contradiction
	}
      else
	let newid_ssavar = 
	  try
	    fst (
	      List.find (fun (var_info, ssaid_list) ->
		ssavar.rvar = var_info.rvar
	      ) phifun_list
	    )
	  with Not_found ->
	    (*printf "rb: %d; preds: " rb.rb_ID;
	      List.iter (printf "%d ") rb.rb_preds;
	      printf "\n"; flush stdout;*)
	    {ssavar with 
	      rvid = 
		let dom_preds =
		  List.filter (fun rbid -> rbid <> rb.rb_ID) rb.rb_preds
		in
		if List.is_empty dom_preds then rb.rb_ID, None
		else if List.length dom_preds = 1 then
		  List.hd dom_preds, Some 0
		else
		  raise Contradiction
	    }   
	in
	newid_ssavar
    ) ssavars
  in
  let id_incre ssaid =
    let new_fst_id = fst ssaid in
    let new_snd_id =
      if Option.is_none (snd ssaid) then Some 1
      else
	Some ((Option.get (snd ssaid)) + 1)
    in
    new_fst_id, new_snd_id
  in
  List.iter (fun (renaming_map, rb) ->
    let id = ref 0 in
    let current_vars = ref (init_ssavars rb ssavars) in 
    let prev_vars = ref !current_vars in
    List.iter (fun ri ->
      id := !id + 1;
      current_vars := 
	List.map (fun ssavar ->
	  if is_set_var ssavar.rvar ri then
	    let new_id =
	      if fst ssavar.rvid = rb.rb_ID then
		id_incre ssavar.rvid
	      else
		rb.rb_ID, Some 1
	    in
	    {ssavar with rvid = new_id}
	  else
	    ssavar
	) !current_vars;
      let lhs_varinfo = get_var_from_ri ri in
      let lhs_var_in_map =
	if Option.is_none lhs_varinfo then
	  None
	else
	  try
	    Some (
	      List.find (fun cur_var ->
		(Option.get lhs_varinfo).rvar = cur_var.rvar
	      ) !current_vars
	    )
	  with Not_found ->
	    None
      in
      H.replace renaming_map !id (lhs_var_in_map,!prev_vars);
      prev_vars := !current_vars
    ) rb.rb_instrs
  ) rf_ssa.rfssa_blocks;
  rf_ssa

let optimizeSSA (rf_ssa : rtl_func_ssa) : rtl_func_ssa =
  (*let equation_system = collect_phimap rf_ssa in*)
  (*print_rtl_func_ssa rf_ssa;*)
  minimPhimap rf_ssa.phimap;
  (*printf "After optimization:\n";
  print_phimap rf_ssa.phimap;*)
  rf_ssa
  
let rtlssa_trans (rfunc : rtl_func) (ssavars : rtl_var_info list) (startID: int): rtl_func_ssa =
  let rf_ssa = doRCSSA rfunc ssavars startID in
  let opt_rf_ssa = optimizeSSA rf_ssa in
  let renamed_rf_ssa = renameSSA opt_rf_ssa ssavars in
  opt_rf_ssa
    
