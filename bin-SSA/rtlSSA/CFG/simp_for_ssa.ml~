open X86Syntax
open X86Semantics
open RTL
module R=X86_RTL

let rec constant_fold re =
  match re with
  | R.Coq_arith_rtl_exp (s, bvop, re1, re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let default_new_re = 
      R.Coq_arith_rtl_exp(s, bvop, cf_re1, cf_re2) 
    in
    begin match cf_re1, cf_re2 with
    | R.Coq_imm_rtl_exp(_,v1), R.Coq_imm_rtl_exp(_,v2) -> 
      begin match bvop with
      | R.Coq_add_op -> R.Coq_imm_rtl_exp(s, Big.add v1 v2) 
      | R.Coq_sub_op -> R.Coq_imm_rtl_exp(s, Big.sub v1 v2) 
      | R.Coq_mul_op -> R.Coq_imm_rtl_exp(s, Big.mult v1 v2) 
      | _ -> default_new_re
      end
    | R.Coq_imm_rtl_exp(_,v), _ -> (* v op () *)
      begin match bvop with
      | R.Coq_add_op -> (* v + () *)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* v + (_ op _) *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* v + (v' op _) *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v + (v' + _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* v + (v' - _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* v + (_ op v') *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v + (_ + v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* v + (_ - v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end
      | R.Coq_sub_op -> (* v - () *)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* v - (_ op _) *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* v - (v' op _) *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v - (v' + _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* v - (v' - _) *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* v - (_ op v') *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* v - (_ + v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re1')
	    | R.Coq_sub_op -> (* v - (_ - v') *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v v') in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re1')
	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end
      | _ -> default_new_re
      end
    | _, R.Coq_imm_rtl_exp(_,v) -> (* () op v *)
      begin match bvop with
      | R.Coq_add_op -> (* () + v *)
	begin match cf_re1 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* (_ op _) + v *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* (v' op _) + v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (v' + _) + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* (v' - _) + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* (_ op v') + v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (_ + v') + v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* (_ - v') + v*)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end
      | R.Coq_sub_op -> (* () - v *)
	begin match cf_re2 with
	| R.Coq_arith_rtl_exp(_,bvop',cf_re1',cf_re2') -> (* (_ op _) - v *)
	  begin match cf_re1', cf_re2' with
	  | R.Coq_imm_rtl_exp(_,v'), _ -> (* (v' op _) -v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (v' + _) - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,new_v,cf_re2')
	    | R.Coq_sub_op -> (* (v' - _) - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,new_v,cf_re2')
	    | _ -> default_new_re
	    end
	  | _, R.Coq_imm_rtl_exp(_,v') -> (* (_ op v') - v *)
	    begin match bvop' with
	    | R.Coq_add_op -> (* (_ + v') - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.sub v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_add_op,cf_re1',new_v)
	    | R.Coq_sub_op -> (* (_ - v') - v *)
	      let new_v = R.Coq_imm_rtl_exp(s,Big.add v' v) in
	      R.Coq_arith_rtl_exp(s,R.Coq_sub_op,cf_re1',new_v)
	    | _ -> default_new_re
	    end
	  |_ -> default_new_re
	  end
	| _ -> default_new_re
	end
      | _ -> default_new_re
      end
    | _ ->
      default_new_re
    end
  | R.Coq_test_rtl_exp (s, top, re1, re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let default_new_re =
      R.Coq_test_rtl_exp (s, top, cf_re1, cf_re2)
    in
    begin match cf_re1, cf_re2 with 
    | R.Coq_imm_rtl_exp(s1,v1), R.Coq_imm_rtl_exp(_,v2) ->
      begin match top with
      | R.Coq_eq_op -> 
	if Big.eq v1 v2 then R.Coq_imm_rtl_exp(Big.zero, Big.one)
	else R.Coq_imm_rtl_exp(Big.zero, Big.zero)
      | R.Coq_lt_op ->
	if Big.lt v1 v2 then R.Coq_imm_rtl_exp(Big.zero, Big.one)
	else R.Coq_imm_rtl_exp(Big.zero, Big.zero)
      | R.Coq_ltu_op -> default_new_re
	(*if Big.sign v1 = Big.sign v2 then
	if Big.lt v1 v2 then R.Coq_imm_rtl_exp(0, Big.of_int 1)
	else R.Coq_imm_rtl_exp(0, Big.of_int 0)*)
      end
    | _ ->
      default_new_re
    end
  | R.Coq_if_rtl_exp(s,re1,re2,re3) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let cf_re3 = constant_fold re3 in
    let default_new_re =
      R.Coq_if_rtl_exp(s,cf_re1,cf_re2,cf_re3)
    in
    begin match cf_re1 with
    | R.Coq_imm_rtl_exp(_,b) ->
      if Big.eq b Big.one then cf_re2
      else cf_re3
    | _ -> 
      default_new_re
    end
  | R.Coq_cast_s_rtl_exp(s1,s2,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_cast_s_rtl_exp(s1,s2,cf_re')
  | R.Coq_cast_u_rtl_exp(s1,s2,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_cast_u_rtl_exp(s1,s2,cf_re')
  | R.Coq_get_array_rtl_exp(l,s,arr,re') ->
    let cf_re' = constant_fold re' in
    R.Coq_get_array_rtl_exp(l,s,arr,cf_re')
  | R.Coq_get_byte_rtl_exp(re') ->
    let cf_re' = constant_fold re' in
    R.Coq_get_byte_rtl_exp(cf_re')
  | R.Coq_farith_rtl_exp(s1,s2,faop,re1,re2,re3) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    let cf_re3 = constant_fold re3 in
    R.Coq_farith_rtl_exp(s1,s2,faop,cf_re1,cf_re2,cf_re3)
  | R.Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2) ->
    let cf_re1 = constant_fold re1 in
    let cf_re2 = constant_fold re2 in
    R.Coq_fcast_rtl_exp(s1,s2,s3,s4,cf_re1,cf_re2)
  | _ -> re

(*let rec sub_neg_opt re =
  match re with
*)
let simp_all_re = 
  [constant_fold]

let simp_re_ssa simpre_list re =
  List.fold_right (fun simpre re -> simpre re) simpre_list re

let simp_set_loc simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_loc_rtl(s,re,loc) ->
    let new_re = simp_re_ssa simpre_list re in
    R.Coq_set_loc_rtl(s,new_re,loc)
  | _ -> rtl_instr

let simp_set_ps_reg simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_ps_reg_rtl(s,re,ps_reg) ->
    let new_re = simp_re_ssa simpre_list re in
    R.Coq_set_ps_reg_rtl(s, new_re, ps_reg)
  | _ -> rtl_instr

let simp_set_arr simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_array_rtl(l,s,arr,re1,re2) ->
    let new_re1 = simp_re_ssa simpre_list re1 in
    let new_re2 = simp_re_ssa simpre_list re2 in
    R.Coq_set_array_rtl(l,s,arr,new_re1,new_re2)
  | _ -> rtl_instr

let simp_set_byte simpre_list rtl_instr =
  match rtl_instr with
  | R.Coq_set_byte_rtl(re1,re2) ->
    let new_re1 = simp_re_ssa simpre_list re1 in
    let new_re2 = simp_re_ssa simpre_list re2 in
    R.Coq_set_byte_rtl(new_re1,new_re2)
  | _ -> rtl_instr

let simp_all_ri = 
  [
    simp_set_loc;
    simp_set_ps_reg;
    simp_set_arr;
    simp_set_byte
  ]

let rec simp_ri_ssa simpri_list simpre_list ri =
  match ri with
  | R.Coq_if_rtl(re, ri') ->
    let new_re = simp_re_ssa simpre_list re in
    let new_ri = simp_ri_ssa simpri_list simpre_list ri' in
    R.Coq_if_rtl(new_re, new_ri)
  | _ ->
    List.fold_right (fun simpri ri -> simpri simpre_list ri) simpri_list ri

let simplify_rtl_ssa simpri_methods simpre_methods rtl_list =
  List.map 
    (fun rtl_instr -> simp_ri_ssa simpri_methods simpre_methods rtl_instr) 
    rtl_list

let no_if_trap ri =
  match ri with
  | R.Coq_if_rtl(_,ri') ->
    ri' = R.Coq_trap_rtl
  | _ -> false
  
let ssa_filters =
  [no_if_trap]

let filter_rtl_ssa filters rtl_list =
  let throw_away ri =
    List.fold_right (fun flt b -> flt ri || b) filters false
  in
  List.filter
    (fun rtl_instr -> not (throw_away rtl_instr))
    rtl_list
