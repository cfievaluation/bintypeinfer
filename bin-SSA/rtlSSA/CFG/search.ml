open X86Syntax
open X86Semantics
open Big
open Bits

open Block
open Smt_syntax
open Logical_formula
open Core
open Smt2_generation_for_Z3

(** CFG **)

type solver_result = 
    SrSat
  | SrUnsat
  | SrUnknown
  | TooLongPath

type cfg = rtl_block IDMap.t

type path = 
  {
    satisfiability: solver_result;
    precondition: logical_formula_S;
    blocks: blockID list;
  }

(** satisfiability:
    path -> solver_result  **)

let satisfiability p = p.satisfiability

(** precondition:
    path -> logical_formula_S **)

let precondition p = p.precondition

(** blocks:
    path -> blockID list **)

let blocks p = p.blocks

let check_sat file_name = 
  Sys.command ("z3 ./FILES.SMT2/" ^ file_name ^ "> ./Output/result.txt")

(** solve:
    logical_formula_S -> solver_result **)

let solve lfs = 
  let file_creation = create_file "workspace.smt2" in
  let () =
    if file_creation = (0,0) then
      let path_to_file = String.concat "" ["./FILES.SMT2/"; "workspace.smt2"] in
      let oc = open_out path_to_file in
        prepare_smt_file oc;
        assert_lfs oc lfs;
        end_smt_file oc;
    else 
      print_string "File creation failed!";
  in
  let _ = check_sat "workspace.smt2" in
  (*let _ = Sys.command "rm ./FILES.SMT2/workspace.smt2" in*)
  let ic = open_in "./Output/result.txt" in
    let rstr = input_line ic in
    let () = close_in ic in
    if rstr = "sat" then SrSat
    else if rstr = "unsat" then SrUnsat
    else SrUnknown  
			   
(** cft_cond:
    rtl block -> logical_formula_S **)

let cft_cond rb = 
  let ieq sbe1 sbe2 = SmtCompare(X86_RTL.Coq_eq_op, sbe1, sbe2) in
  let pc = SmtBvar(of_int 31, BasicName "PC") in
    LfsExp(ieq pc (SmtBv(of_int 31, rb_relAddr rb)) )


(** search_cfg:
    cfg -> blockID -> blockID list -> logical_formula_S -> path list **)

(* todo: try different search bound *)

let max_path_length = 1000


(** We may want to start from a specific instruction in one block **)

type instr_loc = 
  {
    block: blockID; (* -1 represents the exit block *)
    instr_index: int; (* -1 represents the end of the current block *)
  }

let rec search_cfg_by_block cfg startID block_lst postcond = 
  if ((List.length block_lst) - 1) > max_path_length then
    let () = print_string "There is one too long path!\n" in
    let curr_block = IDMap.find startID cfg in
    let blst = [startID] @ block_lst in
    let curr_post = postcond in
    let curr_pre = wp (rb_instrs curr_block) curr_post in
    let next_post = LfsAnd ((cft_cond curr_block), curr_pre) in
    let curr_path = 
      {
	satisfiability = TooLongPath;
	precondition = next_post;
	blocks = blst;
      }
    in
    [curr_path]
  else if startID <> -1 then
    let curr_block = IDMap.find startID cfg in
    let pred_list = rb_pred curr_block in
    let blst = [startID] @ block_lst in
    let curr_post = postcond in
    let curr_pre = wp (rb_instrs curr_block) curr_post in
    let next_post = LfsAnd ((cft_cond curr_block), curr_pre) in
    (*let sr = (solve next_post) in*)
    if (List.length pred_list > 0) (*&& (sr = SrSat)*) then
      List.flatten (List.map (fun b -> search_cfg_by_block cfg b blst next_post) pred_list)
      (*search_cfg cfg (List.nth pred_list 0) blst next_post*)
    else 
      let sr = (solve next_post) in
      let curr_path = 
	{
	  satisfiability = sr;
	  precondition = next_post;
	  blocks = blst;
	}
      in
      [curr_path]
  else 
    let curr_block = IDMap.find startID cfg in
    let pred_list = rb_pred curr_block in
    let blst = [startID] @ block_lst in
    let next_post = postcond in
      List.flatten (List.map (fun b -> search_cfg_by_block cfg b blst next_post) pred_list)
      (*search_cfg cfg (List.nth pred_list 0) blst next_post*)

let rec sublist b e l = 
  match l with
    [] -> failwith "sublist"
  | h :: t -> 
     let tail = if e=0 then [] else sublist (b-1) (e-1) t in
     if b>0 then tail else h :: tail

(* start from either one specific assembly/RTL instruction or a block *)
let search_cfg_by_start_point cfg startpoint postcond = 
  if startpoint.block = -1 || startpoint.instr_index = -1 then 
    search_cfg_by_block cfg startpoint.block [] postcond
  else 
    let curr_block = IDMap.find startpoint.block cfg in
    let pred_list = rb_pred curr_block in
    let blst = [startpoint.block] in
    let next_post = wp (sublist 0 startpoint.instr_index curr_block.rb_instrs) postcond in
    List.flatten (List.map (fun b -> search_cfg_by_block cfg b blst next_post) pred_list)

let search_cfg_by_assembly_start_point cfg astartpoint postcond = 
  if astartpoint.block = -1 || astartpoint.instr_index = -1 then 
    search_cfg_by_block cfg astartpoint.block [] postcond
  else 
    let curr_block = IDMap.find astartpoint.block cfg in
    let pred_list = rb_pred curr_block in
    let blst = [astartpoint.block] in
    let next_post = wp (sublist 0 (List.nth curr_block.rb_instrmap astartpoint.instr_index) curr_block.rb_instrs) postcond in
    List.flatten (List.map (fun b -> search_cfg_by_block cfg b blst next_post) pred_list)

(** search method on assembly block cfg: no instruction simplification can be done **)
(*
let rec search_acfg_by_block acfg startID block_lst postcond = 
  if ((List.length block_lst) - 1) > max_path_length then
    let () = print_string "There is one too long path!\n" in
    let curr_block = IDMap.find startID acfg in
    let blst = [startID] @ block_lst in
    let curr_post = postcond in
    let curr_pre = wpa (ab_instrs curr_block) curr_post in
    let next_post = LfsAnd ((cft_cond curr_block), curr_pre) in
    let curr_path = 
      {
	satisfiability = TooLongPath;
	precondition = next_post;
	blocks = blst;
      }
    in
    [curr_path]
  else if startID <> -1 then
    let curr_block = IDMap.find startID acfg in
    let pred_list = ab_pred curr_block in
    let blst = [startID] @ block_lst in
    let curr_post = postcond in
    let curr_pre = wpa (ab_instrs curr_block) curr_post in
    let next_post = LfsAnd ((cft_cond curr_block), curr_pre) in
    (*let sr = (solve next_post) in*)
    if (List.length pred_list > 0) (*&& (sr = SrSat)*) then
      List.flatten (List.map (fun b -> search_acfg_by_block acfg b blst next_post) pred_list)
      (*search_cfg cfg (List.nth pred_list 0) blst next_post*)
    else 
      let sr = (solve next_post) in
      let curr_path = 
	{
	  satisfiability = sr;
	  precondition = next_post;
	  blocks = blst;
	}
      in
      [curr_path]
  else 
    let curr_block = IDMap.find startID acfg in
    let pred_list = ab_pred curr_block in
    let blst = [startID] @ block_lst in
    let next_post = postcond in
      List.flatten (List.map (fun b -> search_acfg_by_block acfg b blst next_post) pred_list)
      (*search_cfg cfg (List.nth pred_list 0) blst next_post*)

let search_acfg_by_start_point acfg startpoint postcond = 
  if startpoint.block = -1 || startpoint.instr_index = -1 then 
    search_acfg_by_block acfg startpoint.block [] postcond
  else 
    let curr_block = IDMap.find startpoint.block acfg in
    let pred_list = ab_pred curr_block in
    let blst = [startpoint.block] in
    let next_post = wpa (sublist 0 startpoint.instr_index curr_block.rb_instrs) postcond in
    List.flatten (List.map (fun b -> search_acfg_by_block acfg b blst next_post) pred_list)
*)
(**** pretty printer for path list ****)

let print_blocks p = 
  List.iter (fun b -> print_int b; print_string " ") (blocks p)

let print_precondition p =
  print_lfs (precondition p)

let print_sat p = match (satisfiability p) with
    SrSat -> print_string "sat";
  | SrUnsat -> print_string "unsat";
  | SrUnknown -> print_string "unknown";
  | TooLongPath -> print_string "Too Long Path"

let print_path p = 
  print_string "Satisfiability: \n";
  print_sat p;
  print_string "\n";
  (*print_string "Precondition: \n";
  print_precondition p;
  print_string "\n";*)
  print_string "path: \n";
  print_blocks p;
  print_string "\n"

let print_path_list plst= 
  List.iter print_path plst
