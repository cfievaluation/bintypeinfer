open Batteries
open Printf

open Big
open X86Syntax
open X86Semantics
open Config
open Instr

open BB
open Printer
open Ibaux
open RTL_opt
open Simp_for_ssa

(**** Mapping Module *****)

module IntOrder = struct
  type t = int
  let compare = Pervasives.compare
end

module IDMap = Map.Make(IntOrder)

module StrIDMap = Map.Make(String)


(**** RTL Block ****)
type rtl_block = 
  {
    mutable rb_ID: int * int;
    mutable rb_label : string;
    mutable rb_instrs: X86_RTL.rtl_instr list;
    mutable rb_succs: (int*int) list;
    mutable rb_inter_succs : (int*int) list;
    mutable rb_preds: (int*int) list;
    mutable rb_inter_preds : (int*int) list;
    (*mutable rb_instrmap: int list;*)
  }

type rtl_block_aux =
  { 
    mutable rbaux_funcID: int;
    mutable rbaux_label : string;
    mutable rbaux_instrs: X86_RTL.rtl_instr list;
    mutable rbaux_succs: string list;
    mutable rbaux_inter_succs : string list;
    mutable rbaux_preds: string list;
    mutable rbaux_inter_preds : string list;
    (*mutable rbaux_instrmap: int list;*)
  }

type rtl_code_region = {
  start : mword;
  rbs : rtl_block list;
}

let config_instrmap raw_instrmap =
  let work_list = List.rev raw_instrmap in
  let rec inner_config_instrmap worklist = 
    match worklist with
    | [] -> 
      []
    (*failwith "config_instrmap"*)
    | h :: t -> 
       if 0 = List.length t then [h-1]
       else
	 let first_item1 = h in
	 let configured_tail = inner_config_instrmap t in
	 let first_item2 = List.hd configured_tail in
	 let first_instr = first_item1 + first_item2 in
	 first_instr :: configured_tail
  in
  List.rev (inner_config_instrmap work_list)

(** assembly_to_rtl:
    (prefix * instr * Big_int.big_int) list -> (X86_RTL.rtl_instr list * int list **)

let assembly_to_rtl alst = 
  let simpri_methods = 
    simpri_all_methods 
  in
  let simpre_methods =
    simpre_all_methods
  in
  let rlst = List.map ( fun (pre,instr,len) -> X86_Compile.instr_to_rtl pre instr ) alst in
  let opt_rlst = List.map (optimize_rtl_ssa ssa_filters simpri_methods simpre_methods) rlst in
  let raw_instrmap = List.map (fun r -> List.length r) opt_rlst in
  let instrmap = config_instrmap raw_instrmap in
  let instrs = List.flatten opt_rlst in
  (instrs, instrmap)

(*
let rbID = ref 0
*)

let bb_to_rbaux funcID bb =
  let (instrs, _) = assembly_to_rtl bb.bb_instrs in
  {
    rbaux_funcID = funcID;
    rbaux_label = bb.bb_label;
    rbaux_instrs = instrs;
    rbaux_succs = bb.bb_succs;
    rbaux_inter_succs = bb.bb_inter_succs;
    rbaux_preds = bb.bb_preds;
    rbaux_inter_preds = [];
  }

let rbaux_to_rb idmap rbaux =
  {
    rb_ID = StrIDMap.find rbaux.rbaux_label idmap;
    rb_label = rbaux.rbaux_label;
    rb_instrs = rbaux.rbaux_instrs;
    rb_succs = List.filter_map (fun lbl -> 
      try Some (StrIDMap.find lbl idmap)
      with Not_found -> None) rbaux.rbaux_succs;
    rb_inter_succs = List.filter_map (fun lbl -> 
      try Some (StrIDMap.find lbl idmap)
      with Not_found -> None) rbaux.rbaux_inter_succs;
    rb_preds = List.filter_map (fun lbl -> 
      try Some (StrIDMap.find lbl idmap)
      with Not_found -> None) rbaux.rbaux_preds;
    rb_inter_preds = List.filter_map (fun lbl -> 
      try Some (StrIDMap.find lbl idmap)
      with Not_found -> None) rbaux.rbaux_inter_preds;
  }

(*
let translate_bbs_to_rbs bbs =
  let bb_list = BBSet.elements bbs in
  List.map (fun bb ->
    let rb = bb_to_rb bb in
    rb.rb_ID <- !rbID;
    rb
  ) bb_list
*)

let str_of_rbid rbid =
  sprintf "(%d, %d)" (fst rbid) (snd rbid)

let print_rb rb =
  printf "RTL Block ID: %s\n" (str_of_rbid rb.rb_ID); 
  printf "RTL Block Label: %s\n" rb.rb_label;
  printf "Instructions in current RTL block:\n";
  List.iter print_rtl_instr rb.rb_instrs;
  printf "Successors:\n";
  List.iter (fun rbid -> printf "%s " (str_of_rbid rbid)) rb.rb_succs;
  printf "\n";
  printf "Inter-Successors:\n";
  List.iter (fun rbid -> printf "%s " (str_of_rbid rbid)) rb.rb_inter_succs;
  printf "\n";
  printf "Predecessors:\n";
  List.iter (fun rbid -> printf "%s " (str_of_rbid rbid)) rb.rb_preds;
  printf "\n";
  printf "Inter-Predecessors:\n";
  List.iter (fun rbid -> printf "%s " (str_of_rbid rbid)) rb.rb_inter_preds;
  printf "\n"
  (*printf "Instruction map (assembly --> rtl):\n";
  let count = ref 0 in
  List.iter (fun rbind -> count := !count + 1; printf "%d --> %d\n" !count rbind) rb.rb_instrmap;
  printf "RTL Block ends.\n\n"*)
  
  
