open Smt_syntax
open Logical_formula
open X86Syntax
open X86Semantics
open Big
open Block

(** is_name_of_loc name loc :
    name -> X86_MACHINE.location -> bool **)

let rec is_name_of_loc name loc = match name with
    BasicName(str) ->  (str = (name_of_loc loc))
  | _ -> false

(** substitute_loc_in_sbe :
    X86_MACHINE.location -> smt_bit_exp -> smt_cmp_exp -> smt_cmp_exp **)

let rec substitute_loc_in_sbe loc ssbe sbe = match sbe with
    SmtBvop(sbvop, sbe1, sbe2) -> 
      let fst = substitute_loc_in_sbe loc ssbe sbe1 in
      let snd = substitute_loc_in_sbe loc ssbe sbe2 in
        SmtBvop(sbvop, fst, snd)

  | SmtIte(sce, sbe1, sbe2) -> 
      let fst = substitute_loc_in_sce loc ssbe sce in
      let snd = substitute_loc_in_sbe loc ssbe sbe1 in
      let thd = substitute_loc_in_sbe loc ssbe sbe2 in
        SmtIte(fst, snd, thd)

  | SmtConcat(sbe1, sbe2) -> 
      let e1 = substitute_loc_in_sbe loc ssbe sbe1 in
      let e2 = substitute_loc_in_sbe loc ssbe sbe2 in
        SmtConcat(e1, e2)

  | SmtExtract(d1, d2, sbe') ->
      let e = substitute_loc_in_sbe loc ssbe sbe' in
        SmtExtract(d1, d2, e)

  | SmtSExtend(d, sbe') ->
      let e= substitute_loc_in_sbe loc ssbe sbe' in
        SmtSExtend(d, e)

  | SmtSelect(barr, sbe') ->
      let arr = substitute_loc_in_barr loc ssbe barr in
      let e = substitute_loc_in_sbe loc ssbe sbe' in
        SmtSelect(arr, e)

  | SmtBvar(sz, name) when is_name_of_loc name loc -> 
      ssbe

  | _ -> sbe

(** substitute_loc_in_barr :
    X86_MACHINE.location -> smt_bit_exp -> bit_arr -> bit_arr **)

and substitute_loc_in_barr loc sbe barr = match barr with
    BasicArr(_) -> barr
  | SmtStore(barr', sbe1, sbe2) -> 
      let arr = substitute_loc_in_barr loc sbe barr' in
      let e1 = substitute_loc_in_sbe loc sbe sbe1 in
      let e2 = substitute_loc_in_sbe loc sbe sbe2 in
        SmtStore(arr, e1, e2)

(** substitute_loc_in_sce :
    X86_MACHINE.location -> smt_bit_exp -> smt_cmp_exp -> smt_cmp_exp **)

and substitute_loc_in_sce loc sbe sce = match sce with
    SmtCompare(scmp, sbe1', sbe2') ->
      let e1 = substitute_loc_in_sbe loc sbe sbe1' in
      let e2 = substitute_loc_in_sbe loc sbe sbe2' in
        SmtCompare(scmp, e1, e2)

(** substitute_loc_in_lfs :
    X86_MACHINE.location -> smt_bit_exp -> logical_formula_S -> logical_formula_S **)

let rec substitute_loc_in_lfs loc sbe lfs = match lfs with
    LfsExp(sce) -> LfsExp (substitute_loc_in_sce loc sbe sce)
  | LfsNot(lfs') -> LfsNot (substitute_loc_in_lfs loc sbe lfs')
  | LfsAnd(lfs1, lfs2) -> 
      let fst = substitute_loc_in_lfs loc sbe lfs1 in
      let snd = substitute_loc_in_lfs loc sbe lfs2 in
      LfsAnd(fst, snd)
  | LfsOr(lfs1, lfs2) -> 
      let fst = substitute_loc_in_lfs loc sbe lfs1 in
      let snd = substitute_loc_in_lfs loc sbe lfs2 in
      LfsOr(fst, snd)
  | LfsImply(lfs1, lfs2) -> 
      let fst = substitute_loc_in_lfs loc sbe lfs1 in
      let snd = substitute_loc_in_lfs loc sbe lfs2 in
      LfsImply(fst, snd)
  | _ -> lfs

(** substitute_arr_in_sbe :
    bit_arr -> smt_bit_exp -> smt_bit_exp -> smt_cmp_exp -> smt_cmp_exp **)

let rec substitute_arr_in_sbe barr ssbe1 ssbe2 sbe = match sbe with
    SmtBvop(sbvop, sbe1, sbe2) -> 
      let fst = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe1 in
      let snd = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe2 in
        SmtBvop(sbvop, fst, snd)

  | SmtIte(sce, sbe1, sbe2) -> 
      let fst = substitute_arr_in_sce barr ssbe1 ssbe2 sce in
      let snd = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe1 in
      let thd = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe2 in
        SmtIte(fst, snd, thd)

  | SmtConcat(sbe1, sbe2) -> 
      let e1 = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe1 in
      let e2 = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe2 in
        SmtConcat(e1, e2)

  | SmtExtract(d1, d2, sbe') ->
      let e = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe' in
        SmtExtract(d1, d2, e)

  | SmtSExtend(d, sbe') ->
      let e= substitute_arr_in_sbe barr ssbe1 ssbe2 sbe' in
        SmtSExtend(d, e)

  | SmtSelect(barr', sbe') ->
      let arr = substitute_arr_in_barr barr ssbe1 ssbe2 barr' in
      let e = substitute_arr_in_sbe barr ssbe1 ssbe2 sbe' in
        SmtSelect(arr, e)

  | _ -> sbe

(** substitute_arr_in_barr :
    bit_arr -> smt_bit_exp -> smt_bit_exp -> bit_arr -> bit_arr **)

and substitute_arr_in_barr barr sbe1 sbe2 barr' = match barr, barr' with
    BasicArr(BasicName("Fpu_drarray")), BasicArr(BasicName("Fpu_drarray")) ->
      SmtStore(barr', sbe1, sbe2)
  | BasicArr(BasicName("Fpu_tarray")), BasicArr(BasicName("Fpu_tarray")) ->
      SmtStore(barr', sbe1, sbe2)
  | BasicArr(BasicName("Mem")), BasicArr(BasicName("Mem")) ->
      SmtStore(barr', sbe1, sbe2)
  | _, SmtStore(barr'', sbe1', sbe2') -> 
      let arr = substitute_arr_in_barr barr sbe1 sbe2 barr'' in
      let e1 = substitute_arr_in_sbe barr sbe1 sbe2 sbe1' in
      let e2 = substitute_arr_in_sbe barr sbe1 sbe2 sbe2' in
        SmtStore(arr, e1, e2)
  | _ -> barr'

(** substitute_arr_in_sce :
    bit_arr -> smt_bit_exp -> smt_bit_exp -> smt_cmp_exp -> smt_cmp_exp **)

and substitute_arr_in_sce barr sbe1 sbe2 sce = match sce with
    SmtCompare(scmp, sbe1', sbe2') ->
      let e1 = substitute_arr_in_sbe barr sbe1 sbe2 sbe1' in
      let e2 = substitute_arr_in_sbe barr sbe1 sbe2 sbe2' in
        SmtCompare(scmp, e1, e2)

(** substitute_arr_in_lfs :
    bit_arr -> smt_bit_exp -> smt_bit_exp -> logical_formula_S -> logical_formula_S **)

let rec substitute_arr_in_lfs barr sbe1 sbe2 lfs = match lfs with
    LfsExp(sce) -> LfsExp (substitute_arr_in_sce barr sbe1 sbe2 sce)
  | LfsNot(lfs') -> LfsNot (substitute_arr_in_lfs barr sbe1 sbe2 lfs')
  | LfsAnd(lfs1, lfs2) -> 
      let fst = substitute_arr_in_lfs barr sbe1 sbe2 lfs1 in
      let snd = substitute_arr_in_lfs barr sbe1 sbe2 lfs2 in
      LfsAnd(fst, snd)
  | LfsOr(lfs1, lfs2) -> 
      let fst = substitute_arr_in_lfs barr sbe1 sbe2 lfs1 in
      let snd = substitute_arr_in_lfs barr sbe1 sbe2 lfs2 in
      LfsOr(fst, snd)
  | LfsImply(lfs1, lfs2) -> 
      let fst = substitute_arr_in_lfs barr sbe1 sbe2 lfs1 in
      let snd = substitute_arr_in_lfs barr sbe1 sbe2 lfs2 in
      LfsImply(fst, snd)
  | _ -> lfs

(** substitute_rvar_in_sbe :
    smt_bit_exp -> smt_bit_exp **)

let rec substitute_rvar_in_sbe sbe = match sbe with
    SmtBvop(sbvop, sbe1, sbe2) -> 
      let fst = substitute_rvar_in_sbe sbe1 in
      let snd = substitute_rvar_in_sbe sbe2 in
        SmtBvop(sbvop, fst, snd)

  | SmtIte(sce, sbe1, sbe2) -> 
      let fst = substitute_rvar_in_sce sce in
      let snd = substitute_rvar_in_sbe sbe1 in
      let thd = substitute_rvar_in_sbe sbe2 in
        SmtIte(fst, snd, thd)

  | SmtConcat(sbe1, sbe2) -> 
      let e1 = substitute_rvar_in_sbe sbe1 in
      let e2 = substitute_rvar_in_sbe sbe2 in
        SmtConcat(e1, e2)

  | SmtExtract(d1, d2, sbe') ->
      let e = substitute_rvar_in_sbe sbe' in
        SmtExtract(d1, d2, e)

  | SmtSExtend(d, sbe') ->
      let e= substitute_rvar_in_sbe sbe' in
        SmtSExtend(d, e)

  | SmtSelect(barr, sbe') ->
      let arr = substitute_rvar_in_barr barr in
      let e = substitute_rvar_in_sbe sbe' in
        SmtSelect(arr, e)

  | SmtBvar(sz, IndexedName(BasicName "rvar", i)) ->
        SmtBvar(sz, IndexedName(BasicName "rvar", i+1))

  | _ -> sbe

and substitute_rvar_in_barr barr = match barr with
    BasicArr(_) -> barr
  | SmtStore(barr', sbe1, sbe2) ->
      let barr'' = substitute_rvar_in_barr barr' in
      let e1 = substitute_rvar_in_sbe sbe1 in
      let e2 = substitute_rvar_in_sbe sbe2 in
        SmtStore(barr'', e1, e2)

(** substitute_rvar_in_sce : 
    smt_cmp_exp -> smt_cmp_exp **)

and substitute_rvar_in_sce sce = match sce with
    SmtCompare(scmp, sbe1, sbe2) ->
      let e1 = substitute_rvar_in_sbe sbe1 in
      let e2 = substitute_rvar_in_sbe sbe2 in
        SmtCompare(scmp, e1, e2)

(** substitute_rvar_in_lfs :
    logical_formula_S -> logical_formula_S **)

let rec substitute_rvar_in_lfs lfs = match lfs with
    LfsExp(sce) -> LfsExp (substitute_rvar_in_sce sce)
  | LfsNot(lfs') -> LfsNot (substitute_rvar_in_lfs lfs')
  | LfsAnd(lfs1, lfs2) -> 
      let fst = substitute_rvar_in_lfs lfs1 in
      let snd = substitute_rvar_in_lfs lfs2 in
        LfsAnd(fst, snd)
  | LfsOr(lfs1, lfs2) -> 
      let fst = substitute_rvar_in_lfs lfs1 in
      let snd = substitute_rvar_in_lfs lfs2 in
        LfsOr(fst, snd)
  | LfsImply(lfs1, lfs2) -> 
      let fst = substitute_rvar_in_lfs lfs1 in
      let snd = substitute_rvar_in_lfs lfs2 in
        LfsImply(fst, snd)
  | _ -> lfs

(** wp_ri :
    X86_RTL.rtl_instr -> logical_formula_S -> logical_formula_S **)

let rec wp_ri ri lfs = match ri with
    X86_RTL.Coq_set_loc_rtl(s,re,loc) -> 
      let sbe = translate_re re in
        substitute_loc_in_lfs loc sbe lfs

  | X86_RTL.Coq_set_array_rtl(s1,s2,arr,re1,re2) -> 
      let sbe1 = translate_re re1 in
      let sbe2 = translate_re re2 in
        substitute_arr_in_lfs (BasicArr(BasicName(name_of_arr arr))) sbe1 sbe2 lfs

  | X86_RTL.Coq_set_byte_rtl(re1,re2) -> 
      let sbe1 = translate_re re2 in
      let sbe2 = translate_re re1 in
        substitute_arr_in_lfs (BasicArr(BasicName("Mem"))) sbe1 sbe2 lfs

  | X86_RTL.Coq_advance_oracle_rtl -> substitute_rvar_in_lfs lfs

  | X86_RTL.Coq_if_rtl(X86_RTL.Coq_test_rtl_exp(s,top,re1,re2), ri') ->
      let sbe1 = translate_re re1 in
      let sbe2 = translate_re re2 in
      let fst = LfsImply(LfsNot(LfsExp(SmtCompare(top, sbe1, sbe2))), lfs) in
      let snd = LfsImply(LfsExp(SmtCompare(top, sbe1, sbe2)), wp_ri ri' lfs) in
        LfsAnd(fst, snd)

  | X86_RTL.Coq_if_rtl(re, ri') -> 
      let sbe = translate_re re in
      let fst = LfsImply (LfsNot(LfsExp(SmtCompare(X86_RTL.Coq_eq_op, sbe, SmtBv(of_int 0, of_int 1)))), lfs) in
      let snd = LfsImply (LfsExp(SmtCompare(X86_RTL.Coq_eq_op, sbe, SmtBv(of_int 0, of_int 1))), wp_ri ri' lfs) in
        LfsAnd(fst, snd)

  | X86_RTL.Coq_error_rtl -> LfsFalse

  | X86_RTL.Coq_trap_rtl -> LfsTrue

(** wp :
    X86_RTL.rtl_instr list -> logical_formula_S -> logical_formula_S **)

let wp lst_ri postcondition =
  List.fold_right (wp_ri) lst_ri postcondition

(** wp_ai :
    prefix * instr * int -> logical_formula_S -> logical_formula_S **)

let rec wp_ai ai lfs = 
  let lst_ri = X86_Compile.instr_to_rtl (t_fst ai) (t_snd ai) in
  wp lst_ri lfs

(** wpa :
    (prefix * instr * int) list -> logical_formula_S -> logical_formula_S **)

let wpa lst_ai postcondition =
  List.fold_right (wp_ai) lst_ai postcondition




