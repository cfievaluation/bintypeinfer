open Smt_syntax
open X86Syntax
open X86Semantics

type cmpop = X86_RTL.test_op

type logical_formula_S = 
    LfsTrue
  | LfsFalse
  | LfsExp of smt_cmp_exp
  | LfsNot of logical_formula_S
  | LfsAnd of logical_formula_S * logical_formula_S
  | LfsOr of logical_formula_S * logical_formula_S
  | LfsImply of logical_formula_S * logical_formula_S



(* Printing Logical Formula with Intermediate Expressions*)

let rec print_lfs lfs = match lfs with
    LfsTrue -> print_string "True";
  | LfsFalse -> print_string "False";
  | LfsExp(sce) -> 
      print_sce sce;
  | LfsNot(lfs1) ->
      print_string "(Not "; 
      print_lfs lfs1;
      print_string ")"; 
  | LfsAnd(lfs1, lfs2) -> 
      print_string "(";
      print_lfs lfs1;
      print_string " /\\ "; 
      print_lfs lfs2;
      print_string ")"; 
  | LfsOr(lfs1, lfs2) -> 
      print_string "(";
      print_lfs lfs1;
      print_string " \\/ "; 
      print_lfs lfs2;
      print_string ")"; 
  | LfsImply(lfs1, lfs2) -> 
      print_string "(";
      print_lfs lfs1;
      print_string " => "; 
      print_lfs lfs2;
      print_string ")";
