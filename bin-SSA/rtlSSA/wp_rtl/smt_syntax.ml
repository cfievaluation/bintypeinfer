open X86Syntax
open X86Semantics
open Big

type size = Big.big_int

type dec = Big.big_int

type smt_cmp_op = X86_RTL.test_op

type name = 
    BasicName of string
  | IndexedName of name * int

type smt_bv_op =
    Bvadd
  | Bvsub
  | Bvmul
  | Bvsdiv
  | Bvudiv
  | Bvumod
  | Bvsmod
  | Bvand
  | Bvor
  | Bvxor
  | Bvshl
  | Bvashr
  | Bvlshr

type bit_arr = 
    BasicArr of name
  | SmtStore of bit_arr * smt_bit_exp * smt_bit_exp

and smt_cmp_exp = SmtCompare of smt_cmp_op * smt_bit_exp * smt_bit_exp

and smt_bit_exp =
    SmtBv of size * dec
  | SmtBvar of size * name
  | SmtSelect of bit_arr * smt_bit_exp
  | SmtBvop of smt_bv_op * smt_bit_exp * smt_bit_exp
  | SmtConcat of smt_bit_exp * smt_bit_exp
  | SmtExtract of dec * dec * smt_bit_exp
  | SmtSExtend of dec * smt_bit_exp
  | SmtIte of smt_cmp_exp * smt_bit_exp * smt_bit_exp
  | SmtError


(*type smt_exp = 
    SmtCmpExp of smt_cmp_exp
  | SmtBitExp of smt_bit_exp
  | SmtError*)

(* Translation from RTL syntax to Intermediate Representation *)

let name_of_register reg = match reg with 
    EAX ->  "EAX"
  | ECX ->  "ECX"
  | EDX ->  "EDX"
  | EBX ->  "EBX"
  | ESP ->  "ESP"
  | EBP ->  "EBP"
  | ESI ->  "ESI"
  | EDI ->  "EDI"

let name_of_segment_register segreg= match segreg with
    ES ->  "ES"
  | CS ->  "CS"
  | SS ->  "SS"
  | DS ->  "DS"
  | FS ->  "FS"
  | GS ->  "GS"

let name_of_flag f = match f with
    X86_MACHINE.ID ->  "ID"
  | X86_MACHINE.VIP ->  "VIP"
  | X86_MACHINE.VIF ->  "VIF"
  | X86_MACHINE.AC ->  "AC"
  | X86_MACHINE.VM ->  "VM"
  | X86_MACHINE.RF ->  "RF"
  | X86_MACHINE.NT ->  "NT"
  | X86_MACHINE.IOPL ->  "IOPL"
  | X86_MACHINE.OF ->  "OF"
  | X86_MACHINE.DF ->  "DF"
  | X86_MACHINE.IF_flag ->  "IF_flag"
  | X86_MACHINE.TF ->  "TF"
  | X86_MACHINE.SF ->  "SF"
  | X86_MACHINE.ZF ->  "ZF"
  | X86_MACHINE.AF ->  "AF"
  | X86_MACHINE.PF ->  "PF"
  | X86_MACHINE.CF ->  "CF"

let name_of_control_register ctrlseg = match ctrlseg with
    CR0 ->  "CR0"
  | CR2 ->  "CR2"
  | CR3 ->  "CR3"
  | CR4 ->  "CR4"

let name_of_debug_register dbgreg = match dbgreg with
    DR0 ->  "DR0"
  | DR1 ->  "DR1"
  | DR2 ->  "DR2"
  | DR3 ->  "DR3"
  | DR6 ->  "DR6"
  | DR7 ->  "DR7"

let name_of_fpu_flag ff = match ff with
    X86_MACHINE.F_Busy ->  "F_Busy"
  | X86_MACHINE.F_C3 ->  "F_C3"
  | X86_MACHINE.F_C2 ->  "F_C2"
  | X86_MACHINE.F_C1 ->  "F_C1"
  | X86_MACHINE.F_C0 ->  "F_C0"
  | X86_MACHINE.F_ES ->  "F_ES"
  | X86_MACHINE.F_SF ->  "F_SF"
  | X86_MACHINE.F_PE ->  "F_PE"
  | X86_MACHINE.F_UE ->  "F_UE"
  | X86_MACHINE.F_OE ->  "F_OE"
  | X86_MACHINE.F_ZE ->  "F_ZE"
  | X86_MACHINE.F_DE ->  "F_DE"
  | X86_MACHINE.F_IE ->  "F_IE"

let name_of_fpu_ctrl_flag fcf = match fcf with
    X86_MACHINE.F_Res15 ->  "F_Res15"
  | X86_MACHINE.F_Res14 ->  "F_Res14"
  | X86_MACHINE.F_Res13 ->  "F_Res13"
  | X86_MACHINE.F_Res7 ->  "F_Res7"
  | X86_MACHINE.F_Res6 ->  "F_Res6"
  | X86_MACHINE.F_IC ->  "F_IC"
  | X86_MACHINE.F_PM ->  "F_PM"
  | X86_MACHINE.F_UM ->  "F_UM"
  | X86_MACHINE.F_OM ->  "F_OM"
  | X86_MACHINE.F_ZM ->  "F_ZM"
  | X86_MACHINE.F_DM ->  "F_DM"
  | X86_MACHINE.F_IM ->  "F_IM"

let name_of_loc loc = match loc with
    X86_MACHINE.Coq_reg_loc(reg) -> name_of_register reg
  | X86_MACHINE.Coq_seg_reg_start_loc(segreg) -> String.concat "" ["Start"; name_of_segment_register segreg]
  | X86_MACHINE.Coq_seg_reg_limit_loc(segreg) -> String.concat "" ["Limit"; name_of_segment_register segreg]
  | X86_MACHINE.Coq_flag_loc(f) -> name_of_flag f
  | X86_MACHINE.Coq_control_register_loc(ctrlreg) -> name_of_control_register ctrlreg
  | X86_MACHINE.Coq_debug_register_loc (dbgreg) -> name_of_debug_register dbgreg
  | X86_MACHINE.Coq_pc_loc -> "PC"
  | X86_MACHINE.Coq_fpu_stktop_loc -> "Fpu_stktop"
  | X86_MACHINE.Coq_fpu_flag_loc(ff) -> name_of_fpu_flag ff
  | X86_MACHINE.Coq_fpu_rctrl_loc -> "Fpu_rctrl"
  | X86_MACHINE.Coq_fpu_pctrl_loc -> "Fpu_pctrl"
  | X86_MACHINE.Coq_fpu_ctrl_flag_loc(fcf) -> name_of_fpu_ctrl_flag fcf
  | X86_MACHINE.Coq_fpu_lastInstrPtr_loc -> "Fpu_lastInstrPtr"
  | X86_MACHINE.Coq_fpu_lastDataPtr_loc -> "Fpu_lastDataPtr"
  | X86_MACHINE.Coq_fpu_lastOpcode_loc -> "Fpu_lastOpcode"

let name_of_arr arr = match arr with
    X86_MACHINE.Coq_fpu_datareg -> "Fpu_drarray"
  | X86_MACHINE.Coq_fpu_tag -> "Fpu_tarray"

let translate_bvop bvop = try 
  (match bvop with
    X86_RTL.Coq_add_op -> Bvadd
  | X86_RTL.Coq_sub_op -> Bvsub
  | X86_RTL.Coq_mul_op -> Bvmul
  | X86_RTL.Coq_divs_op -> Bvsdiv
  | X86_RTL.Coq_divu_op -> Bvudiv
  | X86_RTL.Coq_modu_op -> Bvumod
  | X86_RTL.Coq_mods_op -> Bvsmod
  | X86_RTL.Coq_and_op -> Bvand
  | X86_RTL.Coq_or_op -> Bvor
  | X86_RTL.Coq_xor_op -> Bvxor
  | X86_RTL.Coq_shl_op -> Bvshl
  | X86_RTL.Coq_shr_op -> Bvashr
  | X86_RTL.Coq_shru_op -> Bvlshr
  | _ -> raise (Invalid_argument "No ror or rol")
  ) with 
    Invalid_argument arg ->
      print_string arg;
      exit 0

(** val translate_re :
    X86_RTL.rtl_exp -> smt_bit_exp **)

let rec translate_re re = match re with
    X86_RTL.Coq_arith_rtl_exp(s,X86_RTL.Coq_ror_op,re1,re2) ->
      let sbe1 = translate_re re1 in
      let sbe2 = translate_re re2 in
      let actual_digits = SmtBvop(Bvsmod, sbe2, SmtBv(s, Big.succ s)) in
      let left_size = SmtBvop(Bvsub, SmtBv(s, Big.succ s), actual_digits) in
      let fst = SmtBvop(Bvshl, sbe1, left_size) in
      let snd = SmtBvop(Bvlshr, sbe1, actual_digits) in
        SmtBvop(Bvor, fst, snd)

  | X86_RTL.Coq_arith_rtl_exp(s,X86_RTL.Coq_rol_op,re1,re2) ->
      let sbe1 = translate_re re1 in
      let sbe2 = translate_re re2 in
      let actual_digits = SmtBvop(Bvsmod, sbe2, SmtBv(s, Big.succ s)) in
      let right_size = SmtBvop(Bvsub, SmtBv(s, Big.succ s), actual_digits) in
      let fst = SmtBvop(Bvshl, sbe1, actual_digits) in
      let snd = SmtBvop(Bvlshr, sbe1, right_size) in
        SmtBvop(Bvor, fst, snd)

  | X86_RTL.Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      let sbvop = translate_bvop bvop in
      let sbe1 = translate_re re1 in
      let sbe2 = translate_re re2 in
        SmtBvop(sbvop, sbe1, sbe2)

  | X86_RTL.Coq_test_rtl_exp(s,top,re1,re2) -> 
      let se1 = translate_re re1 in
      let se2 = translate_re re2 in
        SmtIte(SmtCompare(top, se1, se2), SmtBv(of_int 0, of_int 1), SmtBv(of_int 0, of_int 0))

  | X86_RTL.Coq_if_rtl_exp(s,re1,re2,re3) -> 
      let se1 = translate_re re1 in
      let se2 = translate_re re2 in
      let se3 = translate_re re3 in
        SmtIte(SmtCompare(X86_RTL.Coq_eq_op, se1, SmtBv(of_int 0, of_int 1)), se2, se3)

  | X86_RTL.Coq_cast_s_rtl_exp(s1,s2,re) -> 
      let se = translate_re re in
	if Big.lt s1 s2 then  
          SmtSExtend(Big.sub s2 s1, se)
        else 
          SmtExtract(s2, Big.zero, se)

  | X86_RTL.Coq_cast_u_rtl_exp(s1,s2,re) -> 
      let se = translate_re re in
	if Big.lt s1 s2 then  
          SmtConcat(SmtBv(Big.pred(Big.sub s2 s1), Big.zero), se)
        else 
          SmtExtract(s2, Big.zero, se)

  | X86_RTL.Coq_imm_rtl_exp(s,v) ->
        SmtBv(s, v)

  | X86_RTL.Coq_get_loc_rtl_exp(s,loc) -> 
        SmtBvar(s, BasicName(name_of_loc loc))

  | X86_RTL.Coq_get_array_rtl_exp(s1,s2,arr,re) -> 
      let se = translate_re re in
        SmtSelect(BasicArr(BasicName(name_of_arr arr)), se)

  | X86_RTL.Coq_get_byte_rtl_exp(re) ->
      let se = translate_re re in
        SmtSelect(BasicArr(BasicName "Mem"), se)

  | X86_RTL.Coq_get_random_rtl_exp(s) ->
        SmtBvar(s, IndexedName (BasicName "rvar", 0))

  | _ -> SmtError




(* Printing Intermediate Expressions*)

let name_of_sbvop sbvop = match sbvop with
    Bvadd -> "bvadd"
  | Bvsub -> "bvsub"
  | Bvmul -> "bvmul"
  | Bvsdiv -> "bvsdiv"
  | Bvudiv -> "bvudiv"
  | Bvumod -> "bvumod"
  | Bvsmod -> "bvsmod"
  | Bvand -> "bvand"
  | Bvor -> "bvor"
  | Bvxor -> "bvxor"
  | Bvshl -> "bvshl"
  | Bvashr -> "bvashr"
  | Bvlshr -> "bvlshr"

let name_of_scmp scmp = match scmp with
    X86_RTL.Coq_eq_op -> "="
  | X86_RTL.Coq_lt_op -> "bvslt"
  | X86_RTL.Coq_ltu_op -> "bvult"

let to_size s str = 
  let l = String.length str in
  let size = to_int s in
  let add_digits = size - l in
  let str' = String.make add_digits '0' in
    String.concat "" [str';str] 

let rec to_binary_string bstr bi = 
  let (new_bi, current_bit_bi) = quomod bi two in
  let current_bit = to_string current_bit_bi in
  let bitstr = String.concat "" [current_bit; bstr] in
  (if eq new_bi zero then bitstr else (to_binary_string bitstr new_bi))

let dec_to_binary_string s d = 
  to_size (Big.succ s) (to_binary_string "" (Bits.Word.repr s d))

let print_binary s d = print_string (dec_to_binary_string s d)

let rec print_name name = match name with
    BasicName(str) -> print_string str;
  | IndexedName(n, i) -> 
    print_name n; 
    print_int i 

let print_size sz = print_string (Big.to_string (Big.succ sz))

let print_sbvop sbvop = print_string (name_of_sbvop sbvop)

let print_scmp scmp = print_string (name_of_scmp scmp)

let print_dec d = print_string (Big.to_string d)

let rec print_barr barr = match barr with
    BasicArr(n) -> print_name n
  | SmtStore(barr', sbe1, sbe2) -> 
      print_string "(store(";
      print_barr barr';
      print_string " ";
      print_sbe sbe1;
      print_string " ";
      print_sbe sbe2;
      print_string ")";

and print_sbe sbe = match sbe with
    SmtBv(sz, d) -> 
      print_string "#b";
      print_binary sz d;
  | SmtBvar(sz, n) -> 
      print_string "(_ ";
      print_name n;
      print_string " ";
      print_size sz;
      print_string ")";
  | SmtSelect(barr, sbe') -> 
      print_string "(select ";
      print_barr barr;
      print_sbe sbe';
      print_string ")";
  | SmtBvop(sbvop, sbe1, sbe2) ->
      print_string "(";
      print_sbvop sbvop;
      print_string " ";
      print_sbe sbe1;
      print_string " ";
      print_sbe sbe2;
      print_string ")";
  | SmtConcat(sbe1, sbe2) -> 
      print_string "(concat ";
      print_sbe sbe1;
      print_string " ";
      print_sbe sbe2;
      print_string ")";
  | SmtExtract(d1, d2, sbe') ->
      print_string "((_ extract ";
      print_dec d1;
      print_string " ";
      print_dec d2;
      print_string ") ";
      print_sbe sbe';
      print_string ")";
  | SmtSExtend(d, sbe') -> 
      print_string "((_ sign_extend ";
      print_dec d;
      print_string ") ";
      print_sbe sbe';
      print_string ")";
  | SmtIte(sce, sbe1, sbe2) -> 
      print_string "(ite ";
      print_sce sce;
      print_string " ";
      print_sbe sbe1;
      print_string " ";
      print_sbe sbe2;
      print_string ")"
  | SmtError ->
      (*This should raise exception. Implement later on.*)
      print_string "Error: Doesn't support float_arith for now!"


and print_sce sce = match sce with
    SmtCompare(scmp, sbe1, sbe2) -> 
      print_string "(";
      print_scmp scmp;
      print_string " ";
      print_sbe sbe1;
      print_string " ";
      print_sbe sbe2;
      print_string ")"

(*let print_se se = match se with
    SmtBitExp(sbe) -> print_sbe sbe;
  | SmtCmpExp(sce) -> print_sce sce;
  | SmtError -> 
      (* This should raise exception. Implement later on. *)
      print_string "Error: Doesn't support float_arith for now!"
*)















