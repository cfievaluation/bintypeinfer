open BinInt
open Bits
open Bool
open Datatypes

module X86_PARSER_ARG =
 struct
  type char_p = bool

  (** val char_dec : char_p -> char_p -> bool **)

  let char_dec =
    bool_dec

  (** val char_cmp : char_p -> char_p -> comparison **)

  let char_cmp c1 c2 =
    if c1 then if c2 then Eq else Gt else if c2 then Lt else Eq

  type tipe =
  | Int_t
  | Register_t
  | BitVector_t of Big.big_int
  | Scale_t
  | Condition_t
  | Address_t
  | Operand_t
  | Reg_or_Immed_t
  | Fp_Debug_Register_t
  | Fp_Operand_t
  | Fp_Condition_t
  | MMX_Granularity_t
  | MMX_Operand_t
  | SSE_Operand_t
  | I_Instr1_t
  | I_Instr2_t
  | I_Instr3_t
  | I_Instr4_t
  | I_Instr5_t
  | I_Instr6_t
  | F_Instr1_t
  | F_Instr2_t
  | M_Instr_t
  | S_Instr1_t
  | S_Instr2_t
  | Instruction_t
  | Control_Register_t
  | Debug_Register_t
  | Segment_Register_t
  | Lock_or_Rep_t
  | Bool_t
  | Prefix_t
  | UPair_t of tipe * tipe

  type user_type = tipe

  (** val byte_explode : int8 -> bool list **)

  let byte_explode b =
    let bs =
      Word.bits_of_Z (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ Big.zero)))))))) b
    in
    (bs (Big.doubleplusone (Big.doubleplusone Big.one))) :: ((bs (Big.double
                                                               (Big.doubleplusone
                                                               Big.one))) :: (
    (bs (Big.doubleplusone (Big.double Big.one))) :: ((bs (Big.double
                                                        (Big.double
                                                        Big.one))) :: (
    (bs (Big.doubleplusone Big.one)) :: ((bs (Big.double Big.one)) :: (
    (bs Big.one) :: ((bs Big.zero) :: [])))))))

  (** val nat_explode : Big.big_int -> bool list **)

  let nat_explode n =
    byte_explode (Word.repr size8 (Z.of_nat n))

  type token_id = Big.big_int

  (** val num_tokens : token_id **)

  let num_tokens =
    Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ
      Big.zero)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

  (** val token_id_to_chars : token_id -> char_p list **)

  let token_id_to_chars =
    nat_explode
 end
