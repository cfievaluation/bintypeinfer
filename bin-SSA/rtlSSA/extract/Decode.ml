open BinInt
open Bits
open Coqlib
open Datatypes
open Grammar
open GrammarType
open List0
open X86Syntax

(** val alt : grammar -> grammar -> grammar **)

let alt p1 p2 =
  Map ((fun x ->
    match Obj.magic x with
    | Coq_inl a -> a
    | Coq_inr b -> b), (Alt (p1, p2)))

(** val alts0 : grammar list -> grammar **)

let rec alts0 = function
| [] -> Zero
| p :: rest ->
  (match rest with
   | [] -> p
   | _ :: _ -> alt p (alts0 rest))

(** val half : 'a1 list -> 'a1 list -> 'a1 list -> 'a1 list * 'a1 list **)

let rec half xs ys zs =
  match xs with
  | [] -> (ys, zs)
  | h :: t -> half t zs (h :: ys)

(** val alts' : Big.big_int -> grammar list -> grammar **)

let rec alts' n ps =
  Big.nat_case
    (fun _ ->
    alts0 ps)
    (fun n0 ->
    match ps with
    | [] -> Zero
    | p :: l ->
      (match l with
       | [] -> p
       | _ :: _ ->
         let (ps1, ps2) = half ps [] [] in
         let g1 = alts' n0 ps1 in let g2 = alts' n0 ps2 in alt g1 g2))
    n

(** val alts : grammar list -> grammar **)

let alts ps =
  alts' (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
    (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
    (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
    Big.zero)))))))))))))))))))) ps

(** val map : grammar -> (interp -> interp) -> grammar **)

let map p f =
  Map (f, p)

(** val seq : grammar -> grammar -> grammar **)

let seq p1 p2 =
  Cat (p1, p2)

(** val field' : Big.big_int -> grammar **)

let rec field' n =
  Big.nat_case
    (fun _ ->
    Eps)
    (fun n0 -> Cat (Any,
    (field' n0)))
    n

(** val bits2Z : Big.big_int -> Big.big_int -> interp -> interp **)

let rec bits2Z n a x =
  Big.nat_case
    (fun _ ->
    Obj.magic a)
    (fun n0 ->
    bits2Z n0
      (Z.add (Z.mul (Big.double Big.one) a)
        (if fst (Obj.magic x) then Big.one else Big.zero))
      (snd (Obj.magic x)))
    n

(** val bits2int : Big.big_int -> interp -> interp **)

let bits2int n bs =
  bits2Z n Big.zero bs

(** val bits : char list -> grammar **)

let rec bits = function
| [] -> Eps
| c::s -> Cat ((Char (if (=) c '0' then false else true)), (bits s))

(** val bitsleft : char list -> grammar -> grammar **)

let bitsleft s p =
  map (seq (bits s) p) (Obj.magic snd)

(** val field : Big.big_int -> grammar **)

let field n =
  map (field' n) (bits2int n)

(** val reg : grammar **)

let reg =
  map (field (Big.succ (Big.succ (Big.succ Big.zero))))
    (Obj.magic coq_Z_to_register)

(** val sig_of_bitsn : Big.big_int -> interp -> Big.big_int -> bool **)

let rec sig_of_bitsn n x =
  Big.nat_case
    (fun _ _ ->
    false)
    (fun n' ->
    let f' = sig_of_bitsn n' (snd (Obj.magic x)) in
    (fun x0 -> if zeq x0 (Z.of_nat n') then fst (Obj.magic x) else f' x0))
    n

(** val intn_of_sig : Big.big_int -> (Big.big_int -> bool) -> Word.int **)

let intn_of_sig n f =
  Word.coq_Z_of_bits (Big.succ n) f

(** val intn_of_bitsn : Big.big_int -> interp -> Word.int **)

let intn_of_bitsn n bs =
  intn_of_sig n (sig_of_bitsn (Big.succ n) bs)

(** val field_intn : Big.big_int -> grammar **)

let field_intn n =
  map (field' (Big.succ n)) (Obj.magic intn_of_bitsn n)

(** val int_n : Big.big_int -> grammar **)

let int_n n =
  map (field (Big.succ n)) (Obj.magic Word.repr n)

(** val fpu_reg : grammar **)

let fpu_reg =
  map (field (Big.succ (Big.succ (Big.succ Big.zero))))
    (Obj.magic Word.repr (Big.succ (Big.succ Big.zero)))

(** val mmx_reg : grammar **)

let mmx_reg =
  field_intn (Big.succ (Big.succ Big.zero))

(** val sse_reg : grammar **)

let sse_reg =
  field_intn (Big.succ (Big.succ Big.zero))

(** val byte : grammar **)

let byte =
  int_n (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
    Big.zero)))))))

(** val halfword : grammar **)

let halfword =
  map (seq byte byte) (fun p ->
    let b0 =
      Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ Big.zero))))))))))))))) (fst (Obj.magic p))
    in
    let b1 =
      Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ Big.zero))))))))))))))) (snd (Obj.magic p))
    in
    Obj.magic Word.coq_or (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ Big.zero)))))))))))))))
      (Word.shl (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ Big.zero))))))))))))))) b1
        (Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ Big.zero)))))))))))))))
          (Big.double (Big.double (Big.double Big.one))))) b0)

(** val word : grammar **)

let word =
  map (seq byte (seq byte (seq byte byte))) (fun p ->
    let b0 = zero_extend8_32 (fst (Obj.magic p)) in
    let b1 = zero_extend8_32 (fst (snd (Obj.magic p))) in
    let b2 = zero_extend8_32 (fst (snd (snd (Obj.magic p)))) in
    let b3 = zero_extend8_32 (snd (snd (snd (Obj.magic p)))) in
    let w1 =
      Word.shl (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ
        Big.zero))))))))))))))))))))))))))))))) b1
        (Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ Big.zero)))))))))))))))))))))))))))))))
          (Big.double (Big.double (Big.double Big.one))))
    in
    let w2 =
      Word.shl (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ
        Big.zero))))))))))))))))))))))))))))))) b2
        (Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ Big.zero)))))))))))))))))))))))))))))))
          (Big.double (Big.double (Big.double (Big.double Big.one)))))
    in
    let w3 =
      Word.shl (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ
        Big.zero))))))))))))))))))))))))))))))) b3
        (Word.repr (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ Big.zero)))))))))))))))))))))))))))))))
          (Big.double (Big.double (Big.double (Big.doubleplusone Big.one)))))
    in
    Obj.magic Word.coq_or (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
      Big.zero))))))))))))))))))))))))))))))) w3
      (Word.coq_or (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
        Big.zero))))))))))))))))))))))))))))))) w2
        (Word.coq_or (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ (Big.succ
          (Big.succ (Big.succ Big.zero))))))))))))))))))))))))))))))) w1 b0)))

(** val scale_p : grammar **)

let scale_p =
  map (field (Big.succ (Big.succ Big.zero))) (Obj.magic coq_Z_to_scale)

(** val tttn : grammar **)

let tttn =
  map (field (Big.succ (Big.succ (Big.succ (Big.succ Big.zero)))))
    (Obj.magic coq_Z_to_condition_type)

(** val reg_no_ebp : grammar **)

let reg_no_ebp =
  map
    (alt (bits ('0'::('0'::('0'::[]))))
      (alt (bits ('0'::('0'::('1'::[]))))
        (alt (bits ('0'::('1'::('0'::[]))))
          (alt (bits ('0'::('1'::('1'::[]))))
            (alt (bits ('1'::('0'::('0'::[]))))
              (alt (bits ('1'::('1'::('0'::[]))))
                (bits ('1'::('1'::('1'::[])))))))))) (fun bs ->
    Obj.magic coq_Z_to_register
      (bits2int (Big.succ (Big.succ (Big.succ Big.zero))) bs))

(** val si : grammar **)

let si =
  map (seq scale_p reg) (fun p ->
    match snd (Obj.magic p) with
    | ESP -> Obj.magic None
    | _ -> Obj.magic (Some p))

(** val sib : grammar **)

let sib =
  seq si reg

(** val rm00 : grammar **)

let rm00 =
  alt
    (map
      (alt (bits ('0'::('0'::('0'::[]))))
        (alt (bits ('0'::('0'::('1'::[]))))
          (alt (bits ('0'::('1'::('0'::[]))))
            (alt (bits ('0'::('1'::('1'::[]))))
              (alt (bits ('1'::('1'::('0'::[]))))
                (bits ('1'::('1'::('1'::[]))))))))) (fun bs ->
      Obj.magic { addrDisp = (Word.repr size32 Big.zero); addrBase = (Some
        (coq_Z_to_register
          (Obj.magic bits2int (Big.succ (Big.succ (Big.succ Big.zero))) bs)));
        addrIndex = None }))
    (alt
      (map (seq (bits ('1'::('0'::('0'::[])))) (seq si reg_no_ebp)) (fun p ->
        let (_, i0) = Obj.magic p in
        let (si0, base) = i0 in
        Obj.magic { addrDisp = (Word.repr size32 Big.zero); addrBase = (Some
          base); addrIndex = si0 }))
      (alt
        (map
          (seq (bits ('1'::('0'::('0'::[]))))
            (seq si (seq (bits ('1'::('0'::('1'::[])))) word))) (fun p ->
          let (_, i0) = Obj.magic p in
          let (si0, i) = i0 in
          let (_, disp) = i in
          Obj.magic { addrDisp = disp; addrBase = None; addrIndex = si0 }))
        (map (seq (bits ('1'::('0'::('1'::[])))) word) (fun p ->
          let (_, disp) = Obj.magic p in
          Obj.magic { addrDisp = disp; addrBase = None; addrIndex = None }))))

(** val rm01 : grammar **)

let rm01 =
  alt
    (map
      (seq
        (alt (bits ('0'::('0'::('0'::[]))))
          (alt (bits ('0'::('0'::('1'::[]))))
            (alt (bits ('0'::('1'::('0'::[]))))
              (alt (bits ('0'::('1'::('1'::[]))))
                (alt (bits ('1'::('0'::('1'::[]))))
                  (alt (bits ('1'::('1'::('0'::[]))))
                    (bits ('1'::('1'::('1'::[])))))))))) byte) (fun p ->
      let (bs, disp) = Obj.magic p in
      Obj.magic { addrDisp = (sign_extend8_32 disp); addrBase = (Some
        (coq_Z_to_register
          (Obj.magic bits2int (Big.succ (Big.succ (Big.succ Big.zero))) bs)));
        addrIndex = None }))
    (map (seq (bits ('1'::('0'::('0'::[])))) (seq sib byte)) (fun p ->
      let (_, i0) = Obj.magic p in
      let (i, disp) = i0 in
      let (si0, base) = i in
      Obj.magic { addrDisp = (sign_extend8_32 disp); addrBase = (Some base);
        addrIndex = si0 }))

(** val rm10 : grammar **)

let rm10 =
  alt
    (map
      (seq
        (alt (bits ('0'::('0'::('0'::[]))))
          (alt (bits ('0'::('0'::('1'::[]))))
            (alt (bits ('0'::('1'::('0'::[]))))
              (alt (bits ('0'::('1'::('1'::[]))))
                (alt (bits ('1'::('0'::('1'::[]))))
                  (alt (bits ('1'::('1'::('0'::[]))))
                    (bits ('1'::('1'::('1'::[])))))))))) word) (fun p ->
      let (bs, disp) = Obj.magic p in
      Obj.magic { addrDisp = disp; addrBase = (Some
        (coq_Z_to_register
          (Obj.magic bits2int (Big.succ (Big.succ (Big.succ Big.zero))) bs)));
        addrIndex = None }))
    (map (seq (bits ('1'::('0'::('0'::[])))) (seq sib word)) (fun p ->
      let (_, i0) = Obj.magic p in
      let (i, disp) = i0 in
      let (si0, base) = i in
      Obj.magic { addrDisp = disp; addrBase = (Some base); addrIndex = si0 }))

(** val modrm_gen : grammar -> (address -> interp) -> grammar **)

let modrm_gen reg_p addr_op =
  alt
    (map
      (alt (bitsleft ('0'::('0'::[])) (seq reg_p rm00))
        (alt (bitsleft ('0'::('1'::[])) (seq reg_p rm01))
          (bitsleft ('1'::('0'::[])) (seq reg_p rm10)))) (fun p ->
      let (op1, addr) = Obj.magic p in Obj.magic (op1, (addr_op addr))))
    (map (bitsleft ('1'::('1'::[])) (seq reg_p reg_p)) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (op1, op2)))

(** val reg_op : grammar **)

let reg_op =
  map reg (fun x -> Obj.magic (Reg_op (Obj.magic x)))

(** val modrm : grammar **)

let modrm =
  modrm_gen reg_op (Obj.magic (fun x -> Address_op x))

(** val mmx_reg_op : grammar **)

let mmx_reg_op =
  map mmx_reg (fun r -> Obj.magic (MMX_Reg_op (Obj.magic r)))

(** val modrm_mmx : grammar **)

let modrm_mmx =
  modrm_gen mmx_reg_op (Obj.magic (fun x -> MMX_Addr_op x))

(** val sse_reg_op : grammar **)

let sse_reg_op =
  map sse_reg (fun r -> Obj.magic (SSE_XMM_Reg_op (Obj.magic r)))

(** val modrm_xmm : grammar **)

let modrm_xmm =
  modrm_gen sse_reg_op (Obj.magic (fun x -> SSE_Addr_op x))

(** val modrm_mm : grammar **)

let modrm_mm =
  modrm_gen (map mmx_reg (fun r -> Obj.magic (SSE_MM_Reg_op (Obj.magic r))))
    (Obj.magic (fun x -> SSE_Addr_op x))

(** val modrm_gen_noreg : grammar -> (address -> interp) -> grammar **)

let modrm_gen_noreg reg_p addr_op =
  map
    (alt (bitsleft ('0'::('0'::[])) (seq reg_p rm00))
      (alt (bitsleft ('0'::('1'::[])) (seq reg_p rm01))
        (bitsleft ('1'::('0'::[])) (seq reg_p rm10)))) (fun p ->
    let (op1, addr) = Obj.magic p in Obj.magic (op1, (addr_op addr)))

(** val modrm_noreg : grammar **)

let modrm_noreg =
  modrm_gen_noreg reg (Obj.magic (fun x -> Address_op x))

(** val modrm_xmm_noreg : grammar **)

let modrm_xmm_noreg =
  modrm_gen_noreg sse_reg_op (Obj.magic (fun x -> SSE_Addr_op x))

(** val modrm_xmm_gp_noreg : grammar **)

let modrm_xmm_gp_noreg =
  modrm_gen_noreg
    (map reg (fun r -> Obj.magic (SSE_GP_Reg_op (Obj.magic r))))
    (Obj.magic (fun x -> SSE_Addr_op x))

(** val modrm_mm_noreg : grammar **)

let modrm_mm_noreg =
  modrm_gen_noreg
    (map mmx_reg (fun r -> Obj.magic (SSE_MM_Reg_op (Obj.magic r))))
    (Obj.magic (fun x -> SSE_Addr_op x))

(** val ext_op_modrm_gen : (address -> interp) -> char list -> grammar **)

let ext_op_modrm_gen addr_op bs =
  map
    (alt (seq (bits ('0'::('0'::[]))) (seq (bits bs) rm00))
      (alt (seq (bits ('0'::('1'::[]))) (seq (bits bs) rm01))
        (seq (bits ('1'::('0'::[]))) (seq (bits bs) rm10)))) (fun p ->
    let (_, i0) = Obj.magic p in let (_, addr) = i0 in addr_op addr)

(** val ext_op_modrm : char list -> grammar **)

let ext_op_modrm =
  ext_op_modrm_gen (Obj.magic (fun x -> Address_op x))

(** val ext_op_modrm_sse : char list -> grammar **)

let ext_op_modrm_sse =
  ext_op_modrm_gen (Obj.magic (fun x -> SSE_Addr_op x))

(** val ext_op_modrm_FPM16 : char list -> grammar **)

let ext_op_modrm_FPM16 =
  ext_op_modrm_gen (Obj.magic (fun x -> FPM16_op x))

(** val ext_op_modrm_FPM32 : char list -> grammar **)

let ext_op_modrm_FPM32 =
  ext_op_modrm_gen (Obj.magic (fun x -> FPM32_op x))

(** val ext_op_modrm_FPM64 : char list -> grammar **)

let ext_op_modrm_FPM64 =
  ext_op_modrm_gen (Obj.magic (fun x -> FPM64_op x))

(** val ext_op_modrm_FPM80 : char list -> grammar **)

let ext_op_modrm_FPM80 =
  ext_op_modrm_gen (Obj.magic (fun x -> FPM80_op x))

(** val ext_op_modrm2_gen :
    grammar -> (address -> interp) -> char list -> grammar **)

let ext_op_modrm2_gen reg_p addr_op bs =
  alt
    (map
      (alt (seq (bits ('0'::('0'::[]))) (seq (bits bs) rm00))
        (alt (seq (bits ('0'::('1'::[]))) (seq (bits bs) rm01))
          (seq (bits ('1'::('0'::[]))) (seq (bits bs) rm10)))) (fun p ->
      let (_, i0) = Obj.magic p in let (_, addr) = i0 in addr_op addr))
    (map (bitsleft ('1'::('1'::[])) (seq (bits bs) reg_p)) (fun p ->
      let (_, op) = Obj.magic p in op))

(** val ext_op_modrm2 : char list -> grammar **)

let ext_op_modrm2 =
  ext_op_modrm2_gen reg_op (Obj.magic (fun x -> Address_op x))

(** val coq_AAA_p : grammar **)

let coq_AAA_p =
  map (bits ('0'::('0'::('1'::('1'::('0'::('1'::('1'::('1'::[])))))))))
    (fun _ -> Obj.magic AAA)

(** val coq_AAD_p : grammar **)

let coq_AAD_p =
  map
    (bits
      ('1'::('1'::('0'::('1'::('0'::('1'::('0'::('1'::('0'::('0'::('0'::('0'::('1'::('0'::('1'::('0'::[])))))))))))))))))
    (fun _ -> Obj.magic AAD)

(** val coq_AAM_p : grammar **)

let coq_AAM_p =
  map
    (bits
      ('1'::('1'::('0'::('1'::('0'::('1'::('0'::('0'::('0'::('0'::('0'::('0'::('1'::('0'::('1'::('0'::[])))))))))))))))))
    (fun _ -> Obj.magic AAM)

(** val coq_AAS_p : grammar **)

let coq_AAS_p =
  map (bits ('0'::('0'::('1'::('1'::('1'::('1'::('1'::('1'::[])))))))))
    (fun _ -> Obj.magic AAS)

(** val imm_op : bool -> grammar **)

let imm_op = function
| true ->
  map halfword (fun w -> Obj.magic (Imm_op (sign_extend16_32 (Obj.magic w))))
| false -> map word (fun w -> Obj.magic (Imm_op (Obj.magic w)))

(** val logic_or_arith_p :
    bool -> char list -> char list -> (bool -> operand -> operand -> instr)
    -> grammar **)

let logic_or_arith_p opsize_override op1 op2 instCon =
  alt
    (map (bitsleft op1 (bitsleft ('0'::[]) (seq Any (seq Any modrm))))
      (fun p ->
      let (d, i) = Obj.magic p in
      let (w, i0) = i in
      let (op3, op4) = i0 in
      if d then Obj.magic instCon w op3 op4 else Obj.magic instCon w op4 op3))
    (alt
      (map
        (bitsleft ('1'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::[])) (bitsleft op2 (seq reg byte)))))
        (fun p ->
        let (r, imm) = Obj.magic p in
        Obj.magic instCon true (Reg_op r) (Imm_op (sign_extend8_32 imm))))
      (alt
        (map
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (bitsleft op2 (seq reg byte)))))
          (fun p ->
          let (r, imm) = Obj.magic p in
          Obj.magic instCon false (Reg_op r) (Imm_op (zero_extend8_32 imm))))
        (alt
          (map
            (bitsleft ('1'::('0'::('0'::('0'::[]))))
              (bitsleft ('0'::('0'::('0'::('1'::[]))))
                (bitsleft ('1'::('1'::[]))
                  (bitsleft op2 (seq reg (imm_op opsize_override))))))
            (fun p ->
            let (r, imm) = Obj.magic p in
            Obj.magic instCon true (Reg_op r) imm))
          (alt
            (map (bitsleft op1 (bitsleft ('1'::('0'::('0'::[]))) byte))
              (fun imm ->
              Obj.magic instCon false (Reg_op EAX) (Imm_op
                (zero_extend8_32 (Obj.magic imm)))))
            (alt
              (map
                (bitsleft op1
                  (bitsleft ('1'::('0'::('1'::[]))) (imm_op opsize_override)))
                (fun imm -> Obj.magic instCon true (Reg_op EAX) imm))
              (alt
                (map
                  (bitsleft ('1'::('0'::('0'::('0'::[]))))
                    (bitsleft ('0'::('0'::('0'::('0'::[]))))
                      (seq (ext_op_modrm op2) byte))) (fun p ->
                  let (op, imm) = Obj.magic p in
                  Obj.magic instCon false op (Imm_op (zero_extend8_32 imm))))
                (alt
                  (map
                    (bitsleft ('1'::('0'::('0'::('0'::[]))))
                      (bitsleft ('0'::('0'::('1'::('1'::[]))))
                        (seq (ext_op_modrm op2) byte))) (fun p ->
                    let (op, imm) = Obj.magic p in
                    Obj.magic instCon true op (Imm_op (sign_extend8_32 imm))))
                  (map
                    (bitsleft ('1'::('0'::('0'::('0'::[]))))
                      (bitsleft ('0'::('0'::('0'::('1'::[]))))
                        (seq (ext_op_modrm op2) (imm_op opsize_override))))
                    (fun p ->
                    let (op, imm) = Obj.magic p in
                    Obj.magic instCon true op imm)))))))))

(** val coq_ADC_p : bool -> grammar **)

let coq_ADC_p s =
  logic_or_arith_p s ('0'::('0'::('0'::('1'::('0'::[])))))
    ('0'::('1'::('0'::[]))) (fun x x0 x1 -> ADC (x, x0, x1))

(** val coq_ADD_p : bool -> grammar **)

let coq_ADD_p s =
  logic_or_arith_p s ('0'::('0'::('0'::('0'::('0'::[])))))
    ('0'::('0'::('0'::[]))) (fun x x0 x1 -> ADD (x, x0, x1))

(** val coq_AND_p : bool -> grammar **)

let coq_AND_p s =
  logic_or_arith_p s ('0'::('0'::('1'::('0'::('0'::[])))))
    ('1'::('0'::('0'::[]))) (fun x x0 x1 -> AND (x, x0, x1))

(** val coq_CMP_p : bool -> grammar **)

let coq_CMP_p s =
  logic_or_arith_p s ('0'::('0'::('1'::('1'::('1'::[])))))
    ('1'::('1'::('1'::[]))) (fun x x0 x1 -> CMP (x, x0, x1))

(** val coq_OR_p : bool -> grammar **)

let coq_OR_p s =
  logic_or_arith_p s ('0'::('0'::('0'::('0'::('1'::[])))))
    ('0'::('0'::('1'::[]))) (fun x x0 x1 -> OR (x, x0, x1))

(** val coq_SBB_p : bool -> grammar **)

let coq_SBB_p s =
  logic_or_arith_p s ('0'::('0'::('0'::('1'::('1'::[])))))
    ('0'::('1'::('1'::[]))) (fun x x0 x1 -> SBB (x, x0, x1))

(** val coq_SUB_p : bool -> grammar **)

let coq_SUB_p s =
  logic_or_arith_p s ('0'::('0'::('1'::('0'::('1'::[])))))
    ('1'::('0'::('1'::[]))) (fun x x0 x1 -> SUB (x, x0, x1))

(** val coq_XOR_p : bool -> grammar **)

let coq_XOR_p s =
  logic_or_arith_p s ('0'::('0'::('1'::('1'::('0'::[])))))
    ('1'::('1'::('0'::[]))) (fun x x0 x1 -> XOR (x, x0, x1))

(** val coq_ARPL_p : grammar **)

let coq_ARPL_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm)) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (ARPL (op1, op2)))

(** val coq_BOUND_p : grammar **)

let coq_BOUND_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm)) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (BOUND (op1, op2)))

(** val coq_BSF_p : grammar **)

let coq_BSF_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('0'::[])))) modrm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (BSF (op1, op2)))

(** val coq_BSR_p : grammar **)

let coq_BSR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (BSR (op1, op2)))

(** val coq_BSWAP_p : grammar **)

let coq_BSWAP_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[])))) (bitsleft ('1'::[]) reg))))
    (fun x -> Obj.magic (BSWAP (Obj.magic x)))

(** val bit_test_p :
    char list -> char list -> (operand -> operand -> instr) -> grammar **)

let bit_test_p opcode1 opcode2 instr0 =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('0'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (bitsleft opcode1 (seq reg byte)))))))
      (fun p ->
      let (r, imm) = Obj.magic p in
      Obj.magic instr0 (Reg_op r) (Imm_op (zero_extend8_32 imm))))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::('1'::[]))))
              (bitsleft ('1'::('0'::('1'::('0'::[]))))
                (seq (ext_op_modrm opcode1) byte))))) (fun p ->
        let (op1, imm) = Obj.magic p in
        Obj.magic instr0 op1 (Imm_op (zero_extend8_32 imm))))
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::[])))
              (bitsleft opcode2 (bitsleft ('0'::('1'::('1'::[]))) modrm)))))
        (fun p -> let (op2, op1) = Obj.magic p in Obj.magic instr0 op1 op2)))

(** val coq_BT_p : grammar **)

let coq_BT_p =
  bit_test_p ('1'::('0'::('0'::[]))) ('0'::('0'::[])) (fun x x0 -> BT (x,
    x0))

(** val coq_BTC_p : grammar **)

let coq_BTC_p =
  bit_test_p ('1'::('1'::('1'::[]))) ('1'::('1'::[])) (fun x x0 -> BTC (x,
    x0))

(** val coq_BTR_p : grammar **)

let coq_BTR_p =
  bit_test_p ('1'::('1'::('0'::[]))) ('1'::('0'::[])) (fun x x0 -> BTR (x,
    x0))

(** val coq_BTS_p : grammar **)

let coq_BTS_p =
  bit_test_p ('1'::('0'::('1'::[]))) ('0'::('1'::[])) (fun x x0 -> BTS (x,
    x0))

(** val coq_CALL_p : grammar **)

let coq_CALL_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('1'::('0'::('0'::('0'::[])))) word)) (fun w ->
      Obj.magic (CALL (true, false, (Imm_op (Obj.magic w)), None))))
    (alt
      (map
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (ext_op_modrm2 ('0'::('1'::('0'::[])))))) (fun op ->
        Obj.magic (CALL (true, true, (Obj.magic op), None))))
      (alt
        (map
          (bitsleft ('1'::('0'::('0'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[])))) (seq word halfword)))
          (fun p ->
          Obj.magic (CALL (false, true, (Imm_op (fst (Obj.magic p))), (Some
            (snd (Obj.magic p)))))))
        (map
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (ext_op_modrm2 ('0'::('1'::('1'::[])))))) (fun op ->
          Obj.magic (CALL (false, true, (Obj.magic op), None))))))

(** val coq_CDQ_p : grammar **)

let coq_CDQ_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('0'::('0'::('1'::[])))))) (fun _ -> Obj.magic CDQ)

(** val coq_CLC_p : grammar **)

let coq_CLC_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('0'::('0'::('0'::[])))))) (fun _ -> Obj.magic CLC)

(** val coq_CLD_p : grammar **)

let coq_CLD_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('1'::('0'::('0'::[])))))) (fun _ -> Obj.magic CLD)

(** val coq_CLI_p : grammar **)

let coq_CLI_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('0'::('1'::('0'::[])))))) (fun _ -> Obj.magic CLI)

(** val coq_CLTS_p : grammar **)

let coq_CLTS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bits ('0'::('1'::('1'::('0'::[])))))))) (fun _ -> Obj.magic CLTS)

(** val coq_CMC_p : grammar **)

let coq_CMC_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('0'::('1'::('0'::('1'::[])))))) (fun _ -> Obj.magic CMC)

(** val coq_CMPS_p : grammar **)

let coq_CMPS_p =
  map
    (bitsleft ('1'::('0'::('1'::('0'::[]))))
      (bitsleft ('0'::('1'::('1'::[]))) Any)) (fun x ->
    Obj.magic (CMPS (Obj.magic x)))

(** val coq_CMPXCHG_p : grammar **)

let coq_CMPXCHG_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::[]))) (seq Any modrm))))) (fun p ->
    let (w, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (CMPXCHG (w, op2, op1)))

(** val coq_CPUID_p : grammar **)

let coq_CPUID_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('0'::[]))))
          (bits ('0'::('0'::('1'::('0'::[])))))))) (fun _ -> Obj.magic CPUID)

(** val coq_CWDE_p : grammar **)

let coq_CWDE_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('0'::('0'::('0'::[])))))) (fun _ -> Obj.magic CWDE)

(** val coq_DAA_p : grammar **)

let coq_DAA_p =
  map
    (bitsleft ('0'::('0'::('1'::('0'::[]))))
      (bits ('0'::('1'::('1'::('1'::[])))))) (fun _ -> Obj.magic DAA)

(** val coq_DAS_p : grammar **)

let coq_DAS_p =
  map
    (bitsleft ('0'::('0'::('1'::('0'::[]))))
      (bits ('1'::('1'::('1'::('1'::[])))))) (fun _ -> Obj.magic DAS)

(** val coq_DEC_p : grammar **)

let coq_DEC_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::[])))
          (seq Any (bitsleft ('1'::('1'::('0'::('0'::('1'::[]))))) reg))))
      (fun p -> let (w, r) = Obj.magic p in Obj.magic (DEC (w, (Reg_op r)))))
    (alt
      (map (bitsleft ('0'::('1'::('0'::('0'::[])))) (bitsleft ('1'::[]) reg))
        (fun r -> Obj.magic (DEC (true, (Reg_op (Obj.magic r))))))
      (map
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::[])))
            (seq Any (ext_op_modrm ('0'::('0'::('1'::[]))))))) (fun p ->
        let (w, op1) = Obj.magic p in Obj.magic (DEC (w, op1)))))

(** val coq_DIV_p : grammar **)

let coq_DIV_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::[])))
          (seq Any (bitsleft ('1'::('1'::('1'::('1'::('0'::[]))))) reg))))
      (fun p -> let (w, r) = Obj.magic p in Obj.magic (DIV (w, (Reg_op r)))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::[])))
          (seq Any (ext_op_modrm ('1'::('1'::('0'::[]))))))) (fun p ->
      let (w, op1) = Obj.magic p in Obj.magic (DIV (w, op1))))

(** val coq_HLT_p : grammar **)

let coq_HLT_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('0'::('1'::('0'::('0'::[])))))) (fun _ -> Obj.magic HLT)

(** val coq_IDIV_p : grammar **)

let coq_IDIV_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::[])))
          (seq Any (bitsleft ('1'::('1'::('1'::('1'::('1'::[]))))) reg))))
      (fun p ->
      let (w, r) = Obj.magic p in Obj.magic (IDIV (w, (Reg_op r)))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::[])))
          (seq Any (ext_op_modrm ('1'::('1'::('1'::[]))))))) (fun p ->
      let (w, op1) = Obj.magic p in Obj.magic (IDIV (w, op1))))

(** val coq_IMUL_p : bool -> grammar **)

let coq_IMUL_p opsize_override =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::[])))
          (seq Any (ext_op_modrm2 ('1'::('0'::('1'::[]))))))) (fun p ->
      let (w, op1) = Obj.magic p in Obj.magic (IMUL (w, op1, None, None))))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm)))) (fun p ->
        let (op1, op2) = Obj.magic p in
        Obj.magic (IMUL (false, op1, (Some op2), None))))
      (alt
        (map
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('0'::('1'::('1'::[])))) (seq modrm byte)))
          (fun p ->
          let (i, imm) = Obj.magic p in
          let (op1, op2) = i in
          Obj.magic (IMUL (true, op1, (Some op2), (Some
            (sign_extend8_32 imm))))))
        (if opsize_override
         then map
                (bitsleft ('0'::('1'::('1'::('0'::[]))))
                  (bitsleft ('1'::('0'::('0'::('1'::[]))))
                    (seq modrm halfword))) (fun p ->
                let (i, imm) = Obj.magic p in
                let (op1, op2) = i in
                Obj.magic (IMUL (false, op1, (Some op2), (Some
                  (sign_extend16_32 imm)))))
         else map
                (bitsleft ('0'::('1'::('1'::('0'::[]))))
                  (bitsleft ('1'::('0'::('0'::('1'::[])))) (seq modrm word)))
                (fun p ->
                let (i, imm) = Obj.magic p in
                let (op1, op2) = i in
                Obj.magic (IMUL (true, op1, (Some op2), (Some imm)))))))

(** val coq_IN_p : grammar **)

let coq_IN_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('0'::('1'::('0'::[]))) (seq Any byte))) (fun p ->
      let (w, pt) = Obj.magic p in Obj.magic (IN (w, (Some pt)))))
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('1'::('1'::('0'::[]))) Any)) (fun w ->
      Obj.magic (IN ((Obj.magic w), None))))

(** val coq_INC_p : grammar **)

let coq_INC_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::[])))
          (seq Any (bitsleft ('1'::('1'::('0'::('0'::('0'::[]))))) reg))))
      (fun p -> let (w, r) = Obj.magic p in Obj.magic (INC (w, (Reg_op r)))))
    (alt
      (map (bitsleft ('0'::('1'::('0'::('0'::[])))) (bitsleft ('0'::[]) reg))
        (fun r -> Obj.magic (INC (true, (Reg_op (Obj.magic r))))))
      (map
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::[])))
            (seq Any (ext_op_modrm ('0'::('0'::('0'::[]))))))) (fun p ->
        let (w, op1) = Obj.magic p in Obj.magic (INC (w, op1)))))

(** val coq_INS_p : grammar **)

let coq_INS_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bitsleft ('1'::('1'::('0'::[]))) Any)) (fun x ->
    Obj.magic (INS (Obj.magic x)))

(** val coq_INTn_p : grammar **)

let coq_INTn_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('0'::('1'::[])))) byte)) (fun x ->
    Obj.magic (INTn (Obj.magic x)))

(** val coq_INT_p : grammar **)

let coq_INT_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bits ('1'::('1'::('0'::('0'::[])))))) (fun _ -> Obj.magic INT)

(** val coq_INTO_p : grammar **)

let coq_INTO_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bits ('1'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic INTO)

(** val coq_INVD_p : grammar **)

let coq_INVD_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bits ('1'::('0'::('0'::('0'::[])))))))) (fun _ -> Obj.magic INVD)

(** val coq_INVLPG_p : grammar **)

let coq_INVLPG_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm ('1'::('1'::('1'::[])))))))) (fun x ->
    Obj.magic (INVLPG (Obj.magic x)))

(** val coq_IRET_p : grammar **)

let coq_IRET_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bits ('1'::('1'::('1'::('1'::[])))))) (fun _ -> Obj.magic IRET)

(** val coq_Jcc_p : grammar **)

let coq_Jcc_p =
  alt
    (map (bitsleft ('0'::('1'::('1'::('1'::[])))) (seq tttn byte)) (fun p ->
      let (ct, imm) = Obj.magic p in
      Obj.magic (Jcc (ct, (sign_extend8_32 imm)))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[])))) (seq tttn word))))
      (fun p -> let (ct, imm) = Obj.magic p in Obj.magic (Jcc (ct, imm))))

(** val coq_JCXZ_p : grammar **)

let coq_JCXZ_p =
  map
    (bitsleft ('1'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[])))) byte)) (fun x ->
    Obj.magic (JCXZ (Obj.magic x)))

(** val coq_JMP_p : grammar **)

let coq_JMP_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[])))) byte)) (fun b ->
      Obj.magic (JMP (true, false, (Imm_op (sign_extend8_32 (Obj.magic b))),
        None))))
    (alt
      (map
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('0'::('1'::[])))) word)) (fun w ->
        Obj.magic (JMP (true, false, (Imm_op (Obj.magic w)), None))))
      (alt
        (map
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (ext_op_modrm2 ('1'::('0'::('0'::[])))))) (fun op ->
          Obj.magic (JMP (true, true, (Obj.magic op), None))))
        (alt
          (map
            (bitsleft ('1'::('1'::('1'::('0'::[]))))
              (bitsleft ('1'::('0'::('1'::('0'::[])))) (seq word halfword)))
            (fun p ->
            Obj.magic (JMP (false, true, (Imm_op (fst (Obj.magic p))), (Some
              (snd (Obj.magic p)))))))
          (map
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[]))))
                (ext_op_modrm2 ('1'::('0'::('1'::[])))))) (fun op ->
            Obj.magic (JMP (false, true, (Obj.magic op), None)))))))

(** val coq_LAHF_p : grammar **)

let coq_LAHF_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('1'::('1'::('1'::[])))))) (fun _ -> Obj.magic LAHF)

(** val coq_LAR_p : grammar **)

let coq_LAR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm)))) (fun p ->
    Obj.magic (LAR ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LDS_p : grammar **)

let coq_LDS_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm)) (fun p ->
    Obj.magic (LDS ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LEA_p : grammar **)

let coq_LEA_p =
  map
    (bitsleft ('1'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm_noreg)) (fun p ->
    Obj.magic (LEA ((Reg_op (fst (Obj.magic p))), (snd (Obj.magic p)))))

(** val coq_LEAVE_p : grammar **)

let coq_LEAVE_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bits ('1'::('0'::('0'::('1'::[])))))) (fun _ -> Obj.magic LEAVE)

(** val coq_LES_p : grammar **)

let coq_LES_p =
  map
    (bitsleft ('1'::('1'::('0'::('0'::[]))))
      (bitsleft ('0'::('1'::('0'::('0'::[])))) modrm)) (fun p ->
    Obj.magic (LES ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LFS_p : grammar **)

let coq_LFS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('0'::[])))) modrm)))) (fun p ->
    Obj.magic (LFS ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LGDT_p : grammar **)

let coq_LGDT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm ('0'::('1'::('0'::[])))))))) (fun x ->
    Obj.magic (LGDT (Obj.magic x)))

(** val coq_LGS_p : grammar **)

let coq_LGS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm)))) (fun p ->
    Obj.magic (LGS ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LIDT_p : grammar **)

let coq_LIDT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm ('0'::('1'::('1'::[])))))))) (fun x ->
    Obj.magic (LIDT (Obj.magic x)))

(** val coq_LLDT_p : grammar **)

let coq_LLDT_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::[]))
                (bitsleft ('0'::('1'::('0'::[]))) reg)))))) (fun r ->
      Obj.magic (LLDT (Reg_op (Obj.magic r)))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (ext_op_modrm ('0'::('1'::('0'::[])))))))) (fun x ->
      Obj.magic (LLDT (Obj.magic x))))

(** val coq_LMSW_p : grammar **)

let coq_LMSW_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::[]))
                (bitsleft ('1'::('1'::('0'::[]))) reg)))))) (fun r ->
      Obj.magic (LMSW (Reg_op (Obj.magic r)))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::[]))
                (ext_op_modrm ('1'::('1'::('0'::[]))))))))) (fun x ->
      Obj.magic (LMSW (Obj.magic x))))

(** val coq_LODS_p : grammar **)

let coq_LODS_p =
  map
    (bitsleft ('1'::('0'::('1'::('0'::[]))))
      (bitsleft ('1'::('1'::('0'::[]))) Any)) (fun x ->
    Obj.magic (LODS (Obj.magic x)))

(** val coq_LOOP_p : grammar **)

let coq_LOOP_p =
  map
    (bitsleft ('1'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('1'::('0'::[])))) byte)) (fun x ->
    Obj.magic (LOOP (Obj.magic x)))

(** val coq_LOOPZ_p : grammar **)

let coq_LOOPZ_p =
  map
    (bitsleft ('1'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('0'::('1'::[])))) byte)) (fun x ->
    Obj.magic (LOOPZ (Obj.magic x)))

(** val coq_LOOPNZ_p : grammar **)

let coq_LOOPNZ_p =
  map
    (bitsleft ('1'::('1'::('1'::('0'::[]))))
      (bitsleft ('0'::('0'::('0'::('0'::[])))) byte)) (fun x ->
    Obj.magic (LOOPNZ (Obj.magic x)))

(** val coq_LSL_p : grammar **)

let coq_LSL_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm)))) (fun p ->
    Obj.magic (LSL ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LSS_p : grammar **)

let coq_LSS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm)))) (fun p ->
    Obj.magic (LSS ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_LTR_p : grammar **)

let coq_LTR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (ext_op_modrm2 ('0'::('1'::('1'::[])))))))) (fun x ->
    Obj.magic (LTR (Obj.magic x)))

(** val coq_CMOVcc_p : grammar **)

let coq_CMOVcc_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('0'::[])))) (seq tttn modrm))))
    (fun p ->
    let (tttn0, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (CMOVcc (tttn0, op1, op2)))

(** val coq_MOV_p : bool -> grammar **)

let coq_MOV_p opsize_override =
  alt
    (map
      (bitsleft ('1'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('0'::('1'::[]))) (seq Any modrm))) (fun p ->
      let (w, i) = Obj.magic p in
      let (op1, op2) = i in Obj.magic (MOV (w, op1, op2))))
    (alt
      (map
        (bitsleft ('1'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('0'::('0'::[]))) (seq Any modrm))) (fun p ->
        let (w, i) = Obj.magic p in
        let (op1, op2) = i in Obj.magic (MOV (w, op2, op1))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('0'::[]))))
            (bitsleft ('0'::('1'::('1'::('1'::[]))))
              (bitsleft ('1'::('1'::[]))
                (bitsleft ('0'::('0'::('0'::[])))
                  (seq reg (imm_op opsize_override)))))) (fun p ->
          let (r, w) = Obj.magic p in Obj.magic (MOV (true, (Reg_op r), w))))
        (alt
          (map
            (bitsleft ('1'::('1'::('0'::('0'::[]))))
              (bitsleft ('0'::('1'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::[]))
                  (bitsleft ('0'::('0'::('0'::[]))) (seq reg byte)))))
            (fun p ->
            let (r, b) = Obj.magic p in
            Obj.magic (MOV (false, (Reg_op r), (Imm_op (zero_extend8_32 b))))))
          (alt
            (map
              (bitsleft ('1'::('0'::('1'::('1'::[]))))
                (bitsleft ('1'::[]) (seq reg (imm_op opsize_override))))
              (fun p ->
              let (r, w) = Obj.magic p in
              Obj.magic (MOV (true, (Reg_op r), w))))
            (alt
              (map
                (bitsleft ('1'::('0'::('1'::('1'::[]))))
                  (bitsleft ('0'::[]) (seq reg byte))) (fun p ->
                let (r, b) = Obj.magic p in
                Obj.magic (MOV (false, (Reg_op r), (Imm_op
                  (zero_extend8_32 b))))))
              (alt
                (map
                  (bitsleft ('1'::('1'::('0'::('0'::[]))))
                    (bitsleft ('0'::('1'::('1'::('1'::[]))))
                      (seq (ext_op_modrm ('0'::('0'::('0'::[]))))
                        (imm_op opsize_override)))) (fun p ->
                  let (op, w) = Obj.magic p in Obj.magic (MOV (true, op, w))))
                (alt
                  (map
                    (bitsleft ('1'::('1'::('0'::('0'::[]))))
                      (bitsleft ('0'::('1'::('1'::('0'::[]))))
                        (seq (ext_op_modrm ('0'::('0'::('0'::[])))) byte)))
                    (fun p ->
                    let (op, b) = Obj.magic p in
                    Obj.magic (MOV (false, op, (Imm_op (zero_extend8_32 b))))))
                  (alt
                    (map
                      (bitsleft ('1'::('0'::('1'::('0'::[]))))
                        (bitsleft ('0'::('0'::('0'::('1'::[])))) word))
                      (fun w ->
                      Obj.magic (MOV (true, (Reg_op EAX), (Offset_op
                        (Obj.magic w))))))
                    (alt
                      (map
                        (bitsleft ('1'::('0'::('1'::('0'::[]))))
                          (bitsleft ('0'::('0'::('0'::('0'::[])))) word))
                        (fun w ->
                        Obj.magic (MOV (false, (Reg_op EAX), (Offset_op
                          (Obj.magic w))))))
                      (alt
                        (map
                          (bitsleft ('1'::('0'::('1'::('0'::[]))))
                            (bitsleft ('0'::('0'::('1'::('1'::[])))) word))
                          (fun w ->
                          Obj.magic (MOV (true, (Offset_op (Obj.magic w)),
                            (Reg_op EAX)))))
                        (map
                          (bitsleft ('1'::('0'::('1'::('0'::[]))))
                            (bitsleft ('0'::('0'::('1'::('0'::[])))) word))
                          (fun w ->
                          Obj.magic (MOV (false, (Offset_op (Obj.magic w)),
                            (Reg_op EAX)))))))))))))))

(** val control_reg_p : grammar **)

let control_reg_p =
  alt (map (bits ('0'::('0'::('0'::[])))) (fun _ -> Obj.magic CR0))
    (alt (map (bits ('0'::('1'::('0'::[])))) (fun _ -> Obj.magic CR2))
      (alt (map (bits ('0'::('1'::('1'::[])))) (fun _ -> Obj.magic CR3))
        (map (bits ('1'::('0'::('0'::[])))) (fun _ -> Obj.magic CR4))))

(** val coq_MOVCR_p : grammar **)

let coq_MOVCR_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('1'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (seq control_reg_p reg))))))
      (fun p ->
      Obj.magic (MOVCR (true, (fst (Obj.magic p)), (snd (Obj.magic p))))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (seq control_reg_p reg))))))
      (fun p ->
      Obj.magic (MOVCR (false, (fst (Obj.magic p)), (snd (Obj.magic p))))))

(** val debug_reg_p : grammar **)

let debug_reg_p =
  alt (map (bits ('0'::('0'::('0'::[])))) (fun _ -> Obj.magic DR0))
    (alt (map (bits ('0'::('0'::('1'::[])))) (fun _ -> Obj.magic DR1))
      (alt (map (bits ('0'::('1'::('0'::[])))) (fun _ -> Obj.magic DR2))
        (alt (map (bits ('0'::('1'::('1'::[])))) (fun _ -> Obj.magic DR3))
          (alt (map (bits ('1'::('1'::('0'::[])))) (fun _ -> Obj.magic DR6))
            (map (bits ('1'::('1'::('1'::[])))) (fun _ -> Obj.magic DR7))))))

(** val coq_MOVDR_p : grammar **)

let coq_MOVDR_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('1'::('1'::[]))))
              (bitsleft ('1'::('1'::[])) (seq debug_reg_p reg)))))) (fun p ->
      Obj.magic (MOVDR (true, (fst (Obj.magic p)), (snd (Obj.magic p))))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::[])) (seq debug_reg_p reg)))))) (fun p ->
      Obj.magic (MOVDR (false, (fst (Obj.magic p)), (snd (Obj.magic p))))))

(** val segment_reg_p : grammar **)

let segment_reg_p =
  alt (map (bits ('0'::('0'::('0'::[])))) (fun _ -> Obj.magic ES))
    (alt (map (bits ('0'::('0'::('1'::[])))) (fun _ -> Obj.magic CS))
      (alt (map (bits ('0'::('1'::('0'::[])))) (fun _ -> Obj.magic SS))
        (alt (map (bits ('0'::('1'::('1'::[])))) (fun _ -> Obj.magic DS))
          (alt (map (bits ('1'::('0'::('0'::[])))) (fun _ -> Obj.magic FS))
            (map (bits ('1'::('0'::('1'::[])))) (fun _ -> Obj.magic GS))))))

(** val seg_modrm : grammar **)

let seg_modrm =
  alt
    (map
      (alt (bitsleft ('0'::('0'::[])) (seq segment_reg_p rm00))
        (alt (bitsleft ('0'::('1'::[])) (seq segment_reg_p rm01))
          (bitsleft ('1'::('0'::[])) (seq segment_reg_p rm10)))) (fun p ->
      let (sr, addr) = Obj.magic p in Obj.magic (sr, (Address_op addr))))
    (bitsleft ('1'::('1'::[])) (seq segment_reg_p reg_op))

(** val coq_MOVSR_p : grammar **)

let coq_MOVSR_p =
  alt
    (map
      (bitsleft ('1'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[])))) seg_modrm)) (fun p ->
      Obj.magic (MOVSR (true, (fst (Obj.magic p)), (snd (Obj.magic p))))))
    (map
      (bitsleft ('1'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[])))) seg_modrm)) (fun p ->
      Obj.magic (MOVSR (false, (fst (Obj.magic p)), (snd (Obj.magic p))))))

(** val coq_MOVBE_p : grammar **)

let coq_MOVBE_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[]))))
                (bitsleft ('0'::('0'::('0'::('1'::[])))) modrm))))))
      (fun p ->
      Obj.magic (MOVBE ((snd (Obj.magic p)), (fst (Obj.magic p))))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[]))))
                (bitsleft ('0'::('0'::('0'::('0'::[])))) modrm))))))
      (fun p ->
      Obj.magic (MOVBE ((fst (Obj.magic p)), (snd (Obj.magic p))))))

(** val coq_MOVS_p : grammar **)

let coq_MOVS_p =
  map
    (bitsleft ('1'::('0'::('1'::('0'::[]))))
      (bitsleft ('0'::('1'::('0'::[]))) Any)) (fun x ->
    Obj.magic (MOVS (Obj.magic x)))

(** val coq_MOVSX_p : grammar **)

let coq_MOVSX_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::[]))) (seq Any modrm))))) (fun p ->
    let (w, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (MOVSX (w, op1, op2)))

(** val coq_MOVZX_p : grammar **)

let coq_MOVZX_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::[]))) (seq Any modrm))))) (fun p ->
    let (w, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (MOVZX (w, op1, op2)))

(** val coq_MUL_p : grammar **)

let coq_MUL_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('1'::('1'::[])))
        (seq Any (ext_op_modrm2 ('1'::('0'::('0'::[]))))))) (fun p ->
    Obj.magic (MUL ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_NEG_p : grammar **)

let coq_NEG_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('1'::('1'::[])))
        (seq Any (ext_op_modrm2 ('0'::('1'::('1'::[]))))))) (fun p ->
    Obj.magic (NEG ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_NOP_p : grammar **)

let coq_NOP_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (ext_op_modrm2 ('0'::('0'::('0'::[])))))))) (fun op ->
    Obj.magic (NOP (Obj.magic op)))

(** val coq_NOT_p : grammar **)

let coq_NOT_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('1'::('1'::[])))
        (seq Any (ext_op_modrm2 ('0'::('1'::('0'::[]))))))) (fun p ->
    Obj.magic (NOT ((fst (Obj.magic p)), (snd (Obj.magic p)))))

(** val coq_OUT_p : grammar **)

let coq_OUT_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('0'::('1'::('1'::[]))) (seq Any byte))) (fun p ->
      Obj.magic (OUT ((fst (Obj.magic p)), (Some (snd (Obj.magic p)))))))
    (map
      (bitsleft ('1'::('1'::('1'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::[]))) Any)) (fun w ->
      Obj.magic (OUT ((Obj.magic w), None))))

(** val coq_OUTS_p : grammar **)

let coq_OUTS_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::[]))) Any)) (fun x ->
    Obj.magic (OUTS (Obj.magic x)))

(** val coq_POP_p : grammar **)

let coq_POP_p =
  alt
    (map
      (bitsleft ('1'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (ext_op_modrm2 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (POP (Obj.magic x))))
    (map (bitsleft ('0'::('1'::('0'::('1'::[])))) (bitsleft ('1'::[]) reg))
      (fun r -> Obj.magic (POP (Reg_op (Obj.magic r)))))

(** val coq_POPSR_p : grammar **)

let coq_POPSR_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::[])))
        (bitsleft ('0'::('0'::[])) (bits ('1'::('1'::('1'::[])))))) (fun _ ->
      Obj.magic (POPSR ES)))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::[])))
          (bitsleft ('1'::('0'::[])) (bits ('1'::('1'::('1'::[]))))))
        (fun _ -> Obj.magic (POPSR SS)))
      (alt
        (map
          (bitsleft ('0'::('0'::('0'::[])))
            (bitsleft ('1'::('1'::[])) (bits ('1'::('1'::('1'::[]))))))
          (fun _ -> Obj.magic (POPSR DS)))
        (alt
          (map
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[]))))
                (bitsleft ('1'::('0'::[]))
                  (bitsleft ('1'::('0'::('0'::[])))
                    (bits ('0'::('0'::('1'::[])))))))) (fun _ ->
            Obj.magic (POPSR FS)))
          (map
            (bitsleft ('0'::('0'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[]))))
                (bitsleft ('1'::('0'::[]))
                  (bitsleft ('1'::('0'::('1'::[])))
                    (bits ('0'::('0'::('1'::[])))))))) (fun _ ->
            Obj.magic (POPSR GS))))))

(** val coq_POPA_p : grammar **)

let coq_POPA_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bits ('0'::('0'::('0'::('1'::[])))))) (fun _ -> Obj.magic POPA)

(** val coq_POPF_p : grammar **)

let coq_POPF_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('1'::('0'::('1'::[])))))) (fun _ -> Obj.magic POPF)

(** val coq_PUSH_p : grammar **)

let coq_PUSH_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (ext_op_modrm ('1'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (PUSH (true, (Obj.magic x)))))
    (alt
      (map (bitsleft ('0'::('1'::('0'::('1'::[])))) (bitsleft ('0'::[]) reg))
        (fun r -> Obj.magic (PUSH (true, (Reg_op (Obj.magic r))))))
      (alt
        (map
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[])))) byte)) (fun b ->
          Obj.magic (PUSH (false, (Imm_op (sign_extend8_32 (Obj.magic b)))))))
        (map
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('0'::('0'::('0'::[])))) word)) (fun w ->
          Obj.magic (PUSH (true, (Imm_op (Obj.magic w))))))))

(** val segment_reg2_p : grammar **)

let segment_reg2_p =
  alt (map (bits ('0'::('0'::[]))) (fun _ -> Obj.magic ES))
    (alt (map (bits ('0'::('1'::[]))) (fun _ -> Obj.magic CS))
      (alt (map (bits ('1'::('0'::[]))) (fun _ -> Obj.magic SS))
        (map (bits ('1'::('1'::[]))) (fun _ -> Obj.magic DS))))

(** val coq_PUSHSR_p : grammar **)

let coq_PUSHSR_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::[])))
        (seq segment_reg2_p (bits ('1'::('1'::('0'::[])))))) (fun p ->
      Obj.magic (PUSHSR (fst (Obj.magic p)))))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::[]))
              (bitsleft ('1'::('0'::('0'::[])))
                (bits ('0'::('0'::('0'::[])))))))) (fun _ ->
        Obj.magic (PUSHSR FS)))
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::[]))
              (bitsleft ('1'::('0'::('1'::[])))
                (bits ('0'::('0'::('0'::[])))))))) (fun _ ->
        Obj.magic (PUSHSR GS))))

(** val coq_PUSHA_p : grammar **)

let coq_PUSHA_p =
  map
    (bitsleft ('0'::('1'::('1'::('0'::[]))))
      (bits ('0'::('0'::('0'::('0'::[])))))) (fun _ -> Obj.magic PUSHA)

(** val coq_PUSHF_p : grammar **)

let coq_PUSHF_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('1'::('0'::('0'::[])))))) (fun _ -> Obj.magic PUSHF)

(** val rotate_p :
    char list -> (bool -> operand -> reg_or_immed -> instr) -> grammar **)

let rotate_p extop inst =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::[]))) (seq Any (ext_op_modrm2 extop))))
      (fun p ->
      Obj.magic inst (fst (Obj.magic p)) (snd (Obj.magic p)) (Imm_ri
        (Word.repr size8 Big.one))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::[]))) (seq Any (ext_op_modrm2 extop))))
        (fun p ->
        Obj.magic inst (fst (Obj.magic p)) (snd (Obj.magic p)) (Reg_ri ECX)))
      (map
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::[])))
            (seq Any (seq (ext_op_modrm2 extop) byte)))) (fun p ->
        let (w, i) = Obj.magic p in
        let (op, b) = i in Obj.magic inst w op (Imm_ri b))))

(** val coq_RCL_p : grammar **)

let coq_RCL_p =
  rotate_p ('0'::('1'::('0'::[]))) (fun x x0 x1 -> RCL (x, x0, x1))

(** val coq_RCR_p : grammar **)

let coq_RCR_p =
  rotate_p ('0'::('1'::('1'::[]))) (fun x x0 x1 -> RCR (x, x0, x1))

(** val coq_RDMSR_p : grammar **)

let coq_RDMSR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bits ('0'::('0'::('1'::('0'::[])))))))) (fun _ -> Obj.magic RDMSR)

(** val coq_RDPMC_p : grammar **)

let coq_RDPMC_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bits ('0'::('0'::('1'::('1'::[])))))))) (fun _ -> Obj.magic RDPMC)

(** val coq_RDTSC_p : grammar **)

let coq_RDTSC_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bits ('0'::('0'::('0'::('1'::[])))))))) (fun _ -> Obj.magic RDTSC)

(** val coq_RDTSCP_p : grammar **)

let coq_RDTSCP_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bits ('1'::('0'::('0'::('1'::[])))))))))) (fun _ ->
    Obj.magic RDTSCP)

(** val coq_RET_p : grammar **)

let coq_RET_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('0'::[]))))
        (bits ('0'::('0'::('1'::('1'::[])))))) (fun _ ->
      Obj.magic (RET (true, None))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[])))) halfword)) (fun h ->
        Obj.magic (RET (true, (Some (Obj.magic h))))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('0'::[]))))
            (bits ('1'::('0'::('1'::('1'::[])))))) (fun _ ->
          Obj.magic (RET (false, None))))
        (map
          (bitsleft ('1'::('1'::('0'::('0'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[])))) halfword)) (fun h ->
          Obj.magic (RET (false, (Some (Obj.magic h))))))))

(** val coq_ROL_p : grammar **)

let coq_ROL_p =
  rotate_p ('0'::('0'::('0'::[]))) (fun x x0 x1 -> ROL (x, x0, x1))

(** val coq_ROR_p : grammar **)

let coq_ROR_p =
  rotate_p ('0'::('0'::('1'::[]))) (fun x x0 x1 -> ROR (x, x0, x1))

(** val coq_RSM_p : grammar **)

let coq_RSM_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('0'::[]))))
          (bits ('1'::('0'::('1'::('0'::[])))))))) (fun _ -> Obj.magic RSM)

(** val coq_SAHF_p : grammar **)

let coq_SAHF_p =
  map
    (bitsleft ('1'::('0'::('0'::('1'::[]))))
      (bits ('1'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic SAHF)

(** val coq_SAR_p : grammar **)

let coq_SAR_p =
  rotate_p ('1'::('1'::('1'::[]))) (fun x x0 x1 -> SAR (x, x0, x1))

(** val coq_SCAS_p : grammar **)

let coq_SCAS_p =
  map
    (bitsleft ('1'::('0'::('1'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::[]))) Any)) (fun x ->
    Obj.magic (SCAS (Obj.magic x)))

(** val coq_SETcc_p : grammar **)

let coq_SETcc_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('0'::('1'::[])))) (seq tttn modrm))))
    (fun p ->
    Obj.magic (SETcc ((fst (Obj.magic p)), (snd (snd (Obj.magic p))))))

(** val coq_SGDT_p : grammar **)

let coq_SGDT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm ('0'::('0'::('0'::[])))))))) (fun x ->
    Obj.magic (SGDT (Obj.magic x)))

(** val coq_SHL_p : grammar **)

let coq_SHL_p =
  rotate_p ('1'::('0'::('0'::[]))) (fun x x0 x1 -> SHL (x, x0, x1))

(** val shiftdouble_p :
    char list -> (operand -> interp -> reg_or_immed -> interp) -> grammar **)

let shiftdouble_p opcode inst =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('0'::('1'::('0'::[]))))
            (bitsleft opcode
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::[])) (seq reg (seq reg byte))))))))
      (fun p ->
      let (r2, i) = Obj.magic p in
      let (r1, b) = i in inst (Reg_op r1) r2 (Imm_ri b)))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('0'::('1'::('0'::[]))))
              (bitsleft opcode
                (bitsleft ('0'::('0'::[])) (seq modrm_noreg byte))))))
        (fun p ->
        let (i, b) = Obj.magic p in let (r, op) = i in inst op r (Imm_ri b)))
      (alt
        (map
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('1'::('0'::('1'::('0'::[]))))
                (bitsleft opcode
                  (bitsleft ('0'::('1'::[]))
                    (bitsleft ('1'::('1'::[])) (seq reg reg))))))) (fun p ->
          let (r2, r1) = Obj.magic p in inst (Reg_op r1) r2 (Reg_ri ECX)))
        (map
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('1'::('0'::('1'::('0'::[]))))
                (bitsleft opcode (bitsleft ('0'::('1'::[])) modrm_noreg)))))
          (fun p -> let (r, op) = Obj.magic p in inst op r (Reg_ri ECX)))))

(** val coq_SHLD_p : grammar **)

let coq_SHLD_p =
  shiftdouble_p ('0'::('1'::[]))
    (Obj.magic (fun x x0 x1 -> SHLD (x, x0, x1)))

(** val coq_SHR_p : grammar **)

let coq_SHR_p =
  rotate_p ('1'::('0'::('1'::[]))) (fun x x0 x1 -> SHR (x, x0, x1))

(** val coq_SHRD_p : grammar **)

let coq_SHRD_p =
  shiftdouble_p ('1'::('1'::[]))
    (Obj.magic (fun x x0 x1 -> SHRD (x, x0, x1)))

(** val coq_SIDT_p : grammar **)

let coq_SIDT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm ('0'::('0'::('1'::[])))))))) (fun x ->
    Obj.magic (SIDT (Obj.magic x)))

(** val coq_SLDT_p : grammar **)

let coq_SLDT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (ext_op_modrm2 ('0'::('0'::('0'::[])))))))) (fun x ->
    Obj.magic (SLDT (Obj.magic x)))

(** val coq_SMSW_p : grammar **)

let coq_SMSW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (ext_op_modrm2 ('1'::('0'::('0'::[])))))))) (fun x ->
    Obj.magic (SMSW (Obj.magic x)))

(** val coq_STC_p : grammar **)

let coq_STC_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('0'::('0'::('1'::[])))))) (fun _ -> Obj.magic STC)

(** val coq_STD_p : grammar **)

let coq_STD_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('1'::('0'::('1'::[])))))) (fun _ -> Obj.magic STD)

(** val coq_STI_p : grammar **)

let coq_STI_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('1'::('0'::('1'::('1'::[])))))) (fun _ -> Obj.magic STI)

(** val coq_STOS_p : grammar **)

let coq_STOS_p =
  map
    (bitsleft ('1'::('0'::('1'::('0'::[]))))
      (bitsleft ('1'::('0'::('1'::[]))) Any)) (fun x ->
    Obj.magic (STOS (Obj.magic x)))

(** val coq_STR_p : grammar **)

let coq_STR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (ext_op_modrm2 ('0'::('0'::('1'::[])))))))) (fun x ->
    Obj.magic (STR (Obj.magic x)))

(** val coq_TEST_p : bool -> grammar **)

let coq_TEST_p opsize_override =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('1'::[]))))
          (seq (ext_op_modrm2 ('0'::('0'::('0'::[]))))
            (imm_op opsize_override)))) (fun p ->
      Obj.magic (TEST (true, (fst (Obj.magic p)), (snd (Obj.magic p))))))
    (alt
      (map
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (seq (ext_op_modrm2 ('0'::('0'::('0'::[])))) byte))) (fun p ->
        Obj.magic (TEST (false, (fst (Obj.magic p)), (Imm_op
          (zero_extend8_32 (snd (Obj.magic p))))))))
      (alt
        (map
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (bitsleft ('0'::('1'::('0'::[]))) (seq Any modrm))) (fun p ->
          let (w, i) = Obj.magic p in
          let (op1, op2) = i in Obj.magic (TEST (w, op1, op2))))
        (alt
          (map
            (bitsleft ('1'::('0'::('1'::('0'::[]))))
              (bitsleft ('1'::('0'::('0'::('1'::[]))))
                (imm_op opsize_override))) (fun w ->
            Obj.magic (TEST (true, (Obj.magic w), (Reg_op EAX)))))
          (map
            (bitsleft ('1'::('0'::('1'::('0'::[]))))
              (bitsleft ('1'::('0'::('0'::('0'::[])))) byte)) (fun b ->
            Obj.magic (TEST (false, (Reg_op EAX), (Imm_op
              (zero_extend8_32 (Obj.magic b))))))))))

(** val coq_UD2_p : grammar **)

let coq_UD2_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bits ('1'::('0'::('1'::('1'::[])))))))) (fun _ -> Obj.magic UD2)

(** val coq_VERR_p : grammar **)

let coq_VERR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (ext_op_modrm2 ('1'::('0'::('0'::[])))))))) (fun x ->
    Obj.magic (VERR (Obj.magic x)))

(** val coq_VERW_p : grammar **)

let coq_VERW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (ext_op_modrm2 ('1'::('0'::('1'::[])))))))) (fun x ->
    Obj.magic (VERW (Obj.magic x)))

(** val coq_WBINVD_p : grammar **)

let coq_WBINVD_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bits ('1'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic WBINVD)

(** val coq_WRMSR_p : grammar **)

let coq_WRMSR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bits ('0'::('0'::('0'::('0'::[])))))))) (fun _ -> Obj.magic WRMSR)

(** val coq_XADD_p : grammar **)

let coq_XADD_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('0'::[]))) (seq Any modrm))))) (fun p ->
    let (w, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (XADD (w, op2, op1)))

(** val coq_XCHG_p : grammar **)

let coq_XCHG_p =
  alt
    (map
      (bitsleft ('1'::('0'::('0'::('0'::[]))))
        (bitsleft ('0'::('1'::('1'::[]))) (seq Any modrm))) (fun p ->
      let (w, i) = Obj.magic p in
      let (op1, op2) = i in Obj.magic (XCHG (w, op2, op1))))
    (map (bitsleft ('1'::('0'::('0'::('1'::[])))) (bitsleft ('0'::[]) reg))
      (fun r ->
      Obj.magic (XCHG (false, (Reg_op EAX), (Reg_op (Obj.magic r))))))

(** val coq_XLAT_p : grammar **)

let coq_XLAT_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::[]))))
      (bits ('0'::('1'::('1'::('1'::[])))))) (fun _ -> Obj.magic XLAT)

(** val coq_F2XM1_p : grammar **)

let coq_F2XM1_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('0'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic F2XM1)

(** val coq_FABS_p : grammar **)

let coq_FABS_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FABS)

(** val coq_FADD_p : grammar **)

let coq_FADD_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FADD (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('0'::('0'::('0'::[])))))) (fun x ->
        Obj.magic (FADD (true, (Obj.magic x)))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (seq Any
            (bitsleft ('0'::('0'::('1'::('1'::('0'::('0'::('0'::[])))))))
              fpu_reg))) (fun p ->
        let (d, s) = Obj.magic p in Obj.magic (FADD (d, (FPS_op s))))))

(** val coq_FADDP_p : grammar **)

let coq_FADDP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('0'::('0'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FADDP (FPS_op (Obj.magic x))))

(** val coq_FBLD_p : grammar **)

let coq_FBLD_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('1'::[])))
        (ext_op_modrm_FPM64 ('1'::('0'::('0'::[])))))) (fun x ->
    Obj.magic (FBLD (Obj.magic x)))

(** val coq_FBSTP_p : grammar **)

let coq_FBSTP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('1'::[])))
        (ext_op_modrm_FPM64 ('1'::('1'::('0'::[])))))) (fun x ->
    Obj.magic (FBSTP (Obj.magic x)))

(** val coq_FCHS_p : grammar **)

let coq_FCHS_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('0'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FCHS)

(** val coq_FCMOVcc_p : grammar **)

let coq_FCMOVcc_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('1'::[]))
        (seq Any
          (bitsleft ('1'::('1'::('0'::[]))) (seq Any (seq Any fpu_reg))))))
    (fun p ->
    let (b2, i) = Obj.magic p in
    let (b1, i0) = i in
    let (b0, s) = i0 in
    let n =
      bits2int (Big.succ (Big.succ (Big.succ Big.zero)))
        (Obj.magic (b2, (b1, (b0, ()))))
    in
    Obj.magic (FCMOVcc ((coq_Z_to_fp_condition_type (Obj.magic n)), (FPS_op
      s))))

(** val coq_FCOM_p : grammar **)

let coq_FCOM_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FCOM (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('0'::('1'::('0'::[])))))) (fun x ->
        Obj.magic (FCOM (Obj.magic x))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('0'::('0'::('0'::[])))
            (bitsleft ('1'::('1'::('0'::('1'::('0'::[]))))) fpu_reg)))
        (fun x -> Obj.magic (FCOM (FPS_op (Obj.magic x))))))

(** val coq_FCOMP_p : grammar **)

let coq_FCOMP_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FCOMP (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('0'::('1'::('1'::[])))))) (fun x ->
        Obj.magic (FCOMP (Obj.magic x))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('0'::('0'::('0'::[])))
            (bitsleft ('1'::('1'::('0'::('1'::('1'::[]))))) fpu_reg)))
        (fun x -> Obj.magic (FCOMP (FPS_op (Obj.magic x))))))

(** val coq_FCOMPP_p : grammar **)

let coq_FCOMPP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bits ('0'::('0'::('1'::[]))))))) (fun _ -> Obj.magic FCOMPP)

(** val coq_FCOMIP_p : grammar **)

let coq_FCOMIP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('1'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FCOMIP (FPS_op (Obj.magic x))))

(** val coq_FCOS_p : grammar **)

let coq_FCOS_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::[])))
          (bits ('1'::('1'::('1'::('1'::('1'::[]))))))))) (fun _ ->
    Obj.magic FCOS)

(** val coq_FDECSTP_p : grammar **)

let coq_FDECSTP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::[])))
          (bits ('1'::('0'::('1'::('1'::('0'::[]))))))))) (fun _ ->
    Obj.magic FDECSTP)

(** val coq_FDIV_p : grammar **)

let coq_FDIV_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FDIV (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('1'::('1'::('0'::[])))))) (fun x ->
        Obj.magic (FDIV (true, (Obj.magic x)))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::('1'::[]))))
                  (bitsleft ('0'::[]) fpu_reg))))) (fun i ->
          Obj.magic (FDIV (true, (FPS_op (Obj.magic i))))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('1'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('1'::[]) (bitsleft ('1'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FDIV (false, (FPS_op (Obj.magic i))))))))

(** val coq_FDIVP_p : grammar **)

let coq_FDIVP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('1'::('1'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FDIVP (FPS_op (Obj.magic x))))

(** val coq_FDIVR_p : grammar **)

let coq_FDIVR_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FDIVR (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('1'::('1'::('1'::[])))))) (fun x ->
        Obj.magic (FDIVR (true, (Obj.magic x)))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('1'::[]) (bitsleft ('1'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FDIVR (true, (FPS_op (Obj.magic i))))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('1'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('1'::[]) (bitsleft ('0'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FDIVR (false, (FPS_op (Obj.magic i))))))))

(** val coq_FDIVRP_p : grammar **)

let coq_FDIVRP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('1'::('1'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FDIVRP (FPS_op (Obj.magic x))))

(** val coq_FFREE_p : grammar **)

let coq_FFREE_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('0'::('0'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FFREE (FPS_op (Obj.magic x))))

(** val coq_FIADD_p : grammar **)

let coq_FIADD_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FIADD (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FIADD (Obj.magic x))))

(** val coq_FICOM_p : grammar **)

let coq_FICOM_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FICOM (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FICOM (Obj.magic x))))

(** val coq_FICOMP_p : grammar **)

let coq_FICOMP_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('0'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FICOMP (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FICOMP (Obj.magic x))))

(** val coq_FIDIV_p : grammar **)

let coq_FIDIV_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('1'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FIDIV (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FIDIV (Obj.magic x))))

(** val coq_FIDIVR_p : grammar **)

let coq_FIDIVR_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('1'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FIDIVR (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FIDIVR (Obj.magic x))))

(** val coq_FILD_p : grammar **)

let coq_FILD_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('1'::[])))
          (ext_op_modrm_FPM16 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FILD (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('0'::('1'::('1'::[])))
            (ext_op_modrm_FPM32 ('0'::('0'::('0'::[])))))) (fun x ->
        Obj.magic (FILD (Obj.magic x))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('1'::('1'::[])))
            (ext_op_modrm_FPM64 ('1'::('0'::('1'::[])))))) (fun x ->
        Obj.magic (FILD (Obj.magic x)))))

(** val coq_FIMUL_p : grammar **)

let coq_FIMUL_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('0'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FIMUL (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FIMUL (Obj.magic x))))

(** val coq_FINCSTP_p : grammar **)

let coq_FINCSTP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('1'::('1'::('1'::[])))))))) (fun _ ->
    Obj.magic FINCSTP)

(** val coq_FIST_p : grammar **)

let coq_FIST_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('1'::[])))
          (ext_op_modrm_FPM16 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FIST (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('1'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FIST (Obj.magic x))))

(** val coq_FISTP_p : grammar **)

let coq_FISTP_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('1'::[])))
          (ext_op_modrm_FPM16 ('0'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FISTP (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('0'::('1'::('1'::[])))
            (ext_op_modrm_FPM32 ('0'::('1'::('1'::[])))))) (fun x ->
        Obj.magic (FISTP (Obj.magic x))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('1'::('1'::[])))
            (ext_op_modrm_FPM64 ('1'::('1'::('1'::[])))))) (fun x ->
        Obj.magic (FISTP (Obj.magic x)))))

(** val coq_FISUB_p : grammar **)

let coq_FISUB_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('1'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FISUB (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FISUB (Obj.magic x))))

(** val coq_FISUBR_p : grammar **)

let coq_FISUBR_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('0'::[])))
          (ext_op_modrm_FPM16 ('1'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FISUBR (Obj.magic x))))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('1'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FISUBR (Obj.magic x))))

(** val coq_FLD_p : grammar **)

let coq_FLD_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('1'::[])))
          (ext_op_modrm_FPM32 ('0'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FLD (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('1'::[])))
            (ext_op_modrm_FPM64 ('0'::('0'::('0'::[])))))) (fun x ->
        Obj.magic (FLD (Obj.magic x))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::('1'::('1'::[])))
              (ext_op_modrm_FPM80 ('1'::('0'::('1'::[])))))) (fun x ->
          Obj.magic (FLD (Obj.magic x))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::('0'::('1'::[])))
              (bitsleft ('1'::('1'::('0'::('0'::('0'::[]))))) fpu_reg)))
          (fun x -> Obj.magic (FLD (FPS_op (Obj.magic x)))))))

(** val coq_FLD1_p : grammar **)

let coq_FLD1_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('0'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FLD1)

(** val coq_FLDCW_p : grammar **)

let coq_FLDCW_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (ext_op_modrm_FPM32 ('1'::('0'::('1'::[])))))) (fun x ->
    Obj.magic (FLDCW (Obj.magic x)))

(** val coq_FLDENV_p : grammar **)

let coq_FLDENV_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (ext_op_modrm_FPM32 ('1'::('0'::('0'::[])))))) (fun x ->
    Obj.magic (FLDENV (Obj.magic x)))

(** val coq_FLDL2E_p : grammar **)

let coq_FLDL2E_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('0'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FLDL2E)

(** val coq_FLDL2T_p : grammar **)

let coq_FLDL2T_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FLDL2T)

(** val coq_FLDLG2_p : grammar **)

let coq_FLDLG2_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('1'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FLDLG2)

(** val coq_FLDLN2_p : grammar **)

let coq_FLDLN2_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('1'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FLDLN2)

(** val coq_FLDPI_p : grammar **)

let coq_FLDPI_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('0'::('1'::('1'::[])))))))) (fun _ ->
    Obj.magic FLDPI)

(** val coq_FLDZ_p : grammar **)

let coq_FLDZ_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('1'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FLDZ)

(** val coq_FMUL_p : grammar **)

let coq_FMUL_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('0'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FMUL (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('0'::('0'::('1'::[])))))) (fun x ->
        Obj.magic (FMUL (true, (Obj.magic x)))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (seq Any
            (bitsleft ('0'::('0'::[]))
              (bitsleft ('1'::('1'::('0'::('0'::('1'::[]))))) fpu_reg))))
        (fun p ->
        let (d, s) = Obj.magic p in Obj.magic (FMUL (d, (FPS_op s))))))

(** val coq_FMULP_p : grammar **)

let coq_FMULP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('0'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FMULP (FPS_op (Obj.magic x))))

(** val coq_FNCLEX_p : grammar **)

let coq_FNCLEX_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('1'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('0'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FNCLEX)

(** val coq_FNINIT_p : grammar **)

let coq_FNINIT_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('1'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('0'::('1'::('1'::[])))))))) (fun _ ->
    Obj.magic FNINIT)

(** val coq_FNOP_p : grammar **)

let coq_FNOP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('0'::[]))))))
        (bits ('1'::('0'::('0'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FNOP)

(** val coq_FNSAVE_p : grammar **)

let coq_FNSAVE_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::('1'::('0'::('1'::[]))))))))
      (ext_op_modrm_FPM64 ('1'::('1'::('0'::[]))))) (fun x ->
    Obj.magic (FNSAVE (Obj.magic x)))

(** val coq_FNSTCW_p : grammar **)

let coq_FNSTCW_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (ext_op_modrm_FPM32 ('1'::('1'::('1'::[])))))) (fun x ->
    Obj.magic (FNSTCW (Obj.magic x)))

(** val coq_FNSTSW_p : grammar **)

let coq_FNSTSW_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('1'::('1'::[])))
          (bitsleft ('1'::('1'::('1'::[])))
            (bits ('0'::('0'::('0'::('0'::('0'::[]))))))))) (fun _ ->
      Obj.magic (FNSTSW None)))
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('1'::('0'::('1'::[])))
          (ext_op_modrm_FPM32 ('1'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FNSTSW (Some (Obj.magic x)))))

(** val coq_FPATAN_p : grammar **)

let coq_FPATAN_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('0'::('1'::('1'::[])))))))) (fun _ ->
    Obj.magic FPATAN)

(** val coq_FPREM_p : grammar **)

let coq_FPREM_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('0'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FPREM)

(** val coq_FPREM1_p : grammar **)

let coq_FPREM1_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('1'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FPREM1)

(** val coq_FPTAN_p : grammar **)

let coq_FPTAN_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('0'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FPTAN)

(** val coq_FRNDINT_p : grammar **)

let coq_FRNDINT_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('1'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FRNDINT)

(** val coq_FRSTOR_p : grammar **)

let coq_FRSTOR_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('0'::('1'::[])))
        (ext_op_modrm_FPM32 ('1'::('0'::('0'::[])))))) (fun x ->
    Obj.magic (FRSTOR (Obj.magic x)))

(** val coq_FSCALE_p : grammar **)

let coq_FSCALE_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('1'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FSCALE)

(** val coq_FSIN_p : grammar **)

let coq_FSIN_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('1'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FSIN)

(** val coq_FSINCOS_p : grammar **)

let coq_FSINCOS_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('0'::('1'::('1'::[])))))))) (fun _ ->
    Obj.magic FSINCOS)

(** val coq_FSQRT_p : grammar **)

let coq_FSQRT_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('0'::('1'::('0'::[])))))))) (fun _ ->
    Obj.magic FSQRT)

(** val coq_FST_p : grammar **)

let coq_FST_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('1'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('0'::[])))))) (fun x ->
      Obj.magic (FST (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('1'::[])))
            (ext_op_modrm_FPM64 ('0'::('1'::('0'::[])))))) (fun x ->
        Obj.magic (FST (Obj.magic x))))
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('1'::[])))
            (bitsleft ('1'::('1'::('0'::('1'::('0'::[]))))) fpu_reg)))
        (fun x -> Obj.magic (FST (FPS_op (Obj.magic x))))))

(** val coq_FSTENV_p : grammar **)

let coq_FSTENV_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (ext_op_modrm_FPM32 ('1'::('1'::('0'::[])))))) (fun x ->
    Obj.magic (FSTENV (Obj.magic x)))

(** val coq_FSTP_p : grammar **)

let coq_FSTP_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('1'::[])))
          (ext_op_modrm_FPM32 ('0'::('1'::('1'::[])))))) (fun x ->
      Obj.magic (FSTP (Obj.magic x))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('1'::[])))
            (ext_op_modrm_FPM64 ('0'::('1'::('1'::[])))))) (fun x ->
        Obj.magic (FSTP (Obj.magic x))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::('1'::('1'::[])))
              (ext_op_modrm_FPM80 ('1'::('1'::('1'::[])))))) (fun x ->
          Obj.magic (FSTP (Obj.magic x))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('1'::('0'::('1'::[])))
              (bitsleft ('1'::('1'::('0'::('1'::('1'::[]))))) fpu_reg)))
          (fun x -> Obj.magic (FSTP (FPS_op (Obj.magic x)))))))

(** val coq_FSUB_p : grammar **)

let coq_FSUB_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('0'::('0'::[])))))) (fun x ->
      Obj.magic (FSUB (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('1'::('0'::('0'::[])))))) (fun x ->
        Obj.magic (FSUB (true, (Obj.magic x)))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('0'::[]) (bitsleft ('0'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FSUB (true, (FPS_op (Obj.magic i))))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('1'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('0'::[]) (bitsleft ('1'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FSUB (false, (FPS_op (Obj.magic i))))))))

(** val coq_FSUBP_p : grammar **)

let coq_FSUBP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FSUBP (FPS_op (Obj.magic x))))

(** val coq_FSUBR_p : grammar **)

let coq_FSUBR_p =
  alt
    (map
      (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
        (bitsleft ('0'::('0'::('0'::[])))
          (ext_op_modrm_FPM32 ('1'::('0'::('1'::[])))))) (fun x ->
      Obj.magic (FSUBR (true, (Obj.magic x)))))
    (alt
      (map
        (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
          (bitsleft ('1'::('0'::('0'::[])))
            (ext_op_modrm_FPM64 ('1'::('0'::('1'::[])))))) (fun x ->
        Obj.magic (FSUBR (true, (Obj.magic x)))))
      (alt
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('0'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('0'::[]) (bitsleft ('1'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FSUBR (true, (FPS_op (Obj.magic i))))))
        (map
          (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
            (bitsleft ('1'::[])
              (bitsleft ('0'::('0'::[]))
                (bitsleft ('1'::('1'::('1'::[])))
                  (bitsleft ('0'::[]) (bitsleft ('0'::[]) fpu_reg))))))
          (fun i -> Obj.magic (FSUBR (false, (FPS_op (Obj.magic i))))))))

(** val coq_FSUBRP_p : grammar **)

let coq_FSUBRP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('0'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FSUBRP (FPS_op (Obj.magic x))))

(** val coq_FTST_p : grammar **)

let coq_FTST_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('1'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FTST)

(** val coq_FUCOM_p : grammar **)

let coq_FUCOM_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('0'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FUCOM (FPS_op (Obj.magic x))))

(** val coq_FUCOMP_p : grammar **)

let coq_FUCOMP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FUCOMP (FPS_op (Obj.magic x))))

(** val coq_FUCOMPP_p : grammar **)

let coq_FUCOMPP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('1'::('0'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('1'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FUCOMPP)

(** val coq_FUCOMI_p : grammar **)

let coq_FUCOMI_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('1'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FUCOMI (FPS_op (Obj.magic x))))

(** val coq_FUCOMIP_p : grammar **)

let coq_FUCOMIP_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('1'::('1'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FUCOMIP (FPS_op (Obj.magic x))))

(** val coq_FXAM_p : grammar **)

let coq_FXAM_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('0'::('0'::('1'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FXAM)

(** val coq_FXCH_p : grammar **)

let coq_FXCH_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('0'::('0'::('1'::[]))))) fpu_reg))) (fun x ->
    Obj.magic (FXCH (FPS_op (Obj.magic x))))

(** val coq_FXTRACT_p : grammar **)

let coq_FXTRACT_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::[])))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bits ('0'::('1'::('0'::('0'::[])))))))) (fun _ ->
    Obj.magic FXTRACT)

(** val coq_FYL2X_p : grammar **)

let coq_FYL2X_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('0'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FYL2X)

(** val coq_FYL2XP1_p : grammar **)

let coq_FYL2XP1_p =
  map
    (bitsleft ('1'::('1'::('0'::('1'::('1'::[])))))
      (bitsleft ('0'::('0'::('1'::('1'::('1'::('1'::[]))))))
        (bits ('1'::('1'::('0'::('0'::('1'::[])))))))) (fun _ ->
    Obj.magic FYL2XP1)

(** val coq_FWAIT_p : grammar **)

let coq_FWAIT_p =
  map (bits ('1'::('0'::('0'::('1'::('1'::('0'::('1'::('1'::[])))))))))
    (fun _ -> Obj.magic FWAIT)

(** val mmx_gg_p : bool -> bool -> bool -> bool -> grammar **)

let mmx_gg_p byte0 twob fourb eightb =
  let byte_p =
    if byte0
    then map (bits ('0'::('0'::[]))) (fun _ -> Obj.magic MMX_8)
    else Zero
  in
  let twobytes_p =
    if twob
    then map (bits ('0'::('1'::[]))) (fun _ -> Obj.magic MMX_16)
    else Zero
  in
  let fourbytes_p =
    if fourb
    then map (bits ('1'::('0'::[]))) (fun _ -> Obj.magic MMX_32)
    else Zero
  in
  let eightbytes_p =
    if eightb
    then map (bits ('1'::('1'::[]))) (fun _ -> Obj.magic MMX_64)
    else Zero
  in
  alt byte_p (alt twobytes_p (alt fourbytes_p eightbytes_p))

(** val coq_EMMS_p : grammar **)

let coq_EMMS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('1'::[]))))
          (bits ('0'::('1'::('1'::('1'::[])))))))) (fun _ -> Obj.magic EMMS)

(** val coq_MOVD_p : grammar **)

let coq_MOVD_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (seq mmx_reg reg)))))) (fun p ->
      let (m, r) = Obj.magic p in
      Obj.magic (MOVD ((GP_Reg_op r), (MMX_Reg_op m)))))
    (alt
      (map
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('1'::('1'::[]))))
              (bitsleft ('1'::('1'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::[])) (seq mmx_reg reg)))))) (fun p ->
        let (m, r) = Obj.magic p in
        Obj.magic (MOVD ((MMX_Reg_op m), (GP_Reg_op r)))))
      (alt
        (map
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('1'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::('1'::('0'::[]))))
                  (modrm_gen_noreg mmx_reg_op
                    (Obj.magic (fun x -> MMX_Addr_op x))))))) (fun p ->
          let (op1, op2) = Obj.magic p in Obj.magic (MOVD (op1, op2))))
        (map
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('1'::('1'::('1'::[]))))
                (bitsleft ('1'::('1'::('1'::('0'::[]))))
                  (modrm_gen_noreg mmx_reg_op
                    (Obj.magic (fun x -> MMX_Addr_op x))))))) (fun p ->
          let (mem, mmx) = Obj.magic p in Obj.magic (MOVD (mmx, mem))))))

(** val coq_MOVQ_p : grammar **)

let coq_MOVQ_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVQ (op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVQ (op2, op1))))

(** val coq_PACKSSDW_p : grammar **)

let coq_PACKSSDW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PACKSSDW (op1, op2)))

(** val coq_PACKSSWB_p : grammar **)

let coq_PACKSSWB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PACKSSWB (op1, op2)))

(** val coq_PACKUSWB_p : grammar **)

let coq_PACKUSWB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PACKUSWB (op1, op2)))

(** val coq_PADD_p : grammar **)

let coq_PADD_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PADD (gg, op1, op2)))

(** val coq_PADDS_p : grammar **)

let coq_PADDS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::[]))
            (seq (mmx_gg_p true true false false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PADDS (gg, op1, op2)))

(** val coq_PADDUS_p : grammar **)

let coq_PADDUS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::[]))
            (seq (mmx_gg_p true true false false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PADDUS (gg, op1, op2)))

(** val coq_PAND_p : grammar **)

let coq_PAND_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PAND (op1, op2)))

(** val coq_PANDN_p : grammar **)

let coq_PANDN_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PANDN (op1, op2)))

(** val coq_PCMPEQ_p : grammar **)

let coq_PCMPEQ_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PCMPEQ (gg, op1, op2)))

(** val coq_PCMPGT_p : grammar **)

let coq_PCMPGT_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('1'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PCMPGT (gg, op1, op2)))

(** val coq_PMADDWD_p : grammar **)

let coq_PMADDWD_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMADDWD (op1, op2)))

(** val coq_PMULHUW_p : grammar **)

let coq_PMULHUW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('1'::('0'::('0'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMULHUW (op1, op2)))

(** val coq_PMULHW_p : grammar **)

let coq_PMULHW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMULHW (op1, op2)))

(** val coq_PMULLW_p : grammar **)

let coq_PMULLW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMULLW (op1, op2)))

(** val coq_POR_p : grammar **)

let coq_POR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (POR (op1, op2)))

(** val coq_PSLL_p : grammar **)

let coq_PSLL_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true true) modrm_mmx))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (op1, op2) = i in Obj.magic (PSLL (gg, op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true true)
                (bitsleft ('1'::('1'::('1'::('1'::('0'::[])))))
                  (seq mmx_reg byte))))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (r, imm) = i in
      Obj.magic (PSLL (gg, (MMX_Reg_op r), (MMX_Imm_op
        (zero_extend8_32 imm))))))

(** val coq_PSRA_p : grammar **)

let coq_PSRA_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true false) modrm_mmx))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (op1, op2) = i in Obj.magic (PSRA (gg, op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true false)
                (bitsleft ('1'::('1'::('1'::('0'::('0'::[])))))
                  (seq mmx_reg byte))))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (r, imm) = i in
      Obj.magic (PSRA (gg, (MMX_Reg_op r), (MMX_Imm_op
        (zero_extend8_32 imm))))))

(** val coq_PSRL_p : grammar **)

let coq_PSRL_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('1'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true true) modrm_mmx))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (op1, op2) = i in Obj.magic (PSRL (gg, op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('0'::[]))
              (seq (mmx_gg_p false true true true)
                (bitsleft ('1'::('1'::('0'::('1'::('0'::[])))))
                  (seq mmx_reg byte))))))) (fun p ->
      let (gg, i) = Obj.magic p in
      let (r, imm) = i in
      Obj.magic (PSRL (gg, (MMX_Reg_op r), (MMX_Imm_op
        (zero_extend8_32 imm))))))

(** val coq_PSUB_p : grammar **)

let coq_PSUB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('0'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PSUB (gg, op1, op2)))

(** val coq_PSUBS_p : grammar **)

let coq_PSUBS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::[]))
            (seq (mmx_gg_p true true false false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PSUBS (gg, op1, op2)))

(** val coq_PSUBUS_p : grammar **)

let coq_PSUBUS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::[]))
            (seq (mmx_gg_p true true false false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PSUBUS (gg, op1, op2)))

(** val coq_PUNPCKH_p : grammar **)

let coq_PUNPCKH_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PUNPCKH (gg, op1, op2)))

(** val coq_PUNPCKL_p : grammar **)

let coq_PUNPCKL_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('0'::[]))
            (seq (mmx_gg_p true true true false) modrm_mmx))))) (fun p ->
    let (gg, i) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PUNPCKL (gg, op1, op2)))

(** val coq_PXOR_p : grammar **)

let coq_PXOR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_mmx)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PXOR (op1, op2)))

(** val coq_ADDPS_p : grammar **)

let coq_ADDPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (ADDPS (op1, op2)))

(** val coq_ADDSS_p : grammar **)

let coq_ADDSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('0'::('0'::('0'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (ADDSS (op1, op2)))

(** val coq_ANDNPS_p : grammar **)

let coq_ANDNPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (ANDNPS (op1, op2)))

(** val coq_ANDPS_p : grammar **)

let coq_ANDPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (ANDPS (op1, op2)))

(** val coq_CMPPS_p : grammar **)

let coq_CMPPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[])))) (seq modrm_xmm byte)))))
    (fun p ->
    let (i, imm) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (CMPPS (op1, op2, imm)))

(** val coq_CMPSS_p : grammar **)

let coq_CMPSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::('0'::('0'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[])))) (seq modrm_xmm byte)))))))
    (fun p ->
    let (i, imm) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (CMPSS (op1, op2, imm)))

(** val coq_COMISS_p : grammar **)

let coq_COMISS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (COMISS (op1, op2)))

(** val coq_CVTPI2PS_p : grammar **)

let coq_CVTPI2PS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('1'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (CVTPI2PS (op1, op2)))

(** val coq_CVTPS2PI_p : grammar **)

let coq_CVTPS2PI_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::[])) (seq sse_reg mmx_reg)))))) (fun p ->
      let (sr, mr) = Obj.magic p in
      Obj.magic (CVTPS2PI ((SSE_XMM_Reg_op sr), (SSE_MM_Reg_op mr)))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm_xmm_noreg))))
      (fun p ->
      let (xmm, mem) = Obj.magic p in Obj.magic (CVTPS2PI (xmm, mem))))

(** val coq_CVTSI2SS_p : grammar **)

let coq_CVTSI2SS_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('0'::('1'::('0'::[]))))
                  (bitsleft ('1'::('1'::[])) (seq sse_reg reg))))))))
      (fun p ->
      let (sr, r) = Obj.magic p in
      Obj.magic (CVTSI2SS ((SSE_XMM_Reg_op sr), (SSE_GP_Reg_op r)))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('0'::('1'::('0'::[])))) modrm_xmm_noreg))))))
      (fun p ->
      let (xmm, mem) = Obj.magic p in Obj.magic (CVTSI2SS (xmm, mem))))

(** val coq_CVTSS2SI_p : grammar **)

let coq_CVTSS2SI_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::('0'::('1'::[]))))
                  (bitsleft ('1'::('1'::[])) (seq reg sse_reg))))))))
      (fun p ->
      let (r, sr) = Obj.magic p in
      Obj.magic (CVTSS2SI ((SSE_GP_Reg_op r), (SSE_XMM_Reg_op sr)))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm_xmm_gp_noreg))))))
      (fun p ->
      let (op1, mem) = Obj.magic p in Obj.magic (CVTSS2SI (op1, mem))))

(** val coq_CVTTPS2PI_p : grammar **)

let coq_CVTTPS2PI_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (CVTTPS2PI (op1, op2)))

(** val coq_CVTTSS2SI_p : grammar **)

let coq_CVTTSS2SI_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::('0'::('0'::[]))))
                  (bitsleft ('1'::('1'::[])) (seq reg sse_reg))))))))
      (fun p ->
      let (r, sr) = Obj.magic p in
      Obj.magic (CVTTSS2SI ((SSE_GP_Reg_op r), (SSE_XMM_Reg_op sr)))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[]))))
                (bitsleft ('1'::('1'::('0'::('0'::[])))) modrm_xmm_gp_noreg))))))
      (fun p ->
      let (op1, mem) = Obj.magic p in Obj.magic (CVTTSS2SI (op1, mem))))

(** val coq_DIVPS_p : grammar **)

let coq_DIVPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (DIVPS (op1, op2)))

(** val coq_DIVSS_p : grammar **)

let coq_DIVSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::('1'::('0'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (DIVSS (op1, op2)))

(** val coq_LDMXCSR_p : grammar **)

let coq_LDMXCSR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('1'::('0'::[])))))))) (fun x ->
    Obj.magic (LDMXCSR (Obj.magic x)))

(** val coq_MAXPS_p : grammar **)

let coq_MAXPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (MAXPS (op1, op2)))

(** val coq_MAXSS_p : grammar **)

let coq_MAXSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::('1'::('1'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (MAXSS (op1, op2)))

(** val coq_MINPS_p : grammar **)

let coq_MINPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (MINPS (op1, op2)))

(** val coq_MINSS_p : grammar **)

let coq_MINSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::('0'::('1'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (MINSS (op1, op2)))

(** val coq_MOVAPS_p : grammar **)

let coq_MOVAPS_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('1'::('0'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVAPS (op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('1'::('0'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVAPS (op1, op2))))

(** val coq_MOVHLPS_p : grammar **)

let coq_MOVHLPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::[])) (seq sse_reg sse_reg)))))) (fun p ->
    let (sr1, sr2) = Obj.magic p in
    Obj.magic (MOVHLPS ((SSE_XMM_Reg_op sr1), (SSE_XMM_Reg_op sr2))))

(** val coq_MOVLPS_p : grammar **)

let coq_MOVLPS_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm_xmm_noreg))))
      (fun p ->
      let (op1, mem) = Obj.magic p in Obj.magic (MOVLPS (op1, mem))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm_xmm_noreg))))
      (fun p ->
      let (op1, mem) = Obj.magic p in Obj.magic (MOVLPS (mem, op1))))

(** val coq_MOVMSKPS_p : grammar **)

let coq_MOVMSKPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::[])) (seq reg sse_reg)))))) (fun p ->
    let (r, sr) = Obj.magic p in
    Obj.magic (MOVMSKPS ((SSE_GP_Reg_op r), (SSE_XMM_Reg_op sr))))

(** val coq_MOVSS_p : grammar **)

let coq_MOVSS_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('0'::('1'::[]))))
                (bitsleft ('0'::('0'::('0'::('0'::[])))) modrm_xmm))))))
      (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVSS (op1, op2))))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bitsleft ('0'::('0'::('0'::('1'::[]))))
                (bitsleft ('0'::('0'::('0'::('1'::[])))) modrm_xmm))))))
      (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVSS (op2, op1))))

(** val coq_MOVUPS_p : grammar **)

let coq_MOVUPS_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVUPS (op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[]))))
            (bitsleft ('0'::('0'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (MOVUPS (op2, op1))))

(** val coq_MULPS_p : grammar **)

let coq_MULPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (MULPS (op1, op2)))

(** val coq_MULSS_p : grammar **)

let coq_MULSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('0'::('0'::('1'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (MULSS (op1, op2)))

(** val coq_ORPS_p : grammar **)

let coq_ORPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (ORPS (op1, op2)))

(** val coq_RCPPS_p : grammar **)

let coq_RCPPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (RCPPS (op1, op2)))

(** val coq_RCPSS_p : grammar **)

let coq_RCPSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (RCPSS (op1, op2)))

(** val coq_RSQRTPS_p : grammar **)

let coq_RSQRTPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (RSQRTPS (op1, op2)))

(** val coq_RSQRTSS_p : grammar **)

let coq_RSQRTSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('0'::('0'::('1'::('0'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (RSQRTSS (op1, op2)))

(** val coq_SHUFPS_p : grammar **)

let coq_SHUFPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[])))) (seq modrm_xmm byte)))))
    (fun p ->
    let (i, imm) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (SHUFPS (op1, op2, imm)))

(** val coq_SQRTPS_p : grammar **)

let coq_SQRTPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (SQRTPS (op1, op2)))

(** val coq_SQRTSS_p : grammar **)

let coq_SQRTSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('0'::('0'::('0'::('1'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (SQRTSS (op1, op2)))

(** val coq_STMXCSR_p : grammar **)

let coq_STMXCSR_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('1'::('1'::[])))))))) (fun x ->
    Obj.magic (STMXCSR (Obj.magic x)))

(** val coq_SUBPS_p : grammar **)

let coq_SUBPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (SUBPS (op1, op2)))

(** val coq_SUBSS_p : grammar **)

let coq_SUBSS_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bitsleft ('0'::('0'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('1'::[]))))
            (bitsleft ('0'::('1'::('0'::('1'::[]))))
              (bitsleft ('1'::('1'::('0'::('0'::[])))) modrm_xmm))))))
    (fun p -> let (op1, op2) = Obj.magic p in Obj.magic (SUBSS (op1, op2)))

(** val coq_UCOMISS_p : grammar **)

let coq_UCOMISS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (UCOMISS (op1, op2)))

(** val coq_UNPCKHPS_p : grammar **)

let coq_UNPCKHPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (UNPCKHPS (op1, op2)))

(** val coq_UNPCKLPS_p : grammar **)

let coq_UNPCKLPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('0'::('0'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (UNPCKLPS (op1, op2)))

(** val coq_XORPS_p : grammar **)

let coq_XORPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[])))) modrm_xmm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (XORPS (op1, op2)))

(** val coq_PAVGB_p : grammar **)

let coq_PAVGB_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('0'::('0'::[])))) modrm_mm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (PAVGB (op1, op2))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (bitsleft ('0'::('0'::('1'::('1'::[])))) modrm_mm)))) (fun p ->
      let (op1, op2) = Obj.magic p in Obj.magic (PAVGB (op2, op1))))

(** val coq_PEXTRW_p : grammar **)

let coq_PEXTRW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('0'::[]))))
          (bitsleft ('0'::('1'::('0'::('1'::[]))))
            (bitsleft ('1'::('1'::[])) (seq reg (seq mmx_reg byte)))))))
    (fun p ->
    let (r32, i) = Obj.magic p in
    let (mmx, imm) = i in
    Obj.magic (PEXTRW ((SSE_GP_Reg_op r32), (SSE_MM_Reg_op mmx), imm)))

(** val coq_PINSRW_p : grammar **)

let coq_PINSRW_p =
  alt
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('0'::[]))))
            (bitsleft ('0'::('1'::('0'::('0'::[]))))
              (bitsleft ('1'::('1'::[])) (seq mmx_reg (seq reg byte)))))))
      (fun p ->
      let (mmx, i) = Obj.magic p in
      let (r32, imm) = i in
      Obj.magic (PINSRW ((SSE_MM_Reg_op mmx), (SSE_GP_Reg_op r32), imm))))
    (map
      (bitsleft ('0'::('0'::('0'::('0'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('1'::('1'::('0'::('0'::[]))))
            (bitsleft ('0'::('1'::('0'::('0'::[]))))
              (seq modrm_mm_noreg byte))))) (fun p ->
      let (i, imm) = Obj.magic p in
      let (op1, mem) = i in Obj.magic (PINSRW (op1, mem, imm))))

(** val coq_PMAXSW_p : grammar **)

let coq_PMAXSW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[])))) modrm_mm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMAXSW (op1, op2)))

(** val coq_PMAXUB_p : grammar **)

let coq_PMAXUB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[])))) modrm_mm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMAXUB (op1, op2)))

(** val coq_PMINSW_p : grammar **)

let coq_PMINSW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('1'::('0'::[])))) modrm_mm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMINSW (op1, op2)))

(** val coq_PMINUB_p : grammar **)

let coq_PMINUB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('1'::('0'::[])))) modrm_mm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PMINUB (op1, op2)))

(** val coq_PMOVMSKB_p : grammar **)

let coq_PMOVMSKB_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('0'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::[])) (seq reg mmx_reg)))))) (fun p ->
    let (r, mr) = Obj.magic p in
    Obj.magic (PMOVMSKB ((SSE_GP_Reg_op r), (SSE_MM_Reg_op mr))))

(** val coq_PSADBW_p : grammar **)

let coq_PSADBW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('0'::[])))) modrm_mm)))) (fun p ->
    let (op1, op2) = Obj.magic p in Obj.magic (PSADBW (op1, op2)))

(** val coq_PSHUFW_p : grammar **)

let coq_PSHUFW_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('0'::('0'::('0'::[])))) (seq modrm_mm byte)))))
    (fun p ->
    let (i, imm) = Obj.magic p in
    let (op1, op2) = i in Obj.magic (PSHUFW (op1, op2, imm)))

(** val coq_MASKMOVQ_p : grammar **)

let coq_MASKMOVQ_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('1'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[]))))
            (bitsleft ('1'::('1'::[])) (seq mmx_reg mmx_reg)))))) (fun p ->
    let (mr1, mr2) = Obj.magic p in
    Obj.magic (MASKMOVQ ((SSE_MM_Reg_op mr1), (SSE_MM_Reg_op mr2))))

(** val coq_MOVNTPS_p : grammar **)

let coq_MOVNTPS_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('0'::('1'::('1'::[])))) modrm_xmm_noreg))))
    (fun p -> let (op1, mem) = Obj.magic p in Obj.magic (MOVNTPS (mem, op1)))

(** val coq_MOVNTQ_p : grammar **)

let coq_MOVNTQ_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('1'::('1'::('0'::[]))))
          (bitsleft ('0'::('1'::('1'::('1'::[])))) modrm_mm_noreg))))
    (fun p -> let (op1, mem) = Obj.magic p in Obj.magic (MOVNTQ (mem, op1)))

(** val coq_PREFETCHT0_p : grammar **)

let coq_PREFETCHT0_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('0'::('1'::[])))))))) (fun x ->
    Obj.magic (PREFETCHT0 (Obj.magic x)))

(** val coq_PREFETCHT1_p : grammar **)

let coq_PREFETCHT1_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('1'::('0'::[])))))))) (fun x ->
    Obj.magic (PREFETCHT1 (Obj.magic x)))

(** val coq_PREFETCHT2_p : grammar **)

let coq_PREFETCHT2_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('1'::('1'::[])))))))) (fun x ->
    Obj.magic (PREFETCHT2 (Obj.magic x)))

(** val coq_PREFETCHNTA_p : grammar **)

let coq_PREFETCHNTA_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('0'::('0'::('0'::('1'::[]))))
          (bitsleft ('1'::('0'::('0'::('0'::[]))))
            (ext_op_modrm_sse ('0'::('0'::('0'::[])))))))) (fun x ->
    Obj.magic (PREFETCHNTA (Obj.magic x)))

(** val coq_SFENCE_p : grammar **)

let coq_SFENCE_p =
  map
    (bitsleft ('0'::('0'::('0'::('0'::[]))))
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bitsleft ('1'::('0'::('1'::('0'::[]))))
          (bitsleft ('1'::('1'::('1'::('0'::[]))))
            (bitsleft ('1'::('1'::('1'::('1'::[]))))
              (bits ('1'::('0'::('0'::('0'::[])))))))))) (fun _ ->
    Obj.magic SFENCE)

(** val lock_p : grammar **)

let lock_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('0'::('0'::('0'::('0'::[])))))) (fun _ -> Obj.magic Coq_lock)

(** val rep_or_repn_p : grammar **)

let rep_or_repn_p =
  alt
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bits ('0'::('0'::('1'::('0'::[])))))) (fun _ -> Obj.magic Coq_repn))
    (map
      (bitsleft ('1'::('1'::('1'::('1'::[]))))
        (bits ('0'::('0'::('1'::('1'::[])))))) (fun _ -> Obj.magic Coq_rep))

(** val rep_p : grammar **)

let rep_p =
  map
    (bitsleft ('1'::('1'::('1'::('1'::[]))))
      (bits ('0'::('0'::('1'::('1'::[])))))) (fun _ -> Obj.magic Coq_rep)

(** val segment_override_p : grammar **)

let segment_override_p =
  alt
    (map
      (bitsleft ('0'::('0'::('1'::('0'::[]))))
        (bits ('1'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic CS))
    (alt
      (map
        (bitsleft ('0'::('0'::('1'::('1'::[]))))
          (bits ('0'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic SS))
      (alt
        (map
          (bitsleft ('0'::('0'::('1'::('1'::[]))))
            (bits ('1'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic DS))
        (alt
          (map
            (bitsleft ('0'::('0'::('1'::('0'::[]))))
              (bits ('0'::('1'::('1'::('0'::[])))))) (fun _ -> Obj.magic ES))
          (alt
            (map
              (bitsleft ('0'::('1'::('1'::('0'::[]))))
                (bits ('0'::('1'::('0'::('0'::[])))))) (fun _ ->
              Obj.magic FS))
            (map
              (bitsleft ('0'::('1'::('1'::('0'::[]))))
                (bits ('0'::('1'::('0'::('1'::[])))))) (fun _ ->
              Obj.magic GS))))))

(** val op_s : char list **)

let op_s =
  '0'::('1'::('1'::('0'::('0'::('1'::('1'::('0'::[])))))))

(** val op_override_p : grammar **)

let op_override_p =
  map (bitsleft op_s (Star (bits op_s))) (fun _ -> Obj.magic true)

(** val perm2 : grammar -> grammar -> grammar **)

let perm2 p1 p2 =
  alt (seq p1 p2)
    (map (seq p2 p1) (fun p -> let (a, b) = Obj.magic p in Obj.magic (b, a)))

(** val perm3 : grammar -> grammar -> grammar -> grammar **)

let perm3 p1 p2 p3 =
  alt (seq p1 (perm2 p2 p3))
    (alt
      (map (seq p2 (perm2 p1 p3)) (fun p ->
        let (b, i) = Obj.magic p in let (a, c) = i in Obj.magic (a, (b, c))))
      (map (seq p3 (perm2 p1 p2)) (fun p ->
        let (c, i) = Obj.magic p in let (a, b) = i in Obj.magic (a, (b, c)))))

(** val option_perm : grammar -> grammar **)

let option_perm p1 =
  alt (map Eps (fun _ -> Obj.magic None))
    (map p1 (fun p -> Obj.magic (Some p)))

(** val option_perm2 : grammar -> grammar -> grammar **)

let option_perm2 p1 p2 =
  alt (map Eps (fun _ -> Obj.magic (None, None)))
    (alt (map p1 (fun p -> Obj.magic ((Some p), None)))
      (alt (map p2 (fun p -> Obj.magic (None, (Some p))))
        (map (perm2 p1 p2) (fun p ->
          let (a, b) = Obj.magic p in Obj.magic ((Some a), (Some b))))))

(** val option_perm3 : grammar -> grammar -> grammar -> grammar **)

let option_perm3 p1 p2 p3 =
  alt (map Eps (fun _ -> Obj.magic (None, (None, None))))
    (alt (map p1 (fun p -> Obj.magic ((Some p), (None, None))))
      (alt (map p2 (fun p -> Obj.magic (None, ((Some p), None))))
        (alt (map p3 (fun p -> Obj.magic (None, (None, (Some p)))))
          (alt
            (map (perm2 p1 p2) (fun p ->
              let (a, b) = Obj.magic p in
              Obj.magic ((Some a), ((Some b), None))))
            (alt
              (map (perm2 p1 p3) (fun p ->
                let (a, c) = Obj.magic p in
                Obj.magic ((Some a), (None, (Some c)))))
              (alt
                (map (perm2 p2 p3) (fun p ->
                  let (b, c) = Obj.magic p in
                  Obj.magic (None, ((Some b), (Some c)))))
                (map (perm3 p1 p2 p3) (fun p ->
                  let (a, i) = Obj.magic p in
                  let (b, c) = i in
                  Obj.magic ((Some a), ((Some b), (Some c)))))))))))

(** val option_perm2_variation : grammar -> grammar -> grammar **)

let option_perm2_variation p1 p2 =
  alt (map p2 (fun p -> Obj.magic (None, p)))
    (map (perm2 p1 p2) (fun p ->
      let (a, b) = Obj.magic p in Obj.magic ((Some a), b)))

(** val option_perm3_variation : grammar -> grammar -> grammar -> grammar **)

let option_perm3_variation p1 p2 p3 =
  alt (map p3 (fun p -> Obj.magic (None, (None, p))))
    (alt
      (map (perm2 p1 p3) (fun p ->
        let (a, c) = Obj.magic p in Obj.magic ((Some a), (None, c))))
      (alt
        (map (perm2 p2 p3) (fun p ->
          let (b, c) = Obj.magic p in Obj.magic (None, ((Some b), c))))
        (map (perm3 p1 p2 p3) (fun p ->
          let (a, i) = Obj.magic p in
          let (b, c) = i in Obj.magic ((Some a), ((Some b), c))))))

(** val prefix_grammar_rep : grammar **)

let prefix_grammar_rep =
  map (option_perm3 rep_p segment_override_p op_override_p) (fun p ->
    let (l, i) = Obj.magic p in
    let (s, op) = i in
    Obj.magic { lock_rep = l; seg_override = s; op_override =
      (match op with
       | Some b -> b
       | None -> false); addr_override = false })

(** val instr_grammars_rep : grammar list **)

let instr_grammars_rep =
  coq_INS_p :: (coq_OUTS_p :: (coq_MOVS_p :: (coq_LODS_p :: (coq_STOS_p :: (coq_RET_p :: [])))))

(** val prefix_grammar_rep_or_repn : grammar **)

let prefix_grammar_rep_or_repn =
  map (option_perm3 rep_or_repn_p segment_override_p op_override_p) (fun p ->
    let (l, i) = Obj.magic p in
    let (s, op) = i in
    Obj.magic { lock_rep = l; seg_override = s; op_override =
      (match op with
       | Some b -> b
       | None -> false); addr_override = false })

(** val instr_grammars_rep_or_repn : grammar list **)

let instr_grammars_rep_or_repn =
  coq_CMPS_p :: (coq_SCAS_p :: [])

(** val prefix_grammar_lock_with_op_override : grammar **)

let prefix_grammar_lock_with_op_override =
  map (option_perm3_variation lock_p segment_override_p op_override_p)
    (fun p ->
    let (l, i) = Obj.magic p in
    let (s, op) = i in
    Obj.magic { lock_rep = l; seg_override = s; op_override = op;
      addr_override = false })

(** val instr_grammars_lock_with_op_override : grammar list **)

let instr_grammars_lock_with_op_override =
  (coq_ADD_p true) :: ((coq_ADC_p true) :: ((coq_AND_p true) :: (coq_DEC_p :: (coq_INC_p :: (coq_NEG_p :: (coq_NOT_p :: (
    (coq_OR_p true) :: ((coq_SBB_p true) :: ((coq_SUB_p true) :: ((coq_XOR_p
                                                                    true) :: (coq_XCHG_p :: [])))))))))))

(** val prefix_grammar_lock_no_op_override : grammar **)

let prefix_grammar_lock_no_op_override =
  map (option_perm2 lock_p segment_override_p) (fun p ->
    let (l, s) = Obj.magic p in
    Obj.magic { lock_rep = l; seg_override = s; op_override = false;
      addr_override = false })

(** val instr_grammars_lock_no_op_override : grammar list **)

let instr_grammars_lock_no_op_override =
  (coq_ADD_p false) :: ((coq_ADC_p false) :: ((coq_AND_p false) :: (coq_BTC_p :: (coq_BTR_p :: (coq_BTS_p :: (coq_CMPXCHG_p :: (coq_DEC_p :: (coq_INC_p :: (coq_NEG_p :: (coq_NOT_p :: (
    (coq_OR_p false) :: ((coq_SBB_p false) :: ((coq_SUB_p false) :: (
    (coq_XOR_p false) :: (coq_XADD_p :: (coq_XCHG_p :: []))))))))))))))))

(** val prefix_grammar_seg_with_op_override : grammar **)

let prefix_grammar_seg_with_op_override =
  map (option_perm2_variation segment_override_p op_override_p) (fun p ->
    let (s, op) = Obj.magic p in
    Obj.magic { lock_rep = None; seg_override = s; op_override = op;
      addr_override = false })

(** val instr_grammars_seg_with_op_override : grammar list **)

let instr_grammars_seg_with_op_override =
  (coq_CMP_p true) :: ((coq_IMUL_p true) :: ((coq_MOV_p true) :: ((coq_TEST_p
                                                                    true) :: [])))

(** val prefix_grammar_seg_op_override : grammar **)

let prefix_grammar_seg_op_override =
  map (option_perm2 segment_override_p op_override_p) (fun p ->
    let (s, op) = Obj.magic p in
    Obj.magic { lock_rep = None; seg_override = s; op_override =
      (match op with
       | Some b -> b
       | None -> false); addr_override = false })

(** val instr_grammars_seg_op_override : grammar list **)

let instr_grammars_seg_op_override =
  coq_CDQ_p :: (coq_CMOVcc_p :: (coq_CWDE_p :: (coq_DIV_p :: (coq_IDIV_p :: (coq_MOVSX_p :: (coq_MOVZX_p :: (coq_MUL_p :: (coq_NOP_p :: (coq_ROL_p :: (coq_ROR_p :: (coq_SAR_p :: (coq_SHL_p :: (coq_SHLD_p :: (coq_SHR_p :: (coq_SHRD_p :: (coq_MOVD_p :: []))))))))))))))))

(** val prefix_grammar_seg_override : grammar **)

let prefix_grammar_seg_override =
  map (option_perm segment_override_p) (fun s ->
    Obj.magic { lock_rep = None; seg_override = (Obj.magic s); op_override =
      false; addr_override = false })

(** val instr_grammars_seg_override : grammar list **)

let instr_grammars_seg_override =
  coq_AAA_p :: (coq_AAD_p :: (coq_AAM_p :: (coq_AAS_p :: ((coq_CMP_p false) :: (coq_ARPL_p :: (coq_BOUND_p :: (coq_BSF_p :: (coq_BSR_p :: (coq_BSWAP_p :: (coq_BT_p :: (coq_CALL_p :: (coq_CLC_p :: (coq_CLD_p :: (coq_CLI_p :: (coq_CLTS_p :: (coq_CMC_p :: (coq_CPUID_p :: (coq_DAA_p :: (coq_DAS_p :: (coq_HLT_p :: (
    (coq_IMUL_p false) :: (coq_IN_p :: (coq_INTn_p :: (coq_INT_p :: (coq_INTO_p :: (coq_INVD_p :: (coq_INVLPG_p :: (coq_IRET_p :: (coq_Jcc_p :: (coq_JCXZ_p :: (coq_JMP_p :: (coq_LAHF_p :: (coq_LAR_p :: (coq_LDS_p :: (coq_LEA_p :: (coq_LEAVE_p :: (coq_LES_p :: (coq_LFS_p :: (coq_LGDT_p :: (coq_LGS_p :: (coq_LIDT_p :: (coq_LLDT_p :: (coq_LMSW_p :: (coq_LOOP_p :: (coq_LOOPZ_p :: (coq_LOOPNZ_p :: (coq_LSL_p :: (coq_LSS_p :: (coq_LTR_p :: (
    (coq_MOV_p false) :: (coq_MOVCR_p :: (coq_MOVDR_p :: (coq_MOVSR_p :: (coq_MOVBE_p :: (coq_OUT_p :: (coq_POP_p :: (coq_POPSR_p :: (coq_POPA_p :: (coq_POPF_p :: (coq_PUSH_p :: (coq_PUSHSR_p :: (coq_PUSHA_p :: (coq_PUSHF_p :: (coq_RCL_p :: (coq_RCR_p :: (coq_RDMSR_p :: (coq_RDPMC_p :: (coq_RDTSC_p :: (coq_RDTSCP_p :: (coq_RSM_p :: (coq_SAHF_p :: (coq_SETcc_p :: (coq_SGDT_p :: (coq_SIDT_p :: (coq_SLDT_p :: (coq_SMSW_p :: (coq_STC_p :: (coq_STD_p :: (coq_STI_p :: (coq_STR_p :: (
    (coq_TEST_p false) :: (coq_UD2_p :: (coq_VERR_p :: (coq_VERW_p :: (coq_WBINVD_p :: (coq_WRMSR_p :: (coq_XLAT_p :: (coq_F2XM1_p :: (coq_FABS_p :: (coq_FADD_p :: (coq_FADDP_p :: (coq_FBLD_p :: (coq_FBSTP_p :: (coq_FCHS_p :: (coq_FCMOVcc_p :: (coq_FCOM_p :: (coq_FCOMP_p :: (coq_FCOMPP_p :: (coq_FCOMIP_p :: (coq_FCOS_p :: (coq_FDECSTP_p :: (coq_FDIV_p :: (coq_FDIVP_p :: (coq_FDIVR_p :: (coq_FDIVRP_p :: (coq_FFREE_p :: (coq_FIADD_p :: (coq_FICOM_p :: (coq_FICOMP_p :: (coq_FIDIV_p :: (coq_FIDIVR_p :: (coq_FILD_p :: (coq_FIMUL_p :: (coq_FINCSTP_p :: (coq_FIST_p :: (coq_FISTP_p :: (coq_FISUB_p :: (coq_FISUBR_p :: (coq_FLD_p :: (coq_FLD1_p :: (coq_FLDCW_p :: (coq_FLDENV_p :: (coq_FLDL2E_p :: (coq_FLDL2T_p :: (coq_FLDLG2_p :: (coq_FLDLN2_p :: (coq_FLDPI_p :: (coq_FLDZ_p :: (coq_FMUL_p :: (coq_FMULP_p :: (coq_FNCLEX_p :: (coq_FNINIT_p :: (coq_FNOP_p :: (coq_FNSAVE_p :: (coq_FNSTCW_p :: (coq_FNSTSW_p :: (coq_FPATAN_p :: (coq_FPREM_p :: (coq_FPREM1_p :: (coq_FPTAN_p :: (coq_FRNDINT_p :: (coq_FRSTOR_p :: (coq_FSCALE_p :: (coq_FSIN_p :: (coq_FSINCOS_p :: (coq_FSQRT_p :: (coq_FST_p :: (coq_FSTENV_p :: (coq_FSTP_p :: (coq_FSUB_p :: (coq_FSUBP_p :: (coq_FSUBR_p :: (coq_FSUBRP_p :: (coq_FTST_p :: (coq_FUCOM_p :: (coq_FUCOMP_p :: (coq_FUCOMPP_p :: (coq_FUCOMI_p :: (coq_FUCOMIP_p :: (coq_FXAM_p :: (coq_FXCH_p :: (coq_FXTRACT_p :: (coq_FYL2X_p :: (coq_FYL2XP1_p :: (coq_FWAIT_p :: (coq_EMMS_p :: (coq_MOVQ_p :: (coq_PACKSSDW_p :: (coq_PACKSSWB_p :: (coq_PACKUSWB_p :: (coq_PADD_p :: (coq_PADDS_p :: (coq_PADDUS_p :: (coq_PAND_p :: (coq_PANDN_p :: (coq_PCMPEQ_p :: (coq_PCMPGT_p :: (coq_PMADDWD_p :: (coq_PMULHUW_p :: (coq_PMULHW_p :: (coq_PMULLW_p :: (coq_POR_p :: (coq_PSLL_p :: (coq_PSRA_p :: (coq_PSRL_p :: (coq_PSUB_p :: (coq_PSUBS_p :: (coq_PSUBUS_p :: (coq_PUNPCKH_p :: (coq_PUNPCKL_p :: (coq_PXOR_p :: (coq_ADDPS_p :: (coq_ADDSS_p :: (coq_ANDNPS_p :: (coq_ANDPS_p :: (coq_CMPPS_p :: (coq_CMPSS_p :: (coq_COMISS_p :: (coq_CVTPI2PS_p :: (coq_CVTPS2PI_p :: (coq_CVTSI2SS_p :: (coq_CVTSS2SI_p :: (coq_CVTTPS2PI_p :: (coq_CVTTSS2SI_p :: (coq_DIVPS_p :: (coq_DIVSS_p :: (coq_LDMXCSR_p :: (coq_MAXPS_p :: (coq_MAXSS_p :: (coq_MINPS_p :: (coq_MINSS_p :: (coq_MOVAPS_p :: (coq_MOVHLPS_p :: (coq_MOVLPS_p :: (coq_MOVMSKPS_p :: (coq_MOVSS_p :: (coq_MOVUPS_p :: (coq_MULPS_p :: (coq_MULSS_p :: (coq_ORPS_p :: (coq_RCPPS_p :: (coq_RCPSS_p :: (coq_RSQRTPS_p :: (coq_RSQRTSS_p :: (coq_SHUFPS_p :: (coq_SQRTPS_p :: (coq_SQRTSS_p :: (coq_STMXCSR_p :: (coq_SUBPS_p :: (coq_SUBSS_p :: (coq_UCOMISS_p :: (coq_UNPCKHPS_p :: (coq_UNPCKLPS_p :: (coq_XORPS_p :: (coq_PAVGB_p :: (coq_PEXTRW_p :: (coq_PINSRW_p :: (coq_PMAXSW_p :: (coq_PMAXUB_p :: (coq_PMINSW_p :: (coq_PMINUB_p :: (coq_PMOVMSKB_p :: (coq_PSADBW_p :: (coq_PSHUFW_p :: (coq_MASKMOVQ_p :: (coq_MOVNTPS_p :: (coq_MOVNTQ_p :: (coq_PREFETCHT0_p :: (coq_PREFETCHT1_p :: (coq_PREFETCHT2_p :: (coq_PREFETCHNTA_p :: (coq_SFENCE_p :: []))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))

(** val instruction_grammar_list : grammar list **)

let instruction_grammar_list =
  app (List0.map (fun p -> seq prefix_grammar_rep p) instr_grammars_rep)
    (app
      (List0.map (fun p -> seq prefix_grammar_rep_or_repn p)
        instr_grammars_rep_or_repn)
      (app
        (List0.map (fun p -> seq prefix_grammar_lock_with_op_override p)
          instr_grammars_lock_with_op_override)
        (app
          (List0.map (fun p -> seq prefix_grammar_lock_no_op_override p)
            instr_grammars_lock_no_op_override)
          (app
            (List0.map (fun p -> seq prefix_grammar_seg_with_op_override p)
              instr_grammars_seg_with_op_override)
            (app
              (List0.map (fun p -> seq prefix_grammar_seg_op_override p)
                instr_grammars_seg_op_override)
              (List0.map (fun p -> seq prefix_grammar_seg_override p)
                instr_grammars_seg_override))))))

(** val instruction_grammar : grammar **)

let instruction_grammar =
  alts instruction_grammar_list
