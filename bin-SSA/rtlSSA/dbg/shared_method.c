#include "config.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "shared_method.h"

extern int struct_die_count;

extern Dwarf_Off visited_struct_die[VISITED_SIZE];

extern event_level;

void event_report(const char* event_msg) {
  char* final_msg = 0;
  switch(event_level) {
  case 1:
    perror(event_msg);break;
  case 2:
    printf("%s\n", event_msg);break;
  default:
    break;
  }
}
    
int open_a_file(const char* name) {
  /* Only consider on X86 platform here; the original libdwarf targets
     at different platforms, so they have an open-file wrapper */
  int f = -1;
  f = open(name, O_RDONLY);
  return f;
}

void close_a_file(int f) {
  /* to match open_a_file */
  close(f);
}

void
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf)
{
  Dwarf_Signed sri = 0;
  for (sri = 0; sri < sf->srcfilescount; ++sri) {
    dwarf_dealloc(dbg, sf->srcfiles[sri], DW_DLA_STRING);
  }
  dwarf_dealloc(dbg, sf->srcfiles, DW_DLA_LIST);
  sf->srcfilesres = DW_DLV_ERROR;
  sf->srcfiles = 0;
  sf->srcfilescount = 0;
}

void
get_addr(Dwarf_Attribute attr, Dwarf_Addr *val) {
  Dwarf_Error error = 0;
  int res;
  Dwarf_Addr uval = 0;
  
  /* Translate attribute to address */
  res =dwarf_formaddr(attr, &uval, &error);
  if(res == DW_DLV_OK) {
    *val = uval;
    return;
  }
  return;
}

void
get_name(Dwarf_Attribute attr, char ** name) {
  Dwarf_Error error = 0;
  int res;
  char* localname = 0;
  
  res = dwarf_formstring(attr, &localname, &error);
  if(res == DW_DLV_OK) {
    *name = localname;
    return;
  }
  return;
}

void
get_type_off(Dwarf_Attribute attr, Dwarf_Off* type_off) {
  Dwarf_Off local_off = 0;
  Dwarf_Error error = 0;
  int res;

  res = dwarf_global_formref(attr, &local_off, &error);
  if(res == DW_DLV_OK) {
    *type_off = local_off;
    return;
  }
  return;
}

void
get_uvalue(Dwarf_Attribute attr, Dwarf_Unsigned* ret_value) {
  Dwarf_Unsigned local_ret_value = 0;
  int res;
  Dwarf_Error error = 0;
  
  res = dwarf_formudata(attr,&local_ret_value,&error);
  if(res == DW_DLV_OK) {
    *ret_value = local_ret_value;
  }
  else {
    event_report("Warning: unsigned value formation failed!");
    return;
  }
}

void
get_svalue(Dwarf_Attribute attr, Dwarf_Signed* ret_value) {
  Dwarf_Signed local_ret_value = 0;
  int res;
  Dwarf_Error error = 0;
  
  res = dwarf_formsdata(attr,&local_ret_value,&error);
  if(res == DW_DLV_OK) {
    *ret_value = local_ret_value;
  }
  else {
    event_report("Warning: signed value formation failed!");
    return;
  }
}

void
get_number(Dwarf_Attribute attr,Dwarf_Unsigned *val)
{
  Dwarf_Error error = 0;
  int res;
  Dwarf_Signed sval = 0;
  Dwarf_Unsigned uval = 0;
  res = dwarf_formudata(attr,&uval,&error);
  if(res == DW_DLV_OK) {
    *val = uval;
    return;
  }
  res = dwarf_formsdata(attr,&sval,&error);
  if(res == DW_DLV_OK) {
    *val = sval;
    return;
  }
  return;
}

void
print_union_members(Dwarf_Debug dbg, Dwarf_Die in_die,
	      int is_info, int level,
	      struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$a:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    exit(1);
  }
  
  if(tag == DW_TAG_member) {
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_type) {
	  //printf("(");
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  //printf(")");
	}
	}
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    print_union_members(dbg,sib_die,is_info,level,sf);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
print_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int level,
		     struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$9:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }
  
  if(tag == DW_TAG_member) {
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_type) {
	  //printf("(");
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  //printf(") ");
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    print_struct_members(dbg,sib_die,is_info,level,sf);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
print_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die,
		  int is_info, int level,
		  struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$9:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }
  
  if(tag == DW_TAG_enumerator) {
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_name) {
	  char* name = 0;
	  get_name(attrbuf[i], &name);
	  printf("(%s, ",name);
	  dwarf_dealloc(dbg, name, DW_DLA_STRING);
	}
	if(aform == DW_AT_const_value) {
	  Dwarf_Signed v = 0;
	  get_svalue(attrbuf[i], &v);
	  printf("%"DW_PR_DSd")", v);
	}	  
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
  }

  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    print_enumerators(dbg,sib_die,is_info,level,sf);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
print_subroutine_parameters(Dwarf_Debug dbg, Dwarf_Die in_die,
			    int is_info, int level,
			    struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$9:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }
  
  if(tag == DW_TAG_formal_parameter) {
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_type) {
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  print_type_recursively(dbg, next_type_off, is_info, level, sf);
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    printf(",");
    print_subroutine_parameters(dbg,sib_die,is_info,level,sf);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
print_subranges(Dwarf_Debug dbg, Dwarf_Die in_die,
		int is_info, int level,
		struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$9:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }
  
  if(tag == DW_TAG_subrange_type) {
    printf("[");
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_type) {
	  //printf("(");
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  //printf(")");
	}
	if(aform == DW_AT_upper_bound) {
	  Dwarf_Unsigned upper_bound = 0;
	  get_uvalue(attrbuf[i], &upper_bound);
	  printf("%lld", upper_bound+1);
	}	  
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    printf("]");
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    print_subranges(dbg,sib_die,is_info,level,sf);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
print_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die,
		int is_info, int level,
		struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Die child_die = 0;

  res = dwarf_child(parent_die,&child_die,&error);
  if(res != DW_DLV_OK) {
    perror("Warning: Does not have subrange child or error happened!\n");
    return;
  } else {
    print_subranges(dbg,child_die,is_info,level,sf);
  }
}

int is_visited(Dwarf_Off type_off) {
  int i = 0;
  for(i = 0; i < VISITED_SIZE; i++) {
    if(type_off == visited_struct_die[i]) return 1;
  }
  return 0;
}

void
print_type_recursively(Dwarf_Debug dbg, Dwarf_Off type_off,
		       int is_info, int level, 
		       struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Die start_die = 0;
  Dwarf_Half tag = 0;
  const char* tagname = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char * die_name = 0;
  Dwarf_Unsigned i;
  Dwarf_Bool hastype;
  Dwarf_Die child_die = 0;

  res = dwarf_offdie(dbg, type_off, &start_die, &error);
  if(res != DW_DLV_OK) {
    printf("$a:No type or wrong offset!\n");
    return;
  }
  
  res = dwarf_tag(start_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("$a:Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  res = dwarf_get_TAG_name(tag,&tagname);
  if(res != DW_DLV_OK) {
    printf("$a:Error in dwarf_get_TAG_name , level %d \n",level);
    exit(1);
  }
 
  res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
  if(res == DW_DLV_ERROR) {
    perror("Printing type Attrlist error!");
    exit(1);
  }

  switch(tag) {
  case DW_TAG_base_type: //$0
    res = dwarf_diename(start_die, &die_name, &error);
    if(res != DW_DLV_OK) {
      printf("$9:Warning: base type does not have name!");
    } else {
      printf("$0:%s$", die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    break;

  case DW_TAG_const_type: //$1
    printf("$1:");    
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("$0:void$");
    }
    printf("$");
    break;

  case DW_TAG_array_type: //$2
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      printf("$2:");
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      print_dimension(dbg, start_die, is_info, level, sf);
    } else {
      printf("$9:Error: Array of no type!\n");
      exit(1);
    }
    printf("$");
    break;

  case DW_TAG_pointer_type: //$3
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      printf("$3:");
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    //printf("<%" DW_PR_DUx ">:", next_type_off);
	    print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("$3:$0:void$");
    }
    printf("$");
    break;

  case DW_TAG_typedef: //$4
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else if(res == DW_DLV_OK && hastype == 0) {
      printf("$0:void$");
    } else {
      perror("typedef hastype error!");
      exit(1);
    }
    break;

  case DW_TAG_subroutine_type: //$5
    printf("$5:(");
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("$0:void$");
    }
    printf("),");
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      printf("($0:void$)");
      return;
    } else if(res == DW_DLV_OK){
      printf("(");
      print_subroutine_parameters(dbg,child_die,is_info,level,sf);
      printf(")");
    } else {
      perror("$9:Error: when printing subroutine PARAMETERS!\n");
      exit(1);
    }
    printf("$");
    break;
    
  case DW_TAG_enumeration_type://$6
    printf("$6:");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      perror("Warning: enumeration does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res != DW_DLV_OK) {
      perror("Warning: Does not have enumerator child or error happened!\n");

      printf("enum{empty}");
      return;
    } else {
      printf("{");
      print_enumerators(dbg,child_die,is_info,level,sf);
    }
    printf("}");
    printf("$");
    break;
    
  case DW_TAG_union_type: //$7
    printf("$7:");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      perror("Warning: union does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res != DW_DLV_OK) {
      perror("Warning: Does not have UNION member child or error happened!\n");
      printf("{empty}");
      return;
    } else {
      printf("union{");
      print_union_members(dbg,child_die,is_info,level,sf);
      printf("}");
    }
    printf("$");
    break;

  case DW_TAG_structure_type: //$8
    printf("$8:");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      perror("Warning: struct does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    if(!is_visited(type_off)) {
      if(struct_die_count == VISITED_SIZE) {
	perror("visited list becomes longer than VISITED_SIZE!\n");
	exit(1);
      }
      visited_struct_die[struct_die_count] = type_off;
      struct_die_count++;
      res = dwarf_child(start_die,&child_die,&error);
      if(res != DW_DLV_OK) {
	perror("Warning: Does not have STRUCT member child or error happened!");

	printf("{empty}");
	return;
      } else {
	printf("struct%d{", struct_die_count);
	print_struct_members(dbg,child_die,is_info,level,sf);
	printf("}");
      } 
    } else {
      int dup_index = 0;
      for(i = 0; i < VISITED_SIZE; i++) {
	if(visited_struct_die[i] == type_off) dup_index = i;
      }
      //printf("<%" DW_PR_DUx ">, struct%d", type_off, dup_index);
      printf("struct%d", dup_index);
    }
    printf("$");
    break;
  default:
    printf("$9:Do not support compound types for now!"); 
  }
}


int 
count_tag(Dwarf_Debug dbg, Dwarf_Die start_die, Dwarf_Half target_tag) 
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Die curr_die = start_die;
  int num_tag = 0;
  Dwarf_Half tag = 0;

  res = dwarf_tag(start_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag in count_tag!");
    exit(1);
  }
  
  for(;;) { 
    Dwarf_Die sib_die = 0;
    if(tag == target_tag) num_tag++;
    res = dwarf_siblingof(dbg,curr_die,&sib_die,&error);
    if(res == DW_DLV_OK) {
      res = dwarf_tag(sib_die,&tag,&error);
      if(res != DW_DLV_OK) {
	perror("Error in dwarf_tag in count_tag!");
	exit(1);
      }
      if(curr_die != start_die) {
	dwarf_dealloc(dbg,curr_die,DW_DLA_DIE);
      }
      curr_die = sib_die;
    } else if(res == DW_DLV_NO_ENTRY) {
      return num_tag;
    } else {
      perror("Error in dwarf_siblingof in count_tag!");
      exit(1);
    }
  }
}



void
encode_subranges(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hasbound = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_subranges");
    exit(1);
  }
  
  if(tag == DW_TAG_subrange_type) {
    res = dwarf_hasattr(in_die,DW_AT_upper_bound,&hasbound,&error);
    if(res == DW_DLV_OK && hasbound == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error in dwarf_attrlist in encode_subranges)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	/*
	if(aform == DW_AT_type) {
	  printf("(");
	  Dwarf_Off next_type_off = 0;
	  get_type_off(attrbuf[i], &next_type_off);
	  print_type_recursively(dbg, next_type_off, is_info, level, sf);
	  printf(")");
	}
	*/
	  if(aform == DW_AT_upper_bound) {
	    Dwarf_Unsigned upper_bound = 0;
	    get_uvalue(attrbuf[i], &upper_bound);
	    printf("(%lld)", upper_bound+1);
	  }	  
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("(NaN)");
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    encode_subranges(dbg,sib_die);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
encode_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Die child_die = 0;
  int dimension = 0;

  res = dwarf_child(parent_die,&child_die,&error);
  if(res != DW_DLV_OK) {
    printf("a(Warning: Does not have subrange child or error happened!)");
    return;
  } else {
    dimension = count_tag(dbg,child_die,DW_TAG_subrange_type);
    printf("(%d)",dimension);
    encode_subranges(dbg,child_die);
  }
}

void
encode_subroutine_parameters(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hastype = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_subroutine_parameters!");
    exit(1);
  }
  
  if(tag == DW_TAG_formal_parameter) {
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error in dwarf_attrlist in encode_subroutine_parameters)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      event_report("Warning:Theoretically, a parameter cannot have void type");
      printf("0(void)");
    }
  }  
  if(tag == DW_TAG_unspecified_parameters) {
    printf("9");
  }

  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    encode_subroutine_parameters(dbg,sib_die);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
encode_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res;
  int res0;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hasname = 0;
  Dwarf_Bool hasval = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_enumerators!");
    exit(1);
  }
   
  if(tag == DW_TAG_enumerator) {
    res = dwarf_hasattr(in_die,DW_AT_name,&hasname,&error);
    res0 = dwarf_hasattr(in_die,DW_AT_const_value,&hasval,&error);
    if(res == DW_DLV_OK && hasname == 1 && res0 == DW_DLV_OK && hasval == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error in dwarf_attrlist in encode_enumerators)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_name) {
	    char* name = 0;
	    get_name(attrbuf[i], &name);
	    printf("(%s) ",name);
	    dwarf_dealloc(dbg, name, DW_DLA_STRING);
	  }
	  if(aform == DW_AT_const_value) {
	    Dwarf_Signed v = 0;
	    get_svalue(attrbuf[i], &v);
	    printf("(%"DW_PR_DSd")", v);
	  }	  
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      // For now, stay strict to discover corner cases
      event_report("Error no name or no value for enumerator in encode_enumerators!");
      exit(1);
    }
  }

  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    encode_enumerators(dbg,sib_die);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
encode_union_members(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  char* die_name = 0;
  Dwarf_Bool hastype = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_union_members!");
    exit(1);
  }
  
  if(tag == DW_TAG_member) {
    res = dwarf_hasattr(in_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error in dwarf_attrlist in encode_union_members!)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      event_report("Warning:Theoretically, a UNION member cannot have void type");
      printf("0(void)");
    }
    //printf("*");
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: union member does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    encode_union_members(dbg,sib_die);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
encode_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die)
{
  int res;
  Dwarf_Error error;
  Dwarf_Die sib_die = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Bool hastype = 0;
  char * die_name = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_struct_members");
    exit(1);
  }
  
  if(tag == DW_TAG_member) {
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error in dwarf_tag in encode_struct_members)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      event_report("Warning:Theoretically, a STRUCT member cannot have void type");
      printf("0(void)");
    }
    //printf("*");
    res = dwarf_diename(in_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: struct member does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
  }
  
  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_OK) {
    encode_struct_members(dbg,sib_die);
    dwarf_dealloc(dbg, in_die, DW_DLA_DIE);
  }
  else if (res == DW_DLV_NO_ENTRY) {
    return;
  }
}

void
encode_type_recursively(Dwarf_Debug dbg, Dwarf_Off type_off)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Die start_die = 0;
  Dwarf_Half tag = 0;
  const char* tagname = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char * die_name = 0;
  Dwarf_Unsigned i;
  Dwarf_Bool hastype;
  Dwarf_Die child_die = 0;

  res = dwarf_offdie(dbg, type_off, &start_die, &error);
  if(res != DW_DLV_OK) {
    printf("a(No type or wrong offset!)");
    return;
  }
  
  res = dwarf_tag(start_die,&tag,&error);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_tag in encode_type_recursively");
    exit(1);
  }
  res = dwarf_get_TAG_name(tag,&tagname);
  if(res != DW_DLV_OK) {
    event_report("Error in dwarf_get_TAG_name in encode_type_recursively");
    exit(1);
  }
 
  res = dwarf_attrlist(start_die, &attrbuf, &attrcount, &error);
  if(res == DW_DLV_ERROR) {
    event_report("Printing type Attrlist error!");
    exit(1);
  }

  switch(tag) {
  case DW_TAG_base_type: //$0 Done
    res = dwarf_diename(start_die, &die_name, &error);
    if(res != DW_DLV_OK) {
      printf("a(Warning: base type does not have name!)");
    } else {
      printf("0(%s)", die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    break;

  case DW_TAG_const_type: //$1 Done
    printf("1");    
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("0(void)");
    }
    break;

  case DW_TAG_array_type: //$2 Done
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      printf("2");
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
      encode_dimension(dbg, start_die);
    } else {
      printf("a(Error: Array of no type!)");
      exit(1);
    }
    break;

  case DW_TAG_pointer_type: //$3 Done
    printf("3");
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    //printf("<%" DW_PR_DUx ">:", next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("0(void)");
    }
    break;

  case DW_TAG_typedef: //$4 Done
    printf("4");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: enumeration does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else if(res == DW_DLV_OK && hastype == 0) {
      printf("0(void)");
    } else {
      event_report("typedef hastype error!");
      exit(1);
    }
    break;

  case DW_TAG_subroutine_type: //$5 Done
    printf("5");
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("0(void)");
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      printf("(0)");
      return;
    } else if(res == DW_DLV_OK){
      int paramnum = 0;
      paramnum = count_tag(dbg,child_die,DW_TAG_formal_parameter);
      paramnum += count_tag(dbg,child_die,DW_TAG_unspecified_parameters);
      printf("(%d)",paramnum);
      encode_subroutine_parameters(dbg,child_die);
    } else {
      event_report("Error: when encoding subroutine PARAMETERS!\n");
      exit(1);
    }
    break;
    
  case DW_TAG_enumeration_type://$6 Done
    printf("6");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: enumeration does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      event_report("Warning: Does not have enumerator child or error happened!");
      printf("(0)");
      return;
    } else if(res == DW_DLV_OK) {
      int etornum = 0;
      etornum = count_tag(dbg,child_die,DW_TAG_enumerator);
      printf("(%d)",etornum);
      encode_enumerators(dbg,child_die);
    } else {
      event_report("Error: when encoding ENUMERATORS");
      exit(1);
    }
    break;
    
  case DW_TAG_union_type: //$7 Done
    printf("7");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: union does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    res = dwarf_child(start_die,&child_die,&error);
    if(res == DW_DLV_NO_ENTRY) {
      event_report("Warning: Does not have UNION member child or error happened!\n");
      printf("(0)");
      return;
    } else if(res == DW_DLV_OK) {
      int memnum = 0;
      memnum = count_tag(dbg,child_die,DW_TAG_member);
      printf("(%d)",memnum);
      encode_union_members(dbg,child_die);
    } else {
      event_report("Error: when encoding UNION MEMBERS");
      exit(1);
    }
    break;

  case DW_TAG_structure_type: //$8 Do
    printf("8");
    res = dwarf_diename(start_die,&die_name,&error);
    if(res != DW_DLV_OK) {
      event_report("Warning: struct does not have name!\n");
      printf("()");
    } else {
      printf("(%s)",die_name);
      dwarf_dealloc(dbg,die_name,DW_DLA_STRING);
    }
    if(!is_visited(type_off)) {
      if(struct_die_count == VISITED_SIZE) {
	event_report("visited list becomes longer than VISITED_SIZE!");
	exit(1);
      }
      visited_struct_die[struct_die_count] = type_off;
      struct_die_count++;
      res = dwarf_child(start_die,&child_die,&error);
      if(res == DW_DLV_NO_ENTRY) {
	event_report("Warning: Does not have STRUCT member child or error happened!");
	printf("(0)(struct0)");
	return;
      } else if(res == DW_DLV_OK) {
	int memnum = 0;
	memnum = count_tag(dbg,child_die,DW_TAG_member);
	printf("(%d)", memnum);
	printf("(struct%d)", struct_die_count-1);
	encode_struct_members(dbg,child_die);
      } else {
	event_report("Error: when encoding STRUCT MEMBERS");
	exit(1);
      }
    } else {
      int dup_index = 0;
      for(i = 0; i < VISITED_SIZE; i++) {
	if(visited_struct_die[i] == type_off) dup_index = i;
      }
      //printf("<%" DW_PR_DUx ">, struct%d", type_off, dup_index);
      printf("(0)(struct%d)", dup_index);
    }
    break;
  case DW_TAG_volatile_type: //$b
    printf("b");
    res = dwarf_hasattr(start_die, DW_AT_type, &hastype, &error);
    if(res == DW_DLV_OK && hastype == 1) {
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off next_type_off = 0;
	    get_type_off(attrbuf[i], &next_type_off);
	    //printf("<%" DW_PR_DUx ">:", next_type_off);
	    encode_type_recursively(dbg, next_type_off);
	  }	
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("0(void)");
    }
    break;
  default:
    printf("a(Found unsupported type:%s!)", tagname); 
  }
}

struct operation_descr_s {
    int op_code;
    int op_count;
    const char * op_1type;
};

struct operation_descr_s opdesc[]= {
    {DW_OP_addr,1,"addr" },
    {DW_OP_deref,0 },
    {DW_OP_const1u,1,"1u" },
    {DW_OP_const1s,1,"1s" },
    {DW_OP_const2u,1,"2u" },
    {DW_OP_const2s,1,"2s" },
    {DW_OP_const4u,1,"4u" },
    {DW_OP_const4s,1,"4s" },
    {DW_OP_const8u,1,"8u" },
    {DW_OP_const8s,1,"8s" },
    {DW_OP_constu,1,"uleb" },
    {DW_OP_consts,1,"sleb" },
    {DW_OP_dup,0,""},
    {DW_OP_drop,0,""},
    {DW_OP_over,0,""},
    {DW_OP_pick,1,"1u"},
    {DW_OP_swap,0,""},
    {DW_OP_rot,0,""},
    {DW_OP_xderef,0,""},
    {DW_OP_abs,0,""},
    {DW_OP_and,0,""},
    {DW_OP_div,0,""},
    {DW_OP_minus,0,""},
    {DW_OP_mod,0,""},
    {DW_OP_mul,0,""},
    {DW_OP_neg,0,""},
    {DW_OP_not,0,""},
    {DW_OP_or,0,""},
    {DW_OP_plus,0,""},
    {DW_OP_plus_uconst,1,"uleb"},
    {DW_OP_shl,0,""},
    {DW_OP_shr,0,""},
    {DW_OP_shra,0,""},
    {DW_OP_xor,0,""},
    {DW_OP_skip,1,"2s"},
    {DW_OP_bra,1,"2s"},
    {DW_OP_eq,0,""},
    {DW_OP_ge,0,""},
    {DW_OP_gt,0,""},
    {DW_OP_le,0,""},
    {DW_OP_lt,0,""},
    {DW_OP_ne,0,""},
    /* lit0 thru reg31 handled specially, no operands */
    /* breg0 thru breg31 handled specially, 1 operand */
    {DW_OP_regx,1,"uleb"},
    {DW_OP_fbreg,1,"sleb"},
    {DW_OP_bregx,2,"uleb"},
    {DW_OP_piece,1,"uleb"},
    {DW_OP_deref_size,1,"1u"},
    {DW_OP_xderef_size,1,"1u"},
    {DW_OP_nop,0,""},
    {DW_OP_push_object_address,0,""},
    {DW_OP_call2,1,"2u"},
    {DW_OP_call4,1,"4u"},
    {DW_OP_call_ref,1,"off"},
    {DW_OP_form_tls_address,0,""},
    {DW_OP_call_frame_cfa,0,""},
    {DW_OP_bit_piece,2,"uleb"},
    {DW_OP_implicit_value,2,"u"},
    {DW_OP_stack_value,0,""},
    {DW_OP_GNU_uninit,0,""},
    {DW_OP_GNU_encoded_addr,1,"addr"},
    {DW_OP_implicit_pointer,2,"addr" }, /* DWARF5 */
    {DW_OP_GNU_implicit_pointer,2,"addr" },
    {DW_OP_entry_value,2,"val" }, /* DWARF5 */
    {DW_OP_GNU_entry_value,2,"val" },
    {DW_OP_const_type,3,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_const_type,3,"uleb" },
    {DW_OP_regval_type,2,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_regval_type,2,"uleb" },
    {DW_OP_deref_type,1,"val" }, /* DWARF5 */
    {DW_OP_GNU_deref_type,1,"val" },
    {DW_OP_convert,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_convert,1,"uleb" },
    {DW_OP_reinterpret,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_reinterpret,1,"uleb" },

    {DW_OP_GNU_parameter_ref,1,"val" },
    {DW_OP_GNU_const_index,1,"val" },
    {DW_OP_GNU_push_tls_address,0,"" },

    {DW_OP_addrx,1,"uleb" }, /* DWARF5 */
    {DW_OP_GNU_addr_index,1,"val" },
    {DW_OP_constx,1,"u" }, /* DWARF5 */
    {DW_OP_GNU_const_index,1,"u" },

    {DW_OP_GNU_parameter_ref,1,"u" },

    {DW_OP_xderef,0,"" }, /* DWARF5 */
    {DW_OP_xderef_type,2,"1u" }, /* DWARF5 */
    /* terminator */
    {0,0,""}
};

static int
op_has_no_operands(int op)
{
    unsigned i = 0;
    if (op >= DW_OP_lit0 && op <= DW_OP_reg31) {
        return TRUE;
    }
    for (; ; ++i) {
        struct operation_descr_s *odp = opdesc+i;
        if (odp->op_code == 0) {
            break;
        }
        if (odp->op_code != op) {
            continue;
        }
        if (odp->op_count == 0) {
            return TRUE;
        }
        return FALSE;
    }
    return FALSE;
}

static void
show_contents(unsigned int length, const unsigned char * bp)
{
    unsigned int i = 0;

    if(!length) {
        return;
    }
    printf(" contents 0x");
    for (; i < length; ++i,++bp) {
        /*  Do not use DW_PR_DUx here,
            the value  *bp is a const unsigned char. */
        printf("%02x", *bp);
    }
}

void
encode_expr(Dwarf_Loc expr, int index) {
  //printf("%d\n", index);
  int res;
  
  Dwarf_Small op = expr.lr_atom;
  const char *op_name = NULL;
  Dwarf_Unsigned opd1 = expr.lr_number;
  Dwarf_Unsigned opd2 = expr.lr_number2;
  Dwarf_Unsigned opd3 = 0;
  Dwarf_Unsigned offsetforbranch = expr.lr_offset;

  res = dwarf_get_OP_name(op, &op_name);
  if(res == DW_DLV_OK) printf("%s", op_name);
  else printf("op_name error\n");

  if(op_has_no_operands(op)) {
    /* Nothing to add */
    printf("\n");
  } else if(op >= DW_OP_breg0 && op <= DW_OP_breg31) {
    printf(" %+" DW_PR_DSd "\n", (Dwarf_Signed) opd1);
  } else {
    switch (op) {
        case DW_OP_addr:
            printf(" 0x%" DW_PR_XZEROS DW_PR_DUx "\n", opd1);
            break;
        case DW_OP_const1s:
        case DW_OP_const2s:
        case DW_OP_const4s:
        case DW_OP_const8s:
        case DW_OP_consts:
        case DW_OP_skip:
        case DW_OP_bra:
        case DW_OP_fbreg:
            printf(" %" DW_PR_DSd "\n", (Dwarf_Signed) opd1);
            break;
        case DW_OP_GNU_addr_index: /* unsigned val */
        case DW_OP_addrx: /* DWARF5: unsigned val */
        case DW_OP_GNU_const_index:
        case DW_OP_constx: /* DWARF5: unsigned val */
        case DW_OP_const1u:
        case DW_OP_const2u:
        case DW_OP_const4u:
        case DW_OP_const8u:
        case DW_OP_constu:
        case DW_OP_pick:
        case DW_OP_plus_uconst:
        case DW_OP_regx:
        case DW_OP_piece:
        case DW_OP_deref_size:
        case DW_OP_xderef_size:
            printf(" %" DW_PR_DUu "\n", opd1);
            break;
        case DW_OP_bregx:
            printf(" 0x%" DW_PR_XZEROS DW_PR_DUx , opd1);
            printf(" +%" DW_PR_DSd "\n", (Dwarf_Signed) opd2);
            break;
        case DW_OP_call2:
            printf(" 0x%" DW_PR_XZEROS DW_PR_DUx "\n", opd1);
            break;
        case DW_OP_call4:
            printf(" 0x%" DW_PR_XZEROS DW_PR_DUx "\n", opd1);
            break;
        case DW_OP_call_ref:
            printf(" 0x%" DW_PR_XZEROS DW_PR_DUx "\n", opd1);
            break;
        case DW_OP_bit_piece:
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx , opd1);
            printf(" offset 0x%" DW_PR_DUx "\n", (Dwarf_Signed) opd2);
            break;
        case DW_OP_implicit_value:
            {
#define IMPLICIT_VALUE_PRINT_MAX 12
                unsigned int print_len = 0;
                printf(" 0x%" DW_PR_XZEROS DW_PR_DUx , opd1);
                /*  The other operand is a block of opd1 bytes. */
                /*  FIXME */
                print_len = opd1;
                if (print_len > IMPLICIT_VALUE_PRINT_MAX) {
                    print_len = IMPLICIT_VALUE_PRINT_MAX;
                }
#undef IMPLICIT_VALUE_PRINT_MAX
                {
                    const unsigned char *bp = 0;
                    /*  This is a really ugly cast, a way
                        to implement DW_OP_implicit value in
                        this libdwarf context. */
                    bp = (const unsigned char *) opd2;
                    show_contents(print_len,bp);
		    printf("\n");
                }
            }
            break;

        /* We do not know what the operands, if any, are. */
        case DW_OP_HP_unknown:
        case DW_OP_HP_is_value:
        case DW_OP_HP_fltconst4:
        case DW_OP_HP_fltconst8:
        case DW_OP_HP_mod_range:
        case DW_OP_HP_unmod_range:
        case DW_OP_HP_tls:
        case DW_OP_INTEL_bit_piece:
            break;
        case DW_OP_stack_value:  /* DWARF4 */
            break;
        case DW_OP_GNU_uninit:  /* DW_OP_APPLE_uninit */
            /* No operands. */
            break;
        case DW_OP_GNU_encoded_addr:
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx "\n", opd1);
            break;
        case DW_OP_implicit_pointer:       /* DWARF5 */
        case DW_OP_GNU_implicit_pointer:
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx , opd1);
            printf(" %" DW_PR_DSd "\n", (Dwarf_Signed)opd2);
            break;
        case DW_OP_entry_value:       /* DWARF5 */
        case DW_OP_GNU_entry_value: {
            const unsigned char *bp = 0;
            unsigned int length = 0;
            length = opd1;
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx , opd1);
            bp = (Dwarf_Small *) opd2;
            if (!bp) {
	      printf("ERROR: Null databyte pointer DW_OP_entry_value\n");
            } else {
	      show_contents(length,bp);
	      printf("\n");
            }
            }
            break;
        case DW_OP_const_type:           /* DWARF5 */
        case DW_OP_GNU_const_type:
            {
            const unsigned char *bp = 0;
            unsigned int length = 0;
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx , opd1);

            length = opd2;
	    printf(" const length: ");
            printf("%u" , length);

            /* Now point to the data bytes of the const. */
            bp = (Dwarf_Small *) opd3;
            if (!bp) {
	      printf(
                    "ERROR: Null databyte pointer DW_OP_const_type ");
            } else {
	      show_contents(length,bp);
	      printf("\n");
            }
            }
            break;
        case DW_OP_regval_type:           /* DWARF5 */
        case DW_OP_GNU_regval_type:
            printf(" 0x%" DW_PR_DUx , opd1);
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx "\n", opd2);
            break;
        case DW_OP_deref_type: /* DWARF5 */
        case DW_OP_GNU_deref_type:
            printf(" 0x%02" DW_PR_DUx , opd1);
            printf(" 0x%" DW_PR_XZEROS  DW_PR_DUx "\n", opd2);
            break;
        case DW_OP_convert: /* DWARF5 */
        case DW_OP_GNU_convert:
        case DW_OP_reinterpret: /* DWARF5 */
        case DW_OP_GNU_reinterpret:
        case DW_OP_GNU_parameter_ref:
            printf(" 0x%02"  DW_PR_DUx "\n", opd1);
            break;
        default:
            {
                printf(" dwarf_op unknown 0x%x\n", (unsigned)op);
            }
            break;
        }
  }

}

void
encode_locexprs(Dwarf_Debug dbg, Dwarf_Loc* exprs, Dwarf_Unsigned num) {
  int res, i;
  Dwarf_Error error;

  for(i = 0; i < num; i++) {
    encode_expr(exprs[i], i); 
  }
}

void 
encode_sinfo(Dwarf_Debug dbg, Dwarf_Die in_die, Dwarf_Addr addrbase) {
  int res;
  int i, j;
  Dwarf_Error error;
  
  Dwarf_Bool haslocation;
  Dwarf_Attribute *attrbuf;
  Dwarf_Signed attrcount = 0;

  Dwarf_Signed loc_num;
  Dwarf_Locdesc **loclist;
  
  char* varname;
  /*
  printf("Name:\n");
  res = dwarf_diename(in_die, &varname, &error);
  if(res == DW_DLV_OK) printf("%s\n", varname);
  else printf("NULL\n");
  */

  printf("{\n");
  
  res = dwarf_hasattr(in_die, DW_AT_location, &haslocation, &error);
  if(res != DW_DLV_OK) {
    printf("Error\n}");
    return;
  }
  if(haslocation != 1) {
    printf("NULL\n}");
    return;
  }

  res =dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) return;
  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_location) {
	res = dwarf_loclist_n(attrbuf[i], &loclist, &loc_num, &error);
	if(res == DW_DLV_OK) {
	  //printf("Location list has %lld locdesc entries\n", loc_num);
	  for(j = 0; j < loc_num; ++j) {
	    //printf("NO.%d locdesc has %d location records\n", j+1, loclist[j]->ld_cents);
	    //printf("[\n");
	    if(loclist[j]->ld_lopc != 0 && loclist[j]->ld_hipc != -1) {
	      if(loclist[j]->ld_lopc < addrbase && loclist[j]->ld_hipc < addrbase) {
		printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_lopc + addrbase);
		printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_hipc + addrbase);
	      } else {
		printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_lopc);
		printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_hipc);
	      }
	    } else {
	      printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_lopc);
	      printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", loclist[j]->ld_hipc);
	    }
	    //printf("Le:\n");
	    encode_locexprs(dbg, loclist[j]->ld_s, loclist[j]->ld_cents);
	    printf("\n");
	    //printf("]\n");
	    dwarf_dealloc(dbg, loclist[j]->ld_s, DW_DLA_LOC_BLOCK);
	  }
	  dwarf_dealloc(dbg, loclist, DW_DLA_LOCDESC);
	}
      }
      dwarf_dealloc(dbg, attrbuf, DW_DLA_ATTR);
    }
  }
  printf("}");
}
