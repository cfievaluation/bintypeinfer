#include "config.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "shared_method.h"

static void 
collect_func_and_extnl(Dwarf_Debug dbg);

static int decl_file_id = 0;

static void 
collect_static_vars(Dwarf_Debug dbg);

static void
print_children_param_and_var(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info, int level,
			     struct srcfilesdata *sf);

static void 
print_die_data_if_subprogram_or_var(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info, int level,
			     struct srcfilesdata *sf);
static void 
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf);

static void
print_die_data_if_static_var(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info,int level,
			     struct srcfilesdata *sf);

static void 
get_die_and_siblings_for_static_vars(Dwarf_Debug dbg, Dwarf_Die in_die,
				     int is_info, int in_level,
				     struct srcfilesdata *sf);

static int fissionfordie = -1; // TODO: what is this? what if not -1?

static int dienumber = 0;

//static int sinfo_number = 0;

static Dwarf_Addr addrbase = 0;

int struct_die_count = 0;

Dwarf_Off visited_struct_die[VISITED_SIZE];

char * target_name = 0;

// 0: no report; 1: report to screen; 2: report to stdout, which can be redirected
int event_level = 0; 

int main (int argc, char** argv) {
  /* handle command line for target file */
  int fd = -1;
  const char* filepath = NULL;
  /* dwarf environment */
  Dwarf_Debug dbg = 0;
  /* each dwarf function returns a result showing the status of the function */
  int res = DW_DLV_ERROR; 
  /* each dwarf function repots error message; passed by reference */
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;
  int i = 0;

  /* open target file */
  if(argc != 3) {
    exit(1);
  } else {
    filepath = argv[1];
    fd = open_a_file(filepath);
    if(fd < 0) {
      printf("Failure attempting to open %s\n", filepath);
      exit(1);
    }
    target_name = argv[2];
  }

  /* initialize dwarf handling environment */
  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }
  
  addrbase = 0;
  collect_func_and_extnl(dbg);

  /* free resources; end dwarf handling environment */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  close_a_file(fd);

  fd = open_a_file(filepath);
  if(fd < 0) {
    printf("Failure attempting to open %s\n", filepath);
    exit(1);
  }

  /* initialize dwarf handling environment */
  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  addrbase = 0;
  collect_static_vars(dbg);

  /* free resources; end dwarf handling environment */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  close_a_file(fd);
  return 0;
}

static void collect_func_and_extnl(Dwarf_Debug dbg) {
  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile; // TODO: try other types
  Dwarf_Bool is_info = TRUE; // TODO: what if it is false?
  Dwarf_Error error;
  
  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }

    get_die_and_siblings(dbg, cu_die, is_info, 0, &sf);
    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
}

static void collect_static_vars(Dwarf_Debug dbg) {
  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile; // TODO: try other types
  Dwarf_Bool is_info = TRUE; // TODO: what if it is false?
  Dwarf_Error error;
  
  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }

    get_die_and_siblings_for_static_vars(dbg, cu_die, is_info, 0, &sf);
    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
}

static void 
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf)
{
  int res = DW_DLV_ERROR;
  Dwarf_Die cur_die = in_die;
  Dwarf_Die child = 0;
  Dwarf_Error error;

  Dwarf_Half tag = 0;
  Dwarf_Bool haslopc = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount =0;

  int i;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }

  if(tag == DW_TAG_compile_unit) {
    res = dwarf_hasattr(in_die, DW_AT_low_pc, &haslopc, &error);
    if(res != DW_DLV_OK) { perror("CU no lowpc\n"); exit(1);}
    if(haslopc == 0) addrbase = 0;
    else {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) { perror("CU no attrlist\n"); exit(1);}
      for(i = 0; i < attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_low_pc) {
	    get_addr(attrbuf[i], &addrbase);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    }
  }
  
  print_die_data_if_subprogram_or_var(dbg, in_die, is_info, in_level, sf);
  
  for(;;) {
    Dwarf_Die sib_die = 0;
    res = dwarf_child(cur_die, &child, &error);
    if(res == DW_DLV_ERROR) {
      printf("Error in dwarf_child , level %d \n",in_level);
      exit(1);
    }
    if(res == DW_DLV_OK) {
      get_die_and_siblings(dbg,child,is_info,in_level+1,sf);
      }
    /* res == DW_DLV_NO_ENTRY */
    res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die,&error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof , level %d :%s \n",in_level,em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done at this level. */
      break;
    }
    /* res == DW_DLV_OK */
    if(cur_die != in_die) {
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
    }
    cur_die = sib_die;
    print_die_data_if_subprogram_or_var(dbg,cur_die,is_info,in_level,sf);
  }
  return;
}

static void
print_children_param_and_var(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info, int level,
			     struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  Dwarf_Die sib_die = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Unsigned i = 0;


  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }
  
  if(tag == DW_TAG_formal_parameter || tag == DW_TAG_variable) {
    res = dwarf_hasattr(in_die,DW_AT_type,&hastype,&error);
    if(res == DW_DLV_OK && hastype == 1) {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) {
	printf("a(Error, no attrlist when there is type attr!)");
	return;
      }
      for(i = 0; i < attrcount; ++i) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_type) {
	    Dwarf_Off type_off = 0;
	    get_type_off(attrbuf[i], &type_off);
	    if(tag == DW_TAG_formal_parameter) printf("p\n");
	    else printf("l\n");
	    encode_type_recursively(dbg, type_off);
	    printf("\n");
	    //printf("%d\n", ++sinfo_number);
	    encode_sinfo(dbg, in_die, addrbase);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    } else {
      printf("In print_param_var\n");
      printf("0(void)");
    }
    printf("\n");
  }


  if(tag == DW_TAG_unspecified_parameters) {
    printf("p\n");
    printf("9\n");
    //printf("%d\n", ++sinfo_number);
    encode_sinfo(dbg, in_die, addrbase);
    printf("\n");
  }

  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_NO_ENTRY) 
    return;
  else if(res == DW_DLV_OK) 
    print_children_param_and_var(dbg,sib_die,is_info,level,sf);
  else {
    perror("Error: print_children for param and var when siblingof!");
    exit(1);
  }
}

static void
print_die_data_if_subprogram_or_var_i(Dwarf_Debug dbg, Dwarf_Die in_die,
				      int is_info,int level,
				      struct srcfilesdata *sf)
{
  //char *name = 0;
  //int localname = 0;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  int res = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasexternal = 0;
  Dwarf_Bool hasdeclfile = 0;
  Dwarf_Bool hasdeclaration = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char* func_name = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Unsigned i = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }
  
  /* Print formal parameters and local variables in target function */
  if(tag == DW_TAG_subprogram) {
    res = dwarf_diename(in_die,&func_name,&error);
    if(res != DW_DLV_OK) return;
    if(strcmp(func_name,target_name)) return;
    //dwarf_dealloc(dbg, func_name, DW_DLA_STRING);
    res = dwarf_hasattr(in_die, DW_AT_declaration, &hasdeclaration, &error);
    if (res != DW_DLV_OK) return;
    if (hasdeclaration == 1) return;
    res = dwarf_hasattr(in_die, DW_AT_decl_file, &hasdeclfile, &error);
    if (res != DW_DLV_OK) return;
    if (hasdeclfile == 0) return;
    res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
    if(res != DW_DLV_OK) return;
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_decl_file) {
	  Dwarf_Unsigned file_id = 0;
	  get_uvalue(attrbuf[i], &file_id);
	  decl_file_id = file_id;
	}
      }
      dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
    }
    //printf("Target function decl_file is: %d\n", decl_file_id);
  }
}


static void
print_die_data_if_subprogram_or_var(Dwarf_Debug dbg, Dwarf_Die in_die,
				    int is_info,int level,
				    struct srcfilesdata *sf)
{
  print_die_data_if_subprogram_or_var_i(dbg,in_die,is_info,level,sf);
  dienumber++;
}


static void 
get_die_and_siblings_for_static_vars(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf)
{
  int res = DW_DLV_ERROR;
  Dwarf_Die cur_die = in_die;
  Dwarf_Die child = 0;
  Dwarf_Error error;
  
  Dwarf_Half tag = 0;
  Dwarf_Bool haslopc = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount =0;

  int i;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }

  if(tag == DW_TAG_compile_unit) {
    res = dwarf_hasattr(in_die, DW_AT_low_pc, &haslopc, &error);
    if(res != DW_DLV_OK) { perror("CU no lowpc\n"); exit(1);}
    if(haslopc == 0) addrbase = 0;
    else {
      res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
      if(res != DW_DLV_OK) { perror("CU no attrlist\n"); exit(1);}
      for(i = 0; i < attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i], &aform, &error);
	if(res == DW_DLV_OK) {
	  if(aform == DW_AT_low_pc) {
	    get_addr(attrbuf[i], &addrbase);
	  }
	}
	dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
      }
    }
  }

  print_die_data_if_static_var(dbg, in_die, is_info, in_level, sf);
  
  for(;;) {
    Dwarf_Die sib_die = 0;
    res = dwarf_child(cur_die, &child, &error);
    if(res == DW_DLV_ERROR) {
      printf("Error in dwarf_child , level %d \n",in_level);
      exit(1);
    }
    if(res == DW_DLV_OK) {
      get_die_and_siblings_for_static_vars(dbg,child,is_info,in_level+1,sf);
      }
    /* res == DW_DLV_NO_ENTRY */
    res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die,&error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof , level %d :%s \n",in_level,em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done at this level. */
      break;
    }
    /* res == DW_DLV_OK */
    if(cur_die != in_die) {
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
    }
    cur_die = sib_die;
    print_die_data_if_static_var(dbg,cur_die,is_info,in_level,sf);
  }
  return;
}

static void
print_die_data_if_static_var(Dwarf_Debug dbg, Dwarf_Die in_die,
				    int is_info,int level,
				    struct srcfilesdata *sf)
{
  char *name = 0;
  //int localname = 0;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  int res = 0;
  Dwarf_Bool hastype = 0;
  Dwarf_Bool hasexternal = 0;
  Dwarf_Bool hasdeclaration = 0;
  Dwarf_Bool hasdeclfile = 0;
  Dwarf_Bool hasspec = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  char* func_name = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Unsigned i = 0;
  Dwarf_Unsigned file_id = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    perror("Error in dwarf_tag");
    exit(1);
  }

  /* Print static variables; static variables are declared in the same file at the level of functions */

  if(tag == DW_TAG_subprogram) {
    /*res = dwarf_hasattr(in_die, DW_AT_declaration, &hasdeclaration, &error);
    if (res != DW_DLV_OK) return;
    if (hasdeclaration == 1) return;*/
    res = dwarf_hasattr(in_die,DW_AT_external,&hasexternal,&error);
    if(res == DW_DLV_OK && hasexternal == 0) {
      res = dwarf_hasattr(in_die,DW_AT_decl_file,&hasdeclfile,&error);
      if(res != DW_DLV_OK) return;
      if(hasdeclfile == 1) {
	res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
	if(res != DW_DLV_OK) return;
	for(i = 0; i < attrcount; ++i) {
	  Dwarf_Half aform;
	  res = dwarf_whatattr(attrbuf[i], &aform, &error);
	  if(res == DW_DLV_OK) {
	    if(aform == DW_AT_decl_file) {
	      Dwarf_Unsigned local_file_id = 0;
	      get_number(attrbuf[i], &local_file_id);
	      file_id = local_file_id;
	    }
	  }
	  dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
	}
	//printf("local_file_id = %d; decl_file_id = %d", file_id, decl_file_id);
	if(file_id == decl_file_id) {
	  res = dwarf_diename(in_die, &name, &error);
	  if(res != DW_DLV_OK) {
	    printf("no name\n");
	    return;
	  }
	  printf("%s\n", name);
	}
      }
    }
  }
}
