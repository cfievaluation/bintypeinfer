#include "config.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"

struct srcfilesdata {
  char ** srcfiles;
  Dwarf_Signed srcfilescount;
  int srcfilesres;
};

#define TRUE 1
#define FALSE 0

static void
decide_compiler(Dwarf_Debug dbg);

static void 
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf);

int open_a_file(const char* name) {
  /* Only consider on X86 platform here; the original libdwarf targets
     at different platforms, so they have an open-file wrapper */
  int f = -1;
  f = open(name, O_RDONLY);
  return f;
}

void close_a_file(int f) {
  /* to match open_a_file */
  close(f);
}

static int fissionfordie = -1; // TODO: what is this? what if not -1?

static int dienumber = 0;

int main (int argc, char** argv) {
  /* handle command line for target file */
  int fd = -1;
  const char* filepath = NULL;
  /* dwarf environment */
  Dwarf_Debug dbg = 0;
  /* each dwarf function returns a result showing the status of the function */
  int res = DW_DLV_ERROR; 
  /* each dwarf function repots error message; passed by reference */
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;

  /* open target file */
  if(argc != 2) {
    exit(1);
  } else {
    filepath = argv[1];
    fd = open_a_file(filepath);
    if(fd < 0) {
      printf("Failure attempting to open %s\n", filepath);
      exit(1);
    }
  }

  /* initialize dwarf handling environment */
  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  /* TODO:
     For now we print function starts to stdout and redirect to a file.
     However, in the future, when we work with OCaml-C interface, we will
     let this function return the function starts. */
  decide_compiler(dbg);

  /* free resources; end dwarf handling environment */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  close_a_file(fd);
  return 0;
}

static void decide_compiler(Dwarf_Debug dbg) {
  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile; // TODO: try other types
  Dwarf_Bool is_info = TRUE; // TODO: what if it is false?
  Dwarf_Error error;
  
  Dwarf_Attribute *attrbuf =0;
  Dwarf_Signed attrcount = 0;

  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }
    
    res = dwarf_attrlist(cu_die, &attrbuf, &attrcount, &error);
    if (res != DW_DLV_OK) {
      return;
    }
    
    int i;
    int hasproducer = 0;
    char *compile_info = 0;
    res = dwarf_hasattr(cu_die, DW_AT_producer, &hasproducer, &error);
    if(res == DW_DLV_OK && hasproducer == 1) {
      for(i = 0; i < attrcount; i++) {
	Dwarf_Half aform;
	res = dwarf_whatattr(attrbuf[i],&aform,&error);
	if (res == DW_DLV_OK) {
	  if(aform == DW_AT_producer) {
	    res = dwarf_formstring(attrbuf[i], &compile_info, &error);
	    if(res != DW_DLV_OK) {
	      printf("Error! Compile information cannot be transformed to string!\n");
	      exit(1);
	    } else {
	      printf("%s\n", compile_info);
	      return;
	    }
	  }
	} else {
	  perror("Error in dwarf_whatattr!");
	  exit(1);
	}
	
      }
    }

    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
}

static void
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf)
{
  Dwarf_Signed sri = 0;
  for (sri = 0; sri < sf->srcfilescount; ++sri) {
    dwarf_dealloc(dbg, sf->srcfiles[sri], DW_DLA_STRING);
  }
  dwarf_dealloc(dbg, sf->srcfiles, DW_DLA_LIST);
  sf->srcfilesres = DW_DLV_ERROR;
  sf->srcfiles = 0;
  sf->srcfilescount = 0;
}

