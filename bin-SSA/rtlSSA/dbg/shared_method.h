#define VISITED_SIZE 10000
#define TRUE 1
#define FALSE 0

struct srcfilesdata {
  char ** srcfiles;
  Dwarf_Signed srcfilescount;
  int srcfilesres;
};

void
print_type_recursively(Dwarf_Debug dbg, Dwarf_Off type_off,
		       int is_info, int level,
		       struct srcfilesdata *sf);
void 
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf);

int open_a_file(const char* name);

void close_a_file(int f);

void
get_addr(Dwarf_Attribute attr, Dwarf_Addr *val);

void
get_name(Dwarf_Attribute attr, char ** name);

void
get_type_off(Dwarf_Attribute attr, Dwarf_Off* type_off);

void
get_uvalue(Dwarf_Attribute attr, Dwarf_Unsigned* ret_value);

void
get_svalue(Dwarf_Attribute attr, Dwarf_Signed* ret_value);

void
get_number(Dwarf_Attribute attr, Dwarf_Unsigned *val);

void
print_union_members(Dwarf_Debug dbg, Dwarf_Die in_die,
		    int is_info, int level,
		    struct srcfilesdata *sf);
void
print_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int level,
		     struct srcfilesdata *sf);
void
print_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die,
		  int is_info, int level,
		  struct srcfilesdata *sf);
void
print_subroutine_parameters(Dwarf_Debug dbg, Dwarf_Die in_die,
			    int is_info, int level,
			    struct srcfilesdata *sf);
void
print_subranges(Dwarf_Debug dbg, Dwarf_Die in_die,
		int is_info, int level,
		struct srcfilesdata *sf);
void
print_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die,
		int is_info, int level,
		struct srcfilesdata *sf);

int 
count_tag(Dwarf_Debug dbg, Dwarf_Die start_die, Dwarf_Half target_tag);

void
encode_type_recursively(Dwarf_Debug dbg, Dwarf_Off type_off);

void
encode_subranges(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_dimension(Dwarf_Debug dbg, Dwarf_Die parent_die);

void
encode_subroutine_parameters(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_enumerators(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_union_members(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_struct_members(Dwarf_Debug dbg, Dwarf_Die in_die);

void
encode_sinfo(Dwarf_Debug dbg, Dwarf_Die in_die, Dwarf_Addr addrbase);
