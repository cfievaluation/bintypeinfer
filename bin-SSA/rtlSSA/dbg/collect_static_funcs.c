#include "config.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"
#include "shared_method.h"

static void 
collect_static_functions(Dwarf_Debug dbg);

static void 
print_die_data_if_subprogram(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info, int level,
			     struct srcfilesdata *sf);
static void 
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf);

static int fissionfordie = -1; // TODO: what is this? what if not -1?

static int dienumber = 0;

int struct_die_count = 0;

Dwarf_Off visited_struct_die[VISITED_SIZE];

//0: no report; 1: to screen; 2: stdout, can be redirected
int event_level = 0;

int main (int argc, char** argv) {
  /* handle command line for target file */
  int fd = -1;
  const char* filepath = NULL;
  /* dwarf environment */
  Dwarf_Debug dbg = 0;
  /* each dwarf function returns a result showing the status of the function */
  int res = DW_DLV_ERROR; 
  /* each dwarf function repots error message; passed by reference */
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;
  int i = 0;

  /* open target file */
  if(argc != 2) {
    exit(1);
  } else {
    filepath = argv[1];
    fd = open_a_file(filepath);
    if(fd < 0) {
      printf("Failure attempting to open %s\n", filepath);
      exit(1);
    }
  }

  /* initialize dwarf handling environment */
  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  collect_static_functions(dbg);
  //printf("Param_end\n");

  /* free resources; end dwarf handling environment */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  close_a_file(fd);
  return 0;
}

static void collect_static_functions(Dwarf_Debug dbg) {
  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile; // TODO: try other types
  Dwarf_Bool is_info = TRUE; // TODO: what if it is false?
  Dwarf_Error error;
  
  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }

    get_die_and_siblings(dbg, cu_die, is_info, 0, &sf);
    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
}

static void 
get_die_and_siblings(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf)
{
  int res = DW_DLV_ERROR;
  Dwarf_Die cur_die = in_die;
  Dwarf_Die child = 0;
  Dwarf_Error error;
  
  print_die_data_if_subprogram(dbg, in_die, is_info, in_level, sf);
  
  for(;;) {
    Dwarf_Die sib_die = 0;
    res = dwarf_child(cur_die, &child, &error);
    if(res == DW_DLV_ERROR) {
      printf("Error in dwarf_child , level %d \n",in_level);
      exit(1);
    }
    if(res == DW_DLV_OK) {
      get_die_and_siblings(dbg,child,is_info,in_level+1,sf);
    }
    /* res == DW_DLV_NO_ENTRY */
    res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die,&error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof , level %d :%s \n",in_level,em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done at this level. */
      break;
    }
    /* res == DW_DLV_OK */
    if(cur_die != in_die) {
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
    }
    cur_die = sib_die;
    print_die_data_if_subprogram(dbg,cur_die,is_info,in_level,sf);
  }
  return;
}

static void
print_subprogram(Dwarf_Debug dbg,Dwarf_Die die,
		 int is_info, int level,
		 struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Addr lowpc = 0;
  Dwarf_Signed attrcount = 0;
  char* name = 0;
  Dwarf_Unsigned i;
  int ret_flag = 0;
  Dwarf_Bool haslinker = 0;

  struct_die_count = 0;
  for(i = 0; i < VISITED_SIZE; i++) {
    visited_struct_die[i] = 0;
  }

  /* fetch all attributes */
  res = dwarf_attrlist(die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }

  res = dwarf_hasattr(die, DW_AT_linkage_name,&haslinker,&error);
  if(res == DW_DLV_OK && haslinker == 1) {
    char* linkname = 0;
    for(i = 0; i < attrcount; ++i) {
      Dwarf_Half aform;
      res = dwarf_whatattr(attrbuf[i], &aform, &error);
      if(res == DW_DLV_OK) {
	if(aform == DW_AT_linkage_name) {
	  get_name(attrbuf[i], &linkname);
	  printf("Function_name:\n%s\n", linkname);
	  break;
	}
      }
    }
  } else {
    res = dwarf_diename(die, &name, &error);
    if(res != DW_DLV_OK) {    
      printf("Function_no_name:\n0\n");
      return;
    }
    else {
      printf("Function_name:\n%s\n", name);
    }
  }
  
  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_type) {
	Dwarf_Off type_off = 0;
	ret_flag = 1;
	get_type_off(attrbuf[i], &type_off);
	//printf("Return type_off:\n%" DW_PR_DUx "\n", type_off);
	printf("Return type:\n");
	encode_type_recursively(dbg, type_off);
	printf("\n");
      }	
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
  if(ret_flag == 0) {
    printf("Return type:\n0(void)\n");
  }
}

static void
print_parameter(Dwarf_Debug dbg, Dwarf_Die die,
		int is_info, int level,
		struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Addr lowpc = 0;
  Dwarf_Signed attrcount = 0;
  char* name = 0;
  Dwarf_Unsigned i;

  struct_die_count = 0;
  for(i = 0; i < VISITED_SIZE; i++) {
    visited_struct_die[i] = 0;
  }

  /* fetch all attributes */
  res = dwarf_attrlist(die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    return;
  }
  
  /* search for lowpc attribute */
  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_type) {
	Dwarf_Off type_off = 0;
	get_type_off(attrbuf[i], &type_off);
	//printf("Parameter type_off:\n%" DW_PR_DUx "\n", type_off);
	printf("Parameter type:\n");
      	encode_type_recursively(dbg, type_off);
	printf("\n");
      }
    }
    dwarf_dealloc(dbg, attrbuf[i], DW_DLA_ATTR);
  }
}

static void
print_parameters(Dwarf_Debug dbg, Dwarf_Die in_die,
		int is_info, int level,
		struct srcfilesdata *sf)
{
  int res;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  Dwarf_Die sib_die = 0;
  static Dwarf_Bool hasparam = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  if(tag == DW_TAG_formal_parameter) {
    print_parameter(dbg,in_die,is_info,level,sf);
    hasparam = 1;
  }
  
  if(tag == DW_TAG_unspecified_parameters) {
    printf("Parameter type:\n");
    printf("9\n");
    hasparam = 1;
  }

  res = dwarf_siblingof(dbg,in_die,&sib_die,&error);
  if(res == DW_DLV_NO_ENTRY) {
    if(hasparam == 0) {
      printf("Parameter type:\n");
      printf("0(void)\n");
    }
    return;
  }
  else if(res == DW_DLV_OK) 
    print_parameters(dbg,sib_die,is_info,level,sf);
  else {
    perror("Error: print_children for param and var when siblingof!");
    exit(1);
  }

}

static void
print_die_data_if_subprogram_i(Dwarf_Debug dbg, Dwarf_Die in_die,
			       int is_info,int level,
			       struct srcfilesdata *sf)
{
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  const char *tagname = 0;
  char *diename = 0;
  int res = 0;
  static subprog_flag = 0;
  static param_flag = 0;
  Dwarf_Die child_die = 0;
  Dwarf_Bool hasexternal = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("Error in dwarf_tag , level %d \n",level);
    exit(1);
  }
  res = dwarf_get_TAG_name(tag,&tagname);
  if(res != DW_DLV_OK) {
    printf("Error in dwarf_get_TAG_name , level %d \n",level);
    exit(1);
  }

  if(tag == DW_TAG_subprogram) {
    res = dwarf_hasattr(in_die, DW_AT_external, &hasexternal,&error);
    if(res == DW_DLV_OK && hasexternal == 0) {
      res = dwarf_diename(in_die, &diename, &error);
      if(res != DW_DLV_OK) {
	printf("no name\n");
	return;
      }
      printf("%s\n", diename);
    }
  }
}


static void
print_die_data_if_subprogram(Dwarf_Debug dbg, Dwarf_Die in_die,
			     int is_info,int level,
			     struct srcfilesdata *sf)
{
  print_die_data_if_subprogram_i(dbg,in_die,is_info,level,sf);
  dienumber++;
}
