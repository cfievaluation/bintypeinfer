#include "config.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "dwarf.h"
#include "libdwarf.h"

struct srcfilesdata {
  char ** srcfiles;
  Dwarf_Signed srcfilescount;
  int srcfilesres;
};

#define TRUE 1
#define FALSE 0

static void 
collect_label(Dwarf_Debug dbg);

static void 
search_subprograms(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf);
static void 
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf);

int open_a_file(const char* name) {
  /* Only consider on X86 platform here; the original libdwarf targets
     at different platforms, so they have an open-file wrapper */
  int f = -1;
  f = open(name, O_RDONLY);
  return f;
}

void close_a_file(int f) {
  /* to match open_a_file */
  close(f);
}

static int fissionfordie = -1; // TODO: what is this? what if not -1?

static int dienumber = 0;



int main (int argc, char** argv) {
  /* handle command line for target file */
  int fd = -1;
  const char* filepath = NULL;
  /* dwarf environment */
  Dwarf_Debug dbg = 0;
  /* each dwarf function returns a result showing the status of the function */
  int res = DW_DLV_ERROR; 
  /* each dwarf function repots error message; passed by reference */
  Dwarf_Error error;
  /* TODO: Unknown purpose for now */
  Dwarf_Handler errhand = 0;
  Dwarf_Ptr errarg = 0;

  /* open target file */
  if(argc != 2) {
    exit(1);
  } else {
    filepath = argv[1];
    fd = open_a_file(filepath);
    if(fd < 0) {
      printf("Failure attempting to open %s\n", filepath);
      exit(1);
    }
  }

  /* initialize dwarf handling environment */
  res = dwarf_init(fd, DW_DLC_READ,errhand,errarg,&dbg,&error);
  if(res != DW_DLV_OK) {
    printf("Giving up, cannot do DWARF processing\n");
    exit(1);
  }

  /* TODO:
     For now we print function starts to stdout and redirect to a file.
     However, in the future, when we work with OCaml-C interface, we will
     let this function return the function starts. */
  collect_label(dbg);

  /* free resources; end dwarf handling environment */
  res = dwarf_finish(dbg, &error);
  if(res != DW_DLV_OK) {
    printf("dwarf_finish failed!\n");
  }
  close_a_file(fd);
  return 0;
}

static void collect_label(Dwarf_Debug dbg) {
  int cu_number = 0;

  /* Following are all parameters for function "dwarf_next_cu_header_d" */
  Dwarf_Unsigned cu_header_length = 0;
  Dwarf_Half version_stamp = 0;
  Dwarf_Unsigned abbrev_offset = 0;
  Dwarf_Half address_size = 0;
  Dwarf_Half offset_size = 0;
  Dwarf_Half extension_size = 0;
  Dwarf_Sig8 signature;
  Dwarf_Unsigned typeoffset = 0;
  Dwarf_Unsigned next_cu_header = 0;
  Dwarf_Half header_cu_type = DW_UT_compile; // TODO: try other types
  Dwarf_Bool is_info = TRUE; // TODO: what if it is false?
  Dwarf_Error error;
  
  for(;;++cu_number) {
    Dwarf_Die no_die = 0;
    Dwarf_Die cu_die = 0;
    int res = DW_DLV_ERROR;
    struct srcfilesdata sf;
    sf.srcfilesres = DW_DLV_ERROR;
    sf.srcfiles = 0;
    sf.srcfilescount = 0;
    memset(&signature, 0, sizeof(signature));
    
    /* find the next cu header; TODO: what is cu header? Compile Unit?*/
    res = dwarf_next_cu_header_d(dbg, is_info, &cu_header_length,
				 &version_stamp, &abbrev_offset,
				 &address_size, &offset_size,
				 &extension_size, &signature,
				 &typeoffset, &next_cu_header,
				 &header_cu_type, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_next_cu_header: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done. */
      return;
    }

    /* find the sibling of  */
    res = dwarf_siblingof_b(dbg, no_die, is_info, &cu_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof on CU die: %s\n",em);
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Impossible case. */
      printf("no entry! in dwarf_siblingof on CU die \n");
      exit(1);
    }

    search_subprograms(dbg, cu_die, is_info, 0, &sf);
    dwarf_dealloc(dbg, cu_die, DW_DLA_DIE);
    resetsrcfiles(dbg, &sf);
  }
}

static void
get_addr(Dwarf_Attribute attr, Dwarf_Addr *val) {
  Dwarf_Error error = 0;
  int res;
  Dwarf_Addr uval = 0;
  
  /* Translate attribute to address */
  res =dwarf_formaddr(attr, &uval, &error);
  if(res == DW_DLV_OK) {
    *val = uval;
    return;
  }
  return;
}

static int
is_subprogram(Dwarf_Debug dbg, Dwarf_Die in_die) 
{
  int res = 0;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;

  res = dwarf_tag(in_die,&tag,&error);
  if(res != DW_DLV_OK) {
    printf("Error in dwarf_tag\n");
    exit(1);
  }

  if( tag == DW_TAG_subprogram) {
    return 1;
  }
  return 0;
}

static void
print_label_if_label(Dwarf_Debug dbg, Dwarf_Die in_die, 
		     Dwarf_Addr lpc, Dwarf_Addr hpc)
{
  int res = DW_DLV_ERROR;
  Dwarf_Error error = 0;
  Dwarf_Half tag = 0;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Addr lowpc = -1;
  Dwarf_Unsigned i;

  res = dwarf_tag(in_die, &tag, &error);
  if(res != DW_DLV_OK) {
    printf("Error in dwarf_tag\n");
    exit(1);
  }
  if( tag != DW_TAG_label) {
    return;
  }
  
  res = dwarf_attrlist(in_die, &attrbuf, &attrcount, &error);
  if(res != DW_DLV_OK) {
    printf("Attribute list fail!\n");
    return;
  }
  for(i = 0; i < attrcount; ++i) {
    Dwarf_Half aform;
    res = dwarf_whatattr(attrbuf[i], &aform, &error);
    if(res == DW_DLV_OK) {
      if(aform == DW_AT_low_pc) {
	get_addr(attrbuf[i], &lowpc);
      }
    }
    dwarf_dealloc(dbg,attrbuf[i],DW_DLA_ATTR);
  }
 
  printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", lowpc);
  printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", lpc);
  printf("0x%" DW_PR_XZEROS DW_PR_DUx "\n", hpc);
  
  return;
}

static void
search_subprog_children_for_labels(Dwarf_Debug dbg, Dwarf_Die parent_die,
				   int is_info, struct srcfilesdata *sf)
{
  int res = DW_DLV_ERROR;
  Dwarf_Error error = 0;
  Dwarf_Die cur_die = parent_die;
  Dwarf_Die child = 0;
  Dwarf_Addr lowpc = -1;
  Dwarf_Addr highpc = -1;
  Dwarf_Attribute *attrbuf = 0;
  Dwarf_Signed attrcount = 0;
  Dwarf_Unsigned i;
 
  int hresb = 0;
  int lres = 0;
  Dwarf_Addr hipcoffset = 0;
  Dwarf_Half highform = 0;
  enum Dwarf_Form_Class highclass = 0;
  
  hresb = dwarf_highpc_b(parent_die,&highpc,&highform,&highclass,&error);
  lres = dwarf_lowpc(parent_die,&lowpc,&error);
  if(hresb == DW_DLV_OK) {
    /* present, FORM addr or const. */
    if(highform == DW_FORM_addr) {
    } else {
      if(lres == DW_DLV_OK) {
	hipcoffset = highpc;
	highpc = lowpc + hipcoffset;
      } else {
	printf("highoff  0x%" DW_PR_XZEROS DW_PR_DUx " ",
	       highpc);
      }
    }
  } else if (hresb == DW_DLV_ERROR) {
    printf("dwarf_highpc_b() error!");
  } else {
    /* absent */
  }

  res = dwarf_child(cur_die, &child, &error);
  if(res == DW_DLV_ERROR) {
    printf("Error in dwarf_child\n");
    exit(1);
  }
  if(res == DW_DLV_OK) {
    cur_die = child;
    print_label_if_label(dbg, child, lowpc, highpc);
    for(;;) {
      Dwarf_Die sib_die = 0;
      res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die, &error);
      if(res == DW_DLV_ERROR) {
	char *em = dwarf_errmsg(error);
	printf("Error in dwarf_siblingof\n");
	exit(1);
      }
      if(res == DW_DLV_NO_ENTRY) {
	/* Done at this level. */
	break;
      }
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
      cur_die = sib_die;
      print_label_if_label(dbg, cur_die, lowpc, highpc);
    }
  }
  return;
}

static void 
search_subprograms(Dwarf_Debug dbg, Dwarf_Die in_die,
		     int is_info, int in_level,
		     struct srcfilesdata *sf)
{
  int res = DW_DLV_ERROR;
  Dwarf_Die cur_die = in_die;
  Dwarf_Die child = 0;
  Dwarf_Error error;
  
  if(is_subprogram(dbg, cur_die)) {
    search_subprog_children_for_labels(dbg, cur_die, is_info, sf);
  }

  for(;;) {
    Dwarf_Die sib_die = 0;
    res = dwarf_child(cur_die, &child, &error);
    if(res == DW_DLV_ERROR) {
      printf("Error in dwarf_child , level %d \n",in_level);
      exit(1);
    }
    if(res == DW_DLV_OK) {
      search_subprograms(dbg,child,is_info,in_level+1,sf);
    }
    res = dwarf_siblingof_b(dbg,cur_die,is_info,&sib_die, &error);
    if(res == DW_DLV_ERROR) {
      char *em = dwarf_errmsg(error);
      printf("Error in dwarf_siblingof\n");
      exit(1);
    }
    if(res == DW_DLV_NO_ENTRY) {
      /* Done at this level. */
      break;
    }
    if(cur_die != in_die) {
      dwarf_dealloc(dbg,cur_die,DW_DLA_DIE);
    }
    cur_die = sib_die;
    if(is_subprogram(dbg, cur_die)) {
      search_subprog_children_for_labels(dbg, cur_die, is_info, sf);
    }
  }
  return;
}

static void
resetsrcfiles(Dwarf_Debug dbg,struct srcfilesdata *sf)
{
  Dwarf_Signed sri = 0;
  for (sri = 0; sri < sf->srcfilescount; ++sri) {
    dwarf_dealloc(dbg, sf->srcfiles[sri], DW_DLA_STRING);
  }
  dwarf_dealloc(dbg, sf->srcfiles, DW_DLA_LIST);
  sf->srcfilesres = DW_DLV_ERROR;
  sf->srcfiles = 0;
  sf->srcfilescount = 0;
}
