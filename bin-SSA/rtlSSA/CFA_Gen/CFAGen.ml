(**
   This application is to generate for each assembly-level function a 
   complete list of CFA representations with registers and offsets: 
   
   Input: disassembled functions with CFGs
   Output: a list of CFA representations for each function
   range * (CFAExpr list) list

   Workflow:
   1. Initialize a hash table for storing values of registers in terms
   of CFA and offsets
   2. Split the function into address intervals per instruction; we have
   range * X86_instr list
   3. range * X86_instr list --> range * (rtl_instr list) list
   4. range * (rtl_instr list) list --> range * (CFAExpr list) list

**)

open Batteries
open Printf
module H = Hashtbl

open X86Syntax
open X86Semantics
open X86_MACHINE
open X86_RTL
open Config
open BB

open Block
open Config_cfg
open Printer
open RTL_opt

type cfaRepr = (mword * (rtl_exp list)) list

type cfabb = {
  mutable cfa_bb : basicblock;
  mutable cfa_repr: cfaRepr;
  mutable cfa_in_regvals : ((register, rtl_exp) H.t) option;
  mutable cfa_out_regvals : ((register, rtl_exp) H.t) option;
}

let mkCFABB cfa_input bb cfa_output =
  {
    cfa_bb = bb;
    cfa_repr = [];
    cfa_in_regvals = cfa_input;
    cfa_out_regvals = cfa_output;
  }

let dump_regvals regvals =
  H.iter (fun reg re ->
    printf "%s:\n%s\n" 
      (str_of_reg reg)
      (str_of_rtl_exp_less_size re);
    flush stdout
  ) regvals

let dump_cfa_repr cfa_repr =
  List.iter (fun (lopc,cfa_exprs) ->
    printf "%s:\t" (str_of_mword_flex lopc);
    List.iter (fun re -> 
      printf "%s\t" (str_of_rtl_exp_less_size re);
    ) cfa_exprs;
    printf "\n"
  ) cfa_repr;
  flush stdout

let rec apply_regvals regvals rtl_expr =
  begin match rtl_expr with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    Coq_arith_rtl_exp(s,bvop,new_re1,new_re2)
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    Coq_test_rtl_exp(s,top,new_re1,new_re2)
  | Coq_if_rtl_exp(s,re1,re2,re3) ->
    let new_re1 = apply_regvals regvals re1 in
    let new_re2 = apply_regvals regvals re2 in
    let new_re3 = apply_regvals regvals re3 in
    Coq_if_rtl_exp(s,new_re1,new_re2,new_re3)
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    let new_re = apply_regvals regvals re in
    Coq_cast_s_rtl_exp(s1,s2,new_re)
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    let new_re = apply_regvals regvals re in
    Coq_cast_u_rtl_exp(s1,s2,new_re)
  | Coq_get_loc_rtl_exp(s,loc) ->
    begin match loc with
    | Coq_reg_loc(reg) -> 
      if reg = ESP || reg = EBP then
	begin 
	  try
	    H.find regvals reg
	  with Not_found ->
	    Coq_get_loc_rtl_exp(s,loc)
	end
      else
	Coq_get_loc_rtl_exp(s,loc)
    | _ -> Coq_get_loc_rtl_exp(s,loc)
    end
  | Coq_get_array_rtl_exp(s,l,arr,re) ->
    let new_re = apply_regvals regvals re in
    Coq_get_array_rtl_exp(s,l,arr,new_re)
  | Coq_get_byte_rtl_exp(re) ->
    let new_re = apply_regvals regvals re in
    Coq_get_byte_rtl_exp(new_re)
  | _ -> (* ignore floating point arithmetic expressions *)
    rtl_expr
  end

let translate_regvals regvals =
  H.fold (fun reg re cfa_exprs ->
    let get_reg = Coq_get_loc_rtl_exp(Big.of_int 31, Coq_reg_loc reg) in
    match re with
    | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
      begin match re1 with
      | Coq_get_ps_reg_rtl_exp(s, ps_reg) when Big.to_int ps_reg = 0 ->
	begin match bvop with
	| Coq_add_op ->
	  Coq_arith_rtl_exp(s,Coq_sub_op,get_reg,re2) :: cfa_exprs
	| Coq_sub_op ->
	  Coq_arith_rtl_exp(s,Coq_add_op,get_reg,re2) :: cfa_exprs
	| _ -> cfa_exprs
	end
      | _ -> cfa_exprs
      end
    | _ -> cfa_exprs
  ) regvals []

let transform_lopc start rel_low =
  let start_mword = MWord.of_string start in
  let rel_low_mword = MWord.of_int rel_low in
  let abs_low_mword = MWord.(+%) rel_low_mword start_mword in
  (*str_of_mword_flex abs_low_mword*)
  abs_low_mword

(** For simplicity, we assume stack layout modification only happens in
    prologue and we do not care about epilogue. **)

let rec eq_rtl_exp rtl_exp1 rtl_exp2 =
  match rtl_exp1, rtl_exp2 with
  | Coq_arith_rtl_exp(s,bvop,re1,re2), Coq_arith_rtl_exp(s',bvop',re1',re2') ->
    Big.eq s s' && bvop = bvop' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_test_rtl_exp(s,top,re1,re2), Coq_test_rtl_exp(s',top',re1',re2') ->
    Big.eq s s' && top = top' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_if_rtl_exp(s,re1,re2,re3), Coq_if_rtl_exp(s',re1',re2',re3') ->
    Big.eq s s' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2' && eq_rtl_exp re3 re3'
  | Coq_cast_s_rtl_exp(s1,s2,re), Coq_cast_s_rtl_exp(s1',s2',re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_cast_u_rtl_exp(s1, s2, re), Coq_cast_u_rtl_exp(s1', s2', re') ->
    Big.eq s1 s1' && Big.eq s2 s2' && eq_rtl_exp re re'
  | Coq_imm_rtl_exp(s, v), Coq_imm_rtl_exp(s', v') ->
    Big.eq s s' && Big.eq v v'
  | Coq_get_loc_rtl_exp(s, loc), Coq_get_loc_rtl_exp(s', loc') ->
    Big.eq s s' && loc = loc'
  | Coq_get_ps_reg_rtl_exp(s, ps_reg), Coq_get_ps_reg_rtl_exp(s',ps_reg') ->
    Big.eq s s' && Big.eq ps_reg ps_reg'
  | Coq_get_array_rtl_exp(l, s, arr, re), Coq_get_array_rtl_exp(l',s',arr',re') ->
    Big.eq l l' && Big.eq s s' && arr = arr' && eq_rtl_exp re re'
  | Coq_get_byte_rtl_exp(re), Coq_get_byte_rtl_exp(re') ->
    eq_rtl_exp re re'
  | Coq_get_random_rtl_exp(s),Coq_get_random_rtl_exp(s') ->
    Big.eq s s'
  | Coq_farith_rtl_exp(ew,mw,faop,rtl_rm,re1,re2),
    Coq_farith_rtl_exp(ew',mw',faop',rtl_rm',re1',re2') ->
    Big.eq ew ew' && Big.eq mw mw' && faop = faop' && eq_rtl_exp rtl_rm rtl_rm' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | Coq_fcast_rtl_exp(s1,s2,s3,s4,re1,re2),Coq_fcast_rtl_exp(s1',s2',s3',s4',re1',re2') ->
    Big.eq s1 s1' && Big.eq s2 s2' && Big.eq s3 s3' && Big.eq s4 s4' && eq_rtl_exp re1 re1' && eq_rtl_exp re2 re2'
  | _,_ -> false

let eq_regvals regvals1 regvals2 =
  H.fold (fun key dest b ->
    try
      b && eq_rtl_exp dest (H.find regvals2 key) (*(dest = (H.find regvals2 key))*)
    with Not_found ->
      false
  ) regvals1 true

let rec cfa_in_rtl_exp rtl_exp =
  match rtl_exp with
  | Coq_arith_rtl_exp(s,bvop,re1,re2) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2
  | Coq_test_rtl_exp(s,top,re1,re2) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2
  | Coq_if_rtl_exp(s,re1,re2,re3) ->
    cfa_in_rtl_exp re1 || cfa_in_rtl_exp re2 || cfa_in_rtl_exp re3
  | Coq_cast_s_rtl_exp(s1,s2,re) ->
    cfa_in_rtl_exp re
  | Coq_cast_u_rtl_exp(s1,s2,re) ->
    cfa_in_rtl_exp re
  | Coq_get_ps_reg_rtl_exp(s,ps_reg) ->
    Big.to_int ps_reg = 0
  | _ -> (* ignore floating point arithmetic expressions *)
    false

exception Regvals_Conflict

let cfa_repr_gen asm_fun =
  let regvals = H.create 32 in

  let cfa_pseudo_reg = 
    (*Coq_get_loc_rtl_exp(Big.of_int 31, Coq_reg_loc EAX) in*)
    Coq_get_ps_reg_rtl_exp (Big.of_int 31, Big.of_int 0) in
  (* may consider other value for CFA *)

  let initial_esp = 
    Coq_arith_rtl_exp (Big.of_int 31, Coq_sub_op, cfa_pseudo_reg, Coq_imm_rtl_exp (Big.of_int 31, Big.of_int 4)) 
  in

  let () = H.add regvals ESP initial_esp in
  
  (** To completely reconstruct stack layout, we need do a worklist algorithm from entry
      block. But we cannot guarantee the correctness. **)

  let analyze_bb in_regvals bb =
    let out_regvals = H.copy in_regvals in

    let bb_instrs = bb.bb_instrs in
    
    let bb_start = bb.bb_relAddr in
    
    let instr_start = ref bb_start in

    let patch_call_with_nop bb_instrs =
      List.map (fun (pre,instr,len) -> 
	match instr with
	| CALL(_,_,_,_) -> (pre,NOP (Imm_op (Big.of_int 0)),len)
	| _ -> (pre,instr,len)
      ) bb_instrs
    in

    let addressed_bb_instrs = List.map (fun (pre,instr,len) ->
      let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
      let addressed_rtl_instr = 
	!instr_start, rtl_instrs
      in
      instr_start := !instr_start + len;
      addressed_rtl_instr
    ) (patch_call_with_nop bb_instrs)
    in

    let cfa_repr =
      List.map (fun (lopc, rtl_instr_list) ->
	List.iter (fun rtl_instr ->
	  begin match rtl_instr with
	  | Coq_set_loc_rtl(s,rtl_e,loc) ->
	    begin match loc with
	    | X86_MACHINE.Coq_reg_loc(reg) ->
	      if reg = ESP || reg = EBP then
		let new_rhs = apply_regvals out_regvals rtl_e in
		let simplified_new_rhs = simp_re simpre_all_methods new_rhs in
		(*printf "simplified_new_rhs:%s\n" (str_of_rtl_exp_less_size simplified_new_rhs);*)
		if cfa_in_rtl_exp simplified_new_rhs then 
		  H.replace out_regvals reg simplified_new_rhs
		else
		  H.remove out_regvals reg
	    | _ -> ()
	    end
	  | _ -> ()
	  end
	) rtl_instr_list;
	let cfa_expr_list = translate_regvals out_regvals in
	let abs_lopc = transform_lopc "0x8048000" lopc in
	abs_lopc, cfa_expr_list
      ) addressed_bb_instrs
    in
    out_regvals, cfa_repr
  in
  
  let workq = Queue.create () in
  
  let entry_bb_label = (List.hd asm_fun.func_bbs).bb_label in

  (*let entry_cfabb = mkCFABB None entry_bb None in*)

  Queue.add (regvals, entry_bb_label) workq;

  let cfabbs = ref [] in

  while (not (Queue.is_empty workq)) do
    let task_in_regvals, task_bb_label = Queue.take workq in
    (*printf "task bb label %s with input regvals:\n" task_bb_label; 
    dump_regvals task_in_regvals;
    flush stdout;*)
    let task_in_regvals = H.copy task_in_regvals in
    try 
      let task_cfabb = List.find (fun cfabb -> cfabb.cfa_bb.bb_label = task_bb_label) !cfabbs in
      if Option.is_none task_cfabb.cfa_in_regvals then (
	printf "not handled properly\n"; flush stdout;
	task_cfabb.cfa_in_regvals <- Some task_in_regvals;
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	List.iter (fun bb_label ->
	  if List.exists (fun func_bb -> func_bb.bb_label = bb_label) asm_fun.func_bbs then
	    Queue.add (out_regvals, bb_label) workq
	) task_cfabb.cfa_bb.bb_succs;
	List.iter (fun bb_label ->
	  Queue.add (out_regvals, bb_label) workq
	) task_cfabb.cfa_bb.bb_inter_succs
      )
      else (
	let old_in_regvals = Option.get task_cfabb.cfa_in_regvals in
	if not (eq_regvals old_in_regvals task_in_regvals) then (
	  printf "old input regvals:\n";
	  dump_regvals old_in_regvals;
	  printf "\n";
	  printf "new input regvals:\n";
	  dump_regvals task_in_regvals;
	  printf "\n";
	  flush stdout;
	  raise Regvals_Conflict
	)
      );
    with Not_found ->
      try 
	let task_bb = List.find (fun func_bb -> func_bb.bb_label = task_bb_label) asm_fun.func_bbs in
	let task_cfabb = mkCFABB (Some task_in_regvals) task_bb None in
	let out_regvals, cfa_repr = analyze_bb task_in_regvals task_cfabb.cfa_bb in
	task_cfabb.cfa_repr <- cfa_repr;
	task_cfabb.cfa_out_regvals <- Some out_regvals;
	cfabbs := task_cfabb :: !cfabbs;
	List.iter (fun bb_label ->
	  (*printf "trying to add new successor label %s with regvals:\n" bb_label; flush stdout;
	  dump_regvals out_regvals;
	  printf "\n";
	  flush stdout;*)
	  if List.exists (fun func_bb -> func_bb.bb_label = bb_label) asm_fun.func_bbs then
	    Queue.add (out_regvals, bb_label) workq
	) task_cfabb.cfa_bb.bb_succs;
	List.iter (fun bb_label ->
	  (*printf "trying to add new inter-successor label %s with regvals:\n" bb_label; flush stdout;
	  dump_regvals out_regvals; 
	  printf "\n";
	  flush stdout;*)
	  Queue.add (out_regvals, bb_label) workq
	) task_cfabb.cfa_bb.bb_inter_succs 
      with Not_found ->
	raise (Failure "basic block label not found in function")
  done;
  let func_cfa_repr =
    List.flatten (List.map (fun cfabb -> cfabb.cfa_repr) !cfabbs)
  in
  (*dump_cfa_repr func_cfa_repr;*)
  let sorted_func_cfa_repr = 
    List.sort (fun (lopc1, cfa_exprs1) (lopc2, cfa_exprs2) -> 
      if MWord.(<%) lopc1 lopc2 then -1
      else if MWord.(>%) lopc1 lopc2 then 1
      else 0
    ) func_cfa_repr
  in  
  dump_cfa_repr sorted_func_cfa_repr;
  sorted_func_cfa_repr
  (** Here, we assume stack depth is not modified after prologue **)
  (*
  let prologue_bb = 
    (*try*) 
      List.hd asm_fun.func_bbs
  (* may cause trouble here: this may not be the prologue*)
    (*with Not_found -> exit 1*)
  in
    
  let prologue_instrs = prologue_bb.bb_instrs in

  let prologue_start = prologue_bb.bb_relAddr in
  
  let start = ref prologue_start in

  let ranged_prologue_instrs = List.map (fun (pre,instr,len) ->
    let rtl_instrs = X86_Compile.instr_to_rtl pre instr in
    let ranged_rtl_instr = 
      (!start, len + !start), rtl_instrs
    in
    start := !start + len;
    ranged_rtl_instr
  ) prologue_instrs
  in

  let cfa_repr =
    List.map (fun (range, rtl_instr_list) ->
      List.iter (fun rtl_instr ->
	begin match rtl_instr with
	| Coq_set_loc_rtl(s,rtl_e,loc) ->
	  begin match loc with
	  | X86_MACHINE.Coq_reg_loc(reg) ->
	    if reg = ESP || reg = EBP then
	      let new_rhs = apply_regvals regvals rtl_e in
	      let simplified_new_rhs = simp_re simpre_all_methods new_rhs in
	    (*printf "simplified_new_rhs:%s\n" (str_of_rtl_exp_less_size simplified_new_rhs);*)
	      H.replace regvals reg simplified_new_rhs
	  | _ -> ()
	  end
	| _ -> ()
	end
      ) rtl_instr_list;
      let cfa_expr_list = translate_regvals regvals in
      let abs_range = transform_rel_range "0x8048000" range in
      abs_range, cfa_expr_list
    ) ranged_prologue_instrs
  in
  (*dump_regvals regvals;*)
  dump_cfa_repr cfa_repr;
  cfa_repr*)

let cfaGen asm_fun_list =
  List.map (fun asm_fun ->
    printf "Dealing with function %s...\n" asm_fun.func_name; flush stdout;
    asm_fun, cfa_repr_gen asm_fun
  ) asm_fun_list

