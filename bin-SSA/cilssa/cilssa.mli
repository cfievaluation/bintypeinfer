open Cil

type phifun = varinfo * (int * varinfo) list

(* a fundec in SSA form *)
type fundec_ssa = {
  fdec : fundec;
  phimap : (int, phifun list) Hashtbl.t; (* map from stmtID:int to a list 
				      of phi functions *)
}

val ssaFundec : fundec -> fundec_ssa

val feature: Cil.featureDescr