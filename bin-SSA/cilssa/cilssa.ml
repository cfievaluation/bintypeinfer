(*
 *
 * Copyright (c) 2013. Gang Tan.
 * This file adapts the llvm ssa module to perform CIL-level SSA transform
 *)

open Pretty
open Cil
module E = Errormsg
module H = Hashtbl
open PyObject

let _ = Trace.traceAddSys "cilssa"
let trace = Trace.trace "cilssa"

(* a phi function: v := phi(l1:v1, ..., ln:vn),
   where l_i is the ID of a predecessor statement;
   when the control flows to the phi function through l_i,
   then v should get v_i *)
type phifun = varinfo * (int * varinfo) list

(* a fundec in SSA form *)
type fundec_ssa = {
  fdec : fundec;
  phimap : (int, phifun list) H.t; (* map from stmtID:int to a list 
				      of phi functions *)
}

class phiPrinterVisitor (phimap: (int, phifun list) H.t) = object
  inherit nopCilVisitor
  method vstmt (s:stmt) :stmt visitAction = 
    if (H.mem phimap s.sid) then begin
      let philist = H.find phimap s.sid in
      let dumpphi (pf: phifun) = 
	let (dest,args) = pf in
	trace (dprintf "%s = phi(%a)\n" dest.vname
		 (d_list "," (fun () (id,vi) -> num id ++ text ":" ++ text vi.vname))
		 args)
      in
      if philist <> [] then begin
	trace (dprintf "Before stmt id %d:\n" s.sid);
	List.iter dumpphi philist
      end
    end;
    (* trace (dprintf "%a\n" defaultCilPrinter#pStmt s); *)
    DoChildren
end

let dumpCFG (fd:fundec) : unit = 
  trace (dprintf "Beginning of CFG for function %s\n\n" fd.svar.vname);
  Cfg.printCfgChannel stdout fd; flush stdout;
  trace (dprintf "End of CFG for function %s\n" fd.svar.vname)


(* wrap a function that changes varinfo nodes into a visitor *)
class varVisitor (visitvar:varinfo -> varinfo visitAction) = object
  inherit nopCilVisitor
  method vvrbl (vi: varinfo) = visitvar vi
end

class stmtVisitor (visitstmt: stmt -> stmt visitAction) = object
  inherit nopCilVisitor
  method vstmt (s: stmt) = visitstmt s
end

(* For fundec fd, perform the really crude (RC) SSA transform 
   according to appel's paper *)
let doRCSSA (fd:fundec) (ssavars:varinfo list) : fundec_ssa =
  (* map from (stmtID:int, varID:int) -> varinfo
     giving the ssa variable to use for varID at entry to statement stmtID *)
  let entryName = H.create 32

  (* map from (stmtID:int, varID:int) -> varinfo
     giving the ssa var to use for varID at exit from statement stmtID *)
  and exitName = H.create 32 in

  let id = ref 0 in
  
  (* the initial set of new locals *)
  let newlocals = ref fd.slocals in

  (* create a new unique var for local variable vi *)
  let createNewVar (vi:varinfo) (sid:int) : varinfo = 
    id := !id + 1;
    let newname = "__" ^ vi.vname ^ string_of_int !id in
    let vi' = copyVarinfo vi newname in
    newlocals := vi' :: !newlocals; (* adding the new local variable *)
    vi'
  in

  let phiNodes = H.create 50 in

  (* Rename variables vl in block b: pick new variables for b at
     entry to b (except if 'b' is the function entry point), and
     rename all uses and subsequent assignments. Record the variable's
     new names at entry to b and new varaibles at exit from b in
     entryName and exitName *)
  let rec renameVariablesBlk (vl:varinfo list) (b:block) : block = 
    {b with bstmts = List.map (renameVariablesStmt vl) b.bstmts }

  and renameVariablesStmt (vl:varinfo list) (s:stmt) : stmt =
    (* Initialize the entry and exit var for the statement; for the function
       entry, it is the original variable; otherwise, a new variable is generated *)
    let iniEntryExitNames (vi:varinfo) = 
      let vi' = if s.preds <> [] then createNewVar vi s.sid else vi
      in 	
      H.add entryName (s.sid, vi.vid) vi';
      H.add exitName (s.sid, vi.vid) vi'
    in

    List.iter iniEntryExitNames vl;

    let renameVar (vi:varinfo): varinfo visitAction = 
      if H.mem exitName (s.sid, vi.vid) then begin
	ChangeTo (H.find exitName (s.sid, vi.vid))
      end
      else ChangeTo vi
    in
    let rvVisitor = new varVisitor renameVar in

    let renameExp (e:exp) : exp = 
      visitCilExpr rvVisitor e
      (* trace (dprintf "old:%a\tnew:%a\n" defaultCilPrinter#pExp e defaultCilPrinter#pExp e1) *)
    in
    
    let renameIns (ins:instr) : instr = begin match ins with
      | Set (lv, e, loc) -> 
	
	begin
	  let e' = renameExp e in
	  (match lv with
	  | (Var vi, NoOffset) when H.mem exitName (s.sid, vi.vid) ->
						    (* vi assigned, pick new name *)
	    let newvar = createNewVar vi s.sid in
	    H.replace exitName (s.sid, vi.vid) newvar;
	    Set ((Var newvar, NoOffset), e', loc)

	  | (Mem (Lval (Var vi, NoOffset)), Field (fdinfo, offset)) when H.mem exitName (s.sid, vi.vid) -> begin
									(*updates through data field, retrieves existing var assumign exists; *)
									(* no need to assigne a new variable*)
	    let existVar = H.find exitName (s.sid, vi.vid) in
	    let newEx = Lval (Var existVar, NoOffset) in
	    Set ((Mem newEx, Field (fdinfo, offset)) , e', loc)
	  end
	    
	  | _ ->  
	    Set (lv, e', loc)
	  ) 
	    
	end

      | Call (lvopt, ef, el, loc) ->
	let ef' = renameExp ef in
	let el' = List.map renameExp el in
	(match lvopt with
	  | Some (Var vi, NoOffset) when H.mem exitName (s.sid, vi.vid) -> 
	    let newvar = createNewVar vi s.sid in
	    H.replace exitName (s.sid, vi.vid) newvar;
	    Call (Some (Var newvar, NoOffset), ef', el', loc)
	  | _ -> Call (lvopt, ef', el', loc))
      | Asm _  -> 
	E.s (E.unimp "Inlined assembly unsupported!") end
    in

    let skind' = 
      match s.skind with
	| Instr il -> Instr (List.map renameIns il)
	| Return (Some e, loc) -> Return (Some (renameExp e), loc)
	| If (e, bl1, bl2, loc) -> 
	  let bl1' = renameVariablesBlk vl bl1 in
	  let bl2' = renameVariablesBlk vl bl2 in
	  If (renameExp e, bl1', bl2', loc)
	| Switch (e, bl, sl, loc) -> 
	  let bl' = renameVariablesBlk vl bl in
	  Switch (renameExp e, bl', sl, loc)
	| Loop (bl, loc, sopt1, sopt2) -> 
	  let bl' = renameVariablesBlk vl bl in
	  Loop (bl', loc, sopt1, sopt2)
	| Block bl -> Block (renameVariablesBlk vl bl)
	| TryFinally (bl1, bl2, loc) -> 
	  let bl1' = renameVariablesBlk vl bl1 in
	  let bl2' = renameVariablesBlk vl bl2 in
	  TryFinally(bl1', bl2', loc)
	| TryExcept (bl1, (il, e), bl2, loc) -> 
	  E.s (E.unimp "TryExcept unsupported!")
	| _ -> s.skind
    in
    let s' = {s with skind = skind'} in 
    (* trace (dprintf "After transformation:\n%a\n" defaultCilPrinter#pStmt s'); *)
    s' 
  in

  let sbody' = renameVariablesBlk ssavars fd.sbody in

  let addPhiFuns (b:block) (vi:varinfo) : unit =
    let addPhiStmt (s:stmt): stmt visitAction = 
      if s.preds <> [] then
	let args = List.map (fun ps ->  (ps.sid, H.find exitName (ps.sid, vi.vid))) s.preds in
	let newphifun : phifun = (H.find entryName (s.sid, vi.vid), args) in
	let newfuns =
  	  if H.mem phiNodes s.sid then
  	    let oldfuns = H.find phiNodes s.sid in
  	    newphifun :: oldfuns
  	  else [newphifun]
	in H.replace phiNodes s.sid newfuns
      else ();
      DoChildren
    in
    let apVisitor = new stmtVisitor addPhiStmt in
    ignore (visitCilBlock apVisitor b)
  in

  List.iter (addPhiFuns fd.sbody) ssavars;
  {fdec = {fd with sbody = sbody'; slocals = !newlocals}; 
   phimap = phiNodes }


(* Optimize the SSA according to optimizations described in the paper
   by John Aycock and Nigel Horspool *)
let optimizeSSA (fdssa:fundec_ssa) : fundec_ssa =
  (* A union-find hash table for tracking an SSA variable's current instance *)
  let varmap = H.create 32 in
  let rec remap (vi:varinfo) = 
    if H.mem varmap vi then
      let vi' = H.find varmap vi in
      let vi'' = remap vi' in
      if (vi''.vid <> vi'.vid) then begin
	H.replace varmap vi vi''
      end;
      vi''
    else
      vi
  in

  let phiNodes = fdssa.phimap in

  (* One optimization pass: remove phi-statements of the form d=phi(d, ..., d)
     and d=phi(...d or c...), in the latter case rename d to c. *)
  let onePass () : bool =
    let change = ref false in

    (* return false iff the phi function can be eliminated *)
    let onePhiFun (pf:phifun) : bool = 
      let rec checkRewrite (d:varinfo) (phiargs: (int * varinfo) list) =
	match phiargs with
	  | (_, vi) :: phiargs' ->
	    let vi' = remap vi in
	    let d' = remap d in
	    if (vi'.vid = d'.vid) then 
	      checkRewrite d phiargs'
	    else checkCandidate d phiargs' vi'
	  | [] -> false (* it's all d = phi(d, ..., d) -> nuke the phi *)
	    
      and checkCandidate (d:varinfo)  (phiargs: (int * varinfo) list)
	  (c:varinfo) = 
	match phiargs with
	  | (_, vi) :: phiargs' -> 
	    let d' = remap d in
	    let vi' = remap vi in
	    let c' = remap c in
	    if (vi'.vid = c'.vid || vi'.vid = d'.vid) then
	      checkCandidate d phiargs' c
	    else true
	  | [] -> 
	    (* it's all d = phi(...d or c...) -> nuke the phi and replace d by c *)
	    change := true;
	    H.add varmap d c;
	    false
      in 
      let (d, phiargs) = pf in
      checkRewrite d phiargs
    in
    H.iter 
      (fun sid philist ->
	let newphilist = List.filter onePhiFun philist in
	H.replace phiNodes sid newphilist)
      phiNodes;
    !change
  in

  (* After optimization, remap all variables to their final instances *)
  let doRemap () = 
    let rvVisitor = new varVisitor (fun vi -> ChangeTo (remap vi)) in
    ignore (visitCilFunction rvVisitor fdssa.fdec); (* this has the effect of modifying the fundec *)

    (* renaming variables in phi functions *)
    let remapOnePhi (pf:phifun) : phifun = 
      let (d, phiargs) = pf in
      let phiargs' = List.map 
	(fun (l, vi) ->
	  let vi' = remap vi in
	  (l, vi')) 
	phiargs in
      (remap d, phiargs')
    in
    H.iter
      (fun sid philist ->
	let newphilist = List.map remapOnePhi philist in
	H.replace phiNodes sid newphilist)
      phiNodes

  in

  (* reduce duplicate local variables according to varmap;
     a better way of reducing more local variables should look at the
     code to see if a variable is actually used *)
  let reduceLocals () = 
    let reduced = 
      List.fold_left 
	(fun vl vi -> 
	  let vi' = remap vi in
	  if (List.memq vi' vl || List.memq vi' fdssa.fdec.sformals) 
	  then vl else vi' :: vl)
	[] fdssa.fdec.slocals
    in fdssa.fdec.slocals <- reduced
  in

  while onePass () do () done;
  (* dump the varmap *)
  (* H.iter (fun vi1 vi2 -> trace (dprintf "%s -> %s\n" vi1.vname vi2.vname)) varmap; *)
  doRemap ();
  reduceLocals ();
  {fdssa with phimap=phiNodes}


let dumpFundecSSA (fdssa: fundec_ssa) = 
  let phiPrinter = new phiPrinterVisitor fdssa.phimap in
  trace (dprintf "%a\n" defaultCilPrinter#pGlobal (GFun (fdssa.fdec, locUnknown)));
  dumpCFG fdssa.fdec;
  trace (dprintf "Phi functions:\n");
  ignore (visitCilFunction phiPrinter fdssa.fdec)
	
(*interface function; can be invoked by other function*)
let ssaFundec (fd:fundec) = begin
    Oneret.oneret fd;
		let _ = Cfg.cfgFun fd in (* n is the number of nodes in the CFG *)
    (* do SSA transform for every formal and every local; can change this to do
       SSA tranform for a subset of variables *)
		let ssavars = ref [] in 
		List.iter(
			fun var -> begin
				if(isVarPyObject var) then 
					ssavars := List.append !ssavars [var];
				end
		)fd.sformals;
		List.iter(
			fun var -> begin
				if(isVarPyObject var) then 
					ssavars := List.append !ssavars [var];
				end 
		)fd.slocals;
    let fdssa = doRCSSA fd !ssavars in
    trace (dprintf "Before SSA transformation:\n%a\n" defaultCilPrinter#pGlobal 
	     (GFun (fd, locUnknown)));
    (* trace (dprintf "------------------\nAfter RC SSA transform:\n"); *)
    (* dumpFundecSSA fdssa; *)
    let optfdssa = optimizeSSA fdssa in
    trace (dprintf "------------------\nAfter Optimized SSA transform:\n");
    dumpFundecSSA optfdssa;
    (* Cfg.clearCFGinfo fd; *)
	  optfdssa
	end

class cilSSAVisitor = object
  inherit nopCilVisitor

  method vfunc (fd: fundec) : fundec visitAction =
    let _ = Cfg.cfgFun fd in (* n is the number of nodes in the CFG *)
    (* do SSA transform for every formal and every local; can change this to do
       SSA tranform for a subset of variables *)
		let ssavars = fd.sformals @ fd.slocals in
    let fdssa = doRCSSA fd ssavars in
    trace (dprintf "Before SSA transformation:\n%a\n" defaultCilPrinter#pGlobal 
	     (GFun (fd, locUnknown)));
    (* trace (dprintf "------------------\nAfter RC SSA transform:\n"); *)
    (* dumpFundecSSA fdssa; *)
    let optfdssa = optimizeSSA fdssa in
    trace (dprintf "------------------\nAfter Optimized SSA transform:\n");
    dumpFundecSSA optfdssa;
    (* Cfg.clearCFGinfo fd; *)
    SkipChildren
end

let feature : featureDescr = 
  { fd_name = "cilssa";
    fd_enabled = ref false;
    fd_description = "Transforming CIL code to SSA form";
    fd_extraopt = [];
    fd_doit = 
    (function (f: file) -> 
      let ssaVisitor = new cilSSAVisitor in
      visitCilFileSameGlobals ssaVisitor f
    );
    fd_post_check = true;
  } 
